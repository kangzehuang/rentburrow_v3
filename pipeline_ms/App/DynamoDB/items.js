
const AWS = require('aws-sdk')
const aws_config = require('./aws_config.json')
const dynaDoc = require("dynamodb-doc");
AWS.config.update(aws_config)

const dynamodb = new AWS.DynamoDB({
  dynamodb: '2012-08-10',
  region: "us-east-1"
})
const docClient = new dynaDoc.DynamoDB(dynamodb)

// ====================================




exports.trackActivity = function(activity){

  console.log("==> About to insert DynamoDB item!")
  console.log(activity)

  docClient.putItem(activity, function(err, data) {
    if (err){
        console.log(JSON.stringify(err, null, 2));
    }else{
        console.log(JSON.stringify(data, null, 2));
    }
  });
}








// ====================================


const subletItems = [
    {
      "TableName": "Sublets",
      "Item": {
          "PLACE_ID": "6wdgsdgasdf",
          "SUBLET_ID":"000000010",
          "ACTION":"CARD_ON_HOVER",
          "DATE":1474416000,
          "USER_FB_ID": "000020000",
          "OWNER_ID": "000050000",
          "PROP_GPS": "[1, -1]",
          "USER_GPS": "[1, -1]"
      }
    },
      {
        "TableName": "Sublets",
        "Item": {
            "PLACE_ID": "6wdgsdgasdf",
            "SUBLET_ID":"000000030",
            "ACTION":"SUBLET_CONTACTED",
            "DATE":1474416000,
            "USER_FB_ID": "000020000",
            "OWNER_ID": "000050000",
            "PROP_GPS": "[1, -1]",
            "USER_GPS": "[1, -1]"
        }
      },
    {
      "TableName": "Sublets",
      "Item": {
          "PLACE_ID": "6wdgsdgasdf",
          "SUBLET_ID":"000000060",
          "ACTION":"CARD_FAVORITED",
          "DATE":1474416000,
          "USER_FB_ID": "000500000",
          "OWNER_ID": "000020000",
          "WATCHLIST_ID": "4fdgfadjsf8",
          "PROP_GPS": "[1, -1]",
          "USER_GPS": "[1, -1]"
      }
    },
    {
      "TableName": "Sublets",
      "Item": {
          "PLACE_ID": "6wdgsdgasdf",
          "SUBLET_ID":"0000002000",
          "ACTION":"CARD_UNFAVORITED",
          "DATE":1474416000,
          "USER_FB_ID": "0005000000",
          "OWNER_ID": "0002000000",
          "WATCHLIST_ID": "4fdgfadjsf8",
          "PROP_GPS": "[1, -1]",
          "USER_GPS": "[1, -1]"
      }
    },
    {
      "TableName": "Sublets",
      "Item": {
          "PLACE_ID": "6wdgsdgasdf",
          "SUBLET_ID":"0000060000",
          "ACTION":"CARD_MAP_BUTTON",
          "DATE":1474416000,
          "USER_FB_ID": "0000070000",
          "OWNER_ID": "0000040000",
          "PROP_GPS": "[1, -1]",
          "USER_GPS": "[1, -1]"
      }
    },
    {
      "TableName": "Sublets",
      "Item": {
          "PLACE_ID": "6wdgsdgasdf",
          "SUBLET_ID":"00000009600",
          "ACTION":"CARD_ORIGINAL_BUTTON",
          "DATE":1474416000,
          "USER_FB_ID": "0000rey00000",
          "OWNER_ID": "000sdfg00000",
          "PROP_GPS": "[1, -1]",
          "USER_GPS": "[1, -1]"
      }
    },
    {
      "TableName": "Sublets",
      "Item": {
          "PLACE_ID": "6wdgsdgasdf",
          "SUBLET_ID":"00sdfg0000",
          "ACTION":"PIN_CLICK",
          "DATE":1474416000,
          "USER_FB_ID": "000sdfg00fg000",
          "OWNER_ID": "0sfg000000",
          "PROP_GPS": "[1, -1]",
          "USER_GPS": "[1, -1]"
      }
    }
]

const leaseItems = [
  {
    "TableName": "Leases",
    "Item": {
        "PLACE_ID": "6wdgsdgasdf",
        "LEASE_ID":"0000dsg000",
        "ACTION":"CARD_ON_HOVER",
        "DATE":1474416000,
        "USER_FB_ID": "000000dsfg00",
        "OWNER_ID": "00004h0000",
        "PROP_GPS": "[1, -1]",
        "USER_GPS": "[1, -1]"
    }
  },
  {
    "TableName": "Leases",
    "Item": {
        "PLACE_ID": "6wdgsdgasdf",
        "LEASE_ID":"00sg3f00000",
        "ACTION":"CARD_FAVORITED",
        "DATE":1474416000,
        "USER_FB_ID": "000fgjh000",
        "OWNER_ID": "000ert00000",
        "WATCHLIST_ID": "4fdgfadjsf8",
        "PROP_GPS": "[1, -1]",
        "USER_GPS": "[1, -1]"
    }
  },
  {
    "TableName": "Leases",
    "Item": {
        "PLACE_ID": "6wdgsdgasdf",
        "LEASE_ID":"00000ju000",
        "ACTION":"CARD_UNFAVORITED",
        "DATE":1474416000,
        "USER_FB_ID": "000sdf0000",
        "OWNER_ID": "00000k00",
        "WATCHLIST_ID": "4fdgfadjsf8",
        "PROP_GPS": "[1, -1]",
        "USER_GPS": "[1, -1]"
    }
  },
  {
    "TableName": "Leases",
    "Item": {
        "PLACE_ID": "6wdgsdgasdf",
        "LEASE_ID":"00sfgh00000",
        "ACTION":"CARD_MAP_BUTTON",
        "DATE":1474416000,
        "USER_FB_ID": "0000rth0000",
        "OWNER_ID": "0004500000",
        "PROP_GPS": "[1, -1]",
        "USER_GPS": "[1, -1]"
    }
  },
  {
    "TableName": "Leases",
    "Item": {
        "PLACE_ID": "6wdgsdgasdf",
        "LEASE_ID":"000v50000",
        "ACTION":"PIN_CLICK",
        "DATE":1474416000,
        "USER_FB_ID": "004b6j0000",
        "OWNER_ID": "0b47j00000",
        "PROP_GPS": "[1, -1]",
        "USER_GPS": "[1, -1]"
    }
  },
  {
    "TableName": "Leases",
    "Item": {
        "PLACE_ID": "6wdgsdgasdf",
        "LEASE_ID":"000yku000",
        "ACTION":"DETAILS_HOVER",
        "DATE":1474416000,
        "USER_FB_ID": "00sfgh0000",
        "OWNER_ID": "0000k8000",
        "PROP_GPS": "[1, -1]",
        "USER_GPS": "[1, -1]"
    }
  },
  {
    "TableName": "Leases",
    "Item": {
        "PLACE_ID": "6wdgsdgasdf",
        "LEASE_ID":"00mk0000",
        "ACTION":"IMAGE_VIEW",
        "DATE":1474416000,
        "USER_FB_ID": "000fng000",
        "OWNER_ID": "0004weds000",
        "PROP_GPS": "[1, -1]",
        "USER_GPS": "[1, -1]"
    }
  },
  {
    "TableName": "Leases",
    "Item": {
        "PLACE_ID": "6wdgsdgasdf",
        "LEASE_ID":"000byuk0000",
        "ACTION":"LEASE_REVIEW_VIEWED",
        "DATE":1474416000,
        "USER_FB_ID": "00000loi00",
        "OWNER_ID": "sdftr0000000",
        "PROP_GPS": "[1, -1]",
        "USER_GPS": "[1, -1]"
    }
  },
  {
    "TableName": "Leases",
    "Item": {
        "PLACE_ID": "6wdgsdgasdf",
        "LEASE_ID":"000fghb0000",
        "ACTION":"LEASE_REVIEW_POSTED",
        "DATE":1474416000,
        "USER_FB_ID": "0000fg000",
        "OWNER_ID": "00sfg0000",
        "PROP_GPS": "[1, -1]",
        "USER_GPS": "[1, -1]"
    }
  },
  {
    "TableName": "Leases",
    "Item": {
        "PLACE_ID": "6wdgsdgasdf",
        "LEASE_ID":"000000000",
        "ACTION":"INQUIRE_CLICK",
        "DATE":1474416000,
        "USER_FB_ID": "000sdfg000",
        "OWNER_ID": "0000dghs0000",
        "PROP_GPS": "[1, -1]",
        "USER_GPS": "[1, -1]"
    }
  },
  {
    "TableName": "Leases",
    "Item": {
        "PLACE_ID": "6wdgsdgasdf",
        "LEASE_ID":"000000000",
        "ACTION":"EMAIL_SENT",
        "DATE":1474416000,
        "USER_FB_ID": "000fgjh000",
        "OWNER_ID": "000sdfg00000",
        "PROP_GPS": "[1, -1]",
        "USER_GPS": "[1, -1]"
    }
  },
  {
    "TableName": "Leases",
    "Item": {
        "PLACE_ID": "6wdgsdgasdf",
        "LEASE_ID":"000000000",
        "ACTION":"LINK_TO_WEBSITE",
        "DATE":1474416000,
        "USER_FB_ID": "00000dfgh000",
        "OWNER_ID": "000d0000",
        "PROP_GPS": "[1, -1]",
        "USER_GPS": "[1, -1]"
    }
  },
  {
    "TableName": "Leases",
    "Item": {
        "PLACE_ID": "6wdgsdgasdf",
        "LEASE_ID":"000000000",
        "ACTION":"LINK_TO_PROFILE",
        "DATE":1474416000,
        "USER_FB_ID": "000sdfg00000",
        "OWNER_ID": "000fgh00000",
        "PROP_GPS": "[1, -1]",
        "USER_GPS": "[1, -1]"
    }
  }
]
