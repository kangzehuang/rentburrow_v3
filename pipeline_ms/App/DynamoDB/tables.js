
const AWS = require('aws-sdk')
const aws_config = require('./aws_config.json')
AWS.config.update(aws_config)


const subletTableParams = {
    TableName : "RentBurrow-Sublets_Winter_2017",
    KeySchema: [
        { AttributeName: "PLACE_ID", KeyType: "HASH" },  //Partition key
        { AttributeName: "ID", KeyType: "RANGE" },  //Sort key
    ],
    AttributeDefinitions: [
        { AttributeName: "PLACE_ID", AttributeType: "S" },
        { AttributeName: "ID", AttributeType: "S" },
        { AttributeName: "SUBLET_ID", AttributeType: "S" },
        { AttributeName: "ACTION", AttributeType: "S" },
        { AttributeName: "OWNER_ID", AttributeType: "S" },
        { AttributeName: "USER_FB_ID", AttributeType: "S" },  //String
        { AttributeName: "PROP_GPS", AttributeType: "S" },  // string set ['-442', '435'] converted to NS [-442, 435] on calculations
        { AttributeName: "DATE", AttributeType: "N" }  //String
    ],
    ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 5
    },
    LocalSecondaryIndexes: [
      {
        IndexName: 'DATE', /* required */
        KeySchema: [ /* required */
          {
            AttributeName: 'PLACE_ID', /* required */
            KeyType: 'HASH' /* required */
          },
          {
            AttributeName: 'DATE', /* required */
            KeyType: 'RANGE' /* required */
          }
          /* more items */
        ],
        Projection: { /* required */
          ProjectionType: 'ALL'
        }
      },
      /* more items */
    ],
    GlobalSecondaryIndexes: [
      {
        IndexName: 'ACTION', /* required */
        KeySchema: [ /* required */
          {AttributeName: 'ACTION', KeyType: 'HASH'},
          {AttributeName: 'DATE', KeyType: 'RANGE'}
        ],
        Projection: { /* required */
          ProjectionType: 'ALL'
        },
        ProvisionedThroughput: { /* required */
          ReadCapacityUnits: 1, /* required */
          WriteCapacityUnits: 5 /* required */
        }
      },
      {
        IndexName: 'USER_FB_ID', /* required */
        KeySchema: [ /* required */
          {AttributeName: 'USER_FB_ID', KeyType: 'HASH'},
          {AttributeName: 'DATE', KeyType: 'RANGE'}
        ],
        Projection: { /* required */
          ProjectionType: 'ALL'
        },
        ProvisionedThroughput: { /* required */
          ReadCapacityUnits: 1, /* required */
          WriteCapacityUnits: 5 /* required */
        }
      },
      {
        IndexName: 'OWNER_ID', /* required */
        KeySchema: [
          {AttributeName: 'OWNER_ID', KeyType: 'HASH'},
          {AttributeName: 'DATE', KeyType: 'RANGE'}
        ],
        Projection: { /* required */
          ProjectionType: 'ALL'
        },
        ProvisionedThroughput: { /* required */
          ReadCapacityUnits: 1, /* required */
          WriteCapacityUnits: 5 /* required */
        }
      },
      {
        IndexName: 'SUBLET_ID', /* required */
        KeySchema: [
          {AttributeName: 'SUBLET_ID', KeyType: 'HASH'},
          {AttributeName: 'DATE', KeyType: 'RANGE'}
        ],
        Projection: { /* required */
          ProjectionType: 'ALL'
        },
        ProvisionedThroughput: { /* required */
          ReadCapacityUnits: 1, /* required */
          WriteCapacityUnits: 5 /* required */
        }
      },
      {
        IndexName: 'PROP_GPS', /* required */
        KeySchema: [
          {AttributeName: 'PROP_GPS', KeyType: 'HASH'},
          {AttributeName: 'DATE', KeyType: 'RANGE'}
        ],
        Projection: { /* required */
          ProjectionType: 'ALL'
        },
        ProvisionedThroughput: { /* required */
          ReadCapacityUnits: 1, /* required */
          WriteCapacityUnits: 5 /* required */
        }
      }
    ]
}

const leaseTableParams = {
    TableName : "RentBurrow-Leases_Winter_2017",
    KeySchema: [
        { AttributeName: "PLACE_ID", KeyType: "HASH" },  //Partition key
        { AttributeName: "ID", KeyType: "RANGE" }  //Sort key
    ],
    AttributeDefinitions: [
        { AttributeName: "PLACE_ID", AttributeType: "S" },
        { AttributeName: "ID", AttributeType: "S" },
        { AttributeName: "LEASE_ID", AttributeType: "S" },
        { AttributeName: "ACTION", AttributeType: "S" },
        {AttributeName: 'USER_FB_ID',AttributeType: "S" },
        {AttributeName: 'OWNER_ID', AttributeType: "S" },
        {AttributeName: 'PROP_GPS', AttributeType: "S" },
        {AttributeName: 'DATE', AttributeType: "N" }
    ],
    ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 5
    },
    LocalSecondaryIndexes: [
      {
        IndexName: 'DATE', /* required */
        KeySchema: [ /* required */
          {
          AttributeName: 'PLACE_ID', /* required */
          KeyType: 'HASH' /* required */
        },
          {
            AttributeName: 'DATE', /* required */
            KeyType: 'RANGE' /* required */
          },
          /* more items */
        ],
        Projection: { /* required */
          ProjectionType: 'ALL'
        }
      }
      /* more items */
    ],
    GlobalSecondaryIndexes: [
      {
        IndexName: 'ACTION', /* required */
        KeySchema: [ /* required */
          {AttributeName: 'ACTION', KeyType: 'HASH'},
          {AttributeName: 'DATE', KeyType: 'RANGE'}
        ],
        Projection: { /* required */
          ProjectionType: 'ALL'
        },
        ProvisionedThroughput: { /* required */
          ReadCapacityUnits: 1, /* required */
          WriteCapacityUnits: 5 /* required */
        }
      },
      {
        IndexName: 'USER_FB_ID', /* required */
        KeySchema: [ /* required */
          {AttributeName: 'USER_FB_ID', KeyType: 'HASH'},
          {AttributeName: 'DATE', KeyType: 'RANGE'}
        ],
        Projection: { /* required */
          ProjectionType: 'ALL'
        },
        ProvisionedThroughput: { /* required */
          ReadCapacityUnits: 1, /* required */
          WriteCapacityUnits: 5 /* required */
        }
      },
      {
        IndexName: 'OWNER_ID', /* required */
        KeySchema: [
          {AttributeName: 'OWNER_ID', KeyType: 'HASH'},
          {AttributeName: 'DATE', KeyType: 'RANGE'}
        ],
        Projection: { /* required */
          ProjectionType: 'ALL'
        },
        ProvisionedThroughput: { /* required */
          ReadCapacityUnits: 1, /* required */
          WriteCapacityUnits: 5 /* required */
        }
      },
      {
        IndexName: 'LEASE_ID', /* required */
        KeySchema: [
          {AttributeName: 'LEASE_ID', KeyType: 'HASH'},
          {AttributeName: 'DATE', KeyType: 'RANGE'}
        ],
        Projection: { /* required */
          ProjectionType: 'ALL'
        },
        ProvisionedThroughput: { /* required */
          ReadCapacityUnits: 1, /* required */
          WriteCapacityUnits: 5 /* required */
        }
      },
      {
        IndexName: 'PROP_GPS', /* required */
        KeySchema: [
          {AttributeName: 'PROP_GPS', KeyType: 'HASH'},
          {AttributeName: 'DATE', KeyType: 'RANGE'}
        ],
        Projection: { /* required */
          ProjectionType: 'ALL'
        },
        ProvisionedThroughput: { /* required */
          ReadCapacityUnits: 1, /* required */
          WriteCapacityUnits: 5 /* required */
        }
      }
    ]
}

const landlordTableParams = {
    TableName : "RentBurrow-Landlords_Winter_2017",
    KeySchema: [
        { AttributeName: "LANDLORD_ID", KeyType: "HASH" },  //Partition key
        { AttributeName: "ID", KeyType: "RANGE" }  //Sort key
    ],
    AttributeDefinitions: [
        { AttributeName: "ID", AttributeType: "S" },
        { AttributeName: "LANDLORD_ID", AttributeType: "S" },
        { AttributeName: "ACTION", AttributeType: "S" },
        {AttributeName: 'DATE', AttributeType: 'N'}
    ],
    LocalSecondaryIndexes: [
      {
        IndexName: 'DATE', /* required */
        KeySchema: [ /* required */
          {
            AttributeName: 'LANDLORD_ID', /* required */
            KeyType: 'HASH' /* required */
          },
          {
            AttributeName: 'DATE', /* required */
            KeyType: 'RANGE' /* required */
          }
          /* more items */
        ],
        Projection: { /* required */
          ProjectionType: 'ALL'
        }
      },
      /* more items */
    ],
    GlobalSecondaryIndexes:[
      {
        IndexName: 'ACTION', /* required */
        KeySchema: [
          {AttributeName: 'ACTION', KeyType: 'HASH'},
          {AttributeName: 'DATE', KeyType: 'RANGE'}
        ],
        Projection: { /* required */
          ProjectionType: 'ALL'
        },
        ProvisionedThroughput: { /* required */
          ReadCapacityUnits: 1, /* required */
          WriteCapacityUnits: 1 /* required */
        }
      }
    ],
    ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 1
    }
}


const rentburrowTableParams = {
    TableName : "RentBurrow-App_Winter_2017",
    KeySchema: [
        { AttributeName: "USER_FB_ID", KeyType: "HASH" },  //Partition key
        { AttributeName: "ID", KeyType: "RANGE" }  //Sort key
    ],
    AttributeDefinitions: [
        { AttributeName: "ID", AttributeType: "S" },
        { AttributeName: "ACTION", AttributeType: "S" },
        { AttributeName: "USER_FB_ID", AttributeType: "S" },
        { AttributeName: "USER_GPS", AttributeType: "S" },
        {AttributeName: 'DATE', AttributeType: 'N'}
    ],
    LocalSecondaryIndexes: [
      {
        IndexName: 'DATE', /* required */
        KeySchema: [ /* required */
          {
            AttributeName: 'USER_FB_ID', /* required */
            KeyType: 'HASH' /* required */
          },
          {
            AttributeName: 'DATE', /* required */
            KeyType: 'RANGE' /* required */
          }
          /* more items */
        ],
        Projection: { /* required */
          ProjectionType: 'ALL'
        }
      },
      /* more items */
    ],
    GlobalSecondaryIndexes:[
      {
        IndexName: 'ACTION', /* required */
        KeySchema: [
          {AttributeName: 'ACTION', KeyType: 'HASH'},
          {AttributeName: 'DATE', KeyType: 'RANGE'}
        ],
        Projection: { /* required */
          ProjectionType: 'ALL'
        },
        ProvisionedThroughput: { /* required */
          ReadCapacityUnits: 1, /* required */
          WriteCapacityUnits: 3 /* required */
        }
      },
      {
        IndexName: 'USER_GPS', /* required */
        KeySchema: [
          {AttributeName: 'USER_GPS', KeyType: 'HASH'},
          {AttributeName: 'DATE', KeyType: 'RANGE'}
        ],
        Projection: { /* required */
          ProjectionType: 'ALL'
        },
        ProvisionedThroughput: { /* required */
          ReadCapacityUnits: 1, /* required */
          WriteCapacityUnits: 3 /* required */
        }
      }
    ],
    ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 3
    }
}

exports.createTables = function(){


  console.log("==> About to create DynamoDB tables!")

  const dynamodb = new AWS.DynamoDB({
    dynamodb: '2012-08-10',
    region: "us-east-1"
  })

  // dynamodb.createTable(subletTableParams, function(err, data) {
  //     if (err)
  //         console.log(JSON.stringify(err, null, 2));
  //     else
  //         console.log(JSON.stringify(data, null, 2));
  // });
  // dynamodb.createTable(leaseTableParams, function(err, data) {
  //     if (err)
  //         console.log(JSON.stringify(err, null, 2));
  //     else
  //         console.log(JSON.stringify(data, null, 2));
  // });
  // dynamodb.createTable(landlordTableParams, function(err, data) {
  //     if (err)
  //         console.log(JSON.stringify(err, null, 2));
  //     else
  //         console.log(JSON.stringify(data, null, 2));
  // });
  // dynamodb.createTable(rentburrowTableParams, function(err, data) {
  //     if (err)
  //         console.log(JSON.stringify(err, null, 2));
  //     else
  //         console.log(JSON.stringify(data, null, 2));
  // });

}
