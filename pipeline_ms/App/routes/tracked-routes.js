
const trackActivity = require('../DynamoDB/items').trackActivity
const CryptoJS = require("crypto-js")
const determineTableBasedOnDate = require('../api/dateFilter').determineTableBasedOnDate

// GET /activity
// track an incoming activity from client app
exports.activity = function(req, res, next){
  const trackedActivities = req.body.heartbeat
  const bytes  = CryptoJS.AES.decrypt(trackedActivities.toString(), 'heartbeatIsStronk')
  const decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8))

  const table_date = determineTableBasedOnDate()
  decryptedData.forEach((a)=>{
    if(a){
      a.TableName = a.TableName+table_date
      // console.log(a)
      trackActivity(a)
    }
  })

	res.send("Client is alive")
}
