const express = require('express');
const http = require('http')
const bodyParser = require('body-parser')
const morgan = require('morgan')

const app = express();
const router = require('./router');
// const mongoose = require('mongoose');
const cors = require('cors');

const createTables = require('./DynamoDB/tables').createTables

// Database setup
// 'localhost:27017/database' will set up a db inside mongo called 'database'
// mongoose.connect('mongodb://0.0.0.0:27017/rentburrow');
// mongoose.connect('mongodb://34.195.86.132:27017/rentburrow');


// App setup
// morgan and bodyParser are middlewares. any incoming requests will be passed through each
app.use(morgan('tiny'));				// morgan is a logging framework to see incoming requests. used mostly for debugging
// CORS middleware (cross origin resource sharing) to configure what domain & ips can talk to our server api
// CORS is used for user security on web browsers. Enable CORS on server to allow all I.P. addresses
app.use(cors());
app.use(bodyParser.json({type:'*/*'}));		// bodyParser attempts to parse any request into JSON format

// we instantiate the router function that defines all our HTTP route endpoints
router(app);


// Server setup
// if there is an environment variable of PORT already defined, use it. otherwise use port 3091
const port = process.env.PORT || 3093;
// create a server with the native node http library
const server = http.createServer(app);
// listen to the server on port
server.listen(port);
console.log("Server listening on:", port);


// create the dynamodb tables
// createTables()
