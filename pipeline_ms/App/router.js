
const Tracked = require('./routes/tracked-routes');

module.exports = function(app){

	// Incoming stream of user activity data
	app.post('/websocket', Tracked.activity);

}
