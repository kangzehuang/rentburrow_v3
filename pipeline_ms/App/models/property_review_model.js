// Import dependencies on Mongoose
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Create the Rental post Schema
var PropertyReviewSchema = new mongoose.Schema({
	address: String,
	review_type: String,
	property_id: String,
	landlord_name: String,
	landlord_id: String,
	landlord_logo: String,
	landlord_url: String,
	text: String,
	scores: {
		priceValue: Number,				// how good was the place for its price
		quality: Number,				// how nice is the place
		location: Number				// how good is the location
	},
	reviewer_name: String,
	reviewer_img: String,
	reviewer_id: String,
	visible: Boolean,
	created_at: Date
});

PropertyReviewSchema.pre('save', function(next){
	this.created_at = new Date()
	this.review_type = "property"
	this.visible = true
	next();
});

var PropertyReview = mongoose.model('PropertyReview', PropertyReviewSchema);

module.exports = PropertyReview;
