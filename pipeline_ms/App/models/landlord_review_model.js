// Import dependencies on Mongoose
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Create the Rental post Schema
var LandlordReviewSchema = new mongoose.Schema({
	address: String,
	review_type: String,
	property_id: String,
	landlord_name: String,
	landlord_id: String,
	landlord_logo: String,
	landlord_url: String,
	text: String,
	scores: {
		professionalism: Number,			// how professional is the landlord
		effectiveness: Number,				// how effective is the landlord at solving problems
		satisfaction: Number				// how satisfied are you overall
	},
	reviewer_name: String,
	reviewer_img: String,
	reviewer_id: String,
	visible: Boolean,
	created_at: Date
});

LandlordReviewSchema.pre('save', function(next){
	this.created_at = new Date()
	this.review_type = "landlord"
	this.visible = true
	next();
});

var LandlordReview = mongoose.model('LandlordReview', LandlordReviewSchema);

module.exports = LandlordReview;
