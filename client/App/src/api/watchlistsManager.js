import axios from 'axios'
import { TENANT_MICROSERVICE } from './API_URLS'

export function getWatchlistsForUser(userId){
	const p = new Promise((res, rej)=>{
		axios.post(TENANT_MICROSERVICE+"/my_watchlists", {userId: userId})
			.then((data, err)=>{
				if(err){
					// console.log(err)
				}
				res(data.data)
			})
			.catch((err)=>{
				rej(err)
			})
	})
	return p
}

export function getAWatchlist(watchlist_id){
	const p = new Promise((res, rej)=>{
		axios.post(TENANT_MICROSERVICE+"/watchlist", {watchlist_id})
			.then((data, err)=>{
				if(err){
					// console.log(err)
				}
				res(data.data)
			})
			.catch((err)=>{
				rej(err)
			})
	})
	return p
}

export function populateWatchlist(watchlist){
	const p = new Promise((res, rej)=>{
		axios.post(TENANT_MICROSERVICE+"/populate_watchlist", watchlist)
			.then((data)=>{
				res(data.data)
			})
			.catch((err)=>{
				rej(err)
			})
	})
	return p
}

export function createWatchlist(newWatchlist){
	const p = new Promise((res, rej)=>{
		axios.post(TENANT_MICROSERVICE+"/new_watchlist", newWatchlist)
			.then((data, err)=>{
				if(err){
					// console.log(err)
				}
				res(data)
			})
			.catch((err)=>{
				rej(err)
			})
	})
	return p
}

export function addUserToWatchlist({watchlist_id, userId}){
	const p = new Promise((res, rej)=>{
		axios.post(TENANT_MICROSERVICE+"/add_watchlist_user", {watchlist_id, userId})
			.then((data)=>{
				// console.log(data)
				res()
			})
			.catch((err)=>{
				rej(err)
			})
	})
	return p
}

export function removeUserFromWatchlist({watchlist_id, userId}){
	const p = new Promise((res, rej)=>{
		axios.post(TENANT_MICROSERVICE+"/remove_watchlist_user", {watchlist_id, userId})
			.then((data, err)=>{
				if(err){
					// console.log(err)
				}
				res(data)
			})
			.catch((err)=>{
				rej(err)
			})
	})
	return p
}

export function addPropertyToWatchlist({watchlist_id, postId, type}){
	// console.log(watchlist_id, postId, type)
	const p = new Promise((res, rej)=>{
		axios.post(TENANT_MICROSERVICE+"/add_watchlist_prop", {watchlist_id, postId, type})
			.then((data, err)=>{
				if(err){
					// console.log(err)
				}
				// console.log(data)
				res(data.data)
			})
			.catch((err)=>{
				rej(err)
			})
	})
	return p
}

export function removePropertyFromWatchlist({watchlist_id, postId, type}){
	const p = new Promise((res, rej)=>{
		axios.post(TENANT_MICROSERVICE+"/remove_watchlist_prop", {watchlist_id, postId, type})
			.then((data, err)=>{
				if(err){
					// console.log(err)
				}
				// console.log(data)
				res(data.data)
			})
			.catch((err)=>{
				rej(err)
			})
	})
	return p
}
