
export function getGeolocation(){
	const p = new Promise((res, rej)=>{
		// console.log("Getting geolocation!")
		if (navigator.geolocation) {
	        navigator.geolocation.getCurrentPosition((position)=>{
	        	const x = [position.coords.longitude.toFixed(7), position.coords.latitude.toFixed(7)]
	       		res(x)
	        })
    } else {
        rej("Geolocation is not supported by this browser.")
    }
	})
	return p
}
