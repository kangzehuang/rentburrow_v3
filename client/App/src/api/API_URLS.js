

// LOCAL
// export const TENANT_MICROSERVICE = "http://localhost:3090"
// export const LANDLORD_MICROSERVICE = "http://localhost:3091"
// export const FBPARSER_MICROSERVICE = "http://localhost:3092"
// export const PIPELINE_MICROSERVICE = "http://localhost:3093"
// export const ANALYTICS_MICROSERVICE = "http://localhost:3094"
// export const FB_APP_ID = "1165229330221966"


// PROD
export const TENANT_MICROSERVICE = "http://34.195.86.132:3090"
export const LANDLORD_MICROSERVICE = "http://34.195.86.132:3091"
export const FBPARSER_MICROSERVICE = "http://34.195.86.132:3092"
export const PIPELINE_MICROSERVICE = "http://34.195.86.132:3093"
export const ANALYTICS_MICROSERVICE = "http://34.195.86.132:3094"
export const FB_APP_ID = "1492022030811505"

// set this to FALSE if you want to fake entries
export const MANUAL_FAKED_ENTRIES_CLAIMED = true
