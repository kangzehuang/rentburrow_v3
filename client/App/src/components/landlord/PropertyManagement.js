import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'

import {getLeasesForLandlord, deletePropertyFromDb, toggleActiveStatusOfProperty} from '../../api/landlordManager'
import { editLandlordProperty, saveLeasesToState, selectLandlordProperty } from '../../actions/landlord_actions'
import PropertyForm from './PropertyForm'

class PropertyManagement extends Component {

	renderEditForm(){
		if(this.props.propertyOnForm){
			return (
				<PropertyForm />
			)
		}
	}

	deleteProperty(){
		const x = confirm("Are you sure you want to delete this property?")
		if(x){
			deletePropertyFromDb(this.props.property._id)
				.then(()=>{
					return getLeasesForLandlord(this.props.landlord.id)
				})
				.then((data)=>{
					this.props.saveLeasesToState(data)
				})
				.catch((err)=>{
					// console.log(err)
				})
		}
	}

	toggleActive(){
		// console.log("Toggling State!")
		toggleActiveStatusOfProperty(this.props.property)
			.then((data)=>{
				this.props.selectLandlordProperty(data)
				return getLeasesForLandlord(this.props.landlord.id)
			})
			.then((data)=>{
				this.props.saveLeasesToState(data)
			})
	}

	render() {
		return (
			<div style={comStyles().bar}>
				<div style={comStyles().address}>{this.props.property.address}</div>
				<div style={comStyles().actionbar}>
					<div style={comStyles().toggleActive}>
				     <label className="toggle toggle-balanced">
				       <input onChange={this.toggleActive.bind(this)} type="checkbox" checked={this.props.property.active} />
				       <div className="track">
				         <div className="handle"></div>
				       </div>
				     </label>
				     <div style={comStyles().toggleActiveText}>
				     	{
				     		this.props.property.active
				     		?
				     		<span>ACTIVE</span>
				     		:
				     		<span>INACTIVE</span>
				     	}
				     </div>
				    </div>
					<i onClick={()=>this.props.editLandlordProperty(this.props.property)} style={comStyles().icon} className='ion-edit'>&nbsp; Edit</i>
					<i onClick={this.deleteProperty.bind(this)} style={comStyles().icon} className='ion-trash-a'>&nbsp; Delete</i>
				</div>
				<div>
					{this.renderEditForm()}
				</div>
			</div>
		);
	}
}

PropertyManagement.propTypes = {
	landlord: React.PropTypes.object.isRequired,
	property: React.PropTypes.object.isRequired,		// passed in
	propertyOnForm: React.PropTypes.object
};

const RadiumHOC = Radium(PropertyManagement);

function mapStateToProps(state){
	return {
		landlord: state.landlord.landlord,
		propertyOnForm: state.landlord.propertyOnForm
	}
}

export default connect(mapStateToProps, {editLandlordProperty, saveLeasesToState, selectLandlordProperty})(RadiumHOC);


// =========================================


const comStyles = () => {
	return {
		bar: {
			display: "flex",
			flexDirection: "column",
			justifyContent: "center",
			textAlign: "center",
			color: "white",
			padding: "10px"
		},
		address: {
			fontSize: "2rem",
			fontWeight: "bold"
		},
		actionbar: {
			margin: "20px 0px 0px 0px",
			display: "flex",
			flexDirection: "row",
			justifyContent: "center",
		},
		icon: {
			fontSize: "1.7rem",
			margin: "10px",
			cursor: "pointer"
		},
		toggleActive: {
			display: "flex",
			flexDirection: "row",
			justifyContent: "flex-end",
			alignItems: "center",
			margin: "0px 10px 0px 10px"
		},
		toggleActiveText: {
			display: "inline-block",
			fontSize: "1.3rem",
			color: "white",
			padding: "5px",
			fontWeight: "bold"
		},
	}
}
