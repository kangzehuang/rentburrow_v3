import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import {browserHistory} from 'react-router'

import CanvasLandlord from './CanvasLandlord'
import PropertyStatistics from './stats/PropertyStatistics'
import PropertyManagement from './PropertyManagement'

class PropertyDetails extends Component {

	inferredFromUrl(){
		let property
		let found = false
		this.props.properties.forEach((prop)=>{
			if(prop._id == this.props.params.leaseId){
				found = true
				property = prop
			}
		})
		if(found){
			return property
		}else{
			browserHistory.push('/landlord/dashboard')
		}
	}

	render() {
		return (
			<div>
				<PropertyManagement property={this.props.property || this.inferredFromUrl()} />
				<CanvasLandlord property={this.props.property || this.inferredFromUrl()} />
				<PropertyStatistics property={this.props.property || this.inferredFromUrl()} />
			</div>
		);
	}
}

PropertyDetails.propTypes = {
	property: React.PropTypes.object,
	properties: React.PropTypes.array.isRequired
}

const RadiumHOC = Radium(PropertyDetails);

function mapStateToProps(state){
	return {
		properties: state.landlord.properties
	}
}

export default connect(mapStateToProps)(RadiumHOC);
