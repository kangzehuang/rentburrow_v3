import React, {Component} from 'react'
import {connect} from 'react-redux'
import Radium from 'radium'
import {browserHistory} from 'react-router'
import { xWhiteSmoke } from '../../stylesJS/base_colors'

import SearchWidget from './SearchWidget'
import PropertyForm from './PropertyForm'
import LandlordAnalytics from './LandlordAnalytics'
import { newLandlordProperty, goToLandlordSettings } from '../../actions/landlord_actions'



class LandlordDashboard extends Component {

	renderAppropriateItem(){
		if(this.props.propertyOnForm && !this.props.selectedProperty && this.props.properties){
			return (
				<div style={comStyles().newForm}>
					<PropertyForm />
				</div>
			)
		}else if(this.props.properties && this.props.properties.length > 0){
			return (
				<div>
					<SearchWidget />
				</div>
			)
		}else{
			return (
				<div style={comStyles().noProperties}>
					<i onClick={this.props.goToLandlordSettings} className='ion-ios-telephone' style={comStyles().addButton}>
						&nbsp; SET UP CONTACT INFO
					</i>
					<br />
					<i onClick={this.props.newLandlordProperty} className='ion-plus-circled' style={comStyles().addButton}>
						&nbsp; ADD NEW PROPERTY
					</i>
				</div>
			)
		}
	}

	render() {
		return (
			<div>
				{this.renderAppropriateItem()}
			</div>
		)
	}
}


LandlordDashboard.propTypes = {
	properties: React.PropTypes.array.isRequired,
	selectedProperty: React.PropTypes.object,
	propertyOnForm: React.PropTypes.object,
	chartsView: React.PropTypes.bool,
	settingsView: React.PropTypes.bool
};

const RadiumHOC = Radium(LandlordDashboard);

function mapStateToProps(state){
	return {
		properties: state.landlord.properties,
		selectedProperty: state.landlord.selectedProperty,
		propertyOnForm: state.landlord.propertyOnForm,
		chartsView: state.landlord.chartsView,
		settingsView: state.landlord.settingsView
	}
}

export default connect(mapStateToProps, {newLandlordProperty, goToLandlordSettings})(RadiumHOC);

// ===========================

const comStyles = () => {
	return {
		newForm: {
			padding: "20px"
		},
		noProperties: {
			width: "100%",
			height: "50vh",
			display: "flex",
			justifyContent: "center",
			alignItems: "center"
		},
		addButton: {
			margin: "auto",
			fontSize: "2rem",
			color: xWhiteSmoke,
			cursor: "pointer"
		}
	}
}
