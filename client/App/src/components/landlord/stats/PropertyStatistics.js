import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'

import SumStats from './SumStats'
import { getBasicPropertyStats } from '../../../api/statisticsManager'

class PropertyStatistics extends Component {

		constructor(){
			super()
			this.state = {
				statsData: null
			}
		}

		componentWillMount(){
			getBasicPropertyStats(this.props.property)
				.then((data)=>{
					// console.log(data)
					this.setState({statsData: data})
				})
				.catch((err)=>{
					// console.log(err)
				})
		}

	render() {
		return (
			<div>
				{
					this.state.statsData
					?
					<SumStats statsData={this.state.statsData} />
					:
					null
				}
			</div>
		);
	}
}

PropertyStatistics.propTypes = {
	property: React.PropTypes.object.isRequired		// passed in
};

const RadiumHOC = Radium(PropertyStatistics);

export default connect()(RadiumHOC);
