import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'

import { getBasicPropertyStats, getPropertySums } from '../../../api/statisticsManager'

class SumStats extends Component {

	constructor(){
		super()
		this.state = {
			totalLeaseViews: 0,
			totalLeaseInquires: 0,
			totalEmailsSent: 0,
			totalLeaseFavorites: 0,
			totalSubletViews: 0,
			totalSubletContacts: 0,
			totalSubletting: 0,
			avgSubletPrice: 0,
			subletPriceRange: {
				min: 0,
				max: 0
			}
		}
	}

	componentDidMount(){
		this.calculateSums()
	}

	calculateSums(){
		const totalLeaseViews = getPropertySums(this.props.statsData, 'totalLeaseViews')
		const totalLeaseInquires = getPropertySums(this.props.statsData, 'totalLeaseInquires')
		const totalEmailsSent = getPropertySums(this.props.statsData, 'totalEmailsSent')
		const totalLeaseFavorites = getPropertySums(this.props.statsData, 'totalLeaseFavorites')
		const totalSubletViews = getPropertySums(this.props.statsData, 'totalSubletViews')
		const totalSubletContacts = getPropertySums(this.props.statsData, 'totalSubletContacts')
		const totalSubletting = getPropertySums(this.props.statsData, 'totalSubletting')
		const avgSubletPrice = getPropertySums(this.props.statsData, 'avgSubletPrice')
		const subletPriceRange = getPropertySums(this.props.statsData, 'subletPriceRange')
    this.setState({
      totalLeaseViews,
			totalLeaseInquires,
			totalEmailsSent,
			totalLeaseFavorites,
			totalSubletViews,
			totalSubletContacts,
			totalSubletting,
			avgSubletPrice,
			subletPriceRange
    })
	}

	render() {

		return (
			<div style={comStyles().basicsBar}>

				<div style={comStyles().leaseSumsBar}>
		      <div style={comStyles().sumItem}>
					  {this.state.totalLeaseViews} Ad Views
		      </div>
		      <div style={comStyles().sumItem}>
					  {this.state.totalLeaseInquires} Ad Inquiries
		      </div>
		      <div style={comStyles().sumItem}>
					  {this.state.totalEmailsSent} Emails Received
		      </div>
		      <div style={comStyles().sumItem}>
					  {this.state.totalLeaseFavorites} Times Favorited
		      </div>
				</div>

				<div style={comStyles().subletSumsBar}>
	        <div style={comStyles().sumItem}>
	          {this.state.totalSubletViews} Sublet Views
	        </div>
	        <div style={comStyles().sumItem}>
	          {this.state.totalSubletContacts} Sublets Contacted
	        </div>
	        <div style={comStyles().sumItem}>
	          {this.state.totalSubletting} Tenants Subletting Out
	        </div>
	        <div style={comStyles().sumItem}>
	          ${this.state.subletPriceRange.min} - ${this.state.subletPriceRange.max} Sublet Price
	        </div>
				</div>
			</div>
		);
	}
}

SumStats.propTypes = {
	property: React.PropTypes.object.isRequired,		// passed in
  statsData: React.PropTypes.object.isRequired // passed in
};

const RadiumHOC = Radium(SumStats);

export default connect()(RadiumHOC);

// =================

const comStyles = () => {
	return {
		basicsBar: {
			display: "flex",
			flexDirection: "column",
			width: "100%",
			margin: "30px auto",
			padding: "20px",
			justifyContent: "center"
		},
		leaseSumsBar: {
			display: "flex",
			flexDirection: "row",
			width: "100%",
			justifyContent: "center"
		},
		subletSumsBar: {
			display: "flex",
			flexDirection: "row",
			width: "100%",
			justifyContent: "center"
		},
		sumItem: {
			fontSize: "1.2rem",
			color: "white",
			textAlign: "center",
			margin: "20px"
		}
	}
}
