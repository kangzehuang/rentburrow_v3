import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import WatchlistDetail from './WatchlistDetail'

import SubletCard from '../shared_ui/SubletCard'
import LeaseCard from '../shared_ui/LeaseCard'
import SearchResults from './SearchResults'
import {xMidBlue, xDeepBlue, xGreyText} from '../../stylesJS/base_colors'
import {createWatchlist, getWatchlistsForUser, addPropertyToWatchlist, removeUserFromWatchlist, populateWatchlist, addUserToWatchlist, getAWatchlist} from '../../api/watchlistsManager'
import {saveWatchlistsToState, setWatchlist, unsetWatchlist} from '../../actions/watchlist_actions'
import {goToListingsView} from '../../actions/tenant_actions'
import {submitFilterResults} from '../../actions/search_actions'
import {setViewMode} from '../../actions/map_actions'
import {trackActivity} from '../../actions/activity_actions'

class WatchlistsOverview extends Component {

	constructor(){
		super()
		this.state = {
			subletsList: [],
			leasesList: [],
			watchlistUrl: null
		}
	}

	componentWillMount(){
		this.props.setViewMode('watchlist')
		this.props.submitFilterResults([])
		this.addAnyPendingHearts()
			.then(()=>{
				// console.log("pending hearts added")
				return populateWatchlist(this.props.watchlist)
			})
			.then((data)=>{
				const url = this.getUrlAndFormat(this.props.watchlist._id)
				this.setState({
					subletsList: data.sublets_details,
					leasesList: data.leases_details,
					watchlistUrl: url
				})
				const results = this.state.subletsList.concat(this.state.leasesList)
				this.props.submitFilterResults(results)
			})
	}

	addAnyPendingHearts(){
		// console.log("Checking for pendng hearts!")
		const p = new Promise((res, rej)=>{
			let pending = localStorage.getItem('hearted')
			// console.log(pending)
			if(pending){
				pending = JSON.parse(pending)
				pending.watchlist_id = this.props.watchlist._id
				// console.log(pending)
				addPropertyToWatchlist(pending)
					.then((data)=>{
						// console.log("Property was added to watchlist!")
						// console.log("setting heart to null..")
						localStorage.removeItem('hearted')
						// console.log(localStorage.getItem('hearted'))
						this.props.setWatchlist(data)
						res()
					})
					.catch(()=>{
						res()
					})
			}else{
				res()
			}
		})
		return p
	}

	getUrlAndFormat(watchlistId){
		if(window.location.href.indexOf("&watchlist=") >= 0){
			let position = window.location.href.indexOf("&watchlist=")
			let url = window.location.href.slice(0, position)
			return url + "&watchlist=" + watchlistId
		}else{
			return window.location.href + "?app=v3&watchlist=" + watchlistId
		}
	}

	joinWatchlist(userId){
		const x = {
			userId: userId,
			watchlist_id: this.props.watchlist._id
		}
		addUserToWatchlist(x)
			.then((data)=>{
				this.props.trackActivity("APP", "JOINED_WATCHLIST", null, this.props.fbUser, this.props.geoCoords, {watchlistId: this.props.watchlist._id})
				return getWatchlistsForUser(userId)
			})
			.then((data)=>{
				this.props.saveWatchlistsToState(data)
				data.forEach((w)=>{
					if(w._id == this.props.watchlist._id){
						this.props.setWatchlist(w)
					}
				})
			})
	}

	startHunting(){
		this.props.goToListingsView()
		this.props.setViewMode('lease')
	}

	removeFromWatchlist(watchlist_id){
		const self = this
		const y = confirm("Are you sure you want to delete this favorites list?")
		if(y==true){
			const x = {
				watchlist_id: self.props.watchlist._id,
				userId: self.props.fbUser.id
			}
			removeUserFromWatchlist(x).then((data)=>{
				this.props.trackActivity("APP", "LEFT_WATCHLIST", null, this.props.fbUser, this.props.geoCoords, {watchlistId: this.props.watchlist._id})
				getWatchlistsForUser(self.props.fbUser.id)
					.then((data)=>{
						self.props.goToListingsView()
						self.props.setViewMode('lease')
						self.props.saveWatchlistsToState(data)
						self.props.setWatchlist(null)
					})
			})
		}
	}

	copyWatchlistLink(){
		this.props.trackActivity("APP", "COPIED_WATCHLIST", null, this.props.fbUser, this.props.geoCoords, {watchlistId: this.props.watchlist._id})
		const link = this.refs.watchlistLink
		link.select()
		document.execCommand('copy')
	}

	render() {
		let userPartOfWatchlist = false
		this.props.watchlist.users.forEach((u)=>{
			if(u == this.props.fbUser.id){
				userPartOfWatchlist = true
			}
		})
		return (
			<div style={comStyles().mainview}>
				<div style={comStyles().detailReview}>
					<div style={comStyles().detailReviewTitle}>
						<i className='ion-heart' style={comStyles().detailReviewTitleIcon}> &nbsp; {this.props.watchlist.watchlist_title}</i>
					</div>
					<div>
						{
							userPartOfWatchlist
							?
							<button onClick={this.startHunting.bind(this)} className='btn btn-block' type='button' style={comStyles().startHunting}>Start Hunting</button>
							:
							<button onClick={()=>this.joinWatchlist(this.props.fbUser.id)} className='btn btn-block btn-success' type='button' style={comStyles().joinList}>Join List</button>
						}
						<button type='button' onClick={this.props.unsetWatchlist} className='btn btn-primary' style={comStyles().detailReviewActionbar_back}>
							<i className='ion-android-arrow-back' style={comStyles().icon}></i>
						</button>
						<button type='button' onClick={this.removeFromWatchlist.bind(this)} className='btn btn-primary' style={comStyles().detailReviewActionbar_del}>
							<i className='ion-trash-a' style={comStyles().icon}></i>
						</button>
						<button type='button' onClick={this.copyWatchlistLink.bind(this)} className='btn btn-primary' style={comStyles().detailReviewActionbar_share}>
							<i className='ion-person-add' style={comStyles().icon}></i> &nbsp; &nbsp;
							<div style={comStyles().buttonText}>Share link with Roomates</div>
						</button>
						<input ref='watchlistLink' type='text' value={this.state.watchlistUrl} style={comStyles().watchlistURL} />
					</div>
				</div>
				<SearchResults />
			</div>
		)
	}
}

WatchlistsOverview.propTypes = {
	watchlist: React.PropTypes.object.isRequired,
	fbUser: React.PropTypes.object.isRequired,
	geoCoords: React.PropTypes.array
}

const RadiumHOC = Radium(WatchlistsOverview);

function mapStateToProps(state){
	return {
		watchlist: state.tenant.selectedWatchlist,
		fbUser: state.tenant.fbUser,
		geoCoords: state.app.geoCoords
	}
}

export default connect(mapStateToProps, {setWatchlist, unsetWatchlist, goToListingsView, submitFilterResults, saveWatchlistsToState, setViewMode, trackActivity})(RadiumHOC);


// ============================

const comStyles = () => {
	return {
		mainview: {
			padding: "20px 10px 20px 10px",
			height: "100vh",
			overflowY: "scroll",
			"@media (max-width:600px), and (max-height: 740px)":{
				padding: "0px"
			}
		},
		detailReview: {
			display: "flex",
			flexDirection: "column",
			justifyContent: "center",
			padding: "10px",
			top: "10px",
			backgroundColor: xMidBlue,
		},
		detailReviewTitle: {
			textAlign: "center",
			padding: "20px"
		},
		detailReviewTitleIcon: {
			fontSize: "1.6rem",
			fontWeight: "bold",
			color: "white"
		},
		detailReviewInfo: {
			textAlign: "center",
			color: "white",
			padding: "10px 0px 30px 0px"
		},
		detailReviewActionbar_back: {
			borderRadius: "0px",
			width: "10%"
		},
		detailReviewActionbar_del: {
			borderRadius: "0px",
			width: "10%"
		},
		detailReviewActionbar_share: {
			borderRadius: "0px",
			width: "80%"
		},
		buttonText: {
			fontSize: "1rem",
			fontWeight: "bold",
			color: "white",
			display: "inline-block"
		},
		icon: {
			fontSize: "1.3rem",
			fontWeight: "bold",
			color: "white",
			display: "inline-block"
		},
		startHunting: {
			margin: "30px 10px 10px 0px",
			fontWeight: "bold",
			fontSize: "1rem",
			color: "black"
		},
		joinList: {
			margin: "30px 10px 10px 0px",
			fontWeight: "bold",
			fontSize: "1rem",
			color: "white"
		},
		watchlistURL: {
			width: "100%",
			padding: "5px",
			backgroundColor: "rgba(256,256,256,0.7)"
		}
	}
}
