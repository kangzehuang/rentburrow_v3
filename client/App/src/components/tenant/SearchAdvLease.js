import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'

import {searchLeaseFilter} from '../../api/searchFilter'
import {saveLeaseSearchState, submitFilterResults} from '../../actions/search_actions'
import {toggleAdvancedSearch} from '../../actions/map_actions'
import {xLightBlue, xMidBlue} from '../../stylesJS/base_colors'
import {trackActivity} from '../../actions/activity_actions'

import InputRange from 'react-input-range';
require("../../../node_modules/react-input-range/dist/react-input-range.min.css");


class SearchAdvLease extends Component {

	constructor(){
		super()
		this.state = {
				price: {
					min: 500,
					max: 800
				},
				leaseTerms: 12,
				groupSize: 1,
				water: false,
				heat: false,
				electric: false,
				ac: false,
				ensuite: false,
				parking: false,
				furnished: false,
				internet: false,
				type: {
					apartment: true,
					house: true
				}
			}
	}

	componentWillMount(){
		if(this.props.leaseSearchParams){
			this.setState(this.props.leaseSearchParams)
		}
	}

	handleFormChange(paramName, event){
		this.setState({
			[paramName]: !this.state[paramName]
		})
	}

	handlePriceChange(component, values){
		this.setState({
			price: values
		})
	}

	incrementgroupSize(amount){
		let newgroupSize = this.state.groupSize += amount
		if(newgroupSize < 1){
			newgroupSize = 1
		}else if(newgroupSize > 9){
			newgroupSize = 9
		}
		this.setState({
			groupSize: newgroupSize
		})
	}

	selectLeaseTerms(leaseTerms){
		let terms
		if(leaseTerms == 8){
			terms = leaseTerms
		}else if(leaseTerms == 12){
			terms = leaseTerms
		}else if(leaseTerms == 'ONGOING'){
			terms = leaseTerms
		}
		this.setState({
			leaseTerms: terms
		})
	}

	submitForm(){
		this.props.saveLeaseSearchState(this.state)
		this.props.trackActivity("APP", "ADVANCED_LEASE_SEARCH", null, this.props.fbUser, this.props.geoCoords, this.state)
		searchLeaseFilter(this.props.listOfResults, this.state)
			.then((data)=>{
				this.props.submitFilterResults(data)
				this.props.toggleAdvancedSearch()
			})
	}

	selectPropType(type){
		this.setState({
			type: {
				...this.state.type,
				[type]: !this.state.type[type]
			}
		})
	}

	render() {
		return (
			<div style={comStyles().searchForm}>
				<div style={comStyles().bulk}>
					<div className='col-md-12' style={comStyles().price}>
						 <span style={comStyles().roomsText}>$</span> {this.state.price.min} - {this.state.price.max} {this.state.price.max === 1200 ? "+" : null}
					</div>
					<div className='col-md-2' style={comStyles().sliderBox}>
						<InputRange
							step={5}
							maxValue={1200}
							minValue={300}
							value={this.state.price}
							onChange={this.handlePriceChange.bind(this)}
							formatLabel={(label)=>{return ""}}
						/>
					</div>
					<div className='col-md-12' style={comStyles().rooms}>
						<i className='ion-minus-circled col-md-2 col-md-offset-1' onClick={()=>this.incrementgroupSize(-1)} style={comStyles().roomInc}></i>
						<div className='col-md-6'><span style={comStyles().roomsText}>Group of</span> &nbsp; {this.state.groupSize}</div>
						<i className='ion-plus-circled col-md-2' onClick={()=>this.incrementgroupSize(1)} style={comStyles().roomInc}></i>
					</div>
					<div style={comStyles().toggleBoxes}>
						<div style={comStyles().toggleRow}>
							<div className='col-md-3' style={comStyles().toggleButton}>
						     <label className="toggle toggle-balanced">
						       <input type="checkbox" checked={this.state.ensuite} value={this.state.ensuite} onChange={this.handleFormChange.bind(this, "ensuite")} />
						       <div className="track">
						         <div className="handle"></div>
						       </div>
						     </label>
						    </div>
						    <div className='col-md-9' style={comStyles().toggleAttr}>
						    	Ensuite Bath
						    </div>
						</div>
						<div style={comStyles().toggleRow}>
							<div className='col-md-3' style={comStyles().toggleButton}>
						     <label className="toggle toggle-balanced">
						       <input type="checkbox" checked={this.state.furnished} value={this.state.furnished} onChange={this.handleFormChange.bind(this, "furnished")} />
						       <div className="track">
						         <div className="handle"></div>
						       </div>
						     </label>
						    </div>
						    <div className='col-md-9' style={comStyles().toggleAttr}>
						    	Furnished
						    </div>
						</div>
						<div style={comStyles().toggleRow}>
							<div className='col-md-3' style={comStyles().toggleButton}>
						     <label className="toggle toggle-balanced">
						       <input type="checkbox" checked={this.state.internet} value={this.state.internet} onChange={this.handleFormChange.bind(this, "internet")} />
						       <div className="track">
						         <div className="handle"></div>
						       </div>
						     </label>
						    </div>
						    <div className='col-md-9' style={comStyles().toggleAttr}>
						    	Internet Incl
						    </div>
						</div>
						<div style={comStyles().toggleRow}>
							<div className='col-md-3' style={comStyles().toggleButton}>
						     <label className="toggle toggle-balanced">
						       <input type="checkbox" checked={this.state.parking} value={this.state.parking} onChange={this.handleFormChange.bind(this, "parking")} />
						       <div className="track">
						         <div className="handle"></div>
						       </div>
						     </label>
						    </div>
						    <div className='col-md-9' style={comStyles().toggleAttr}>
						    	Parking
						    </div>
						</div>
						<div style={comStyles().toggleRow}>
							<div className='col-md-3' style={comStyles().toggleButton}>
						     <label className="toggle toggle-balanced">
						       <input type="checkbox" checked={this.state.electric} value={this.state.electric} onChange={this.handleFormChange.bind(this, "electric")} />
						       <div className="track">
						         <div className="handle"></div>
						       </div>
						     </label>
						    </div>
						    <div className='col-md-9' style={comStyles().toggleAttr}>
						    	Electric Incl
						    </div>
						</div>
						<div style={comStyles().toggleRow}>
							<div className='col-md-3' style={comStyles().toggleButton}>
						     <label className="toggle toggle-balanced">
						       <input type="checkbox" checked={this.state.heat} value={this.state.heat} onChange={this.handleFormChange.bind(this, "heat")} />
						       <div className="track">
						         <div className="handle"></div>
						       </div>
						     </label>
						    </div>
						    <div className='col-md-9' style={comStyles().toggleAttr}>
						    	Heating Incl
						    </div>
						</div>
						<div style={comStyles().toggleRow}>
							<div className='col-md-3' style={comStyles().toggleButton}>
						     <label className="toggle toggle-balanced">
						       <input type="checkbox" checked={this.state.water} value={this.state.water} onChange={this.handleFormChange.bind(this, "water")} />
						       <div className="track">
						         <div className="handle"></div>
						       </div>
						     </label>
						    </div>
						    <div className='col-md-9' style={comStyles().toggleAttr}>
						    	Water Incl
						    </div>
						</div>
						<div style={comStyles().toggleRow}>
							<div className='col-md-3' style={comStyles().toggleButton}>
						     <label className="toggle toggle-balanced">
						       <input type="checkbox" checked={this.state.ac} value={this.state.ac} onChange={this.handleFormChange.bind(this, "ac")} />
						       <div className="track">
						         <div className="handle"></div>
						       </div>
						     </label>
						    </div>
						    <div className='col-md-9' style={comStyles().toggleAttr}>
						    	A/C
						    </div>
						</div>
					</div>
					<div style={comStylesHighlight().termBox}>
						<div key='terms_8' onClick={()=>this.selectLeaseTerms(8)} className='col-md-4' style={comStylesHighlight(this.state.leaseTerms, 8).termOption}>
							8 MONTHS
						</div>
						<div key='terms_12' onClick={()=>this.selectLeaseTerms(12)} className='col-md-4' style={comStylesHighlight(this.state.leaseTerms, 12).termOption}>
							12 MONTHS
						</div>
						<div key='terms_ongoing' onClick={()=>this.selectLeaseTerms('ONGOING')} className='col-md-4' style={comStylesHighlight(this.state.leaseTerms, 'ONGOING').termOption}>
							ONGOING
						</div>
					</div>
					<div style={comStylesHighlight().termBox}>
						<div key='type_apartment' onClick={()=>this.selectPropType("apartment")} className='col-md-6' style={comStylesPropType("apartment", this.state.type.apartment).apartment}>
							APARTMENT
						</div>
						<div key='type_house' onClick={()=>this.selectPropType("house")} className='col-md-6' style={comStylesPropType("house", this.state.type.house).house}>
							HOUSE
						</div>
					</div>
					<div style={comStyles().submit}>
						<button className='btn btn-primary btn-block' onClick={this.submitForm.bind(this)} style={comStyles().searchButton}>SEARCH</button>
					</div>
				</div>
			</div>
		);
	}
}

SearchAdvLease.propTypes = {
	leaseSearchParams: React.PropTypes.object.isRequired,
	listOfResults: React.PropTypes.array.isRequired,
	fbUser: React.PropTypes.object.isRequired,
	geoCoords: React.PropTypes.array
};

const RadiumHOC = Radium(SearchAdvLease);

function mapStateToProps(state){
	return {
		leaseSearchParams: state.searchForm.leaseSearchParams,
		listOfResults: state.content.listOfResults,
		fbUser: state.tenant.fbUser,
		geoCoords: state.app.geoCoords
	}
}

export default connect(mapStateToProps, {saveLeaseSearchState, toggleAdvancedSearch, submitFilterResults, trackActivity})(RadiumHOC);

// ==============================

const comStyles = () => {
	return {
		searchForm: {
			display: "flex",
			justifyContent: "flex-start",
			flexDirection: "row",
			alignItems: "center",
			padding: "10px",
			margin: "15px",
			color: "white",
			borderRadius: "8px",
			backgroundColor: xMidBlue
		},
		sliderBox: {
			padding: "10px",
			width: "100%",
		},
		range: {
			backgroundColor: "rgba(0,0,0,0)",
			fontSize: "2rem",
			fontWeight: "bold",
			color: "white",
			border: "1px solid rgba(0,0,0,0)"
		},
		bulk: {
			height: "100%",
			width: "100%",
			display: "flex",
			flexDirection: "column"
		},
		price: {
			width: "100%",
			textAlign: "center",
			fontSize: "2.5rem",
			fontWeight: "bold",
			padding: "30px"
		},
		rooms: {
			width: "100%",
			fontSize: "2rem",
			fontWeight: "bold",
			padding: "20px",
			textAlign: "center"
		},
		roomsText: {
			fontSize: "1.3rem",
			textAlign: "center"
		},
		toggleBoxes: {
			width: "100%",
			display: "flex",
			justifyContent: "center",
			flexDirection: "row",
			flexWrap: "wrap",
			padding: "5px",
			margin: "15px 0px 0px 0px"
		},
		toggleRow: {
			width: "50%",
			display: "flex",
			flexDirection: "row",
			justifyContent: "flex-start",
			padding: "7px"
		},
		toggleButton: {
			textAlign: "left",
		},
		toggleAttr: {
			textAlign: "center",
			fontSize: "1.2rem",
			fontWeight: "bold",
			alignSelf: "center",
		},
		submit: {
			margin: "20px 0px 0px 0px"
		},
		searchButton: {
			fontSize: "1.3rem",
			fontWeight: "bold",
		},
		roomInc: {
			"@media (max-width: 600px), and (max-height: 740px)": {
				margin: "10px"
			}
		}
	}
}

const comStylesHighlight = (selectedTerm, thisTerm) => {
	let color = "rgba(0,0,0,0.3)"
	if(selectedTerm == thisTerm){
		color = "white"
	}
	return {
		termBox: {
			width: "100%",
			display: "flex",
			flexDirection: "row",
			margin: "20px 0px 0px 0px"
		},
		termOption: {
			padding: "7px",
			fontSize: "1.3rem",
			fontWeight: "bold",
			color: color,
			textAlign: "center",
			":hover": {
	          color: "rgba(256,256,256,1)",
	          cursor: "pointer"
	        }
		}
	}
}


const comStylesPropType = (attr, bool) => {

	const typeList = {
		apartment: false,
		house: false
	}
	typeList[attr] = bool

	for(let key in typeList){
		if(typeList[key]){
			typeList[key] = "white"
		}else{
			typeList[key] = "rgba(0,0,0,0.3)"
		}
	}

	return {
		apartment: {
			color: typeList["apartment"],
			padding: "7px",
			fontSize: "1.3rem",
			fontWeight: "bold",
			textAlign: "center",
			":hover": {
	          color: "rgba(256,256,256,1)",
	          cursor: "pointer"
	        }
		},
		house: {
			color: typeList["house"],
			padding: "7px",
			fontSize: "1.3rem",
			fontWeight: "bold",
			textAlign: "center",
			":hover": {
	          color: "rgba(256,256,256,1)",
	          cursor: "pointer"
	        }
		}
	}
}
