import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import moment from 'moment'

import {sortIconsSublet} from '../../api/iconSorter'
import {panToMap} from '../../actions/map_actions'
import {triggerSubletPopup, exitPopup} from '../../actions/popup_actions'

import { xGreyText } from '../../stylesJS/base_colors'

class LivePreviewSublet extends Component {

	shortenedAddress(){
		if(this.props.livePreviewSublet.address){
			const comma = this.props.livePreviewSublet.address.indexOf(',')
			return this.props.livePreviewSublet.address.slice(0, comma)
		}else{
			return ""
		}
	}

	viewSubletDetails(bool, sublet){
		if(bool){
			this.props.triggerSubletPopup(sublet)
		}else{
			this.props.exitPopup()
		}
	}

	render() {
		return (
			<div style={comStyles().card}>

				<div id='infobar' style={comStyles().infobar}>
					{/* Profile Picture */}
					<a href={this.props.livePreviewSublet.userurl} target='_blank'>
						<div className='img-circle' style={profileStyle(this.props.livePreviewSublet.userpic).profilePic}></div>
					</a>

					<div id='infobadge' style={comStyles().infobadge}>
						{/* Address */}
						<div style={comStyles().address}>
							{this.shortenedAddress()}
						</div>
						{/* User Name */}
						<div style={comStyles().userinfo}>
							<a href={this.props.livePreviewSublet.userurl} target="_blank">{this.props.livePreviewSublet.username}</a> &nbsp;
							on &nbsp;
							<b>{moment(this.props.livePreviewSublet.updated_at).format("MMM Do")}</b>
						</div>
					</div>
				</div>

				{/* Price */}
				<div style={comStyles().pricediv}>
					<span style={comStyles().price}>$ {this.props.livePreviewSublet.price}</span>
				</div>
				
				{/* Icons */}
				<div id='iconbar' style={comStyles().iconbar}>
					{sortIconsSublet(this.props.livePreviewSublet)}
				</div>

				{/* Buttons Bar */}
				<div id='buttonsBar' style={comStyles().buttonsBar}>
					<a href={this.props.livePreviewSublet.posturl} target='_blank' style={comStyles().seeOriginalAhref}>
						<button className="btn btn-info" style={comStyles().seeOriginal} onMouseOver={()=>this.viewSubletDetails(true, this.props.livePreviewSublet)} onMouseOut={()=>this.viewSubletDetails(false)} disabled>See Original</button>
					</a>
					<button className="btn btn-danger" onClick={()=>{this.props.panToMap(this.props.livePreviewSublet)}} style={comStyles().map} disabled>
						<i className='ion-ios-location' style='font-size:1em;'></i> &nbsp;
						Map
					</button>
				</div>
			</div>
		);
	}
}

LivePreviewSublet.propTypes = {
	livePreviewSublet: React.PropTypes.object.isRequired
};

const RadiumHOC = Radium(LivePreviewSublet);

function mapStateToProps(state){
	return {
		livePreviewSublet: state.tenant.livePreviewSublet
	}
}

export default connect(mapStateToProps, {panToMap, triggerSubletPopup, exitPopup})(RadiumHOC);


// ==============================

const comStyles = () => {
	return {
		card: {
			margin: "20px",
			padding: "3%",
			borderRadius: "5px",
			backgroundColor: "white",
		},
		infobar: {
			width: "100%",
			display: "flex",
			justifyContent: "center",
			alignItems: "flex-start",
			flexDirection: "row",
		},
		infobadge: {
			display: "flex",
			flexDirection: "column",
			margin: "1% 1% 1% 3%",
		},
		address: {
			fontSize:"1.5rem",
			fontWeight:"bold",
			width: "100%",
			height:"60%",
			color: xGreyText,
		},
		userinfo: {
			width: "100%",
			height: "35%",
			color: xGreyText,
			fontSize: "1.1rem",
			margin: "5px 0px 0px 0px"
		},
		pricediv: {
			textAlign:"center",
			padding: "30px",
		},
		price: {
			fontSize:"2.5rem",
			fontWeight:"bold",
			color: xGreyText,
		},
		iconbar: {
			width: "100%",
			display: "flex",
			justifyContent: "center",
			fontSize: "1rem",
			color: xGreyText,
		},
		buttonsBar: {
			display: "flex",
			justifyContent: "center",
			width: "100%",
			margin: "auto",
			fontSize: "1.1rem",
			fontWeight: "bold"
		},
		seeOriginalAhref: {
			flexGrow: 3
		},
		seeOriginal: {
			borderRadius: "0px",
			width: "100%"
		},
		map: {
			borderRadius: "0px",
			flexGrow: 1
		},
	}
}


const profileStyle = (src) => {
	return {
		profilePic: {
			height: "70px",
			width: "70px",
		    borderRadius:'50%',
		    backgroundImage: "url("+src+")",
		    backgroundPosition: 'center',
		    backgroundSize:'100% auto',
		    backgroundRepeat: 'no-repeat',
			display: "inline-block",
		}
	}
}