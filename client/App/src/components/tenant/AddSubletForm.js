import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import {browserHistory} from 'react-router'
import {xMidBlue, xDeepBlue, xGreyText} from '../../stylesJS/base_colors'
import { parseTextToSublet, getSubletsFromDB, getLeasesFromDB } from '../../api/tenantManager'
import { postToFacebook } from '../../api/fbExtractor'
import { setLivePreviewSublet } from '../../actions/tenant_actions'
import LivePreviewSublet from './LivePreviewSublet'
import { saveSubletsToRedux, saveLeasesToRedux } from '../../actions/database_actions'
import { extractAndSaveFacebookPostsToDB } from '../../api/fbExtractor'
import {trackActivity} from '../../actions/activity_actions'

class AddSubletForm extends Component {

	constructor(){
		super()
		this.state = {
			textbox: "",
			posted: false
		}
	}

	componentDidUpdate(){
		parseTextToSublet(this.state.textbox, this.props.city.city_name, this.props.fbUser)
			.then((data)=>{
				this.props.setLivePreviewSublet(data)
			})
	}

	handleText(event){
		this.setState({
			textbox: event.target.value
		})
	}

	renderButton(){
		if(this.state.posted){
			return (
				<a href={"http://facebook.com/"+this.state.posted} target="_blank"><button className='btn btn-primary' style={comStyles().submitButton} type='button'>
					<img src='../../../res/images/fb-icon.png' style={comStyles().fbIcon} />
					<div style={comStyles().postText}>View</div>
				</button></a>
			)
		}else{
			return (
				<button onClick={this.postToFacebook.bind(this)} className='btn btn-primary' style={comStyles().submitButton} type='button'>
					<img src='../../../res/images/fb-icon.png' style={comStyles().fbIcon} />
					<div style={comStyles().postText}>Post to Facebook</div>
				</button>
			)
		}
	}

	postToFacebook(){
		const self = this
		this.props.trackActivity("APP", "POST_SUBLET", null, this.props.fbUser, this.props.geoCoords, {text: this.state.textbox, fbPostId: this.state.posted})
		postToFacebook("1591404561120090", this.state.textbox)
			.then((data)=>{
				self.setState({
					posted: data.id
				})
				extractAndSaveFacebookPostsToDB(self.props.city, self.props.fbUser)
				getSubletsFromDB(self.props.city).then((sublets)=>{
					this.props.saveSubletsToRedux(sublets)
				})
				getLeasesFromDB(self.props.city).then((leases)=>{
					this.props.saveLeasesToRedux(leases)
				})
			})
	}

	goToLandlordLogin(){
		browserHistory.push('/landlord')
	}

	render() {
		return (
			<div style={comStyles().mainview}>
				<div style={comStyles().formPart}>
					<div style={comStyles().submissionForm}>
						<div style={comStyles().profileHeader}>
							<a href={"http://facebook.com/"+this.props.fbUser.id} target='_blank'>
								<div className='img-circle' style={profileStyle(this.props.fbUser.picurl).profilePic}></div>
							</a>
							<div style={comStyles().profileName}>{this.props.fbUser.name}</div>
						</div>
						<textarea value={this.state.textbox} onChange={this.handleText.bind(this)} style={comStyles().textareaBox}  placeholder="You must be a member of the group to post. To post with images, visit the Facebook Group directly." required></textarea>
						{this.renderButton()}
						<div style={comStyles().visitLink}>
							<a href='https://www.facebook.com/groups/1591404561120090/' target='_blank' style={comStyles().visitLinkText}>
								Visit Group Page
							</a>
						</div>
					</div>
					{
						this.state.textbox != ""
						?
						<div style={comStyles().livePreview}>
							<LivePreviewSublet />
						</div>
						:
						null
					}
					<div style={comStyles().iconHintBar}>
						<i className='ion-person' title='Rooms Available' style={comStyles().icon}></i>
						<i className='ion-cube' title='Ensuite Bath' style={comStyles().icon}></i>
						<i className='ion-outlet' title='Utilities' style={comStyles().icon}></i>
						<i className='ion-ios-telephone' title='Phone' style={comStyles().icon}></i>
						<i className='ion-female' title='Female Only' style={comStyles().icon}></i>
						<i className='ion-calendar' title='Semester' style={comStyles().icon}></i>
					</div>
				</div>
				{
					this.state.textbox
					?
					null
					:
					<div style={comStyles().loginPart}>
						<h4 onClick={this.goToLandlordLogin} style={comStyles().loginLink}>Landlord Login</h4>
					</div>
				}
			</div>
		)
	}
}

AddSubletForm.propTypes = {
	fbUser: React.PropTypes.object.isRequired,
	city: React.PropTypes.object.isRequired,
	geoCoords: React.PropTypes.array
};

const RadiumHOC = Radium(AddSubletForm);

function mapStateToDispatch(state){
	return {
		fbUser: state.tenant.fbUser,
		city: state.tenant.city,
		geoCoords: state.app.geoCoords
	}
}

export default connect(mapStateToDispatch, {setLivePreviewSublet, saveSubletsToRedux, saveLeasesToRedux, trackActivity})(RadiumHOC);


// ============================

const comStyles = () => {
	return {
		mainview: {
			padding: "20px",
			height: "100vh",
			overflowY: "scroll",
			display: "flex",
			flexDirection: "column"
		},
		formPart:{
			height: "95%"
		},
		loginPart:{
			height: "5%",
			textAlign: "center"
		},
		loginLink: {
			cursor: "pointer",
			color: xDeepBlue
		},
		submissionForm: {
			display: "flex",
			flexDirection: "column",
			justifyContent: "center",
			padding: "10px",
			top: "10px",
			backgroundColor: xMidBlue,
		},
		profileHeader: {
			display: "flex",
			flexDirection: "row",
			justifyContent: "center",
			padding: "10px",
			alignItems: "center"
		},
		profileName: {
			fontSize: "2rem",
			fontWeight: "bold",
			color: "white",
			margin: "0px 0px 0px 20px"
		},
		textareaBox: {
			width: "100%",
			height: "200px",
			borderRadius: "5px",
			padding: "15px"
		},
		submitButton: {
			width: "100%",
			padding: "20px",
			margin: "20px 0px 0px 0px"
		},
		fbIcon: {
			width: "8%",
			height: "auto",
			display: "inline-block"
		},
		postText: {
			fontSize: "1.4rem",
			fontWeight: "bold",
			color: "white",
			display: "inline-block",
			margin: "0px 0px 0px 10px"
		},
		visitLink: {
			fontSize: "1rem",
			fontWeight: "bold",
			textAlign: "center",
			padding: "20px",
		},
		visitLinkText: {
			color: "white",
		},
		livePreview: {
			margin: "20px"
		},
		iconHintBar: {
			padding: "5px",
			display: "flex",
			flexDirection: "row",
			justifyContent: "center"
		},
		icon: {
			fontSize: "1rem",
			margin: "10px",
			color: xMidBlue,
			cursor: "pointer"
		}
	}
}


const profileStyle = (src) => {
	return {
		profilePic: {
			height: "60px",
			width: "60px",
		    borderRadius:'50%',
		    backgroundImage: "url("+src+")",
		    backgroundPosition: 'center',
		    backgroundSize:'100% auto',
		    backgroundRepeat: 'no-repeat',
			display: "inline-block",
		}
	}
}
