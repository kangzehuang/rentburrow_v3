import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'

import { goToSettingsView, goToWatchlistView, goToAddView } from '../../actions/tenant_actions'
import {xMidBlue, xDeepBlue} from '../../stylesJS/base_colors'
import {trackActivity} from '../../actions/activity_actions'

class ExtraActionsBar extends Component {

	handleSettingsView(){
		this.props.trackActivity("APP", "GO_TO_SETTINGS", null, this.props.fbUser, this.props.geoCoords)
		this.props.goToSettingsView()
	}

	handleAddView(){
		this.props.trackActivity("APP", "GO_TO_POST_SUBLET", null, this.props.fbUser, this.props.geoCoords)
		this.props.goToAddView()
	}

	handleWatchlistView(){
		this.props.trackActivity("APP", "GO_TO_FAVORITES", null, this.props.fbUser, this.props.geoCoords)
		this.props.goToWatchlistView()
	}

	render() {
		return (
			<div style={comStyles().ExtraActionsBar}>
				<button className='btn' style={comStyles().actionButton} title='Settings' onClick={this.handleSettingsView.bind(this)}>
					<i className='ion-android-settings' style={comStyles().icon}></i>
				</button>
				<button className='btn' style={comStyles().actionButton} title='Post New Sublet' onClick={this.handleAddView.bind(this)}>
					<i className='ion-plus-round' style={comStyles().icon}></i>
				</button>
				<button className='btn' style={comStyles().actionButton} title='Favorites' onClick={this.handleWatchlistView.bind(this)}>
					<i className='ion-heart' style={comStyles().icon}></i>
				</button>
			</div>
		)
	}
}

ExtraActionsBar.propTypes = {
	fbUser: React.PropTypes.object.isRequired,
	geoCoords: React.PropTypes.array
};

const RadiumHOC = Radium(ExtraActionsBar);

function mapStateToDispatch(state){
	return {
		fbUser: state.tenant.fbUser,
		geoCoords: state.app.geoCoords
	}
}

export default connect(mapStateToDispatch, {goToSettingsView, goToWatchlistView, goToAddView, trackActivity})(RadiumHOC);


// ============================

const comStyles = () => {
	return {
		ExtraActionsBar: {
			zIndex: "10",
			position: "absolute",
			width: "auto",
			right: "22px",
			top: "60px",
			display: "flex",
			flexDirection: "column",
			"@media (max-width: 600px), and (max-height: 740px)": {
				position: "relative",
				display: "none"
			}
		},
		actionButton: {
			backgroundColor: xMidBlue,
			margin: "5px 0px 5px 0px"
		},
		icon: {
			fontSize: "1.5rem",
			fontWeight: "bold",
			color: "white",
		}
	}
}
