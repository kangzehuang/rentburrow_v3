import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'

import {searchSubletFilter} from '../../api/searchFilter'
import {saveSubletSearchState, submitFilterResults} from '../../actions/search_actions'
import {toggleAdvancedSearch} from '../../actions/map_actions'
import {xLightBlue, xMidBlue} from '../../stylesJS/base_colors'
import {trackActivity} from '../../actions/activity_actions'

import InputRange from 'react-input-range';
require("../../../node_modules/react-input-range/dist/react-input-range.min.css");

class SearchAdvSublet extends Component {

	constructor(){
		super()
		this.state = {
				price: {
					min: 400,
					max: 700
				},
				rooms: 1,
				utils: false,
				ensuite: false,
				femaleOnly: false,
				semester: null
			}
	}

	componentWillMount(){
		if(this.props.subletSearchParams){
			this.setState(this.props.subletSearchParams)
		}
	}

	componentDidMount(){
		const now = new Date()
		const month = now.getMonth()
		let semester
		if(month >= 0 && month < 3){
			semester = "spring"
		}else if(month > 4 && month < 8){
			semester = "fall"
		}else{
			semester = "winter"
		}
		if(this.props.subletSearchParams.semester){
			this.setState({
				semester: this.props.subletSearchParams.semester
			})
		}else{
			this.setState({
				semester: semester
			})
		}
	}

	handleFormChange(paramName, event){
		this.setState({
			[paramName]: !this.state[paramName]
		})
	}

	handlePriceChange(component, values){
		this.setState({
			price: values
		})
	}

	incrementRooms(amount){
		let newRoomAmount = this.state.rooms += amount
		if(newRoomAmount < 1){
			newRoomAmount = 1
		}else if(newRoomAmount > 9){
			newRoomAmount = 9
		}
		this.setState({
			rooms: newRoomAmount
		})
	}

	selectSemester(semester){
		this.setState({
			semester: semester
		})
	}

	submitForm(){
		this.props.saveSubletSearchState(this.state)
		this.props.trackActivity("APP", "ADVANCED_SUBLET_SEARCH", null, this.props.fbUser, this.props.geoCoords, this.state)
		searchSubletFilter(this.props.listOfResults, this.state)
			.then((data)=>{
				// console.log("Submitting filtered results...")
				this.props.submitFilterResults(data)
				this.props.toggleAdvancedSearch()
			})
	}

	render() {
		let room = 'room'
		if(this.state.rooms > 1){
			room = 'rooms'
		}
		return (
			<div style={comStyles().searchForm}>
				<div style={comStyles().bulk}>
					<div className='col-md-12' style={comStyles().price}>
						 <span style={comStyles().roomsText}>$</span> {this.state.price.min} - {this.state.price.max} {this.state.price.max === 1200 ? "+" : null}
					</div>
					<div className='col-md-12' style={comStyles().sliderBox}>
						<InputRange
							step={5}
			        maxValue={1200}
			        minValue={200}
			        value={this.state.price}
			        onChange={this.handlePriceChange.bind(this)}
							formatLabel={(label)=>{return ""}}
			      />
					</div>
					<div className='col-md-12' style={comStyles().rooms}>
						<i className='ion-minus-circled col-md-2 col-md-offset-2' onClick={()=>this.incrementRooms(-1)} style={comStyles().roomInc}></i>
						<div className='col-md-4'>{this.state.rooms} <span style={comStyles().roomsText}>{room}</span></div>
						<i className='ion-plus-circled col-md-2' onClick={()=>this.incrementRooms(1)} style={comStyles().roomInc}></i>
					</div>
					<div style={comStyles().toggleBoxes}>
						<div style={comStyles().toggleRow}>
							<div className='col-md-4' style={comStyles().toggleButton}>
						     <label className="toggle toggle-balanced">
						       <input type="checkbox" checked={this.state.utils} value={this.state.utils} onChange={this.handleFormChange.bind(this, "utils")} />
						       <div className="track">
						         <div className="handle"></div>
						       </div>
						     </label>
						    </div>
						    <div className='col-md-8' style={comStyles().toggleAttr}>
						    	Utilities Included
						    </div>
						</div>
						<div style={comStyles().toggleRow}>
							<div className='col-md-4' style={comStyles().toggleButton}>
						     <label className="toggle toggle-balanced">
						       <input type="checkbox" checked={this.state.ensuite} value={this.state.ensuite} onChange={this.handleFormChange.bind(this, "ensuite")} />
						       <div className="track">
						         <div className="handle"></div>
						       </div>
						     </label>
						    </div>
						    <div className='col-md-8' style={comStyles().toggleAttr}>
						    	Ensuite Bath
						    </div>
						</div>
						<div style={comStyles().toggleRow}>
							<div className='col-md-4' style={comStyles().toggleButton}>
						     <label className="toggle toggle-balanced">
						       <input type="checkbox" checked={this.state.femaleOnly} value={this.state.femaleOnly} onChange={this.handleFormChange.bind(this, "femaleOnly")} />
						       <div className="track">
						         <div className="handle"></div>
						       </div>
						     </label>
						    </div>
						    <div className='col-md-8' style={comStyles().toggleAttr}>
						    	Female Only
						    </div>
						</div>
					</div>
					<div style={comStylesHighlight().semesterBox}>
						<div key='sem_winter' onClick={()=>this.selectSemester('winter')} className='col-md-4' style={comStylesHighlight(this.state.semester, 'winter').semesterOption}>
							WINTER
						</div>
						<div key='sem_spring' onClick={()=>this.selectSemester('spring')} className='col-md-4' style={comStylesHighlight(this.state.semester, 'spring').semesterOption}>
							SPRING
						</div>
						<div key='sem_fall' onClick={()=>this.selectSemester('fall')} className='col-md-4' style={comStylesHighlight(this.state.semester, 'fall').semesterOption}>
							FALL
						</div>
					</div>
					<div style={comStyles().submit}>
						<button className='btn btn-primary btn-block' onClick={this.submitForm.bind(this)} style={comStyles().searchButton}>SEARCH</button>
					</div>
				</div>
			</div>
		);
	}
}

SearchAdvSublet.propTypes = {
	subletSearchParams: React.PropTypes.object.isRequired,
	listOfResults: React.PropTypes.array.isRequired,
	fbUser: React.PropTypes.object.isRequired,
	geoCoords: React.PropTypes.array
};

const RadiumHOC = Radium(SearchAdvSublet);

function mapStateToProps(state){
	return {
		subletSearchParams: state.searchForm.subletSearchParams,
		listOfResults: state.content.listOfResults,
		fbUser: state.tenant.fbUser,
		geoCoords: state.app.geoCoords
	}
}

export default connect(mapStateToProps, {saveSubletSearchState, toggleAdvancedSearch, submitFilterResults, trackActivity})(RadiumHOC);

// ==============================

const comStyles = () => {
	return {
		searchForm: {
			display: "flex",
			justifyContent: "flex-start",
			flexDirection: "row",
			alignItems: "center",
			padding: "10px",
			margin: "15px",
			color: "white",
			borderRadius: "8px",
			backgroundColor: xMidBlue,
		},
		sliderBox: {
			padding: "10px",
			width: "100%",
		},
		range: {
			backgroundColor: "rgba(0,0,0,0)",
			fontSize: "2rem",
			fontWeight: "bold",
			color: "white",
			border: "1px solid rgba(0,0,0,0)"
		},
		bulk: {
			height: "100%",
			width: "100%",
			display: "flex",
			flexDirection: "column"
		},
		price: {
			width: "100%",
			textAlign: "center",
			fontSize: "2.5rem",
			fontWeight: "bold",
			padding: "30px"
		},
		rooms: {
			width: "100%",
			fontSize: "2rem",
			fontWeight: "bold",
			padding: "20px",
			textAlign: "center"
		},
		roomsText: {
			fontSize: "1.5rem",
			textAlign: "center"
		},
		toggleBoxes: {
			width: "100%",
			display: "flex",
			justifyContent: "center",
			flexDirection: "column",
			flexWrap: "wrap",
			padding: "5px",
			margin: "15px 0px 0px 0px"
		},
		toggleRow: {
			width: "100%",
			display: "flex",
			flexDirection: "row",
			justifyContent: "flex-start",
			padding: "7px"
		},
		toggleButton: {
			textAlign: "center",
		},
		toggleAttr: {
			textAlign: "left",
			fontSize: "1.5rem",
			fontWeight: "bold",
			alignSelf: "center",
		},
		submit: {
			margin: "20px 0px 0px 0px"
		},
		searchButton: {
			fontSize: "1.3rem",
			fontWeight: "bold",
		},
		roomInc: {
			"@media (max-width: 600px)": {
				margin: "20px",
			}
		}
	}
}

const comStylesHighlight = (selectedSemester, thisSemester) => {
	let color = "rgba(0,0,0,0.3)"
	if(selectedSemester == thisSemester){
		color = "white"
	}
	return {
		semesterBox: {
			width: "100%",
			display: "flex",
			flexDirection: "row",
			margin: "20px 0px 0px 0px"
		},
		semesterOption: {
			padding: "7px",
			fontSize: "1.5rem",
			fontWeight: "bold",
			color: color,
			textAlign: "center",
			":hover": {
	          color: "rgba(256,256,256,1)",
	          cursor: "pointer"
	        }
		}
	}
}
