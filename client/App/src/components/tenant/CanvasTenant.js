import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import { browserHistory } from 'react-router'

import {xLightBlue, xMidBlue, xDeepBlue} from '../../stylesJS/base_colors'
import {loadForViewMode, panToMap, setViewMode, selectPinsFromHere, appendLeaseToList, appendSubletToList,setMapViewMode } from '../../actions/map_actions'
import { saveSubletsToRedux, saveLeasesToRedux } from '../../actions/database_actions'
import {setFBProfile, setCity, goToWatchlistView, setPropertyClaim} from '../../actions/tenant_actions'
import {setWatchlist} from '../../actions/watchlist_actions'
import {toggleMobileMap} from '../../actions/mobile_actions'
import { extractAndSaveFacebookPostsToDB, grabFBProfile, convertTokenIntoLongLived } from '../../api/fbExtractor'
import { getCitiesFromDB, getSubletsFromDB, getLeasesFromDB, getSpecificLease, getSpecificSublet, checkIfLandlordExists } from '../../api/tenantManager'
import {getAWatchlist} from '../../api/watchlistsManager'
import {registerFacebookLoginWithCognito, landlordClaimViewIdentity} from '../../api/aws/aws-cognito'
import MobileMenuPop from './MobileMenuPop'
import MapView from '../shared_ui/MapView'
import Search from './Search'
import SearchResults from './SearchResults'
import ViewModeBar from './ViewModeBar'
import ExtraActionsBar from './ExtraActionsBar'
import Popup from '../shared_ui/Popup/Popup'
import PopupImage from '../shared_ui/Popup/PopupImage'
import TenantSettings from './TenantSettings'
import AddSubletForm from './AddSubletForm'
import WatchlistsOverview from './WatchlistsOverview'
import LandlordProfile from './LandlordProfile'
import MobileBackButton from './MobileBackButton'
import {trackActivity} from '../../actions/activity_actions'
import {FB_APP_ID} from '../../api/API_URLS'

class CanvasTenant extends Component {

	constructor(){
		super()
		this.state = {
			canvasWidth: 15,
			FB: null,
			hoverViewMode: false
		}
	}

	componentWillMount(){
		const self = this
        self.props.setCity({
        	city_name:'Waterloo',
        	coords: [43.473897,-80.531995]
        })
				window.fbAsyncInit = function() {
          FB.init({
            appId      : FB_APP_ID,
            xfbml      : true,
            version    : 'v2.8'
          });
          FB.AppEvents.logPageView();
          self.setState({
          	FB: FB
        })
				self.checkIfUrlClaimView()
        self.checkIfLoggedIn()
				// Check for city in url, and automatically set that city if present
			  self.checkCityUrl()
					.then(()=>{
						self.getDataFromDatabase()
					})
	      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
	}

	componentDidUpdate(){
		this.loadForView()
	}

	checkIfLoggedIn(){
		const longLiveToken = localStorage.getItem("fbToken")
		grabFBProfile()
			.then((profile)=>{
				// console.log(profile)
				profile = {
					...profile,
					accessToken: longLiveToken
				}
				this.props.trackActivity("APP", "LOGGED_IN", null, profile, this.props.geoCoords)
				// Save the FB user to Redux State
				this.props.setFBProfile(profile)
				this.props.trackActivity("APP", "SET_CITY", null, this.props.fbUser, this.props.geoCoords, this.props.city)
				// Extract and Save Facebook Posts To DB
			    // Also save the fb user to db and accessToken in backend
				extractAndSaveFacebookPostsToDB(this.props.city, profile)
				registerFacebookLoginWithCognito({
					authResponse: {
						accessToken: longLiveToken
					}
				})
				// Check for watchlist in url, and automatically go to watchlist view if present
				this.checkWatchlistUrl()
			})
			.catch((err)=>{
				// console.log(err)
				localStorage.removeItem("fbToken")
			})
	}

	checkIfUrlClaimView(){
		const urlParams = this.props.location.query
		if(urlParams.claim || urlParams.lease){
			checkIfLandlordExists(urlParams.claim)
				.then((exists)=>{
					if(exists){
						const profile = {
							profile: {
								id: urlParams.claim,
								name: "LANDLORD_URL_CLAIM_VIEW",
								picurl: "https://s3.amazonaws.com/rentburrow-images/ideas%2540bytenectar.io/default_home_icon.png"
							},
							accessToken: "xxxxxxxxxxxxxxx"
						}
						// assume 'UNAUTHENTICATED' role for federated identity group
						landlordClaimViewIdentity()
						this.props.trackActivity("APP", "LANDLORD_URL_CLAIM_VIEW", null, profile, this.props.geoCoords)
						// console.log("SETTING PROPERTY CLAIM")
						this.props.setPropertyClaim()
						// Save the FB user to Redux State
						this.props.setFBProfile(profile)
						this.props.trackActivity("APP", "SET_CITY", null, this.props.fbUser, this.props.geoCoords, this.props.city)
						this.checkLeaseOrSubletUrl()
					}
				})
		}
	}

	loginWithFacebook(){
		const self = this
		this.state.FB.login(function(response) {
			   	if (response.status === 'connected') {
						self.props.trackActivity("APP", "LOGGED_IN", null, self.props.fbUser, self.props.geoCoords)
				    // get access token
				    const accessToken = response.authResponse.accessToken
				    localStorage.setItem("fbToken", accessToken)
				    // get profile info
				    grabFBProfile().then((profile)=>{
							// console.log(profile)
				    	profile = {
				    		...profile,
				    		accessToken: accessToken
				    	}
				    	// Turn access token into long lived token
				    	convertTokenIntoLongLived(accessToken)
				    		.then((longToken)=>{
				    			// console.log(longToken)
				    			// console.log("Saving longlivedtoken to localstorage!")
				    			localStorage.setItem("fbToken", longToken)
				    		})
					    // Save the FB user to Redux State
							self.props.setFBProfile(profile)
							self.props.trackActivity("APP", "SET_CITY", null, self.props.fbUser, self.props.geoCoords, self.props.city)
					    // Extract and Save Facebook Posts To DB
					    // Also save the fb user to db and accessToken in backend
						extractAndSaveFacebookPostsToDB(self.props.city, profile)
						registerFacebookLoginWithCognito(response)
						// Check for watchlist in url, and automatically go to watchlist view if present
						self.checkWatchlistUrl()
				    })
				} else if (response.status === 'not_authorized') {
				    // The person is logged into Facebook, but not your app.
				    // console.log('already logged in');
				} else {
				    // The person is not logged into Facebook, so we're not sure if
				    // they are logged into this app or not.
				    // console.log('not logged in');
				}
			}, {scope: 'public_profile,email'}
		)
	}

	checkCityUrl(){
		// http://localhost:8080/?app=v3&city=Hamilton
		const p = new Promise((res, rej)=>{
			const urlParams = this.props.location.query
			// console.log(urlParams)
			if(urlParams.city){
				// console.log("Found city!")
				getCitiesFromDB()
					.then((data)=>{
						const match = data.filter((city)=>{
							return city.city_name == urlParams.city
						})[0]
						// console.log(match)
						if(match){
							this.props.setCity(match)
							this.props.panToMap({coords: match.coords})
							res()
						}else{
							res()
						}
					})
			}else{
				// console.log("No city found!")
				res()
			}
		})
		return p
	}

	getDataFromDatabase(){
		//// console.log(this.props.city)
		getSubletsFromDB(this.props.city).then((sublets)=>{
			this.props.saveSubletsToRedux(sublets)
		})
		getLeasesFromDB(this.props.city).then((leases)=>{
			this.props.saveLeasesToRedux(leases)
		})
		this.checkLeaseOrSubletUrl()
	}

	checkWatchlistUrl(){
		// http://localhost:8080/?app=v3&watchlist=5823a80d04a8443268f1351c
		const urlParams = this.props.location.query
		if(urlParams.watchlist){
			getAWatchlist(urlParams.watchlist)
				.then((data)=>{
					//// console.log(data)
					this.props.setWatchlist(data)
					this.props.goToWatchlistView()
					if(this.props.mobileViewer){
						this.props.toggleMobileMap(false)
					}
				})
		}
	}

	checkLeaseOrSubletUrl(){
		// console.log("Getting a specific lease...")
		const urlParams = this.props.location.query
		if(urlParams.lease){
			getSpecificLease(urlParams.lease)
				.then((data)=>{
					this.props.setViewMode("lease")
					this.props.appendLeaseToList(data)
					this.props.panToMap(data)
					this.props.selectPinsFromHere([data])
				})
		}else if(urlParams.sublet){
			getSpecificSublet(urlParams.sublet)
				.then((data)=>{
					this.props.setViewMode("sublet")
					this.props.appendSubletToList(data)
					this.props.panToMap(data)
					this.props.selectPinsFromHere([data])
				})
		}
	}

	redirectToLandlord(){
		browserHistory.push('/landlord')
	}

	renderLeftSide(){
		if(this.props.fbUser){
			if(this.props.selectedLandlord){
				return (
					<LandlordProfile />
				)
			}else if(this.props.settingsView){
				return (
					<TenantSettings />
				)
			}else if(this.props.watchlistView){
				return (
					<WatchlistsOverview />
				)
			}else if(this.props.addView){
				return (
					<AddSubletForm />
				)
			}else{
				return(
					<div>
						<Search style={comStyles().search} />
						<SearchResults style={comStyles().searchResults} />
					</div>
				)
			}
		}else{
			if(this.props.mobileViewer){
				return (
					<button className='btn btn-primary btn-block' onClick={this.loginWithFacebook.bind(this)} style={comStyles().login}>
						<img src='../../res/images/rentburrow_logo.png' style={comStyles().facebookLoginImg} />
						<div id='facebookLoginText' style={comStyles().facebookLoginText}>SIGN IN WITH FACEBOOK</div>
						<div style={comStyles().facebookLoginMobile}>Mobile</div>
					</button>
				)
			}else{
				return (
					<div style={comStyles().loginDiv}>
						<button className='btn btn-primary btn-block' onClick={this.loginWithFacebook.bind(this)} style={comStyles().login}>
							<img src='../../res/images/facebook.png' style={comStyles().facebookLoginImg} />
							<br/>
							<span id='facebookLoginText' className='col-md-12' style={comStyles().facebookLoginText}>SIGN IN WITH FACEBOOK</span>
						</button>
						<button className='btn btn-primary btn-block' onClick={this.redirectToLandlord} style={comStyles().landlordLogin}>
							<span style={comStyles().landlordLoginText}>Landlord Login</span>
						</button>
					</div>
				)
			}
		}
	}

	loadForView(){
		if(this.props.viewMode == "sublet"){
			this.props.loadForViewMode(this.props.sublets)
		}else if(this.props.viewMode == "lease"){
			this.props.loadForViewMode(this.props.leases)
		}else if(this.props.viewMode == "both"){
			this.props.loadForViewMode(this.props.leases.concat(this.props.sublets))
		}
	}

	renderPopup(){
		if(this.props.popupToggle){
			return (
				<Popup />
			)
		}
	}

	renderPopupImage(){
		if(this.props.imgToggle){
			return(
				<PopupImage />
			)
		}
	}

	renderMobileBackButton(){
		if(this.props.mobileViewer){
			return (<MobileBackButton />)
		}
	}

	renderMobileMenu(){
		if(this.props.mobileViewer){
			if(this.props.settingsView || this.props.watchlistView || this.props.addView || this.props.mobileMapView){
				return (
					<MobileMenuPop />
				)
			}
		}
	}

	renderLogo(){
		if(this.props.mobileViewer){
			return (
				<img src='../../../res/images/rentburrow_logo.png' style={screenStyles(this.props.mobileViewer, this.props.mobileMapView).RentBurrowLogo} />
			)
		}else{
			return (
				<img src='../../../res/images/rentburrow_logo.png' style={comStyles().RentBurrowLogo} />
			)
		}
	}

	renderViewOptions(){
		if(!this.props.mobileViewer && this.props.panTo){
			return (
				<div style={comStyles().viewMapOptions} onMouseEnter={()=>this.setState({hoverViewMode: true})} onMouseLeave={()=>this.setState({hoverViewMode: false})}>
					<div style={comStyles().viewModeIconContainer}>
						<i className='ion-eye' style={comStyles().viewModeIcon}></i>
					</div>
					{
						this.state.hoverViewMode
						?
						<div style={comStyles().viewModeButtonSet}>
							<div onClick={()=>this.props.setMapViewMode('roadmap')} style={comStyles().viewModeButton}>ROADMAP</div>
							<div onClick={()=>this.props.setMapViewMode('hybrid')} style={comStyles().viewModeButton}>3D SATELLITE</div>
							<div onClick={()=>this.props.setMapViewMode('street')} style={comStyles().viewModeButton}>STREET VIEW</div>
						</div>
						:
						null
					}
				</div>
			)
		}
	}

	render() {
		return (
			<div id='canvas' style={comStyles(this.state.canvasWidth).canvas}>
				<div id='blueCanvas' style={screenStyles(this.props.mobileViewer, this.props.mobileMapView).blueCanvas}>
					{this.renderMobileMenu()}
					{this.renderLeftSide()}
				</div>
				<div id='mapView' style={screenStyles(this.props.mobileViewer, this.props.mobileMapView).mapView}>
					<ViewModeBar style={comStyles().ViewModeBar}/>
					{
						this.props.fbUser
						?
						<ExtraActionsBar style={comStyles().ExtraActionsBar}/>
						:
						null
					}
					{this.renderViewOptions()}
					{this.renderLogo()}
					{this.renderPopup()}
					{this.renderPopupImage()}
					{this.renderMobileBackButton()}
					<MapView rootView={true} />
				</div>
			</div>
		)
	}
}

CanvasTenant.propTypes = {
	sublets: React.PropTypes.array,
	leases: React.PropTypes.array,
	viewMode: React.PropTypes.string.isRequired,
	popupToggle: React.PropTypes.string,
	imgToggle: React.PropTypes.string,
	city: React.PropTypes.object,
	fbUser: React.PropTypes.object,
	selectedLandlord: React.PropTypes.object,
	settingsView: React.PropTypes.bool,
	watchlistView: React.PropTypes.bool,
	addView: React.PropTypes.bool,
	geoCoords: React.PropTypes.array,
	mobileViewer: React.PropTypes.bool,
	mobileMapView: React.PropTypes.bool,
	panTo: React.PropTypes.object,
}

const RadiumHOC = Radium(CanvasTenant);

function mapStateToProps(state){
	return {
		viewMode: state.content.viewMode,
		sublets: state.content.sublets,
		leases: state.content.leases,
		popupToggle: state.popup.toggle,
		imgToggle: state.popup.imgForPopup,
		city: state.tenant.city,
		fbUser: state.tenant.fbUser,
		selectedLandlord: state.tenant.selectedLandlord,
		settingsView: state.tenant.settingsView,
		watchlistView: state.tenant.watchlistView,
		addView: state.tenant.addView,
		geoCoords: state.app.geoCoords,
		mobileViewer: state.app.mobileViewer,
		mobileMapView: state.app.mobileMapView,
		panTo: state.content.panTo
	}
}

export default connect(mapStateToProps, {loadForViewMode, setFBProfile, saveSubletsToRedux, saveLeasesToRedux, setCity, goToWatchlistView, setWatchlist, panToMap, setViewMode, selectPinsFromHere, appendLeaseToList, appendSubletToList, toggleMobileMap, trackActivity, setPropertyClaim, setMapViewMode })(RadiumHOC);

// =====================================

const comStyles = () => {
	return {
		loginDiv: {
			height: "100%",
			minHeight: "100%"
		},
		login: {
			height: "95%",
			minHeight: "95%",
			minWidth: "100%",
			margin: "0px",
			borderRadius: "0px"
		},
		landlordLogin: {
			height: "5%",
			minHeight: "5%",
			minWidth: "100%",
			margin: "0px",
			borderRadius: "0px"
		},
		landlordLoginText: {
			fontWeight: "bold",
			fontSize: "1.2rem"
		},
		facebookLoginImg: {
			width:"50%",
			height:"auto",
			margin:"0 auto",
			"@media (max-width: 600px), and (max-height: 740px)": {
				width: "50%",
				height: "auto"
			}
		},
		RentBurrowLogo: {
			height:"150px",
			width: "auto",
			position: "absolute",
			right: "60px",
			bottom: "20px",
			zIndex: 9
		},
		canvas: {
			display: "flex",
			justifyContent: "center",
			alignItems: "stretch"
		},
		search: {
			height: "auto"
		},
		searchResults: {
			flexGrow: 1
		},
		facebookLoginText: {
			fontSize: "1.3rem",
			fontWeight: "bold",
			"@media (max-width: 600px), and (max-height: 740px)": {
				margin: "20px auto",
				width: "60%",
				fontSize: "0.8rem",
				padding: "10px",
				border: "3px solid white",
				borderRadius: "15px",
				textAlign: "center"
			}
		},
		facebookLoginMobile: {
			"@media (max-width: 600px), and (max-height: 740px)": {
				textAlign: "center"
			}
		},
		viewMapOptions: {
			width: 'auto',
			height: 'auto',
			position: 'absolute',
			right: '180',
			top: '20',
			zIndex: 9,
			justifyContent: 'flex-end',
			alignItems: 'flex-end',
			width: '150px',
		},
		viewModeIconContainer: {
			width: '50px',
			height: '50px',
			right: 0,
			borderRadius: '50%',
			padding: '10px',
			backgroundColor: xDeepBlue,
			textAlign: 'center',
			alignItems: 'center',
			justifyContent: 'center',
			boxShadow: '0px 0px 50px '+xMidBlue,
		},
		viewModeIcon: {
			fontSize: "2rem",
			fontWeight: "bold",
			color: 'white',
		},
		viewModeButtonSet: {
			flexDirection: 'column',
			margin: '10px 0px 0px 0px'
		},
		viewModeButton: {
			borderRadius: '0px',
			fontWeight: 'bold',
			width: '150px',
			padding: '10px',
			backgroundColor: xMidBlue,
			textAlign: 'center',
			alignItems: 'center',
			justifyContent: 'center',
			fontSize: '1rem',
			fontWeight: 'bold',
			color: 'white',
			cursor: 'pointer'
		}
	}
}


const screenStyles = (mobileViewer, mobileMapView) => {
	let blueCanvasStyles = {
		flexGrow: 15,
		maxWidth: "30vw",
		minWidth: "500px",
		overflowY: "scroll"
	}
	let mapViewStyles = {
		flexGrow: 85,
	}
	let logoStyles = {}
	if(mobileViewer){
		if(mobileMapView){
			blueCanvasStyles = {
				...blueCanvasStyles,
				width: "0vw",
				maxWidth: "0vw",
				minWidth: "0vw"
			}
			mapViewStyles = {
				...mapViewStyles,
				width: "100vw"
			}
		}else{
			blueCanvasStyles = {
				...blueCanvasStyles,
				width: "100vw",
				maxWidth: "100vw",
				minWidth: "100vw"
			}
			mapViewStyles = {
				...mapViewStyles,
				width: "0vw"
			}
			logoStyles = {
				...logoStyles,
				display: "none"
			}
		}
	}
	return {
		blueCanvas: {
			backgroundColor: xLightBlue,
			height: "100vh",
			display: "inline-block",
			alignItems: "stretch",
			flexDirection: "column",
			...blueCanvasStyles
		},
		mapView: {
			height: "100vh",
			display: "inline-block",
			position: "relative",
			...mapViewStyles
		},
		RentBurrowLogo: {
			height:"150px",
			width: "auto",
			position: "absolute",
			right: "10px",
			bottom: "20px",
			zIndex: 9,
			...logoStyles
		},
	}
}
