import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import {browserHistory} from 'react-router'
import {toggleMobileMap} from '../../actions/mobile_actions'
import {xMidBlue, xDeepBlue} from '../../stylesJS/base_colors'
import {setCity} from '../../actions/tenant_actions'
import { getCitiesFromDB, getSubletsFromDB, getLeasesFromDB } from '../../api/tenantManager'
import { saveSubletsToRedux, saveLeasesToRedux } from '../../actions/database_actions'
import { panToMap, setMapViewMode } from '../../actions/map_actions'
import { extractAndSaveFacebookPostsToDB } from '../../api/fbExtractor'
import {trackActivity} from '../../actions/activity_actions'

class TenantSettings extends Component {

	constructor(){
		super()
		this.state = {
			cities: []
		}
	}

	componentWillMount(){
		getCitiesFromDB()
			.then((data)=>{
				this.setState({
					cities: data
				})
			})
	}

	generateCities(){
		const self = this
		// console.log()
		return this.state.cities.map((city)=>{
			if(city.city_name.toLowerCase() == this.props.currentCity.city_name.toLowerCase()){
				return (
					<option key={city.city_name} value={city.city_name} selected>
						{city.city_name}
					</option>
				)
			}else{
				return (
					<option key={city.city_name} value={city.city_name}>
						{city.city_name}
					</option>
				)
			}
		})
	}

	setCity(event){
		const match = this.state.cities.filter((city)=>{
			return city.city_name == event.target.value
		})[0]
		// console.log(match)
		this.props.trackActivity("APP", "CHANGE_CITY", null, this.props.fbUser, this.props.geoCoords, {city: match})
		this.props.trackActivity("APP", "SET_CITY", null, this.props.fbUser, this.props.geoCoords, {city: match})
		this.props.setCity(match)
		this.props.panToMap({coords: match.coords})
		extractAndSaveFacebookPostsToDB(match, this.props.fbUser)
		getSubletsFromDB(match).then((sublets)=>{
			this.props.saveSubletsToRedux(sublets)
		})
		getLeasesFromDB(match).then((leases)=>{
			this.props.saveLeasesToRedux(leases)
		})
		if(this.props.mobileViewer){
			this.props.toggleMobileMap(true)
		}
	}

	generateMapViews(){
		const self = this
		// console.log()
		const mapViews = [
			{ name: 'Regular', type: 'roadmap'},
			{ name: '3D Satellite', type: 'hybrid'},
			{ name: 'Street View', type: 'street'}
		]
		return mapViews.map((view)=>{
			if(view.type == this.props.mapViewMode){
				return (
					<option key={view.type} value={view.type} selected>
						{view.name}
					</option>
				)
			}else{
				return (
					<option key={view.type} value={view.type}>
						{view.name}
					</option>
				)
			}
		})
	}

	setMapView(event){
		this.props.setMapViewMode(event.target.value)
	}

	goToPrivacyPolicy(){
		this.props.trackActivity("APP", "PRIVACY_POLICY_VIEWED", null, this.props.fbUser, this.props.geoCoords)
		browserHistory.push('/policy')
	}

	logout(){
		this.props.trackActivity("APP", "LOGGED_OUT", null, this.props.fbUser, this.props.geoCoords)
		localStorage.removeItem("fbToken")
		browserHistory.push('/')
		location.reload()
	}

	goToLandlordLogin(){
		browserHistory.push('/landlord')
	}

	render() {
		return (
			<div style={comStyles().mainview}>
				<div style={comStyles().settings}>
					<div style={comStyles().title}>
						Settings
					</div>
					<div style={comStyles().citybar}>
						<div className='col-md-5' style={comStyles().cityText}>
							City
						</div>
						<select onChange={this.setCity.bind(this)} className='col-md-7' name="dropdown" style={comStyles().selectCity}>
							{this.generateCities()}
						</select>
					</div>
					<div style={comStyles().viewModeBar}>
						<div className='col-md-5' style={comStyles().cityText}>
							Map View
						</div>
						<select onChange={this.setMapView.bind(this)} className='col-md-7' name="dropdown" style={comStyles().selectCity}>
							{this.generateMapViews()}
						</select>
					</div>
				</div>
				<div style={comStyles().landlordButton}>
					<button onClick={this.goToLandlordLogin} className='btn btn-primary btn-block' type='button' style={comStyles().logout}>LANDLORD LOGIN</button>
				</div>
				<div style={comStyles().bottomButtons}>
					<button onClick={this.goToPrivacyPolicy.bind(this)} className='btn btn-info' type='button' style={comStyles().privacyPolicy}>Privacy Policy</button>
					<button onClick={this.logout.bind(this)} className='btn btn-danger' type='button' style={comStyles().logout}>LOGOUT</button>
				</div>
			</div>
		)
	}
}

TenantSettings.propTypes = {
	currentCity: React.PropTypes.object.isRequired,
	fbUser: React.PropTypes.object.isRequired,
	geoCoords: React.PropTypes.array,
	mobileViewer: React.PropTypes.bool,
	mapViewMode: React.PropTypes.string.isRequired
};

const RadiumHOC = Radium(TenantSettings);

function mapStateToDispatch(state){
	return {
		currentCity: state.tenant.city,
		fbUser: state.tenant.fbUser,
		mobileViewer: state.app.mobileViewer,
		geoCoords: state.app.geoCoords,
		mapViewMode: state.app.mapViewMode
	}
}

export default connect(mapStateToDispatch, {setCity, saveSubletsToRedux, saveLeasesToRedux, panToMap, toggleMobileMap, trackActivity, setMapViewMode })(RadiumHOC);


// ============================

const comStyles = () => {
	return {
		mainview:{
			padding: "10px"
		},
		settings: {
			backgroundColor: xDeepBlue,
			height: "70vh",
			borderRadius: "10px 10px 0px 0px",
			display: "flex",
			flexDirection: "column"
		},
		title: {
			padding: "40px",
			fontSize: "2rem",
			color: "white",
			fontWeight: "bold",
			textAlign: "center"
		},
		citybar: {
			display: "flex",
			flexDirection: "row",
			margin: "30px 20px 0px 20px"
		},
		viewModeBar: {
			display: "flex",
			flexDirection: "row",
			margin: "30px 20px 0px 20px"
		},
		cityText: {
			fontSize: "1.5rem",
			color: "white",
			padding: "10px",
			fontWeight: "bold",
			textAlign: "center"
		},
		selectCity: {
			fontSize: "1.4rem",
			fontWeight: "bold",
			backgroundColor: "rgba(256,256,256,0.9)",
			borderRadius: "10px"
		},
		privacyPolicy: {
			margin: "auto",
			fontSize: "1.5rem",
			fontWeight: "bold",
			width: "96%",
			borderRadius: "0px",
			"@media (max-width: 600px), and (max-height: 740px)": {
				fontSize: "1rem"
			}
		},
		logout: {
			margin: "auto",
			fontSize: "1.5rem",
			fontWeight: "bold",
			width: "96%",
			borderRadius: "0px",
			"@media (max-width: 600px), and (max-height: 740px)": {
				fontSize: "1rem"
			}
		},
		GPSbar: {
			display: "flex",
			flexDirection: "row",
			margin: "20px",
			padding: "10px"
		},
		bottomButtons: {
			backgroundColor: xDeepBlue,
			height: "15vh",
			margin: "20px 0px 0px 0px",
			padding: "10px",
			borderRadius: "10px",
			display: "flex",
			flexDirection: "column",
			alignItems: "center"
		},
		landlordButton: {
			backgroundColor: xDeepBlue,
			height: "8vh",
			padding: "10px",
			borderRadius: "0px 0px 10px 10px",
		}
	}
}
