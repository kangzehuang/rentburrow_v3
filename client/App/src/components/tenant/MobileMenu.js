import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import {toggleMobileMap} from '../../actions/mobile_actions'
import {xDeepBlue, xMidBlue} from '../../stylesJS/base_colors'
import {setViewMode} from '../../actions/map_actions'
import {goToListingsView} from '../../actions/tenant_actions'
import { goToSettingsView, goToWatchlistView, goToAddView } from '../../actions/tenant_actions'
import {trackActivity} from '../../actions/activity_actions'


class MobileMenu extends Component {

	handleAdvancedSearch(){
		this.props.advancedSearch()
		this.props.closeMenu()
	}

	handleViewSwitch(bool){
		this.props.toggleMobileMap(bool)
		this.props.closeMenu()
	}

	handleSublets(){
		this.props.trackActivity("APP", "SUBLET_VIEW", null, this.props.fbUser, this.props.geoCoords)
		this.props.setViewMode("sublet")
		this.props.goToListingsView()
		this.props.closeMenu()
	}

	handleLeases(){
		this.props.trackActivity("APP", "LEASE_VIEW", null, this.props.fbUser, this.props.geoCoords)
		this.props.setViewMode("lease")
		this.props.goToListingsView()
		this.props.closeMenu()
	}

	handleWatchlist(){
		this.props.trackActivity("APP", "GO_TO_FAVORITES", null, this.props.fbUser, this.props.geoCoords)
		this.props.goToWatchlistView()
		this.props.toggleMobileMap(false)
		this.props.closeMenu()
	}

	handleAdd(){
		this.props.trackActivity("APP", "GO_TO_POST_SUBLET", null, this.props.fbUser, this.props.geoCoords)
		this.props.goToAddView()
		this.props.toggleMobileMap(false)
		this.props.closeMenu()
	}

	handleSettings(){
		this.props.trackActivity("APP", "GO_TO_SETTINGS", null, this.props.fbUser, this.props.geoCoords)
		this.props.goToSettingsView()
		this.props.toggleMobileMap(false)
		this.props.closeMenu()
	}

	detemineView(){
		let view
		if(this.props.viewMode == 'sublet'){
			view = 'sublet'
		}
		if(this.props.viewMode == 'lease'){
			view = 'lease'
		}
		if(this.props.settingsView){
			view = 'settingsView'
		}
		if(this.props.watchlistView){
			view = 'watchlistView'
		}
		if(this.props.addView){
			view = 'addView'
		}
		return view
	}

	render() {
		return (
			<div style={comStyles().mainview}>
				{
					this.props.advancedSearch
					?
					<div onClick={this.handleAdvancedSearch.bind(this)} style={comStyles().menuButton}>
						<div className='col-xs-3' style={comStyles().icon}><i className='ion-search'></i></div>
						<div className='col-xs-9'>ADVANCED SEARCH</div>
					</div>
					:
					null
				}
				{
					this.props.mobileMapView
					?
					<div onClick={()=>this.handleViewSwitch(false)} style={comStyles().menuButton}>
						<div className='col-xs-3' style={comStyles().icon}><i className='ion-android-list'></i></div>
						<div className='col-xs-9'>LIST VIEW</div>
					</div>
					:
					<div onClick={()=>this.handleViewSwitch(true)} style={comStyles().menuButton}>
						<div className='col-xs-3' style={comStyles().icon}><i className='ion-android-pin'></i></div>
						<div className='col-xs-9'>MAP VIEW</div>
					</div>
				}
				<div onClick={this.handleSublets.bind(this)} style={comStyles(this.detemineView(), 'sublet').menuButton}>
					<div className='col-xs-3' style={comStyles().icon}><i className='ion-social-facebook'></i></div>
					<div className='col-xs-9'>SUBLETS</div>
				</div>
				<div onClick={this.handleLeases.bind(this)} style={comStyles(this.detemineView(), 'lease').menuButton}>
					<div className='col-xs-3' style={comStyles().icon}><i className='ion-home'></i></div>
					<div className='col-xs-9'>LEASES</div>
				</div>
				<div onClick={this.handleWatchlist.bind(this)} style={comStyles(this.detemineView(), 'watchlistView').menuButton}>
					<div className='col-xs-3' style={comStyles().icon}><i className='ion-heart'></i></div>
					<div className='col-xs-9'>FAVORITES</div>
				</div>
				<div onClick={this.handleAdd.bind(this)} style={comStyles(this.detemineView(), 'addView').menuButton}>
					<div className='col-xs-3' style={comStyles().icon}><i className='ion-plus-round'></i></div>
					<div className='col-xs-9'>POST SUBLET</div>
				</div>
				<div onClick={this.handleSettings.bind(this)} style={comStyles(this.detemineView(), 'settingsView').menuButton}>
					<div className='col-xs-3' style={comStyles().icon}><i className='ion-android-settings'></i></div>
					<div className='col-xs-9'>SETTINGS</div>
				</div>
			</div>
		);
	}
}

MobileMenu.propTypes = {
	advancedSearch: React.PropTypes.func,		// passed in
	closeMenu: React.PropTypes.func.isRequired,				// passed in
	mobileMapView: React.PropTypes.bool,
	viewMode: React.PropTypes.string.isRequired,
	settingsView: React.PropTypes.bool,
	watchlistView: React.PropTypes.bool,
	addView: React.PropTypes.bool
};

const RadiumHOC = Radium(MobileMenu);

function mapStateToProps(state){
	return {
		mobileMapView: state.app.mobileMapView,
		viewMode: state.content.viewMode,
		settingsView: state.tenant.settingsView,
		watchlistView: state.tenant.watchlistView,
		addView: state.tenant.addView
	}
}

export default connect(mapStateToProps, {setViewMode, goToListingsView, goToSettingsView, goToWatchlistView, goToAddView, toggleMobileMap, trackActivity})(RadiumHOC);


// ======================

export const comStyles = (deteminedView, buttonView) => {
	let backgroundColor = xDeepBlue
	if(deteminedView && buttonView){
		if(deteminedView == buttonView){
			backgroundColor = xMidBlue
		}
	}
	return {
		mainview: {
			backgroundColor: xDeepBlue,
			display: "flex",
			flexDirection: "column",
			width: "100vw",
			minHeight: "50vh",
			borderRadius: "0px 0px 10px 10px",
			padding: "15px 20px 15px 20px"
		},
		menuButton: {
			color: 'white',
			fontSize: "1.3rem",
			fontWeight: "bold",
			padding: "10px",
			backgroundColor: backgroundColor
		},
		icon: {
			textAlign: "center"
		}
	}
}
