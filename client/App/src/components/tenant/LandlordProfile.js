import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import moment from 'moment'
import { xGreyText, xStarYellow, xDeepBlue } from '../../stylesJS/base_colors'
import {getReviewsAndPropertiesForLandlord} from '../../api/tenantManager'
import {submitFilterResults} from '../../actions/search_actions'
import SearchResults from './SearchResults'
import ReviewLandlord from '../shared_ui/Review/ReviewLandlord'
import {trackActivity} from '../../actions/activity_actions'



class LandlordProfile extends Component {

	constructor(){
		super()
		this.state = {
			landlord: {
				landlord_id: "",
				company: "",
				url: "",
				thumbnail: "../../../res/images/no_logo_company.png",
				reviews: []
			},
			selection: "properties",
			reviewForm: false
		}
	}

	componentWillMount(){
		this.getReviewsAndProperties()
	}

	getReviewsAndProperties(){
			getReviewsAndPropertiesForLandlord(this.props.selectedLandlord.landlord_id)
				.then((data)=>{
					this.props.submitFilterResults(data.leases)
					this.setState({
						landlord: {
							...this.state.landlord,
							landlord_id: this.props.selectedLandlord.landlord_id,
							company: this.props.selectedLandlord.company,
							url: this.props.selectedLandlord.url || "#",
							thumbnail: this.props.selectedLandlord.thumbnail || "../../../res/images/no_logo_company.png",
							reviews: data.reviews
						}
					})
				})
	}

	determineIcon(reviews, rating){
		let emptyOrFilledStar = 'ion-ios-star-outline'
		// if hover = 0, then follow set rating
		if(determineAverage(reviews) >= rating){
			emptyOrFilledStar = 'ion-ios-star'
		}
		return emptyOrFilledStar
	}

	linkToWebsite(){
		this.props.trackActivity("LANDLORD", "LINK_TO_WEBSITE", {id: this.state.landlord.landlord_id}, this.props.fbUser, this.props.geoCoords)
		// console.log("Link To Website!")
	}

	renderHeader(){
		return (
			<div style={comStyles().header}>
				<div style={comStyles().cardHeader}>
					<div style={comStyles().thumbnail}>
						{/* Company Logo */}
						<a href={this.state.landlord.url} target='_blank'>
							<div onClick={this.linkToWebsite.bind(this)} className='img-circle' style={profileStyle(this.state.landlord.thumbnail).profilePic}></div>
						</a>
					</div>
					<div style={comStyles().infobar}>
						{/* Company Name */}
						<div onClick={this.linkToWebsite.bind(this)} style={comStyles().building_name}>
							<a href={this.state.landlord.url} target='_blank' style={comStyles().building_name}>
								{this.state.landlord.company}
							</a>
						</div>
					</div>
				</div>
				<div style={comStyles().totalStars}>
					<div style={comStyles().oneDecimalAverage}>{oneDecimalAverage(this.state.landlord.reviews)}</div> &nbsp; &nbsp;
					<i id='star1' className={this.determineIcon(this.state.landlord.reviews, 1)} style={comStyles().star} />
					<i id='star2' className={this.determineIcon(this.state.landlord.reviews, 2)} style={comStyles().star} />
					<i id='star3' className={this.determineIcon(this.state.landlord.reviews, 3)} style={comStyles().star} />
					<i id='star4' className={this.determineIcon(this.state.landlord.reviews, 4)} style={comStyles().star} />
					<i id='star5' className={this.determineIcon(this.state.landlord.reviews, 5)} style={comStyles().star} />
				</div>
			</div>
		)
	}

	selectView(selection){
		this.setState({selection: selection})
	}

	handleAddReview(){
		this.toggleReviewForm(true)
		this.selectView('reviews')
	}

	renderListSelection(){
		return (
			<div style={comStyles().listSelection}>
				<button className='btn btn-primary' onClick={()=>this.selectView('properties')} style={comStyles().selectProperties}>
					PROPERTIES
				</button>
				<button className='btn btn-primary' onClick={()=>this.selectView('reviews')} style={comStyles().selectReviews}>
					REVIEWS
				</button>
				{
					this.state.reviewForm
					?
					<button className='btn btn-primary' onClick={()=>this.toggleReviewForm(false)} style={comStyles().toggleReviewForm}>
						<i className='ion-close-round'></i>
					</button>
					:
					<button className='btn btn-primary' onClick={this.handleAddReview.bind(this)} style={comStyles().toggleReviewForm}>
						<i className='ion-plus-round'></i>
					</button>
				}
			</div>
		)
	}

	renderWhichList(){
		if(this.state.selection == 'properties'){
			return (
				<SearchResults />
			)
		}else if(this.state.selection == 'reviews'){
			return (
				<ReviewLandlord landlordForReview={this.props.selectedLandlord} reviews={this.state.landlord.reviews} reviewForm={this.state.reviewForm} toggleReviewForm={this.toggleReviewForm.bind(this)} getReviewsAndProperties={this.getReviewsAndProperties.bind(this)} />
			)
		}
	}

	toggleReviewForm(bool){
		this.setState({
			reviewForm: bool
		})
	}

	render() {
		return (
			<div style={comStyles().mainview}>
				{this.renderHeader()}
				{this.renderListSelection()}
				{this.renderWhichList()}
			</div>
		);
	}
}

LandlordProfile.propTypes = {
	selectedLandlord: React.PropTypes.object.isRequired,
	fbUser: React.PropTypes.object.isRequired,
	geoCoords: React.PropTypes.array
}

const RadiumHOC = Radium(LandlordProfile);

function mapStateToProps(state){
	return {
		selectedLandlord: state.tenant.selectedLandlord,
		fbUser: state.tenant.fbUser,
		geoCoords: state.app.geoCoords
	}
}

export default connect(mapStateToProps, {submitFilterResults, trackActivity})(RadiumHOC);

// ==============================


const determineAverage = (reviews) => {
	let total = 0
	let count = 0
	reviews.forEach((r)=>{
		if(r.scores){
			total += r.scores.professionalism
			total += r.scores.effectiveness
			total += r.scores.satisfaction
			count += 3
		}
	})
	const avg = Math.ceil(total/count)
	return avg
}

const oneDecimalAverage = (reviews) => {
	let total = 0
	let count = 0
	reviews.forEach((r)=>{
		if(r.scores){
			total += r.scores.professionalism
			total += r.scores.effectiveness
			total += r.scores.satisfaction
			count += 3
		}
	})
	const avg = (total/count).toFixed(1)
	if(isNaN(avg)){
		return ""
	}else{
		return avg
	}
}


// ==============================

const comStyles = () => {
	return {
		mainview: {
			margin: "15px"
		},
		header: {
			display: "flex",
			flexDirection: "row",
			backgroundColor: xDeepBlue,
			width: "100%",
			padding: "10px",
			borderRadius: "10px",
			"@media (max-width: 600px), and (max-height: 740px)":{
				flexDirection: "column"
			}
		},
		cardHeader: {
			padding: "10px",
			display: "flex",
			width: "60%",
			flexDirection: "row",
			alignItems: "center",
			"@media (max-width: 600px), and (max-height: 740px)":{
				width: "100%"
			}
		},
		thumbnail: {
			width: "100px",
		},
		infobar: {
			margin: "15px"
		},
		building_name: {
			fontSize: "1.4rem",
			color: "white",
			fontWeight: "bold"
		},
		oneDecimalAverage: {
			fontSize: "1.2rem",
			fontWeight: "bold",
			color: xStarYellow
		},
		totalStars: {
			display: "flex",
			flexDirection: "row",
			justifyContent: "center",
			alignItems: "center",
			width: "40%",
			"@media (max-width: 600px), and (max-height: 740px)":{
				width: "100%"
			}
		},
		star: {
			fontSize: "2rem",
			color: xStarYellow,
			margin: "0px 2px 0px 2px",
			fontWeight: "bold"
		},
		listSelection: {
			margin: "10px",
			display: "flex",
			flexDirection: "row",
			fontWeight: "bold",
		},
		selectProperties: {
			width: "45%",
			borderRadius: "10px 0px 0px 10px",
			fontWeight: "bold"
		},
		selectReviews: {
			width: "45%",
			borderRadius: "0px 0px 0px 0px",
			fontWeight: "bold"
		},
		toggleReviewForm: {
			width: "10%",
			borderRadius: "0px 10px 10px 0px",
			fontWeight: "bold",
			fontSize: "1.2rem"
		}
	}
}


const profileStyle = (src) => {
	return {
		profilePic: {
			height: "70px",
			width: "70px",
		    borderRadius:'50%',
		    backgroundImage: "url("+src+")",
		    backgroundPosition: 'center',
		    backgroundSize:'100% auto',
		    backgroundRepeat: 'no-repeat',
			display: "inline-block",
		}
	}
}
