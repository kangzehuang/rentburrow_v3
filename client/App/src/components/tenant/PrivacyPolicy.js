import React, {Component} from 'react'
import {connect} from 'react-redux'
import Radium from 'radium'
import {browserHistory} from 'react-router'


class PrivacyPolicy extends Component {

	goToMain(){
		browserHistory.push('/')
		location.reload()
	}

	render() {
		return (
			<div style={comStyles().mainview}>
				<h1 style={comStyles().header}>RentBurrow Privacy Policy</h1>

				<p> &nbsp; &nbsp; &nbsp; &nbsp; This privacy statement (“Privacy Statement”) applies to the treatment of personally identifiable information submitted by, or otherwise obtained from, you in connection with the associated application (RentBurrow). The Application is provided by RentBurrow Inc (and may be provided by RentBurrow Inc on behalf of a RentBurrow Inc licensor or partner (“Application Partner”). By using or otherwise accessing the Application, you acknowledge that you accept the practices and policies outlined in this Privacy Statement. </p>

				<h4>WHAT PERSONAL INFORMATION DOES RentBurrow INC COLLECT? </h4>
				<p>We collect the following types of information from our users:

				<b>Personal Information You Provide to Us:</b>
				 &nbsp; &nbsp; &nbsp; &nbsp; We may receive and store any information you submit to the Application (or otherwise authorize us to obtain – such as, from (for example) your Facebook account). The types of personal information collected may include your full name, email address, gender, IP address, browser information, username, demographic information, and any other information necessary for us to provide the Application services.

				<b>Personal Information Collected Automatically:</b>
				 &nbsp; &nbsp; &nbsp; &nbsp; We receive and store certain types of usage related information whenever you interact with Application. For example, RentBurrow Inc may automatically receive and record information regarding your computer’s IP address, browser information, Facebook user ID, Facebook Page fan status, and URLs accessed. Such information may be shared in aggregate (non-personally identifiable) form with our partners.
				</p>

				<h4>HOW DOES RentBurrow INC USE THE INFORMATION IT COLLECTS? </h4>
				<p>RentBurrow Inc uses the information described in this Privacy Statement (i) internally, to analyze, develop and improve its products and services, and (ii) as set forth below in the “Will RentBurrow Inc Share any of the personal information it Collects” section below. </p>

				<h4>APPLICATION PARTNER TREATMENT OF PERSONAL INFORMATION. </h4>
				<p>RentBurrow Inc may provide personal information to the applicable Application Partner. The Application Partner’s use of your personal information is subject to the Application Partner’s separate privacy policy – and not this Privacy Statement. The Application Partner’s privacy policy is linked to from within the Partner’s Facebook application. </p>

				<h4>WILL RentBurrow INC SHARE ANY OF THE PERSONAL INFORMATION IT RECEIVES? </h4>
				<p>Personal information about our users is an integral part of our business. We neither rent nor sell your personal information to anyone (with the exception of sharing your information with an applicable Application Partner – see the “Application Partner Treatment” section above). We share your personal information only as described below.

				<b>Application Partners:</b>
				We will share your personal information with an applicable Application Partner (see the “Application Partner Treatment” section above).

				<b>Agents:</b>
				We employ other companies and people to perform tasks on our behalf and need to share your information with them to provide products or services to you. Unless we tell you differently, RentBurrow Inc’s agents do not have any right to use personal information we share with them beyond what is necessary to assist us. You hereby consent to our sharing of personal information for the above purposes. Business Transfers: In some cases, we may choose to buy or sell assets. In these types of transactions, customer information is typically one of the business assets that are transferred. Moreover, if RentBurrow Inc, or substantially all of its assets were acquired, or in the unlikely event that RentBurrow Inc goes out of business or enters bankruptcy, user information would be one of the assets that is transferred or acquired by a third party. You acknowledge that such transfers may occur, and that any acquirer of RentBurrow Inc may continue to use your personal information as set forth in this policy.

				<b>Protection of RentBurrow Inc and Others:</b>
				We may release personal information when we believe in good faith that release is necessary to comply with the law; enforce or apply our conditions of use and other agreements; or protect the rights, property, or safety of RentBurrow Inc, our employees, our users, or others. This includes exchanging information with other companies and organizations for fraud protection and credit risk reduction.

				<b>With Your Consent:</b>
				Except as set forth above, you will be notified when your personal information may be shared with third parties, and will be able to prevent the sharing of this information.
				</p>

				<h4>CONDITIONS OF USE</h4>
				<p>If you decide to use or otherwise access the Application, your use/access and any possible dispute over privacy is subject to this Privacy Statement and our Terms of Use, including limitations on damages, arbitration of disputes, and application of Ontario provincial law. </p>

				<h4>THIRD PARTY APPLICATIONS/WEBSITES</h4>
				<p>The Application may permit you to link to other applications or websites. Such third party applications/websites are not under RentBurrow Inc’s control, and such links do not constitute an endorsement by RentBurrow Inc of those other applications/websites or the services offered through them. The privacy and security practices of such third party application/websites linked to the Application are not covered by this Privacy Statement, and RentBurrow Inc is not responsible for the privacy or security practices or the content of such websites. </p>

				<h4>WHAT PERSONAL INFORMATION CAN I ACCESS? </h4>
				<p>RentBurrow Inc allows you to access the following information about you for the purpose of viewing, and in certain situations, updating that information. This list may change in the event the Application changes.
				- Account and user profile information
				- User e-mail address, if applicable
				- Facebook profile information, if applicable
				- User preferences
				- Application specific data
				</p>

				<h4>CAN CHILDREN USE THE APPLICATION? </h4>
				<p>Our site and the services available through RentBurrow Inc are not intended for children under the age of 13. RentBurrow Inc does not knowingly or specifically collect information about children under the age of 13 and believes that children of any age should get their parents’ consent before giving out any personal information. We encourage you to participate in your child’s web experience. </p>

				<h4>CHANGES TO THIS PRIVACY STATEMENT. </h4>
				<p>RentBurrow Inc may amend this Privacy Statement from time to time. Use of information we collect now is subject to the Privacy Statement in effect at the time such information is used. If we make changes in the way we use personal information, we will notify you by posting an announcement on our Site or sending you an email. Users are bound by any changes to the Privacy Statement when he or she uses or otherwise accesses the Application after such changes have been first posted. </p>

				<h4>QUESTIONS OR CONCERNS. </h4>
				<p>If you have any questions or concerns regarding privacy on our Website, please send us a detailed message at support@RentBurrow.io. We will make every effort to resolve your concerns. </p>

				<p>Effective Date: April, 2016</p>

				<button onClick={this.goToMain} className='btn btn-block btn-primary'>I Agree, take me to the website.</button>
			</div>
		)
	}
}

PrivacyPolicy.propTypes = {
};

const RadiumHOC = Radium(PrivacyPolicy);

function mapStateToProps(state){
	return {
	}
}

export default connect(mapStateToProps)(RadiumHOC);

// =======================


const comStyles = () => {
	return {
		mainview: {
			width:"100vw",
			height:"100vh",
			backgroundColor:"#99ccff",
			overflow:"scroll",
			padding: "20px"
		},
		header: {
			textAlign: "center"
		},
		link: {
			color: "white",
			fontSize: "1rem",
			fontWeight: "bold"
		}
	}
}
