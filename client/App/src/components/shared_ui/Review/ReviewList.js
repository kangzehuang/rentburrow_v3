import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import {xDeepBlue} from '../../../stylesJS/base_colors'
import ReviewCard from './ReviewCard'

class ReviewList extends Component {

	renderNoReviewsMessage(){
		if(this.props.reviews.length == 0){
			return (
				<div style={comStyles().noneFoundMessage}>
					No Reviews Yet
				</div>
			)
		}
	}

	render() {
		return (
			<div style={this.props.selectedLandlord?comStyles().mainviewLandlord:comStyles().mainview}>
				{this.props.reviews.map((r)=>{
					return (
						<ReviewCard key={r._id} review={r} />
					)		
				})}
				{this.renderNoReviewsMessage()}
			</div>
		);
	}
}

ReviewList.propTypes = {
	reviews: React.PropTypes.array.isRequired,				// passed in
	selectedLandlord: React.PropTypes.object
};

const RadiumHOC = Radium(ReviewList);

function mapStateToProps(state){
	return {
		leaseForReview: state.popup.leaseForReview,
		selectedLandlord: state.tenant.selectedLandlord
	}
}

export default connect(mapStateToProps)(RadiumHOC);

// ==========================

const comStyles = () => {
	return {
		mainview: {
			margin: "10px",
			height: "60vh",
			overflowY: "scroll",
		},
		mainviewLandlord: {
			margin: "10px",
			height: "75vh",
			overflowY: "scroll",
		},
		noneFoundMessage: {
			height: "150px",
			width: "80%",
			margin: "auto",
			borderRadius: "20px",
			display: "flex",
			textAlign: "center",
			justifyContent: "center",
			alignItems: "center",
			fontSize: "1.2rem",
			fontWeight: "bold",
			color: xDeepBlue,
			backgroundColor: "rgba(256,256,256,0.2)"
		}
	}
}

