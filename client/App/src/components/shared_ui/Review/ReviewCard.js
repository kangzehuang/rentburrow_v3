import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import moment from 'moment'
import { xStarYellow, xMidLightBlue, xDeepBlue } from '../../../stylesJS/base_colors'
import {trackActivity} from '../../../actions/activity_actions'

class ReviewCard extends Component {

	changeHover(bool){
		this.setState({
			hover: bool
		})
	}

	calculateAverage(){
		let total = 0
		if(this.props.review.scores){
			// indicates a landlord review
			if(this.props.review.scores.location){
				total += this.props.review.scores.priceValue
				total += this.props.review.scores.quality
				total += this.props.review.scores.location
			// indicates a property review
			}else if(this.props.review.scores.professionalism){
				total += this.props.review.scores.professionalism
				total += this.props.review.scores.effectiveness
				total += this.props.review.scores.satisfaction
			}
		}
		const avg = (total/3).toFixed(1)
		if(isNaN(avg) || avg <= 0){
			return null
		}else{
			return avg
		}
	}

  handleReviewHover(){
    if(this.props.review.scores.professionalism){
      this.props.trackActivity("LANDLORD", "LANDLORD_REVIEW_VIEWED", {id: this.props.review.landlord_id}, this.props.fbUser, this.props.geoCoords, {reviewId: this.props.review._id})
    }else{
      this.props.trackActivity("LEASE", "PROPERTY_REVIEW_VIEWED", {id: this.props.review.property_id}, this.props.fbUser, this.props.geoCoords, {reviewId: this.props.review._id})
    }
  }

	render() {
		return (
			<div onMouseEnter={this.handleReviewHover.bind(this)} style={this.props.review.scores.professionalism?comStyles().mainviewLandlord:comStyles().mainview}>
				<div style={comStyles().left}>
					<div style={comStyles().reviewer_img}>
						<a href={"http://facebook.com/"+this.props.review.reviewer_id} target='_blank'>
							<div className='img-circle' style={profileStyle(this.props.review.reviewer_img).profilePic}></div>
						</a>
					</div>
					<div style={comStyles().score}>
						{this.calculateAverage()} &nbsp; <i className='ion-ios-star' style={comStyles().star} />
					</div>
				</div>
				<div style={comStyles().right}>
					<div style={comStyles().reviewer_name}>
						{this.props.review.reviewer_name}
						<span style={comStyles().datePosted}>&nbsp; on &nbsp; {moment(this.props.review.created_at).format("MMM Do YYYY")}</span>
					</div>
					<div style={comStyles().details}>
						<div style={this.props.review.scores.professionalism?comStyles().textLandlord:comStyles().text}>
							{this.props.review.text}
						</div>
						{
							this.props.review.scores.professionalism
							?
							<div style={comStyles().starDetailsLandlord}>
								<div style={comStyles().starDetail}>{this.props.review.scores.professionalism}/5 Professionalism</div>
								<div style={comStyles().starDetail}>{this.props.review.scores.effectiveness}/5 Effectiveness</div>
								<div style={comStyles().starDetail}>{this.props.review.scores.satisfaction}/5 Satisfaction</div>
							</div>
							:
							<div style={comStyles().starDetails}>
								<div style={comStyles().starDetail}>{this.props.review.scores.priceValue}/5 Price Value</div>
								<div style={comStyles().starDetail}>{this.props.review.scores.quality}/5 Quality</div>
								<div style={comStyles().starDetail}>{this.props.review.scores.location}/5 Location</div>
							</div>
						}
					</div>
				</div>
			</div>
		);
	}
}

ReviewCard.propTypes = {
	review: React.PropTypes.object.isRequired,			// passed in
	fbUser: React.PropTypes.object.isRequired,
	geoCoords: React.PropTypes.array
};

const RadiumHOC = Radium(ReviewCard);

function mapStateToProps(state){
	return {
	  fbUser: state.tenant.fbUser,
	  geoCoords: state.app.geoCoords
	}
}

export default connect(mapStateToProps, {trackActivity})(RadiumHOC);

// ==========================

const comStyles = () => {
	return {
		mainview: {
			margin: "10px",
			width: "90%",
			backgroundColor: xMidLightBlue,
			borderRadius: "15px",
			display: "flex",
			flexDirection:"row",
			padding: "10px"
		},
		mainviewLandlord: {
			margin: "10px",
			backgroundColor: xDeepBlue,
			borderRadius: "15px",
			display: "flex",
			flexDirection:"row",
			padding: "10px"
		},
		left: {
			display: "flex",
			flexDirection: "column",
			padding: "5px",
			width: "20%",
		},
		reviewer_img: {
			width: "100%",
			display: "flex",
			justifyContent: "center"
		},
		score: {
			width: "100%",
			display: "flex",
			justifyContent: "center",
			alignItems: "center",
			fontWeight: "bold",
			fontSize: "1rem",
			color: "white",
		},
		star: {
			color: xStarYellow,
			fontSize: "1.5rem"
		},
		right: {
			display: "flex",
			flexDirection: "column",
			padding: "5px",
			width: "80%",
		},
		reviewer_name: {
			fontSize: "1.3rem",
			padding: "5px",
			color: "white",
		},
		details: {
			width: "100%",
			// display: "flex",
			// flexDirection: "column",
			color: xDeepBlue
		},
		text: {
			fontSize: "1.2rem",
			display: "flex",
			// minHeight: "120px",
			justifyContent: "center",
			padding: "10px",
			alignItems: "center",
			fontSize: "bold",
			color: "white"
		},
		textLandlord: {
			fontSize: "1rem",
			display: "flex",
			// minHeight: "120px",
			justifyContent: "center",
			padding: "10px",
			alignItems: "center",
			overflow: "hidden",
			color: "white"
		},
		datePosted: {
			fontSize: "0.9rem"
		},
		starDetails: {
			display: "flex",
			justifyContent: "center",
			padding: "10px 10px 10px 10px",
			color: "white"
		},
		starDetailsLandlord: {
			display: "flex",
			justifyContent: "center",
			padding: "10px 10px 10px 10px",
			color: "white"
		},
		starDetail: {
			margin: "0px 10px 0px 10px",
			textAlign: "center"
		}
	}
}


const profileStyle = (src) => {
	return {
		profilePic: {
			height: "70px",
			width: "70px",
		    borderRadius:'50%',
		    backgroundImage: "url("+src+")",
		    backgroundPosition: 'center',
		    backgroundSize:'100% auto',
		    backgroundRepeat: 'no-repeat',
			display: "inline-block",
		}
	}
}
