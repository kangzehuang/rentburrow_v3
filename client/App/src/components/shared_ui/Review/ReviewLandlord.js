import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import ReviewLandlordForm from './ReviewLandlordForm'
import ReviewList from './ReviewList'
import {xWhiteSmoke, xGreyText, xDeepBlue, xStarYellow} from '../../../stylesJS/base_colors'

class ReviewLandlord extends Component {

	render() {
		return (
			<div id='reviewLandlord' style={comStyles().mainview}>
				{
					this.props.reviewForm
					?
					<ReviewLandlordForm landlordForReview={this.props.landlordForReview} toggleReviewForm={this.props.toggleReviewForm} getReviewsAndProperties={this.props.getReviewsAndProperties} />
					:
					null
				}
				<ReviewList reviews={this.props.reviews} />
			</div>
		);
	}
}

ReviewLandlord.propTypes = {
	reviews: React.PropTypes.array,			// passed in
	landlordForReview: React.PropTypes.object,	// passed in
	reviewForm: React.PropTypes.bool, 	// passed in
	toggleReviewForm: React.PropTypes.func, 	// passed in
	getReviewsAndProperties: React.PropTypes.func
};

const RadiumHOC = Radium(ReviewLandlord);

function mapStateToProps(state){
	return {
	}
}

export default connect(mapStateToProps)(RadiumHOC);


// ==========================

const comStyles = () => {
	return {
		mainview: {
			maxWidth: "60vw",
			display: "flex",
			flexDirection: "column",
			overflowY: "scroll",
			"@media (max-width: 600px), and (max-height: 740px)":{
				maxWidth: "90vw",
				overflow: "scroll"
			}
		}
	}
}
