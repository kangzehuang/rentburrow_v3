import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'

class PopupSublet extends Component {

	render() {
		return (
			<div style={comStyles().popupSublet}>
				<div style={comStyles().viewOnFb}>
					<a href={this.props.subletForPopup.posturl} target='_blank' style={comStyles().fbLink}>
						<i className='ion-social-facebook' style={comStyles().fbLogo}></i> &nbsp;
						View Original on Facebook
					</a>
				</div>
				<div style={comStyles().textDiv}>
					<p style={comStyles().text}>{this.props.subletForPopup.message}</p>
				</div>
			</div>
		);
	}
}

PopupSublet.propTypes = {
	subletForPopup: React.PropTypes.object.isRequired
};

const RadiumHOC = Radium(PopupSublet);

function mapStateToProps(state){
	return {
		subletForPopup: state.popup.subletForPopup
	}
}

export default connect(mapStateToProps)(RadiumHOC);


// ======================

const comStyles = () => {
	return {
		popupSublet: {
			display: "flex",
			margin: "20px 0px 0px 0px",
			flexDirection: "column",
			overflow: "scroll",
		},
		textDiv: {
			padding: "20px",
			fontSize: "1.2rem",
			maxWidth: "800px",
			maxHeight: "80vh",
			color: "white"
		},
		text: {
			wordWrap: "normal",
			overflowWrap: "break-word",
			whiteSpace: "pre-line"
		},
		viewOnFb: {
			padding: "10px",
			textAlign: "center",
			zIndex: "30",
			color: "white",
			border: "1px solid red",
		},
		fbLink: {
			color: "white",
			padding: "10px",
			textAlign: "center",
			zIndex: "30",
		},
		viewOnFb: {
			fontSize: "1rem"
		},
		fbLogo: {
			fontSize: "1.4rem"
		}
	}
}
