import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'

import {xWhiteSmoke, xGreyText} from '../../../stylesJS/base_colors'
import {sendAWSInquiryEmail} from '../../../api/aws/aws-SES'
import {trackActivity} from '../../../actions/activity_actions'

class PopupContact extends Component {

	constructor(){
		super()
		this.state = {
			message: "",
			replyPhone: "",
			replyEmail: "",
			sending: false,
			errorMessage: "",
			successMessage: ""
		}
	}

	componentDidMount(){
		const savedEmail = localStorage.getItem('savedTenantEmail')
		const savedPhone = localStorage.getItem('savedTenantPhone')
		const savedMessage = localStorage.getItem('savedTenantMessage')
		if(savedEmail){
			this.setState({
				replyEmail: savedEmail
			})
		}
		if(savedPhone){
			this.setState({
				replyPhone: savedPhone
			})
		}
		if(savedMessage){
			this.setState({
				message: savedMessage
			})
		}
	}

	handleChange(attr, event){
		this.setState({
			[attr]: event.target.value
		})
	}

	generateContacts(){
		const contacts = []
		this.props.leaseForPopup.contacts.forEach((contact)=>{
			if(contact.phone && contact.phone != "" && contact.phone != " "){
				contacts.push(
					<div style={comStyles().contactinfo}>
				  		<i className='ion-ios-telephone' style='font-size:1rem;'>&nbsp; {contact.phone}</i>
				  		<div>( {contact.name} )</div>
				  	</div>
				)
			}
		})
		return contacts
	}

	sendEmail(){
		const savedEmail = localStorage.getItem('savedTenantEmail')
		const savedPhone = localStorage.getItem('savedTenantPhone')
		const savedMessage = localStorage.getItem('savedTenantMessage')
		this.setState({sending: true})
		if(savedEmail !== this.state.replyEmail){
			localStorage.setItem('savedTenantEmail', this.state.replyEmail)
		}
		if(savedPhone !== this.state.replyPhone){
			localStorage.setItem('savedTenantPhone', this.state.replyPhone)
		}
		if(savedMessage !== this.state.message){
			localStorage.setItem('savedTenantMessage', this.state.message)
		}
		if(this.state.replyEmail && this.state.message){
			const landlordEmail = this.props.leaseForPopup.contacts[0].email
			const tenantInfo = {
				tenantEmail: localStorage.getItem('savedTenantEmail'),
				tenantPhone: localStorage.getItem('savedTenantPhone'),
				tenantMessage: this.state.message,
				tenantName: this.props.fbUser.name
			}
			sendAWSInquiryEmail(landlordEmail, tenantInfo, this.props.leaseForPopup)
				.then((data)=>{
					this.props.trackActivity("LEASE", "EMAIL_SENT", this.props.leaseForPopup, this.props.fbUser, this.props.geoCoords, this.state)
					this.setState({
						sending: false,
						successMessage: data,
						errorMessage: null
					})
				})
				.catch((err)=>{
					// console.log(err)
					this.setState({
						sending: false,
						errorMessage: err.message
					})
				})
		}else{
			this.setState({
				sending: false,
				errorMessage: "Please include a message and a reply email."
			})
		}
	}

	renderAlert(){
		if(this.state.errorMessage){
			return (
				<div className='alert alert-danger'>
					{this.state.errorMessage}
					<b onClick={()=>this.setState({errorMessage: ""})} style={comStyles().messageClose}>&times;</b>
				</div>
			)
		}else if(this.state.successMessage){
			return (
				<div className='alert alert-success'>
					{this.state.successMessage}
					<b onClick={()=>this.setState({successMessage: ""})} style={comStyles().messageClose}>&times;</b>
				</div>
			)
		}
	}

	render() {
		return (
			<div style={comStyles().form}>
				<div className='col-md-12' style={comStyles().header}>
					<div className='col-md-8'>
						<div style={comStyles().company}>{this.props.leaseForPopup.company || this.props.leaseForPopup.contacts[0].name}</div>
					</div>
					<div className='col-md-4'>
						<a href={this.props.leaseForPopup.url}>
							<img src={this.props.leaseForPopup.thumbnail} width="100%;" height="100%" style={profileStyle(this.props.leaseForPopup.thumbnail).profilePic} />
						</a>
					</div>
				</div>

			  	<div className="form-group" margin-top='25px'>
			    	<textarea value={this.state.message} onChange={this.handleChange.bind(this, 'message')} className="form-control" style={comStyles().textarea} placeholder="RentBurrow does not spam users. You and the company communicate privately with your own email." required></textarea>
			  	</div>
			    <div className="form-group" margin-top='15px'>
			      	<input value={this.state.replyPhone} onChange={this.handleChange.bind(this, 'replyPhone')} className="form-control" type='text' placeholder="&nbsp; Reply: Cell Phone" style={comStyles().phone} />
			      	<input value={this.state.replyEmail} onChange={this.handleChange.bind(this, 'replyEmail')} className="form-control" type='email' placeholder="&nbsp; Reply: youraddress@email.com" style={comStyles().email} required />
			    </div>

			    {this.renderAlert()}

					{
						this.state.sending
						?
						<div style={comStyles().loadingGifBox}>
							<img src='../../../res/images/loading.gif' style={comStyles().loadingGif} />
						</div>
						:
				    <button onClick={this.sendEmail.bind(this)} type="submit" className="btn btn-info btn-block" style={comStyles().submit}>
				  	  	SEND EMAIL
				    </button>
					}
			    {this.generateContacts()}
			</div>
		);
	}
}

PopupContact.propTypes = {
	leaseForPopup: React.PropTypes.object.isRequired,
	fbUser: React.PropTypes.object,
	geoCoords: React.PropTypes.array,
};

const RadiumHOC = Radium(PopupContact);

function mapStateToProps(state){
	return {
		leaseForPopup: state.popup.leaseForPopup,
		fbUser: state.tenant.fbUser,
		geoCoords: state.app.geoCoords
	}
}

export default connect(mapStateToProps, {trackActivity})(RadiumHOC);

// ==========================

const comStyles = () => {
	return {
		form: {
			display: "inline-block",
			minWidth: "250px",
			"@media (max-width: 600px), and (max-height: 740px)": {
				maxWidth: "100%",
				maxHeight: "100%",
				overflow: "scroll"
			}
		},
		header: {
			backgroundColor: xWhiteSmoke,
			padding: "5px",
			borderRadius: "5px",
			margin: "0px 0px 15px 0px"
		},
		company: {
			color: xGreyText,
			fontSize: "1.3rem",
			fontWeight: "bold"
		},
		textarea: {
			minHeight: "250px",
			"@media (max-width: 600px), and (max-height: 740px)":{
				minHeight: "150px"
			}
		},
		submit: {
			fontWeight:"bold",
			borderRadius:"0px"
		},
		contactinfo:{
			fontWeight:"bold",
			color:"black",
			borderRadius:"0px",
			backgroundColor: "#DDDDDD",
			textAlign: "center",
			margin: "0px",
			width: "100%",
			padding: "5px",
			display: "flex",
			flexDirection: "column",
			justifyContent: "center",
		},
		phone: {
			borderRadius: "5px 5px 0px 0px",
			padding: "10px",
			textAlign: "center"
		},
		email: {
			borderRadius: "0px 0px 5px 5px",
			padding: "10px",
			textAlign: "center"
		},
		messageClose: {
			float: "right",
			fontSize: "1rem",
			cursor: "pointer"
		},
		loadingGifBox: {
			width: "100%",
			display: "flex",
			justifyContent: "center"
		},
		loadingGif: {
			width: "30px",
			height: "30px",
			margin: "20px auto"
		}
	}
}


const profileStyle = (src) => {
	return {
		profilePic: {
			height: "50px",
			width: "50px",
		    borderRadius:'50%',
		    backgroundImage: "url("+src+")",
		    backgroundPosition: 'center',
		    backgroundSize:'100% auto',
		    backgroundRepeat: 'no-repeat',
			display: "inline-block",
		}
	}
}
