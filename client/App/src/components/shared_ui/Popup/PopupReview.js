import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import ReviewFormSelection from '../Review/ReviewFormSelection'
import ReviewList from '../Review/ReviewList'
import {goToLandlordProfile} from '../../../actions/tenant_actions'
import {shortenAddress} from '../../../api/iconSorter'
import {xWhiteSmoke, xGreyText, xDeepBlue, xStarYellow} from '../../../stylesJS/base_colors'
import {getReviewsForProperty} from '../../../api/tenantManager'

class PopupReview extends Component {

	constructor(){
		super()
		this.state = {
			reviews: [],
			form: false
		}
	}

	componentWillMount(){
		this.getReviews()
	}

	componentDidUpdate(nextProps, nextState){
		if(nextProps.leaseForReview != this.props.leaseForReview){
			this.getReviews()
		}
	}

	getReviews(){
		// console.log(this.props.leaseForReview)
		getReviewsForProperty(this.props.leaseForReview._id)
			.then((reviews)=>{
				this.setState({
					reviews: reviews
				})
			})
	}

	setForm(bool){
		this.setState({
			form: bool
		})
	}

	renderHeader(){
		return (
			<div style={comStyles().header}>
				<div style={comStyles().cardHeader}>
					<div style={comStyles().thumbnail}>
						{/* Company Logo */}
						<div onClick={()=>this.props.goToLandlordProfile(this.props.leaseForReview)}>
							<div className='img-circle' style={profileStyle(this.props.leaseForReview.thumbnail).profilePic}></div>
						</div>
					</div>
					<div style={comStyles().infobar}>
						{/* Address */}
						<div style={comStyles().building_name}>
							{shortenAddress(this.props.leaseForReview.address)}
						</div>
						{/* Company Name */}
						<div style={comStyles().userinfo}>
							From &nbsp;
							<span onClick={()=>this.props.goToLandlordProfile(this.props.leaseForReview)} style={comStyles().landlordLink}>{this.props.leaseForReview.company}</span>
						</div>
					</div>
				</div>
				<div style={comStyles().totalStars}>
					<div style={comStyles().oneDecimalAverage}>{oneDecimalAverage(this.state.reviews)}</div> &nbsp; &nbsp;
					<i id='star1' className={this.determineIcon(this.state.reviews, 1)} style={comStyles().star} />
					<i id='star2' className={this.determineIcon(this.state.reviews, 2)} style={comStyles().star} />
					<i id='star3' className={this.determineIcon(this.state.reviews, 3)} style={comStyles().star} />
					<i id='star4' className={this.determineIcon(this.state.reviews, 4)} style={comStyles().star} />
					<i id='star5' className={this.determineIcon(this.state.reviews, 5)} style={comStyles().star} />
				</div>
				<div style={comStyles().writeReview}>
					{
						this.state.form
						?
						<i className='ion-backspace' onClick={()=>this.setForm(false)} style={comStyles().writeIcon}></i>
						:
						<i className='ion-compose' onClick={()=>this.setForm(true)} style={comStyles().writeIcon}></i>
					}
				</div>
			</div>
		)
	}

	determineIcon(reviews, rating){
		let emptyOrFilledStar = 'ion-ios-star-outline'
		// if hover = 0, then follow set rating
		if(determineAverage(reviews) >= rating){
			emptyOrFilledStar = 'ion-ios-star'
		}
		return emptyOrFilledStar
	}

	exitFormAndRefresh(){
		// console.log("exitFormAndRefresh")
		getReviewsForProperty(this.props.leaseForReview._id)
			.then((reviews)=>{
				this.setState({
					reviews: reviews,
					form: false
				})
			})
	}

	render() {
		return (
			<div style={comStyles().mainview}>
				{this.renderHeader()}
				{
					this.state.form
					?
					<ReviewFormSelection exitForm={()=>this.exitFormAndRefresh()} />
					:
					<ReviewList reviews={this.state.reviews} />
				}
			</div>
		);
	}
}

PopupReview.propTypes = {
	leaseForReview: React.PropTypes.object.isRequired
};

const RadiumHOC = Radium(PopupReview);

function mapStateToProps(state){
	return {
		leaseForReview: state.popup.leaseForReview,
	}
}

export default connect(mapStateToProps, {goToLandlordProfile})(RadiumHOC);


// ==========================

const determineAverage = (reviews) => {
	let total = 0
	let count = 0
	reviews.forEach((r)=>{
		if(r.scores){
			total += r.scores.priceValue
			total += r.scores.quality
			total += r.scores.location
			count += 3
		}
	})
	const avg = Math.ceil(total/count)
	return avg
}

const oneDecimalAverage = (reviews) => {
	let total = 0
	let count = 0
	reviews.forEach((r)=>{
		if(r.scores){
			total += r.scores.priceValue
			total += r.scores.quality
			total += r.scores.location
			count += 3
		}
	})
	const avg = (total/count).toFixed(1)
	if(isNaN(avg)){
		return ""
	}else{
		return avg
	}
}

// ==========================

const comStyles = () => {
	return {
		mainview: {
			maxWidth: "60vw",
			display: "flex",
			flexDirection: "column",
			"@media (max-width: 600px), and (max-height: 740px)":{
				maxWidth: "100vw",
				overflow: "scroll"
			}
		},
		header: {
			display: "flex",
			flexDirection: "row",
			backgroundColor: "rgba(256,256,256,0.4)",
			width: "90%",
			borderRadius: "10px",
			"@media (max-width: 600px), and (max-height: 740px)":{
				flexDirection: "column"
			}
		},
		cardHeader: {
			padding: "10px",
			display: "flex",
			width: "60%",
			flexDirection: "row",
			alignItems: "center",
			"@media (max-width: 600px), and (max-height: 740px)":{
				width: "100%"
			}
		},
		thumbnail: {
			width: "100px",
			cursor: "pointer"
		},
		landlordLink: {
			cursor: "pointer",
			color: xDeepBlue
		},
		infobar: {
			width: "300px",
		},
		oneDecimalAverage: {
			fontSize: "1.2rem",
			fontWeight: "bold",
			color: xStarYellow
		},
		totalStars: {
			display: "flex",
			flexDirection: "row",
			justifyContent: "center",
			alignItems: "center",
			width: "30%",
			"@media (max-width: 600px), and (max-height: 740px)":{
				width: "100%"
			}
		},
		writeReview: {
			width: "10%",
			display: "flex",
			alignItems: "center",
			justifyContent: "center",
			"@media (max-width: 600px), and (max-height: 740px)":{
				width: "100%"
			}
		},
		writeIcon: {
			fontSize: "1.8rem",
			fontWeight: "bold",
			color: xDeepBlue,
			cursor: "pointer"
		},
		building_name: {
			fontSize:"1.5rem",
			fontWeight:"bold",
			width: "100%",
			height:"60%",
			color: xDeepBlue,
		},
		userinfo: {
			width: "100%",
			height: "35%",
			color: xGreyText,
			fontSize: "1rem",
			fontWeight: "bold",
			margin: "10px 0px 0px 0px"
		},
		star: {
			fontSize: "2rem",
			color: xStarYellow,
			margin: "0px 2px 0px 2px",
			fontWeight: "bold"
		}
	}
}


const profileStyle = (src) => {
	return {
		profilePic: {
			height: "60px",
			width: "60px",
		    borderRadius:'50%',
		    backgroundImage: "url("+src+")",
		    backgroundPosition: 'center',
		    backgroundSize:'100% auto',
		    backgroundRepeat: 'no-repeat',
			display: "inline-block",
		}
	}
}
