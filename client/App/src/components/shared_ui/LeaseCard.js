import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import {Glyphicon} from 'react-bootstrap'
import {toggleMobileMap} from '../../actions/mobile_actions'
import {sortIconsLease, shortenAddress} from '../../api/iconSorter'
import {panToMap} from '../../actions/map_actions'
import {triggerLeasePopup, exitPopup, popupImage, popupImageExit, selectLeaseForReviews} from '../../actions/popup_actions'
import {goToWatchlistView, goToLandlordProfile} from '../../actions/tenant_actions'
import { xMidBlue, xGreyText, xWhiteSmoke, xBootstrapRed, xDeepBlue } from '../../stylesJS/base_colors'
import {saveWatchlistsToState, setWatchlist} from '../../actions/watchlist_actions'
import {getWatchlistsForUser, addPropertyToWatchlist, removePropertyFromWatchlist} from '../../api/watchlistsManager'
import {setLeaseCardIndex} from '../../actions/performance_actions'
import {trackActivity} from '../../actions/activity_actions'
import {handleClaimClick} from '../../api/landlordManager'


class LeaseCard extends Component {

	constructor(){
		super()
		this.state = {
			matchedHeight: null,
			heartHover: false,
			heartSaved: false,
			scrollHover: false,
			imageViewColumnTracked: false
		}
		this.exitPopupIfNoContact.bind(this)
	}

	// By calculating the node height
	componentDidMount(){
		let { clientHeight } = this.refs.infoCol
    	this.setState({
    		matchedHeight: clientHeight
    	})
	}

	renderThumbnails(lease){
		// for display on desktop
		if(!this.props.mobileViewer){
			if(this.props.leaseCardIndex-5 < this.props.index && this.props.index < this.props.leaseCardIndex+5	){
				return lease.images.map((img)=>{
					return (
						<img key={img.photoKey} ref={img.photoKey} onMouseEnter={()=>this.handleMouseEnterImageCol(img.urlThumbnail)} src={img.urlThumbnail} onClick={()=>this.handleMobileImageClick(img)} onMouseOver={()=>{this.props.popupImage(img.urlOriginal)}} onMouseOut={this.props.popupImageExit} width="100%;" height="100%" style={comStyles().thumbImg} />
					)
				})
			}else{
				// return (
				// 	<div style={comStyles().blankImagePlaceholder}></div>
				// )
			}
		}else{
		// for display on mobile
			return lease.images.map((img)=>{
				return (
					<img key={img.photoKey} ref={img.photoKey} onMouseEnter={()=>this.handleMouseEnterImageCol(img.urlThumbnail)} src={img.urlThumbnail} onClick={()=>this.handleMobileImageClick(img)} onMouseOver={()=>{this.props.popupImage(img.urlOriginal)}} onMouseOut={this.props.popupImageExit} width="100%;" height="100%" style={comStyles().thumbImg} />
				)
			})
		}
		// for display on landlord dashboard
		if(this.props.landlord){
			return lease.images.map((img)=>{
				return (
					<img key={img.photoKey} ref={img.photoKey} onMouseEnter={()=>this.handleMouseEnterImageCol(img.urlThumbnail)} src={img.urlThumbnail} onClick={()=>this.handleMobileImageClick(img)} onMouseOver={()=>{this.props.popupImage(img.urlOriginal)}} onMouseOut={this.props.popupImageExit} width="100%;" height="100%" style={comStyles().thumbImg} />
				)
			})
		}
	}

	renderRoomHovers(lease){
		return lease.rooms.map((room)=>{
			let displayPrice
			if(room.promo){
				displayPrice = room.promo_price
			}else{
				displayPrice = room.reg_price
			}
			return(
				<button key={room.room_type} ref={room.room_type} className='btn btn-primary' style={comStyles().roomHover}>
					<div className='col-md-8' style={comStyles().roomHoverName}>
						{room.room_type}
					</div>
					<div className='col-md-4' style={comStyles().roomHoverPrice}>
						$ {displayPrice}
					</div>
				</button>
			)
		})
	}

	exitPopupIfNoContact(){
		if(!this.props.contactToggle && !this.props.leaseForReview){
			this.props.exitPopup()
		}
	}

	addToFavorites(bool){
		if(this.props.selectedWatchlist){
			if(bool){
				this.setState({
					heartSaved: false
				})
				removePropertyFromWatchlist({
					watchlist_id: this.props.selectedWatchlist._id,
					postId: this.props.lease._id,
					type: "lease"
				}).then((data)=>{
					this.props.trackActivity("LEASE", "CARD_UNFAVORITED", this.props.lease, this.props.fbUser, this.props.geoCoords)
					this.props.setWatchlist(data)
				})
			}else{
				this.setState({
					heartSaved: true
				})
				addPropertyToWatchlist({
					watchlist_id: this.props.selectedWatchlist._id,
					postId: this.props.lease._id,
					type: "lease"
				}).then((data)=>{
					this.props.trackActivity("LEASE", "CARD_FAVORITED", this.props.lease, this.props.fbUser, this.props.geoCoords)
					this.props.setWatchlist(data)
				})
			}
		}else{
			// console.log("Setting heart")
			localStorage.setItem('hearted', JSON.stringify({
				postId: this.props.lease._id,
				type: "lease"
			}))
			// console.log(localStorage.getItem('hearted'))
			this.props.trackActivity("LEASE", "CARD_FAVORITED", this.props.lease, this.props.fbUser, this.props.geoCoords)
			this.props.goToWatchlistView()
		}
	}

	renderHeart(){
		let alreadySaved = false
		if(this.props.selectedWatchlist){
			this.props.selectedWatchlist.leases.forEach((lease)=>{
				if(lease == this.props.lease._id){
					alreadySaved = true
				}
			})
		}
		if(this.state.heartSaved){
			alreadySaved = true
		}
		if(alreadySaved || this.state.heartHover || this.state.heartSaved){
			return (
				<i className='ion-ios-heart' onClick={()=>this.addToFavorites(alreadySaved)} style={comStyles().heartIcon} onMouseOver={()=>this.setState({heartHover: true})} onMouseOut={()=>this.setState({heartHover: false})}></i>
			)
		}else{
			return (
				<i className='ion-ios-heart-outline' onClick={()=>this.addToFavorites(alreadySaved)} style={comStyles().heartIcon} onMouseOver={()=>this.setState({heartHover: true})} onMouseOut={()=>this.setState({heartHover: false})}></i>
			)
		}
	}

	handleMap(){
		this.props.panToMap(this.props.lease)
		this.props.trackActivity("LEASE", "CARD_MAP_BUTTON", this.props.lease, this.props.fbUser, this.props.geoCoords)
		if(this.props.mobileViewer){
			this.props.toggleMobileMap(true)
		}
	}

	handleDetails(){
		this.props.triggerLeasePopup(this.props.lease)
		this.props.trackActivity("LEASE", "DETAILS_HOVER", this.props.lease, this.props.fbUser, this.props.geoCoords)
		if(this.props.mobileViewer){
			this.props.toggleMobileMap(true)
		}
	}

	handleInquire(){
		if(this.props.leaseForPopup){
			this.props.exitPopup()
		}else{
			this.props.triggerLeasePopup(this.props.lease, true)
			this.props.trackActivity("LEASE", "INQUIRE_CLICK", this.props.lease, this.props.fbUser, this.props.geoCoords)
			if(this.props.mobileViewer){
				this.props.toggleMobileMap(true)
			}
		}
	}

	handleMobileImageClick(img){
		this.props.popupImage(img.url)
		if(!this.state.imageViewColumnTracked){
			this.props.trackActivity("LEASE", "IMAGE_VIEW", this.props.lease, this.props.fbUser, this.props.geoCoords)
			this.setState({imageViewColumnTracked: true})
		}
		if(this.props.mobileViewer){
			this.props.toggleMobileMap(true)
		}
	}

	handleReviews(){
		if(this.props.leaseForReview){
			this.props.exitPopup()
		}else{
			this.props.exitPopup()
			this.props.selectLeaseForReviews(this.props.lease)
			this.props.trackActivity("LEASE", "LEASE_REVIEW_VIEWED", this.props.lease, this.props.fbUser, this.props.geoCoords)
			if(this.props.mobileViewer){
				this.props.toggleMobileMap(true)
			}
		}
	}

	leaseCardHover(event){
			this.props.setLeaseCardIndex(this.props.index)
			this.props.trackActivity("LEASE", "CARD_ON_HOVER", this.props.lease, this.props.fbUser, this.props.geoCoords)
	}

	handleMouseEnterImageCol(url){
		this.props.trackActivity("LEASE", "IMAGE_VIEW", this.props.lease, this.props.fbUser, this.props.geoCoords, {url: url})
	}


	handleLandlordProfileLink(){
		this.props.goToLandlordProfile(this.props.lease)
		this.props.trackActivity("LANDLORD", "LINK_TO_PROFILE", {id: this.props.lease.landlord_id}, this.props.fbUser, this.props.geoCoords)
	}

	handleLandlordClaimAttempt(){
		this.props.trackActivity("LANDLORD", "LANDLORD_CLAIM_EMAIL", {id: this.props.lease.landlord_id}, this.props.fbUser, this.props.geoCoords)
	}

	generateClaimPropertyButton(){
		if(this.props.propertyClaim && !this.props.lease.claimed){
			return (
				<button onClick={()=>handleClaimClick(this.props.lease)} className='btn btn-block' style={comStyles().claimThisProperty}>
					CLAIM THIS PROPERTY
				</button>
			)
		}
	}

	render() {
		return (
			<div key={this.props.lease._id} style={comStyles().card} onMouseEnter={this.leaseCardHover.bind(this)} >
				<div style={comStyles().cardHeader}>
					<div style={comStyles().thumbnail}>
						{/* Company Logo */}
						<div className='img-circle' onClick={this.handleLandlordProfileLink.bind(this)} style={profileStyle(this.props.lease.thumbnail).profilePic}></div>
					</div>
					<div style={comStyles().infobar}>
						{/* Address */}
						<div style={comStyles().building_name}>
							{this.props.lease.building_name || shortenAddress(this.props.lease.address)}
						</div>
						{/* Company Name */}
						<div style={comStyles().userinfo}>
							From &nbsp;
							<span onClick={this.handleLandlordProfileLink.bind(this)} style={comStyles().landlordLink}>{this.props.lease.company || this.props.contacts[0].name}</span>
						</div>
					</div>
					{this.renderHeart()}
				</div>
				<div style={comStyles().actionbar}>
					<button className='btn btn-primary col-md-8' onClick={this.handleInquire.bind(this)} style={comStyles().inquire}>
						<i className='ion-ios-telephone'></i> &nbsp;
						<i className='ion-email'></i> &nbsp;
						INQUIRE
					</button>
					<button className='btn btn-danger col-md-4' onClick={this.handleMap.bind(this)} style={comStyles().inquire}>
						<i className='ion-ios-location' style='font-size:1em;'></i> &nbsp;
						MAP
					</button>
				</div>
				<div style={comStyles().columnBox}>
						<div ref='infoCol' className='col-md-8' style={comStyles().infoCol}>
							<div style={comStyles().buttonRow}>
								<button className='btn btn-primary' style={comStyles().detailsHover} onClick={this.handleDetails.bind(this)} onMouseOver={()=>{this.props.triggerLeasePopup(this.props.lease)}} onMouseOut={this.exitPopupIfNoContact.bind(this)}>
									<Glyphicon glyph="glyphicon glyphicon-home" /> &nbsp;
									DETAILS
								</button>
								<button className='btn btn-primary' style={comStyles().detailsHover} onClick={this.handleReviews.bind(this)}>
									<Glyphicon glyph="glyphicon glyphicon-thumbs-up" /> &nbsp;
									REVIEWS
								</button>
							</div>
							{this.renderRoomHovers(this.props.lease)}
						</div>
						<div id="imageCol" className='col-md-4' style={matchedHeights(this.state.matchedHeight).imageCol}>
							{this.renderThumbnails(this.props.lease)}
						</div>
				</div>
				{this.generateClaimPropertyButton()}
			</div>
		);
	}
}

LeaseCard.propTypes = {
	lease: React.PropTypes.object.isRequired,		// passed in
	contactToggle: React.PropTypes.bool,
	selectedWatchlist: React.PropTypes.object,
	fbUser: React.PropTypes.object.isRequired,
	geoCoords: React.PropTypes.array,
	filteredResults: React.PropTypes.array,
	selectedPins: React.PropTypes.array,
	watchlistView: React.PropTypes.bool,
	leaseForReview: React.PropTypes.object,
	mobileViewer: React.PropTypes.bool,
	index: React.PropTypes.number,						// passed in
	leaseCardIndex: React.PropTypes.number,
	landlord: React.PropTypes.object,
	leaseForPopup: React.PropTypes.object,
	propertyClaim: React.PropTypes.bool
};

const RadiumHOC = Radium(LeaseCard);

function mapStateToProps(state){
	return {
		contactToggle: state.popup.contact,
		selectedWatchlist: state.tenant.selectedWatchlist,
		fbUser: state.tenant.fbUser,
		geoCoords: state.app.geoCoords,
		filteredResults: state.content.filteredResults,
		selectedPins: state.content.selectedPins,
		watchlistView: state.tenant.watchlistView,
		leaseForReview: state.popup.leaseForReview,
		mobileViewer: state.app.mobileViewer,
		leaseCardIndex: state.perform.leaseCardIndex,
		landlord: state.landlord.landlord,
		leaseForPopup: state.popup.leaseForPopup,
		propertyClaim: state.app.propertyClaim
	}
}

export default connect(mapStateToProps, {panToMap, triggerLeasePopup, exitPopup, popupImage, popupImageExit, goToWatchlistView, saveWatchlistsToState, setWatchlist, selectLeaseForReviews, toggleMobileMap, goToLandlordProfile, setLeaseCardIndex, trackActivity})(RadiumHOC);


// ==============================

const comStyles = () => {
	return {
		card: {
			margin: "20px",
			padding: "3%",
			borderRadius: "5px",
			backgroundColor: "white",
			minWidth: "250px"
		},
		cardHeader: {
			padding: "3%",
			backgroundColor: xWhiteSmoke,
			display: "flex",
			justifyContent: "center",
			width: "100%",
			alignItems: "flex-start",
			flexDirection: "row"
		},
		infobar: {
			flexGrow: 7,
			padding: '5px 5px 5px 10px'
		},
		thumbnail: {
			flexGrow: 2,
			cursor: "pointer"
		},
		landlordLink: {
			cursor: "pointer",
			color: xDeepBlue
		},
		heartSpace: {
			flexGrow: 1
		},
		building_name: {
			fontSize:"1.5rem",
			fontWeight:"bold",
			width: "100%",
			height:"60%",
			color: xGreyText,
		},
		userinfo: {
			width: "100%",
			height: "35%",
			color: xGreyText,
			fontSize: "1rem",
			fontWeight: "bold",
			margin: "10px 0px 0px 0px"
		},
		actionbar: {
			display: "flex",
			justifyContent: "center",
			width: "100%",
			margin: "auto",
			margin: "10px 0px 10px 0px"
		},
		inquire: {
			borderRadius: "0px",
			fontSize: "1.2rem",
			fontWeight: "bold",
		},
		map: {
			borderRadius: "0px",
			fontSize: "1.2rem",
			fontWeight: "bold",
		},
		columnBox: {
			display: "flex",
			justifyContent: "center",
			width: "100%",
			alignItems: "flex-start",
			flexDirection: "row",
			backgroundColor: xMidBlue,
			padding: "0px",
		},
		infoCol: {
			height: "400px",
			padding: "0px",
			display: "flex",
			justifyContent: "center",
			flexDirection: "column",
			"@media (max-width: 600px), and (max-height: 740px)": {
				width: "75%"
			}
		},
		buttonRow: {
			display: "flex",
			flexDirection: "row"
		},
		detailsHover: {
			width: "50%",
			fontSize:"1rem",
			fontWeight:"bold",
			padding: "10px",
			color:"white",
			borderRadius: "0px",
			border: "0px solid white",
			"@media (max-width: 600px), and (max-height: 740px)": {
				fontSize: "0.8rem"
			}
		},
		thumbImg: {
			maxWidth: "100%",
			height: "auto"
		},
		roomHover: {
			margin: "5px",
			padding: "5px",
			flexGrow: 1,
			cursor: "default"
		},
		roomHoverName: {
			textAlign: "center",
			fontSize: "1.3rem",
			fontWeight: "bold",
			flexWrap: "wrap",
			display: "flex"
		},
		roomHoverPrice: {
			textAlign: "center",
			fontSize: "1.5rem",
			fontWeight: "bold"
		},
		heartIcon: {
			fontSize: "2rem",
			fontWeight: "bold",
			right: "0px",
			width: "10%",
			color: xBootstrapRed,
			cursor: "pointer"
		},
		blankImagePlaceholder: {
			width: "100%",
			height: "100%",
			display: "flex",
			justifyContent: "center",
			alignItems: "center"
		},
		claimThisProperty: {
			fontWeight: "bold",
			size: "2rem",
			color: "black",
			height: "50px"
		}
	}
}


const profileStyle = (src) => {
	return {
		profilePic: {
			height: "60px",
			width: "60px",
		    borderRadius:'50%',
		    backgroundImage: "url("+src+")",
		    backgroundPosition: 'center',
		    backgroundSize:'100% auto',
		    backgroundRepeat: 'no-repeat',
			display: "inline-block",
		}
	}
}

const matchedHeights = (match) => {
	return {
		imageCol: {
			display: "flex",
			justifyContent: "flex-start",
			flexDirection: "column",
			padding: "0px",
			overflowY: "scroll",
			maxHeight: match+"px",
			"@media (max-width: 600px), and (max-height: 740px)": {
				width: "25%"
			}
		}
	}
}
