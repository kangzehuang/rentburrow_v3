import { SAVE_SUBLET_SEARCH_PARAMS, SAVE_LEASE_SEARCH_PARAMS } from '../actions/action_types'

const INITIAL_STATE = {
	subletSearchParams: {
			price: {
				min: 400,
				max: 700
			},
			rooms: 1,
			utils: false,
			ensuite: false,
			femaleOnly: false,
			semester: null
		},
	leaseSearchParams: {
			price: {
				min: 400,
				max: 700
			},
			leaseTerms: 12,
			rooms: 1,
			water: false,
			heat: false,
			electric: false,
			ac: false,
			ensuite: false,
			parking: false,
			furnished: false,
			internet: false
		}
}

export default function(state = INITIAL_STATE, action){
	switch(action.type){
		case SAVE_SUBLET_SEARCH_PARAMS:
			return {
				...state,
				subletSearchParams: action.payload
			}
		case SAVE_LEASE_SEARCH_PARAMS:
			return {
				...state,
				leaseSearchParams: action.payload
			}
	}
	return state;
}
