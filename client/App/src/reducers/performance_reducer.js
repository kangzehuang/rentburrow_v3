import { SET_SUBLET_CARD_INDEX, SET_LEASE_CARD_INDEX } from '../actions/action_types'

const INITIAL_STATE = {
	subletCardIndex: 0,
	leaseCardIndex: 0
}

export default function(state = INITIAL_STATE, action){
	switch(action.type){
		case SET_SUBLET_CARD_INDEX:
			return {
				...state,
				subletCardIndex: action.payload
			}
		case SET_LEASE_CARD_INDEX:
			return {
				...state,
				leaseCardIndex: action.payload
			}
	}
	return state;
}
