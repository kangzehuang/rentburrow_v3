import { TRACK_ACTIVITY, CLEAR_ACTIVITIES_LIST } from './action_types'
import { createActivityItem } from '../api/aws/aws-dynamodb'

export function trackActivity(type, action, subject, fbUser, geoCoords, extraDetails){
	const x = createActivityItem(type, action, subject, fbUser, geoCoords, extraDetails)
	// console.log(x)
	return function(dispatch){
		dispatch({
			type: TRACK_ACTIVITY,
			payload: x
		})
	}
}

export function clearTrackedActivities(){
  return function(dispatch){
		dispatch({
			type: CLEAR_ACTIVITIES_LIST
		})
	}
}
