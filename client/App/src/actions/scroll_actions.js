import { SET_SUBLET_SCROLL, SET_LEASE_SCROLL, SET_WATCHLIST_SCROLL } from './action_types'

export function setSubletScroll(position){
	return function(dispatch){
		dispatch({
			type: SET_SUBLET_SCROLL,
			payload: position
		})
	}
}

export function setLeaseScroll(position){
	return function(dispatch){
		dispatch({
			type: SET_LEASE_SCROLL,
			payload: position
		})
	}
}

export function setWatchlistScroll(position){
	return function(dispatch){
		dispatch({
			type: SET_WATCHLIST_SCROLL,
			payload: position
		})
	}
}