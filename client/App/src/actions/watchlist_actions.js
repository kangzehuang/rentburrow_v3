import { SAVE_WATCHLISTS, SET_WATCHLIST, UNSET_WATCHLIST } from './action_types';

export function saveWatchlistsToState(watchlists){
	return function(dispatch){
		dispatch({
			type: SAVE_WATCHLISTS,
			payload: watchlists
		})
	}
}

export function setWatchlist(watchlist){
	return function(dispatch){
		dispatch({
			type: SET_WATCHLIST,
			payload: watchlist
		})
	}
}

export function unsetWatchlist(){
	return function(dispatch){
		dispatch({
			type: UNSET_WATCHLIST
		})
	}
}