import { SET_SUBLET_CARD_INDEX, SET_LEASE_CARD_INDEX } from './action_types'

export function setSubletCardIndex(index){
	return function(dispatch){
		dispatch({
			type: SET_SUBLET_CARD_INDEX,
			payload: index
		})
	}
}

export function setLeaseCardIndex(index){
	return function(dispatch){
		dispatch({
			type: SET_LEASE_CARD_INDEX,
			payload: index
		})
	}
}
