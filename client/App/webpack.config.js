const webpack = require('webpack')

const UglifyJS = new webpack.optimize.UglifyJsPlugin({
  compress: {
    warnings: false
  }
})

const Environment = new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development')
})

module.exports = {
  entry: [
    './src/index.js'
  ],
  output: {
    path: __dirname,
    filename: 'bundle.js'
  },
  // devtool: "source-map",
  module: {
    loaders: [
      {
        exclude: /node_modules/,
        loader: 'babel',
        query: {
          presets: ['react', 'es2015', 'stage-1']
        }
      },
      {
        test: /\.json$/,
        loader: 'json'
      },
      {
        test: /\.css$/,
        loader: "style-loader!css-loader"
      }
    ]
  },
  plugins: [
    Environment,
    UglifyJS
  ],
  devServer: {
    historyApiFallback: true,
    contentBase: './',
    host: '0.0.0.0'
  }
};
