
const Authentication = require('./routes/auth_routes');
const Tenant = require('./routes/tenant_routes');
const Sublets = require('./routes/sublet_routes');
const Leases = require('./routes/lease_routes');
const City = require('./routes/city_routes');
const Group = require('./routes/group_routes');
const Watchlist = require('./routes/watchlist_routes');
const Review = require('./routes/review_routes');
const Landlord = require('./routes/landlord_routes')

// use `requireAuth` or `requireSignin` to wrap routes with auth conditions
module.exports = function(app){

	// Auth related routes
	app.post('/longlivetoken', Authentication.longlivetoken);

	// App viewing related routes
	app.post('/checkIfLandlordExists', Authentication.checkIfLandlordExists)

	// Tenant related routes
	app.get('/test', Tenant.test)
	app.post('/save_visit', Tenant.save_visit);

	// Landlord related routes
	app.post('/landlord_profile', Landlord.landlord_profile)

	// Sublet related routes
	app.post('/get_sublets', Sublets.get_sublets)
	app.post('/specific_sublet', Sublets.specific_sublet)

	// Lease related routes
	app.post('/get_leases', Leases.get_leases)
	app.post('/specific_lease', Leases.specific_lease)

	// City related routes
	app.get('/get_cities', City.get_cities)

	// Group related routes
	app.post('/city_groups', Group.city_groups)
	app.get('/rentburrow_group', Group.rentburrow_group)

	// Watchlist related routes
	app.post('/my_watchlists', Watchlist.my_watchlists)
	app.post('/watchlist', Watchlist.watchlist)
	app.post('/new_watchlist', Watchlist.new_watchlist)
	app.post('/add_watchlist_user', Watchlist.add_watchlist_user)
	app.post('/remove_watchlist_user', Watchlist.remove_watchlist_user)
	app.post('/add_watchlist_prop', Watchlist.add_watchlist_prop)
	app.post('/remove_watchlist_prop', Watchlist.remove_watchlist_prop)
	app.post('/populate_watchlist', Watchlist.populate_watchlist)

	// Review related routes
	app.post('/submit_property_review', Review.submit_property_review)
	app.post('/submit_landlord_review', Review.submit_landlord_review)
	app.post('/get_property_reviews', Review.get_property_reviews)
	app.post('/get_landlord_reviews', Review.get_landlord_reviews)


}
