const mongoose = require('mongoose');
const async = require('async');

const Lease = require('../models/lease_model');

// ======================================================================================================

exports.get_leases = function(req, res, next){
	const city_name = req.body.city_name.toLowerCase()
	console.log("...................LEASE........................")
	console.log(typeof req.body)
	console.log(req.body)
	console.log(city_name)
	// find leases with matching cities
	Lease.find({$and: [{city_name: city_name}, {active: true}]}, function(err, leases){
		if(err){console.log(err)};
		//console.log(leases);
		// return the matching Leases
		if(leases.length > 0){
			res.json(leases);
		}else{
			res.json([]);
		}
	})
}

// ====================================================================================================== //

exports.specific_lease = function(req, res, next){
	const leaseId = req.body.leaseId;
	console.log("========================= SPECIFIC LEASE ==============================");
	console.log("We found a lease and his name is " + leaseId);
	// find leases with matching cities
	Lease.findById(leaseId, function(err, lease){
		if(err){console.log(err)};
		// return the matching Leases
		if(lease){
			res.json(lease);
		}else{
			res.json(null);
		}
	});
}


