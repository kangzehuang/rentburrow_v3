const Landlord = require('../models/landlord_model');
const axios = require('axios')


// GET /
exports.authtest = function(req, res, next){
	res.send({message:'Super secret code is ABC123'});
}

exports.longlivetoken = function(req, res, next){
	const fbAccessToken = req.body.accessToken
	console.log(fbAccessToken)
	axios.get(`https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=${"1492022030811505"}&client_secret=${"ab7ff18d40625c64f6a12516f5be8de2"}&fb_exchange_token=${fbAccessToken}` )
		.then((data)=>{
			console.log("================================ FB TOKEN ALL ================================")
			console.log(data)
			console.log("================================ FB TOKEN START ================================")
			let longLiveToken = data.data.slice(13) 	// remove the 'access_token=' from beggining
			let positionEnd = longLiveToken.indexOf('&expires=')
			longLiveToken = longLiveToken.slice(0, positionEnd)
			console.log(longLiveToken)
			console.log("================================ FB TOKEN END ================================")
			res.json({
				message: "Success getting the long lived fb token!",
				longLiveToken: longLiveToken
			})
		})
		.catch((err)=>{
			console.log(err)
			res.json({
				message: "Failure getting the long lived fb token!"
			})
		})

}

// POST/ checkIfLandlordExists
exports.checkIfLandlordExists = function(req, res, next){
	const landlordId = req.body.landlordId
	res.json({exists: true})
}
