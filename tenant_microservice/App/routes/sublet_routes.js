const mongoose = require('mongoose');
const async = require('async');

const Sublet = require('../models/sublet_model');

const subletParser = require('../api/subletParser')
const fbExtractor = require('../api/fbExtractor')

// ======================================================================================================

// return last 200 active posts
exports.get_sublets = function(req, res, next){
	const city_name = req.body.city_name
	console.log("...................SUBLET........................")
	console.log(typeof req.body)
	console.log(req.body)
	// use our modules to process logic
	// return to client the last 150 posts in db
	Sublet.find({$and:[{city: city_name}, {active: true}]}, null, {sort: {updated_time: 1}}, function(err, posts){
		if(err){return next(err)}
		if(posts.length > 250){
			const lastFew = posts.slice(-250)
			res.json(lastFew)
		}else{
			res.json(posts)
		}
	})
}

exports.specific_sublet = function(req, res, next){
	const subletId = req.body.subletId;
	console.log("========================= SPECIFIC LEASE ==============================");
	console.log("We found a sublet and his name is " + subletId);
	// find leases with matching cities
	Sublet.findById(subletId, function(err, sublet){
		if(err){console.log(err)};
		// return the matching Leases
		if(sublet){
			res.json(sublet);
		}else{
			res.json(null);
		}
	});
}
