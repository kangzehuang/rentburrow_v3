const PropertyReview = require('../models/property_review_model');
const LandlordReview = require('../models/landlord_review_model');

exports.submit_property_review = function(req, res, next){
	console.log("============== SUBMIT PROPERTY REVIEW ===============")
	console.log(req.body)
	const new_review = {
		address: req.body.address,
		property_id: req.body.property_id,
		landlord_name: req.body.landlord,
		landlord_id: req.body.landlord_id,
		landlord_logo: req.body.landlord_logo,
		landlord_url: req.body.landlord_url,
		text: req.body.text,
		scores: {
			priceValue: req.body.scores.priceValue,				// how fast do they respond
			quality: req.body.scores.quality,				// do they fix the problem
			location: req.body.scores.location				// overall satisfaction
		},
		reviewer_name: req.body.reviewer_name,
		reviewer_img: req.body.reviewer_img,
		reviewer_id: req.body.reviewer_id,
	}
	const review = new PropertyReview(new_review);

	review.save(function(err, review){
		if(err){
			console.log(err)
			res.status(500).send("Failed to save review")
		}
		console.log("saving new review...");
		res.json(review);
	});
};

exports.get_property_reviews = function(req, res, next){
	const property_id = req.body.propertyId
	PropertyReview.find({property_id: property_id}, null, {sort: {created_at: -1}}, function(err, reviews){
		if(err){
			console.log(err)
			res.status(500).send("Error getting reviews")
		};

		if(reviews){
			console.log("========= GETTING REVIEWS ==========")
			console.log(property_id)
			console.log(reviews)
			res.json(reviews);
		}else{
			res.json([]);
		}
	});
};


exports.submit_landlord_review = function(req, res, next){
	console.log("============== SUBMIT LANDLORD REVIEW ===============")
	console.log(req.body)
	const new_review = {
		address: req.body.address,
		property_id: req.body.property_id,
		landlord_name: req.body.landlord,
		landlord_id: req.body.landlord_id,
		landlord_logo: req.body.landlord_logo,
		landlord_url: req.body.landlord_url,
		text: req.body.text,
		scores: {
			professionalism: req.body.scores.professionalism,				// how professional is the landlord
			effectiveness: req.body.scores.effectiveness,				// how effective is the landlord at solving problems
			satisfaction: req.body.scores.satisfaction				// how satisfied are you overall
		},
		reviewer_name: req.body.reviewer_name,
		reviewer_img: req.body.reviewer_img,
		reviewer_id: req.body.reviewer_id,
	}
	const review = new LandlordReview(new_review);

	review.save(function(err, review){
		if(err){
			console.log(err)
			res.status(500).send("Failed to save review")
		}
		console.log("saving new review...");
		res.json(review);
	});
};

exports.get_landlord_reviews = function(req, res, next){
	const landlord_id = req.body.landlordId
	LandlordReview.find({landlord_id: landlord_id}, null, {sort: {created_at: -1}}, function(err, reviews){
		if(err){
			console.log(err)
			res.status(500).send("Error getting reviews")
		};

		if(reviews){
			res.json(reviews);
		}else{
			res.json([]);
		}
	});
};
