const Lease = require('../models/lease_model');
const LandlordReview = require('../models/landlord_review_model');

module.exports.buildLandlordProfile = function(landlord_id){
	const p = new Promise((res, rej)=>{
		const profile = {
			landlord_id: landlord_id
		}
		extractLeases(profile)
			.then((profile)=>{
				return extractLandlordReviews(profile)
			})
			.then((profile)=>{
				res(profile)
			})
			.catch((err)=>{
				rej(err)
			})
	})
	return p
}

function extractLeases(profile){
	const p = new Promise((res, rej)=>{
		Lease.find({$and:[{landlord_id: profile.landlord_id}, {active: true}]}, function(err, leases){
			if(err){
				rej(err)
			}
			console.log(leases)
			const newProfile = {
				landlord_id: profile.landlord_id,
				leases: leases
			}
			res(newProfile)
		})
	})
	return p
}

function extractLandlordReviews(profile){
	const p = new Promise((res, rej)=>{
		LandlordReview.find({landlord_id: profile.landlord_id}, null, {sort: {created_at: -1}}, function(err, reviews){
			if(err){
				rej(err)
			}
			const newProfile = {
				landlord_id: profile.landlord_id,
				leases: profile.leases,
				reviews: reviews
			}
			res(newProfile)
		})
	})
	return p
}