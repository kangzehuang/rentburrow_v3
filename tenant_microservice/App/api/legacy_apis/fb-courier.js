// fb-courier.js

// declare dependencies
var mongoose = require('mongoose');
var async = require('async');

var FB = require('fb');
var Tenant = require('../../models/tenant_model');

// FB.options({version:'v2.7'});


var exports = module.exports = {};
     
// first we log the user to our database   
exports.logTenantInDB = function(profile) {
  	console.log('about to log user in db');
  	console.log(profile);
	var newTenant = {
		id: profile.id,
		name: profile.name,
		coords: profile.user_coords,
		city: profile.city
	};
	var user = new Tenant(newTenant);

	user.save(function(err, saved_user){
		if(err){console.log(err)}
		console.log('======> saved user:');
		console.log(saved_user);
	});
};

exports.setFbToken = function(user_profile){
	// console.log(user_profile);
	FB.setAccessToken(user_profile.accessToken);
	console.log("Set the accessToken!");
}
