// Import dependencies on Mongoose
var mongoose = require('mongoose');
var Lease = mongoose.Schema;
const { range } = require('lodash')

// Create the Rental post Schema
var LeaseSchema = new mongoose.Schema({
	landlord_id: String,
	building_name: String,
	address: String,
	city_name: String,
	company: String,
	type: String,
	rooms: [{
		id: String,
		room_type: String,
		reg_price: Number,
		promo_price: Number,
		rooms_per: Number,
		lease_terms: Number,
		promo_terms: Number,
		bathrooms: Number,
		note: String,
		promo: Boolean
	}],
	utils_list: {
		id: String,
		water: Boolean,
		heat: Boolean,
		electric: Boolean,
		furnished: Boolean,
		parking: Boolean,
		free_parking: Boolean,
		internet: Boolean,
		ac: Boolean,
		laundry: Boolean
	},
	url: String,
	thumbnail: String,
	images: [{
		photoKey: String,
		urlThumbnail: String,
		urlOriginal: String
	}],
	place_id: String,
	coords: {
		type: [Number],
		index: '2d'
	},
	contacts: [{
		name: String,
		email: String,
		phone: String
	}],
	note: String,
	active: Boolean,
	deleted: String,
	claimed: Boolean,
	created_at: Date
});

LeaseSchema.pre('save', function(next){
	var currentDate = new Date();

	this.created_at = currentDate;
	next();
});

// Indexes this schema in 2dsphere format (critical for running proximity searches)
LeaseSchema.index({coords: '2dsphere'});

var Lease = mongoose.model('Lease', LeaseSchema);

module.exports = Lease;

// ============ MODEL DUMP FOR STRESS TESTING =======================
//
//
// const saveRecord = (record) => {
// 	return new Promise((resolve, reject) => {
// 		record.save((err) => {
// 			if (err) return reject(err)
// 			resolve()
// 		})
// 	})
// }
//
// const createModelClass = (({ id }) => {
// 	let setCoords = [
// 		-80.526854 - id/10000,
// 		43.479983 + id/10000
// 	]
// 	return new Lease({
// 		building_name: "330 Spruce St",
// 		landlord_id: "jdsfjos"+id,
// 		address: "330 Spruce St, Waterloo ON",
// 		city_name: "waterloo",
// 		company: "Domus Student Housing",
// 		type: "apartment",
// 		rooms: [{
// 			room_type: "Floor 1",
// 			reg_price: 600,
// 			promo_price: 560,
// 			rooms_per: 5,
// 			lease_terms: 12,
// 			promo_terms: 12,
// 			bathrooms: 5,
// 			note: "Well rounded student housing",
// 			promo: true
// 		}],
// 		utils_list: {
// 			water: true,
// 			heat: true,
// 			electric: true,
// 			furnished: true,
// 			parking: false,
// 			free_parking: false,
// 			internet: true,
// 			ac: true,
// 			laundry: true
// 		},
// 		url: "https://www.domushousing.com/",
// 		thumbnail: "https://www.domushousing.com/images/domus-logo.png",
// 		images: [
// 			{
// 				photoKey: "sdifhkansdfsdf"+id,
// 				url: "https://www.domushousing.com/primgs/4969"
// 			},
// 			{
// 				photoKey: "61267"+id,
// 				url: "https://www.domushousing.com/primgs/3727"
// 			},
// 			{
// 				photoKey: "sdifhkansdf42sdf"+id,
// 				url: "https://www.domushousing.com/primgs/3725"
// 			},
// 			{
// 				photoKey: "sdifh456kansd7545fsdf"+id,
// 				url: "https://www.domushousing.com/primgs/3724"
// 			},
// 			{
// 				photoKey: "sdifhka46nsdfsdf"+id,
// 				url: "https://www.domushousing.com/primgs/3749"
// 			},
// 			{
// 				photoKey: "sdifhkan387sdfsdf"+id,
// 				url: "https://www.domushousing.com/primgs/3729"
// 			},
// 			{
// 				photoKey: "sdifhkan4+5sdfsdf"+id,
// 				url: "https://www.domushousing.com/primgs/3728"
// 			},
// 			{
// 				photoKey: "sdifhk4giuansdfsdf"+id,
// 				url: "https://www.domushousing.com/primgs/3965"
// 			}
// 		],
// 		coords: setCoords,
// 		contacts: [{
// 			name: "Ann Wylder",
// 			email: "Ann.Wylder@domus.com",
// 			phone: "519-345-2345"
// 		}],
// 		note: "Near UW and WLU. Close to food and bus stops.",
// 		active: true,
// 		deleted: false,
// 		created_at: new Date()
// 	})
// })
//
// const runDump = (num) => {
// 	const records = range(200).map((id) => createModelClass({ id: id + 10 * num }))
//
// 	const saveRecordPromises = records
// 		.map((record, id) => {
// 			return saveRecord(record)
// 				.then(() => { console.log("Successfully added lease id: " + id + 10 * num) })
// 		})
//
// 	const saveRecordPromisesInSerial = saveRecordPromises
// 		.reduce((endPromise, currentPromise) => endPromise.then(() => currentPromise), Promise.resolve())
//
// 	return saveRecordPromisesInSerial.then(() => {
// 		console.log("Successfully added all leases")
// 	})
// }
//
// const runDumpSerial = range(1)
// 	.map((num) => runDump(num))
// 	.reduce((endPromise, currentPromise) => endPromise.then(() => currentPromise), Promise.resolve())
//
// runDumpSerial.then(() => {
// 	console.log("Successfully ran dump")
// })
