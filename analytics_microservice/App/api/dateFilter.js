
exports.unixDateSince = function(numDays){
  const today = new Date()
  const todayUnix = today.getTime() / 1000
  const sinceUnix = todayUnix - (numDays*24*60*60)
  return sinceUnix
}



exports.determineTableBasedOnDate = function(){
  const today = new Date()
  const m = today.getMonth()
  const y = today.getFullYear()

  let table_date = "_"+getAbbvMonth(m)+"_"+y
  return table_date
}

function getAbbvMonth(m){
  return "Winter"
  // switch(m){
  //   case 0:
  //     return 'Jan'
  //   case 1:
  //     return 'Feb'
  //   case 2:
  //     return 'Mar'
  //   case 3:
  //     return 'Apr'
  //   case 4:
  //     return 'May'
  //   case 5:
  //     return 'Jun'
  //   case 6:
  //     return 'Jul'
  //   case 7:
  //     return 'Aug'
  //   case 8:
  //     return 'Sep'
  //   case 9:
  //     return 'Oct'
  //   case 10:
  //     return 'Nov'
  //   case 11:
  //     return 'Dec'
  //   default:
  //     return 'MissingMonth'
  // }
}
