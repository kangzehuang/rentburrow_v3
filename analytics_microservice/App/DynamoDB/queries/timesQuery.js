
const AWS = require('aws-sdk')
const aws_config = require('../aws_config.json')
// const dynaDoc = require("dynamodb-doc");
AWS.config.update(aws_config)

const dynamodb = new AWS.DynamoDB({
  dynamodb: '2012-08-10',
  region: "us-east-1"
})
// const docClient = new dynaDoc.DynamoDB(dynamodb)

const Sublet = require('../../models/sublet_model');
const unixDateSince = require('../../api/dateFilter').unixDateSince
const determineTableBasedOnDate = require('../../api/dateFilter').determineTableBasedOnDate

// ========================================================

exports.getStatsForPropertyOverTime = function(PLACE_ID){
  const p = new Promise((res, rej)=>{
    const timeSince = unixDateSince(365)
    getSubletsFromDb(PLACE_ID, timeSince)
      .then((sublets)=>{
        console.log("======== FILTERED SUBLETS ========")
        console.log(sublets)
        res(sublets)
      })
  })
  return p
}

function getSubletsFromDb(PLACE_ID, timeSince){
  const p = new Promise((res, rej)=>{
    Sublet.find({$and:[{place_id: PLACE_ID}, {created_time_unix: {$gte: timeSince}}]}, null, {sort: {created_time_unix: -1}}, function(err, sublets){
  		if(err){return next(err)}
  		res(sublets)
  	})
  })
  return p
}

function removeDuplicates(sublets){
  const p = new Promise((res, rej)=>{
    const uniquePriceLevelsOverTime = []
    sublets.forEach((s)=>{
      // if(s && !isDuplicatePostAtSamePointInTime(uniquePriceLevelsOverTime, s)){
      if(s){
        console.log("========== ADDING A NEW POST ==============")
        uniquePriceLevelsOverTime.push(s)
      }
    })
    res(uniquePriceLevelsOverTime)
  })
  return p
}

function isDuplicatePostAtSamePointInTime(uniquePriceLevelsOverTime, subletPost){
  let duplicate = false
  console.log(" >>>>>>>>>>>>>>>>>>>>>>  +++  <<<<<<<<<<<<<<<<<<<< ")
  uniquePriceLevelsOverTime.forEach((u, i)=>{
    if(u.username == subletPost.username && u.place_id == subletPost.place_id){

      // FOR PRICE CHANGES ONLY
      // if(u.price == subletPost.price){
      //   console.log("===== FOUND A DUPLICATE POST ======  " + i)
      //     console.log(u.price)
      //     console.log(subletPost.price)
      //     console.log("=====================")
      //     duplicate = true
      // }


      // FOR TIME SERIES CHECKING AN INTERVAL
      // console.log(">>> Same User and Price")
      // const dateBeforeTimeX = subletPost.created_time_unix + unixDateSince(1)
      // const dateAfterTimeX = subletPost.created_time_unix - unixDateSince(1)
      // if(u.created_time_unix <= dateBeforeTimeX || u.created_time_unix >= dateAfterTimeX){
      //   console.log("===== FOUND A DUPLICATE POST ======  " + i)
      //   console.log(u.created_time)
      //   console.log(subletPost.created_time)
      //   console.log("=====================")
      //   duplicate = true
      // }else{
      //   console.log(">>> Not within date range")
      //   console.log("===== NOT A DUPLICATE POST ======  " + i)
      //   console.log(u.created_time)
      //   console.log(subletPost.created_time)
      //   console.log("=====================")
      // }
    }else{
      console.log(">>> Not same User and Price")
      console.log("===== NOT A DUPLICATE POST ======  " + i)
      console.log("=====================")
    }
  })
  console.log(" >>>>>>>>>>>>>>>>>>>>>>  +++  <<<<<<<<<<<<<<<<<<<< ")
  return duplicate
}
