
const AWS = require('aws-sdk')
const aws_config = require('../aws_config.json')
const dynaDoc = require("dynamodb-doc");
AWS.config.update(aws_config)

const dynamodb = new AWS.DynamoDB({
  dynamodb: '2012-08-10',
  region: "us-east-1"
})
const docClient = new dynaDoc.DynamoDB(dynamodb)

const Sublet = require('../../models/sublet_model');
const unixDateSince = require('../../api/dateFilter').unixDateSince
const determineTableBasedOnDate = require('../../api/dateFilter').determineTableBasedOnDate

// ===================================================



exports.basicStatsForProperty = function(PLACE_ID){
  const p = new Promise((res, rej)=>{
    console.log("==> About to get basic stats for the property at "+PLACE_ID)
    console.log(PLACE_ID)
    let stats = {}
    const timeSince = unixDateSince(100)    // unix date for last 100 days from today
    getStatsForLease(PLACE_ID, timeSince, stats)
      .then((data)=>{
        return getStatsForSublet(PLACE_ID, timeSince, stats)
      })
      .then((data)=>{
        return getSubletsForProperty(PLACE_ID, timeSince, stats)
      })
      .then((data)=>{
        res(stats)
      })
      .catch((err)=>{
        rej(err)
      })
  })
  return p
}

function getStatsForLease(PLACE_ID, timeSince, stats){
  const p = new Promise((res, rej)=>{
    const params = {
      TableName: "RentBurrow-Leases" + determineTableBasedOnDate(),
      KeyConditionExpression: "#PLACE_ID = :PlaceId",
      FilterExpression: "#DATE > :date",
      ExpressionAttributeNames: {
        "#PLACE_ID": "PLACE_ID",
        "#DATE": "DATE"
      },
      ExpressionAttributeValues: {
        ":PlaceId": PLACE_ID,
        ":date": timeSince
      }
    }
    docClient.query(params, function(err, data) {
      if (err){
        console.log(err, err.stack); // an error occurred
        rej(err)
      }else{
        console.log("====== GOT STATS ON LEASE ACTIONS =====")
        console.log(data);           // successful response
        stats.leaseData = data
        res(stats)
      }
    })
  })
  return p
}

function getStatsForSublet(PLACE_ID, timeSince, stats){
  const p = new Promise((res, rej)=>{
    const params = {
      TableName: "RentBurrow-Sublets" + determineTableBasedOnDate(),
      KeyConditionExpression: "#PLACE_ID = :PlaceId",
      FilterExpression: "#DATE > :date",
      ExpressionAttributeNames: {
        "#PLACE_ID": "PLACE_ID",
        "#DATE": "DATE"
      },
      ExpressionAttributeValues: {
        ":PlaceId": PLACE_ID,
        ":date": timeSince
      }
    }
    docClient.query(params, function(err, data) {
      if (err){
        console.log(err, err.stack); // an error occurred
        rej(err)
      }else{
        console.log("====== GOT STATS ON SUBLET ACTIONS =====")
        console.log(data);           // successful response
        stats.subletData = data
        res(stats)
      }
    })
  })
  return p
}


function getSubletsForProperty(PLACE_ID, timeSince, stats){
  const p = new Promise((res, rej)=>{
    console.log("====== GOT SUBLETS FOR PROPERTY =====")
    Sublet.find({$and:[{place_id: PLACE_ID}, {created_time_unix: {$gte:timeSince}}, {active: true}]}, null, {sort: {updated_time: -1}}, function(err, posts){
  		if(err){
        rej(err)
        return
      }
      console.log(posts)
      stats.fbPosts = posts
      console.log("====== GOT ALL DATA FOR PROPERTY =====")
      console.log(stats)
  		res(stats)
  	})
  })
  return p
}
