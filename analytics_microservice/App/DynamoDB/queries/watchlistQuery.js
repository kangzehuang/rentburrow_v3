
const AWS = require('aws-sdk')
const aws_config = require('../aws_config.json')
// const dynaDoc = require("dynamodb-doc");
AWS.config.update(aws_config)

const dynamodb = new AWS.DynamoDB({
  dynamodb: '2012-08-10',
  region: "us-east-1"
})
// const docClient = new dynaDoc.DynamoDB(dynamodb)
const Bluebird = require('bluebird')
const Lease = require('../../models/lease_model');
const Watchlist = require('../../models/watchlist_model');
const unixDateSince = require('../../api/dateFilter').unixDateSince
const determineTableBasedOnDate = require('../../api/dateFilter').determineTableBasedOnDate

// ===================================================



exports.competingWatchlistProperties = function(PLACE_ID){
  const p = new Promise((res, rej)=>{
    const timeSince = unixDateSince(100)
    const stats = {}
    // console.log(PLACE_ID)
    getAllLeaseFavoritesForThisPlace(PLACE_ID, timeSince, stats)
      .then((stats)=>{
        return filterUniqueWatchlists(stats)
      })
      .then((uniqueWatchlists)=>{
        return getWatchlistsFromDb(uniqueWatchlists)
      })
      .then((watchlists)=>{
        return getLeasesFromDb(watchlists)
      })
      .then((allLeases)=>{
        res(allLeases)
      })
      .catch((err)=>{
        rej(err)
      })
  })
  return p
}


function getAllLeaseFavoritesForThisPlace(PLACE_ID, timeSince, stats){
  const p = new Promise((res, rej)=>{
    console.log("========= PLACE_ID =========")
    console.log(PLACE_ID)
    const params = {
      TableName: "RentBurrow-Leases"+determineTableBasedOnDate(),
      KeyConditionExpression: "#PLACE_ID = :PlaceId",
      FilterExpression: "(#DATE > :date) AND ((#ACTION = :cardFavorited) OR (#ACTION = :cardUnfavorited))",
      ExpressionAttributeNames: {
        "#PLACE_ID": "PLACE_ID",
        "#DATE": "DATE",
        "#ACTION": "ACTION"
      },
      ExpressionAttributeValues: {
        ":PlaceId": {"S":PLACE_ID},
        ":date": {"N":timeSince.toString()},
        ":cardFavorited": {"S":"CARD_FAVORITED"},
        ":cardUnfavorited": {"S":"CARD_UNFAVORITED"}
      }
    }
    dynamodb.query(params, function(err, data) {
      if (err){
        console.log(err, err.stack); // an error occurred
        rej(err)
      }else{
        console.log("====== GOT STATS ON LEASE ACTIONS =====")
        console.log(data);           // successful response
        stats.leaseData = data
        res(stats)
      }
    })
  })
  return p
}

function filterUniqueWatchlists(stats){
  const p = new Promise((res, rej)=>{
    const uniqueWatchlists = []
    stats.leaseData.Items.forEach((x, i)=>{
      // console.log("===========================")
      // console.log(x)
      // console.log(x.WATCHLIST_ID.S)
      if(x.WATCHLIST_ID && x.WATCHLIST_ID.S && !checkIfInArray(uniqueWatchlists, x.WATCHLIST_ID.S)){
        uniqueWatchlists.push(x.WATCHLIST_ID.S)
        // console.log(x.WATCHLIST_ID)
      }
      if(i==stats.leaseData.Items.length-1){
        console.log(uniqueWatchlists)
        res(uniqueWatchlists)
      }
    })
  })
  return p
}

function getWatchlistsFromDb(uniqueWatchlists){
  const p = new Promise((res, rej)=>{
    const watchlistArrayOfPromises = uniqueWatchlists.map((w)=>{
      return getWatchList(w)
    })
    console.log(watchlistArrayOfPromises)

    console.log("========= ABOUT TO DO PROMISE.ALL ==========")
    Promise.all(watchlistArrayOfPromises).then((watchlists)=>{
      console.log(" ======= JUST RAN ALL PROMISES =======")
      console.log(watchlists)
      res(watchlists)
  	})
  })
  return p
}

function getWatchList(watchlistId){
  const p = new Promise((res, rej)=>{
    Watchlist.findById(watchlistId, function(err, watchlist){
  		if(err){
        console.log(err)
        res({
        	watchlist_title: "Empty",
        	users: [],
        	sublets: [],
        	leases: [],
        	active: false
        })
      };
  		// return the matching Leases
  		if(watchlist){
  			res(watchlist);
  		}else{
  			res({
        	watchlist_title: "Empty",
        	users: [],
        	sublets: [],
        	leases: [],
        	active: false
        })
  		}
  	});
  })
  return p
}


function getLeasesFromDb(watchlists){
  const p = new Promise((res, rej)=>{
    const arrayOfLeaseArrayOfPromises = watchlists.map((w)=>{
      return w.leases.map((l)=>{
          return getLease(l)
        })
    })
    const leaseArrayOfPromises = []
    arrayOfLeaseArrayOfPromises.forEach((promiseArray)=>{
      promiseArray.forEach((p)=>{
        leaseArrayOfPromises.push(p)
      })
    })
    Promise.all(leaseArrayOfPromises).then((leases)=>{
      console.log(" ========= ALL LEASES FROM MONGO ==========")
      console.log(leases)
      res(leases)
    })
  })
  return p
}

function getLease(leaseId){
  const p = new Promise((res, rej)=>{
    Lease.findById(leaseId, function(err, lease){
  		if(err){
        console.log(err)
        res(null)
      };
  		// return the matching Leases
  		if(lease){
  			res(lease);
  		}else{
  			res(null)
  		}
  	});
  })
  return p
}

function checkIfInArray(array, check){
  let exists = false
  array.forEach((x)=>{
    if(x === check){
      exists = true
    }
  })
  return exists
}
