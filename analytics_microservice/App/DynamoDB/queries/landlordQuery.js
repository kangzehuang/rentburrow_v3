
const AWS = require('aws-sdk')
const aws_config = require('../aws_config.json')
const dynaDoc = require("dynamodb-doc");
AWS.config.update(aws_config)

const dynamodb = new AWS.DynamoDB({
  dynamodb: '2012-08-10',
  region: "us-east-1"
})
const docClient = new dynaDoc.DynamoDB(dynamodb)
const determineTableBasedOnDate = require('../../api/dateFilter').determineTableBasedOnDate

// ===================================================




exports.basicStatsForLandlord = function(LANDLORD_ID){
  const p = new Promise((res, rej)=>{
    console.log("==> About to get basic stats for the landlord "+LANDLORD_ID)
    let stats = {}
    getLeaseStatsForLandlord(LANDLORD_ID, stats)
      .then((data)=>{
        return getLandlordStatsForLandlord(LANDLORD_ID, stats)
      })
      .then((data)=>{
        res(stats)
      })
      .catch((err)=>{
        rej(err)
      })
  })
  return p
}

function getLeaseStatsForLandlord(LANDLORD_ID, stats){
  const p = new Promise((res, rej)=>{
    const params = {
      TableName: "RentBurrow-Leases" + determineTableBasedOnDate(),
      IndexName: "OWNER_ID",
      FilterExpression: "#OWNER_ID = :OwnerId",
      ExpressionAttributeNames: {
        "#OWNER_ID": "OWNER_ID"
      },
      ExpressionAttributeValues: {
        ":OwnerId": LANDLORD_ID
      }
    }
    docClient.scan(params, function(err, data) {
      if (err){
        console.log(err, err.stack); // an error occurred
        rej(err)
      }else{
        console.log(data);           // successful response
        stats.leaseData = data
        res(stats)
      }
    })
  })
  return p
}


function getLandlordStatsForLandlord(LANDLORD_ID, stats){
  const p = new Promise((res, rej)=>{
    const params = {
      TableName: "RentBurrow-Landlords" + determineTableBasedOnDate(),
      KeyConditionExpression: "#LANDLORD_ID = :LandlordId",
      // FilterExpression: "#PROP_GPS = :PropGps",
      ExpressionAttributeNames: {
        "#LANDLORD_ID": "LANDLORD_ID"
        // "#PROP_GPS": {"S":"PROP_GPS"}
      },
      ExpressionAttributeValues: {
        ":LandlordId": LANDLORD_ID
        // ":PropGps": {"S":lease.coords}
      }
    }
    docClient.query(params, function(err, data) {
      if (err){
        console.log(err, err.stack); // an error occurred
        rej(err)
      }else{
        console.log(data);           // successful response
        stats.landlordData = data
        res(stats)
      }
    })
  })
  return p
}
