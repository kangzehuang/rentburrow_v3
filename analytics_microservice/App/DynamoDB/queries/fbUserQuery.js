
const AWS = require('aws-sdk')
const aws_config = require('../aws_config.json')
const dynaDoc = require("dynamodb-doc");
AWS.config.update(aws_config)

const dynamodb = new AWS.DynamoDB({
  dynamodb: '2012-08-10',
  region: "us-east-1"
})
const docClient = new dynaDoc.DynamoDB(dynamodb)
const unixDateSince = require('../../api/dateFilter').unixDateSince
const determineTableBasedOnDate = require('../../api/dateFilter').determineTableBasedOnDate

// ===================================================



exports.basicStatsForFBUser = function(FB_USER_ID){
  const p = new Promise((res, rej)=>{
    console.log("==> About to get basic stats for the Fb User "+FB_USER_ID)
    console.log(FB_USER_ID)
    const timeSince = unixDateSince(100)
    let stats = {}
    getFbUserSublets(FB_USER_ID, timeSince, stats)
      .then((data)=>{
        return getFbUserLeases(FB_USER_ID, timeSince, stats)
      })
      .then((data)=>{
        return getFbUserApp(FB_USER_ID, timeSince, stats)
      })
      .then((data)=>{
        res(stats)
      })
      .catch((err)=>{
        rej(err)
      })
  })
  return p
}

function getFbUserSublets(FB_USER_ID, timeSince, stats){
  const p = new Promise((res, rej)=>{
    const params = {
      TableName: "RentBurrow-Sublets" + determineTableBasedOnDate(),
      IndexName: "USER_FB_ID",
      FilterExpression: "#USER_FB_ID = :FbUserId AND #DATE > :date",
      ExpressionAttributeNames: {
        "#USER_FB_ID": "USER_FB_ID",
        "#DATE": "DATE"
      },
      ExpressionAttributeValues: {
        ":FbUserId": FB_USER_ID,
        ":date": timeSince
      }
    }
    docClient.scan(params, function(err, data) {
      if (err){
        console.log(err, err.stack); // an error occurred
        rej(err)
      }else{
        console.log(data);           // successful response
        stats.subletData = data
        res(stats)
      }
    })
  })
  return p
}

function getFbUserLeases(FB_USER_ID, timeSince, stats){
  const p = new Promise((res, rej)=>{
    const params = {
      TableName: "RentBurrow-Leases" + determineTableBasedOnDate(),
      IndexName: "USER_FB_ID",
      FilterExpression: "#USER_FB_ID = :FbUserId AND #DATE > :date",
      ExpressionAttributeNames: {
        "#USER_FB_ID": "USER_FB_ID",
        "#DATE": "DATE"
      },
      ExpressionAttributeValues: {
        ":FbUserId": FB_USER_ID,
        ":date": timeSince
      }
    }
    docClient.scan(params, function(err, data) {
      if (err){
        console.log(err, err.stack); // an error occurred
        rej(err)
      }else{
        console.log(data);           // successful response
        stats.leaseData = data
        res(stats)
      }
    })
  })
  return p
}


function getFbUserApp(FB_USER_ID, timeSince, stats){
  const p = new Promise((res, rej)=>{
    const params = {
      TableName: "RentBurrow-App" + determineTableBasedOnDate(),
      KeyConditionExpression: "#USER_FB_ID = :FbUserId",
      FilterExpression: "#DATE > :date",
      ExpressionAttributeNames: {
        "#USER_FB_ID": "USER_FB_ID",
        "#DATE": "DATE"
      },
      ExpressionAttributeValues: {
        ":FbUserId": FB_USER_ID,
        ":date": timeSince
      }
    }
    docClient.query(params, function(err, data) {
      if (err){
        console.log(err, err.stack); // an error occurred
        rej(err)
      }else{
        console.log(data);           // successful response
        stats.appData = data
        res(stats)
      }
    })
  })
  return p
}
