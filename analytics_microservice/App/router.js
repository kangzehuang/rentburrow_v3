
const Property = require('./routes/property-routes')
const FbUser = require('./routes/fbUser-routes')
const Landlord = require('./routes/landlord-routes')

module.exports = function(app){

	// get all activity related to an address (GPS coord)
	app.post('/basicPropertyStats', Property.basicPropertyStats)
	// get all activity related to a fb user
	app.post('/basicFBUserStats', FbUser.basicFBUserStats)
	// get all activity related to a landlord
	app.post('/basicLandlordStats', Landlord.basicLandlordStats)
	// get a list of leases that compete on the same watchlists
	app.post('/competingFavorites', Property.competingFavorites)
	// get the a series of sublet posts for a property over time
	app.post('/propertyChangeOverTime', Property.propertyChangeOverTime)
}
