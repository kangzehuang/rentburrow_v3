// Import dependencies on Mongoose
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Create the Rental post Schema
var SubletSchema = new mongoose.Schema({
	id: String,
	postid: String,
	posturl: String,
	address: String,
	city: String,
	coords: {
		type: [Number],
		index: '2d'
	},
	price: Number,
	rooms_left: Number,
	ensuite_incl: Boolean,
	utils_incl: Boolean,
	phone: String,
	female_only: Boolean,
	semester: String,
	message: String,
	images: [{
		id: String,
		url: String
	}],
	place_id: String,
	created_time: Date,
	created_time_unix: Number,
	userid: String,
	username: String,
	userurl: String,
	userpic: String,
	extracted_at: Date,
	groupid: String,
	active: Boolean
});

SubletSchema.pre('save', function(next){
	var currentDate = new Date();

	this.extracted_at = currentDate;
	next();
});

// Indexes this schema in 2dsphere format (critical for running proximity searches)
SubletSchema.index({coords: '2dsphere'});

var Sublet = mongoose.model('Sublet', SubletSchema);

module.exports = Sublet;
