const basicStatsForLandlord = require('../DynamoDB/queries/landlordQuery').basicStatsForLandlord


// POST/ basicLandlordStats
// Get a list of all actions related to a landlord's leases, as well as any landlord actions that the landlord themselves have conducted
exports.basicLandlordStats = function(req, res, next){
  const LANDLORD_ID = req.body.id
  basicStatsForLandlord(LANDLORD_ID)
    .then((data)=>{
      res.json(data)
    })
}
