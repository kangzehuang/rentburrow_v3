
const basicStatsForProperty = require('../DynamoDB/queries/propertyQuery').basicStatsForProperty
const competingWatchlistProperties = require('../DynamoDB/queries/watchlistQuery').competingWatchlistProperties
const getStatsForPropertyOverTime = require('../DynamoDB/queries/timesQuery').getStatsForPropertyOverTime

// POST /basicPropertyStats
// See user-actions related to this property (eg. Sublet hovers, Lease inquires, Image views, favorited...etc)
exports.basicPropertyStats = function(req, res, next){
  const PLACE_ID = req.body.place_id
  basicStatsForProperty(PLACE_ID)
    .then((statsData)=>{
    	res.json(statsData)
    })
    .catch((err)=>{
      console.log(err)
      res.json({
        "Message": "A failure occurred",
        "Details": err
      })
    })
}

// POST /competingFavorites
// See a list of competing leases that tenants have also favorited in the same watchlist as this property
exports.competingFavorites = function(req, res, next){
  const PLACE_ID = req.body.place_id
  competingWatchlistProperties(PLACE_ID)
    .then((statsData)=>{
      res.json(statsData)
    })
    .catch((err)=>{
      console.log(err)
      res.json({
        "Message": "A failure occurred",
        "Details": err
      })
    })
}


// POST /propertyChangeOverTime
// observe changes in sublet posts for this property over time (aggregate of all student sublet posts)
exports.propertyChangeOverTime = function(req, res, next){
  const PLACE_ID = req.body.place_id
  getStatsForPropertyOverTime(PLACE_ID)
    .then((statsData)=>{
      res.json(statsData)
    })
    .catch((err)=>{
      console.log(err)
      res.json({
        "Message": "A failure occurred",
        "Details": err
      })
    })
}
