const basicStatsForFBUser = require('../DynamoDB/queries/fbUserQuery').basicStatsForFBUser

// POST/ basicFBUserStats
// retreive a series of user activity for a specific fb user
exports.basicFBUserStats = function(req, res, next){
  const FB_USER_ID = req.body.id
  basicStatsForFBUser(FB_USER_ID)
    .then((data)=>{
      res.json(data)
    })
    .catch((err)=>{
      console.log(err)
      res.json({
        "Message": "A failure occurred",
        "Details": err
      })
    })
}
