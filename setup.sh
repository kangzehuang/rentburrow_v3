mkdir db
sudo mongod --dbpath "./db" --fork --logpath "/var/log/mongod.log"
sudo iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 8080
