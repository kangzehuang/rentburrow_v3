import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import App from './components/app';
import Signin from './components/auth/signin';
import Signout from './components/auth/signout';
import Signup from './components/auth/signup';
import VerifyAccount from './components/auth/verify_account';
import ResetPassword from './components/auth/reset_password'
import Feature from './components/misc/feature';
import RequireAuth from './components/auth/require_auth';
import Welcome from './components/misc/welcome';
import Landlord from './components/landlord/Landlord';
import LandlordDashboard from './components/landlord/LandlordDashboard';
import PropertyDetails from './components/landlord/PropertyDetails'
import LandlordSettings from './components/landlord/LandlordSettings'
import LandlordAnalytics from './components/landlord/LandlordAnalytics'
import CanvasTenant from './components/tenant/CanvasTenant';
import CanvasLandlord from './components/landlord/CanvasLandlord';
import PrivacyPolicy from './components/tenant/PrivacyPolicy'
import Claim from './components/landlord/Claim'
import Sandbox from './components/shared_ui/Sandbox'


import Store from './store';

import { AUTH_USER } from './actions/action_types';


ReactDOM.render(
  <Provider store={Store}>
  	<Router history={browserHistory}>
      <Route path='/' component={App}>
        <IndexRoute component={CanvasTenant} />
        <Route path='signin' component={Signin}></Route>
        <Route path='signout' component={Signout}></Route>
        <Route path='signup' component={Signup}></Route>
        <Route path='verify_account' component={VerifyAccount}></Route>
        <Route path='reset_password' component={ResetPassword}></Route>
        <Route path='landlord' component={RequireAuth(Landlord)}>
          <Route path='dashboard' component={RequireAuth(LandlordDashboard)}></Route>
          <Route path='property/:leaseId' component={RequireAuth(PropertyDetails)}></Route>
          <Route path='analytics' component={RequireAuth(LandlordAnalytics)}></Route>
          <Route path='settings' component={RequireAuth(LandlordSettings)}></Route>
          <Route path='claim' component={RequireAuth(Claim)}></Route>
        </Route>
        <Route path='policy' component={PrivacyPolicy}></Route>
        <Route path='sandbox' component={Sandbox}></Route>
      </Route>
  	</Router>
  </Provider>
  , document.querySelector('.container'));
