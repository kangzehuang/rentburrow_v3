import axios from 'axios';
import { SET_GEO_COORD, SELECT_PINS, BACK_TO_PINS, SET_VIEWMODE, LOAD_FOR_VIEWMODE, STRING_FILTER, PAN_TO, UNPAN_TO, TOGGLE_ADVANCED_SEARCH, CLEAR_FILTERED_RESULTS, APPEND_LEASE, APPEND_SUBLET } from './action_types';

export function setGeoCoords(geoCoords){
	return function(dispatch){
		dispatch({
			type: SET_GEO_COORD,
			payload: geoCoords
		})
	}
}

export function selectPinsFromHere(pins){
	// using redux-thunk to allow for actions with functions
	return function(dispatch){
		dispatch({
			type: SELECT_PINS,
			payload: pins
		})
	}
} 

export function backToPins(){
	return function(dispatch){
		dispatch({
			type: BACK_TO_PINS
		})
	}
}

export function setViewMode(mode){
	return function(dispatch){
		dispatch({
			type: SET_VIEWMODE,
			payload: mode
		})
		dispatch({
			type: BACK_TO_PINS
		})
	}
}

export function loadForViewMode(list){
	return function(dispatch){
		dispatch({
			type: LOAD_FOR_VIEWMODE,
			payload: list
		})
	}
}

export function filterStringSearch(searchString, listofResults){
	return function(dispatch){
		const filteredResults = listofResults.filter((pin)=>{
			if(pin.building_name){
				return pin.building_name.toLowerCase().indexOf(searchString.toLowerCase()) >= 0 || pin.address.toLowerCase().indexOf(searchString.toLowerCase()) >= 0
			}else{
				return pin.address.toLowerCase().indexOf(searchString.toLowerCase()) >= 0
			}
		})
		dispatch({
			type: STRING_FILTER,
			payload: filteredResults
		})
	}
}

export function panToMap(card){
	return function(dispatch){
		dispatch({
			type: PAN_TO,
			payload: card
		})
	}
}

export function unPanToMap(){
	return function(dispatch){
		dispatch({
			type: UNPAN_TO
		})
	}
}

export function toggleAdvancedSearch(){
	return function(dispatch){
		dispatch({
			type: TOGGLE_ADVANCED_SEARCH
		})
	}
}


export function clearFilteredResults(){
	return function(dispatch){
		dispatch({
			type: CLEAR_FILTERED_RESULTS
		})
	}
}

export function appendLeaseToList(lease){
	return function(dispatch){
		dispatch({
			type: APPEND_LEASE,
			payload: lease
		})
	}
}

export function appendSubletToList(sublet){
	return function(dispatch){
		dispatch({
			type: APPEND_SUBLET,
			payload: sublet
		})
	}
}
