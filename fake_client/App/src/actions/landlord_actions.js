import {browserHistory} from 'react-router';
import { SELECT_LANDLORD_PROPERTY, EDIT_LANDLORD_PROPERTY, BACK_TO_LANDLORD_GRID, EXIT_PROPERTY_FORM, SAVE_LEASES_TO_STATE, LANDLORD_SETTINGS, SET_LANDLORD, LANDLORD_CHARTS_VIEW, SET_CLAIM_ID} from './action_types'

export function selectLandlordProperty(property){
	return function(dispatch){
		browserHistory.push('/landlord/property/'+property._id)
		dispatch({
			type: SELECT_LANDLORD_PROPERTY,
			payload: property
		})
	}
}

export function backToLandlordGrid(){
	return function(dispatch){
		browserHistory.push('/landlord/dashboard')
		dispatch({
			type: BACK_TO_LANDLORD_GRID
		})
	}
}

export function exitPropertyForm(){
	return function(dispatch){
		dispatch({
			type: EXIT_PROPERTY_FORM
		})
	}
}

export function editLandlordProperty(property){
	return function(dispatch){
		dispatch({
			type: EDIT_LANDLORD_PROPERTY,
			payload: property
		})
	}
}

export function newLandlordProperty(){
	return function(dispatch){
		browserHistory.push('/landlord/dashboard')
		dispatch({
			type: BACK_TO_LANDLORD_GRID
		})
		dispatch({
			type: EDIT_LANDLORD_PROPERTY,
			payload: {}
		})
	}
}

export function saveLeasesToState(leases){
	// console.log(leases)
	return function(dispatch){
		dispatch({
			type: SAVE_LEASES_TO_STATE,
			payload: leases
		})
	}
}

export function goToLandlordSettings(){
	browserHistory.push('/landlord/settings')
	return function(dispatch){
		dispatch({
			type: LANDLORD_SETTINGS
		})
	}
}

export function loadEmailCredsOfVerification({email}){
	return function(dispatch){
		dispatch({
			type: SET_LANDLORD,
			payload: {email}
		})
	}
}

export function goToCharts(){
	browserHistory.push('/landlord/analytics')
	return function(dispatch){
		dispatch({
			type: LANDLORD_CHARTS_VIEW
		})
	}
}

export function setClaimId(claimId){
	return function(dispatch){
		// console.log("setting claim id to " + claimId)
		dispatch({
			type: SET_CLAIM_ID,
			payload: claimId
		})
	}
}
