// Actions related to the app
export const SET_GEO_COORD = "SET_GEO_COORD"
export const SET_PROPERTY_CLAIM = "SET_PROPERTY_CLAIM"
export const SET_CLAIM_ID = "SET_CLAIM_ID"


// Actions related to auth of landlord
export const AUTH_LANDLORD = "AUTH_LANDLORD"
export const UNAUTH_LANDLORD = "UNAUTH_LANDLORD"
export const AUTH_ERROR = "AUTH_ERROR"
export const FETCH_MESSAGE = "FETCH_MESSAGE"


// Actions related to Facebook (auth of tenant)
export const SET_FB_PROFILE = "SET_FB_PROFILE"


// Actions related to Landlord dashboard actions
export const SET_LANDLORD = "SET_LANDLORD"
export const LOAD_LANDLORD_PROPERTIES = "LOAD_LANDLORD_PROPERTIES"
export const SELECT_LANDLORD_PROPERTY = "SELECT_LANDLORD_PROPERTY"
export const EDIT_LANDLORD_PROPERTY = "EDIT_LANDLORD_PROPERTY"
export const SAVE_LANDLORD_PROPERTY = "SAVE_LANDLORD_PROPERTY"
export const BACK_TO_LANDLORD_GRID = "BACK_TO_LANDLORD_GRID"
export const EXIT_PROPERTY_FORM = "EXIT_PROPERTY_FORM"
export const SAVE_LEASES_TO_STATE = "SAVE_LEASES_TO_STATE"
export const LANDLORD_SETTINGS = "LANDLORD_SETTINGS"
export const LANDLORD_CHARTS_VIEW = "LANDLORD_CHARTS_VIEW"


// Actions related to the tenant
export const SET_TENANT = "SET_TENANT"
export const SET_CITY = "SET_CITY"
export const TOGGLE_ADVANCED_SEARCH = "TOGGLE_ADVANCED_SEARCH"
export const LANDLORD_VIEW = "LANDLORD_VIEW"
export const SETTINGS_VIEW = "SETTINGS_VIEW"
export const ADD_VIEW = "ADD_VIEW"
export const WATCHLIST_VIEW = "WATCHLIST_VIEW"
export const LISTINGS_VIEW = "LISTINGS_VIEW"
export const LIVE_PREVIEW_SUBLET = "LIVE_PREVIEW_SUBLET"
export const SAVE_WATCHLISTS = "SAVE_WATCHLISTS"
export const SET_WATCHLIST = "SET_WATCHLIST"
export const UNSET_WATCHLIST = "UNSET_WATCHLIST"


// Actions related to the mapview and pins
export const DROP_PINS = "DROP_PINS"
export const SELECT_PINS = "SELECT_PINS"
export const BACK_TO_PINS = "BACK_TO_PINS"
export const SET_VIEWMODE = "SET_VIEWMODE"
export const LOAD_FOR_VIEWMODE = "LOAD_FOR_VIEWMODE"
export const STRING_FILTER = "STRING_FILTER"
export const PAN_TO = "PAN_TO"
export const UNPAN_TO = "UNPAN_TO"
export const FILTERED_RESULTS = "FILTERED_RESULTS"
export const APPEND_LEASE = "APPEND_LEASE"
export const APPEND_SUBLET = "APPEND_SUBLET"


// Actions related to the popup
export const TOGGLE_SUBLET_POPUP_TRUE = "TOGGLE_SUBLET_POPUP_TRUE"
export const TOGGLE_LEASE_POPUP_TRUE = "TOGGLE_LEASE_POPUP_TRUE"
export const TOGGLE_POPUP_FALSE = "TOGGLE_POPUP_FALSE"
export const TOGGLE_CONTACT_FORM = "TOGGLE_CONTACT_FORM"
export const IMAGE_POPUP = "IMAGE_POPUP"
export const CLEAR_IMAGE_POPUP = "CLEAR_IMAGE_POPUP"
export const REVIEW_LEASE = "REVIEW_LEASE"


// Actions related to content to/from database
export const GET_SUBLETS_FROM_DB = "GET_SUBLETS_FROM_DB"
export const GET_LEASES_FROM_DB = "GET_LEASES_FROM_DB"


// Actions related to sublet & lease advanced search
export const SEARCH_SUBLET_RESULTS = "SEARCH_SUBLET_RESULTS"
export const SEARCH_LEASE_RESULTS = "SEARCH_LEASE_RESULTS"
export const SAVE_SUBLET_SEARCH_PARAMS = "SAVE_SUBLET_SEARCH_PARAMS"
export const SAVE_LEASE_SEARCH_PARAMS = "SAVE_LEASE_SEARCH_PARAMS"


// Actions related to scroll
export const SET_SUBLET_SCROLL = "SET_SUBLET_SCROLL"
export const SET_LEASE_SCROLL = "SET_LEASE_SCROLL"
export const SET_WATCHLIST_SCROLL = "SET_WATCHLIST_SCROLL"
export const CLEAR_FILTERED_RESULTS = "CLEAR_FILTERED_RESULTS"


// Actions related to mobile audience viewer
export const SET_MOBILE_VIEWER = "SET_MOBILE_VIEWER"
export const TOGGLE_MOBILE_VIEW = "TOGGLE_MOBILE_VIEW"
export const CLEAR_ALL_POPUPS = "CLEAR_ALL_POPUPS"


// Actions related to browser performance
export const SET_SUBLET_CARD_INDEX = "SET_SUBLET_CARD_INDEX"
export const SET_LEASE_CARD_INDEX = "SET_LEASE_CARD_INDEX"

// Actions related to tracked activities
export const TRACK_ACTIVITY = "TRACK_ACTIVITY"
export const CLEAR_ACTIVITIES_LIST = "CLEAR_ACTIVITIES_LIST"
