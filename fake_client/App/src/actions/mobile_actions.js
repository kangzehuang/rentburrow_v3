import { SET_MOBILE_VIEWER, TOGGLE_MOBILE_VIEW, CLEAR_ALL_POPUPS } from './action_types'


export function setMobileViewer(){
	return function(dispatch){
		dispatch({
			type: SET_MOBILE_VIEWER
		})
	}
}

export function toggleMobileMap(bool){
	return function(dispatch){
		dispatch({
			type: TOGGLE_MOBILE_VIEW,
			payload: bool
		})
		if(bool == false){
			dispatch({
				type: CLEAR_ALL_POPUPS
			})
		}
	}
}