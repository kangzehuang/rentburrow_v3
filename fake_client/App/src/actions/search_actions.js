import { SAVE_SUBLET_SEARCH_PARAMS, SAVE_LEASE_SEARCH_PARAMS, FILTERED_RESULTS } from './action_types'

export function saveSubletSearchState(params){
	return function(dispatch){
		dispatch({
			type: SAVE_SUBLET_SEARCH_PARAMS,
			payload: params
		})
	}
}

export function saveLeaseSearchState(params){
	return function(dispatch){
		dispatch({
			type: SAVE_LEASE_SEARCH_PARAMS,
			payload: params
		})
	}
}

export function submitFilterResults(filteredResults){
	// console.log(filteredResults)
	return function(dispatch){
		dispatch({
			type: FILTERED_RESULTS,
			payload: filteredResults
		})
	}
}