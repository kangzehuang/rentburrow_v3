import axios from 'axios';
import {browserHistory} from 'react-router';
import { AUTH_LANDLORD, AUTH_ERROR, UNAUTH_LANDLORD, FETCH_MESSAGE, SET_LANDLORD } from './action_types';
import { getLandlord } from '../api/landlordManager'

import { LANDLORD_MICROSERVICE } from '../api/API_URLS'

export function authCorrect(){
	return function(dispatch){
		dispatch({type: AUTH_LANDLORD});
	}
}

export function signinLandlord({email, password}){
	// console.log("signing in with: ", email, password);

	// using redux-thunk to allow for actions with functions
	return function(dispatch){
		// returned for handleSubmit() from reduxForm 
		axios.post(LANDLORD_MICROSERVICE+'/signin', {email, password})
			.then(response => {
				// if request is good, update state to indicate user is authenticated
				dispatch({type: AUTH_LANDLORD})
				// and save the JWT token for follow-up requests
				// localStorage is browser storage that is private from other domains/websites
				// localStorage is available at global scope
				localStorage.setItem('email', email);
				localStorage.setItem('token', response.data.token);
				return getLandlord(email)
			})
			.then((data)=>{
				dispatch({
					type: SET_LANDLORD,
					payload: data
				})
				// redirect to route '/feature'
				browserHistory.push('/landlord/dashboard');
			})
			.catch(()=>{
				// if request is bad, show an error to user
				dispatch(authError("Bad Login Info"));
			});
	}
} 

export function signupLandlord({email, password, company_name, default_email, default_phone, email_forward}){
	// console.log("Attempting sign up with: ", email, password);

	return function(dispatch){
		axios.post(LANDLORD_MICROSERVICE+'/signup', {email, password, company_name, default_email, default_phone, email_forward})
			.then(response=>{
				dispatch({type: AUTH_LANDLORD});
				localStorage.setItem('email', email);
				localStorage.setItem('token', response.data.token);
				dispatch({
					type: SET_LANDLORD,
					payload: response.data.landlord
				})
				browserHistory.push('/landlord/dashboard');
			})
			.catch(err=>{
				// console.log(typeof err);
				// console.log(err);
				for(let key in err){
					// console.log(key)
					// console.log(err[key])
				}
				dispatch(authError(err.response.data.errorMessage));
			});
	}
}

export function signoutLandlord(){
	// remove the jwt token from localStorage
	localStorage.removeItem('token');
	return {
		type: UNAUTH_LANDLORD
	}
}

// redux-thunk versin of fetchMessage()
export function fetchMessage(){
	return function(dispatch){
		axios.get(LANDLORD_MICROSERVICE, {
			// we can specify headers in axios like this
			headers: {authorization: localStorage.getItem('token')}
		})
		.then(response=>{
			dispatch({
				type: FETCH_MESSAGE,
				payload: response.data.message
			})
		});
	}
}

export function sendError(msg){
	return function(dispatch){
		dispatch({
			type: AUTH_ERROR,
			payload: msg
		})
	}
}

export function authError(error){
	return {
		type: AUTH_ERROR,
		payload: error
	}
}

export function setLandlord(landlord){
	return function(dispatch){
		dispatch({
			type: AUTH_LANDLORD
		})
		dispatch({
			type: SET_LANDLORD,
			payload: landlord
		})
		// redirect to route
		browserHistory.push('/landlord/dashboard');
	}
}