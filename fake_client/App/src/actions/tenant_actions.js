import { SET_FB_PROFILE, SETTINGS_VIEW, ADD_VIEW, WATCHLIST_VIEW, LISTINGS_VIEW, SET_CITY, LIVE_PREVIEW_SUBLET, LANDLORD_VIEW, CLEAR_ALL_POPUPS, SET_PROPERTY_CLAIM } from './action_types';

export function setFBProfile(profile){
	return function(dispatch){
		dispatch({
			type: SET_FB_PROFILE,
			payload: profile
		})
	}
}

export function goToListingsView(){
	return function(dispatch){
		dispatch({
			type: LISTINGS_VIEW
		})
		dispatch({
			type: CLEAR_ALL_POPUPS
		})
	}
}

export function goToLandlordProfile(lease){
	return function(dispatch){
		const landlord = {
			landlord_id: lease.landlord_id,
			company: lease.company,
			url: lease.url,
			thumbnail: lease.thumbnail
		}
		dispatch({
			type: LANDLORD_VIEW,
			payload: landlord
		})
		dispatch({
			type: CLEAR_ALL_POPUPS
		})
	}
}

export function goToSettingsView(){
	return function(dispatch){
		dispatch({
			type: SETTINGS_VIEW
		})
		dispatch({
			type: CLEAR_ALL_POPUPS
		})
	}
}

export function goToAddView(){
	return function(dispatch){
		dispatch({
			type: ADD_VIEW
		})
		dispatch({
			type: CLEAR_ALL_POPUPS
		})
	}
}


export function goToWatchlistView(){
	return function(dispatch){
		dispatch({
			type: WATCHLIST_VIEW
		})
		dispatch({
			type: CLEAR_ALL_POPUPS
		})
	}
}

export function setCity(city){
	return function(dispatch){
		dispatch({
			type: SET_CITY,
			payload: city
		})
	}
}


export function setLivePreviewSublet(preview){
	return function(dispatch){
		dispatch({
			type: LIVE_PREVIEW_SUBLET,
			payload: preview
		})
	}
}


export function setPropertyClaim(){
	return function(dispatch){
		dispatch({
			type: SET_PROPERTY_CLAIM,
			payload: true
		})
	}
}
