// word bank used for hints, keywords..etc

	// street signs (case insensitive) for matching
	export const street_signs = [
			'street', 
			'st', 
			'way', 
			'rd',
			'road', 
			'ave', 
			'avenue', 
			'blvd', 
			'ct',
			'crt', 
			'court', 
			'cres', 
			'crescent', 
			'park', 
			'pk', 
			'parkway', 
			'path',
			'round',
			'dr',
			'drive'
		];

	// words for women for matching
	export const words_for_women = [
			'female', 
			'females',
			'girls', 
			'girl',  
			'women', 
			'ladies'
		];
