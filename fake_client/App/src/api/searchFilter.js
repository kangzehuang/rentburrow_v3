

export function searchSubletFilter(sublets, params){
	/*
		params = {
				price: {
					min: 200,
					max: 500
				},
				rooms: 1,
				utils: false,
				ensuite: false,
				femaleOnly: false,
				semester: null
			}
	*/
	const p = new Promise((res, rej)=>{
		let filteredSublets = sublets.filter((sublet)=>{
				return filterSubletPrice(sublet, params.price)
			})
			.filter((sublet)=>{
				return filterSubletRooms(sublet, params.rooms)
			})
			.filter((sublet)=>{
				return filterSubletUtils(sublet, params.utils)
			})
			.filter((sublet)=>{
				return filterSubletEnsuite(sublet, params.ensuite)
			})
			.filter((sublet)=>{
				return filterSubletFemaleOnly(sublet, params.femaleOnly)
			})
			.filter((sublet)=>{
				return filterSubletSemester(sublet, params.semester)
			})
		// console.log("filteredSublets: ")
		// console.log(filteredSublets)
		res(filteredSublets)
	})
	return p
}


export function searchLeaseFilter(leases, params){

	/*
		params = {
				price: {
					min: 200,
					max: 500
				},
				leaseTerms: 12,
				groupSize: 1,
				water: false,
				heat: false,
				electric: false,
				ac: false,
				ensuite: false,
				parking: false,
				furnished: false,
				internet: false,
				type: {
					apartment: true,
					house: true
				}
			}
	*/

	const p = new Promise((res, rej)=>{
		let filteredLeases = leases.filter((lease)=>{
				return filterLeasePrice(lease, params.price)
			})
			.filter((lease)=>{
				return filterLeaseTerms(lease, params.leaseTerms)
			})
			.filter((lease)=>{
				return filterLeasePropType(lease, params.type)
			})
			.filter((lease)=>{
				return filterLeaseRooms(lease, params.groupSize)
			})
			.filter((lease)=>{
				return filterLeaseWater(lease, params.water)
			})
			.filter((lease)=>{
				return filterLeaseHeat(lease, params.heat)
			})
			.filter((lease)=>{
				return filterLeaseElectric(lease, params.electric)
			})
			.filter((lease)=>{
				return filterLeaseAC(lease, params.ac)
			})
			.filter((lease)=>{
				return filterLeaseEnsuite(lease, params.ensuite)
			})
			.filter((lease)=>{
				return filterLeaseParking(lease, params.parking)
			})
			.filter((lease)=>{
				return filterLeaseFurnished(lease, params.furnished)
			})
			.filter((lease)=>{
				return filterLeaseInternet(lease, params.internet)
			})
		res(filteredLeases)
	})
	return p
}










function filterSubletPrice(sublet, price){
	return sublet.price <= price.max && sublet.price >= price.min
}

function filterSubletRooms(sublet, rooms){
	if(!sublet.rooms_left){
		return 1 == rooms
	}else{
		return sublet.rooms_left >= rooms
	}
}

function filterSubletUtils(sublet, utils){
	if(utils){
		return sublet.utils_incl == utils
	}else{
		return true
	}
}

function filterSubletEnsuite(sublet, ensuite){
	if(ensuite){
		return sublet.ensuite_incl == ensuite
	}else{
		return true
	}
}

function filterSubletFemaleOnly(sublet, femaleOnly){
	if(femaleOnly){
		return sublet.female_only == femaleOnly
	}else{
		return true
	}
}

function filterSubletSemester(sublet, semester){
	let defaultSemester
	const now = new Date()
	const month = now.getMonth()
	if(month > 1 && month < 4){
		defaultSemester = "spring"
	}else if(month > 5 && month < 9){
		defaultSemester = "fall"
	}else{
		defaultSemester = "winter"
	}
	if(sublet.semester && semester){
		return sublet.semester.toLowerCase() == semester.toLowerCase()
	}else{
		// console.log(defaultSemester)
		return defaultSemester.toLowerCase() == semester.toLowerCase()
	}
}

function filterLeasePrice(lease, price){
	let anyMatch
	const results = lease.rooms.filter((room)=>{
		if(room.promo_price == 0){
			return room.reg_price <= price.max && room.reg_price >= price.min
		}else{
			return (room.reg_price <= price.max && room.reg_price >= price.min) || (room.promo_price <= price.max && room.promo_price >= price.min)
		}
	})
	if(results && results.length > 0){
		anyMatch = true
	}else{
		anyMatch = false
	}
	return anyMatch
}

function filterLeaseTerms(lease, leaseTerms){
	let anyMatch
	const results = lease.rooms.filter((room)=>{
		return room.lease_terms <= leaseTerms
	})
	if(results && results.length > 0){
		anyMatch = true
	}else{
		anyMatch = false
	}
	return anyMatch
}

function filterLeasePropType(lease, types){
	let anyMatch = false
	if(lease.type.toLowerCase() == "apartment" && types.apartment){
		anyMatch = true
	}else if(lease.type.toLowerCase() == "house" && types.house){
		anyMatch = true
	}else if(!types.apartment && !types.house){
		anyMatch = true
	}
	return anyMatch
}

function filterLeaseRooms(lease, groupSize){
	let anyMatch
	const results = lease.rooms.filter((room)=>{
		return room.rooms_per >= groupSize
	})
	if(results && results.length > 0){
		anyMatch = true
	}else{
		anyMatch = false
	}
	return anyMatch
}

function filterLeaseWater(lease, water){
	if(water){
		return lease.utils_list.water == water
	}else{
		return true
	}
}

function filterLeaseHeat(lease, heat){
	if(heat){
		return lease.utils_list.heat == heat
	}else{
		return true
	}
}

function filterLeaseElectric(lease, electric){
	if(electric){
		return lease.utils_list.electric == electric
	}else{
		return true
	}
}

function filterLeaseAC(lease, ac){
	if(ac){
		return lease.utils_list.ac == ac
	}else{
		return true
	}
}

function filterLeaseEnsuite(lease, ensuite){
	let anyMatch
	const results = lease.rooms.filter((room)=>{
		return room.rooms_per == room.bathrooms
	})
	if(results && results.length > 0){
		anyMatch = true
	}else{
		anyMatch = false
	}

	if(ensuite){
		return anyMatch
	}else{
		return true
	}
}

function filterLeaseParking(lease, parking){
	if(parking){
		return lease.utils_list.parking == parking
	}else{
		return true
	}
}

function filterLeaseFurnished(lease, furnished){
	if(furnished){
		return lease.utils_list.furnished == furnished
	}else{
		return true
	}
}

function filterLeaseInternet(lease, internet){
	if(internet){
		return lease.utils_list.internet == internet
	}else{
		return true
	}
}
