import axios from 'axios'
import { ANALYTICS_MICROSERVICE } from './API_URLS'

export function updateTrackedActivities(trackedActivities, selectedWatchlistID){
  // console.log(trackedActivities)
  // console.log(selectedWatchlistID)
  const updatedActivities = trackedActivities.map((a)=>{
        if(a){
          // console.log(a)
          if(a.Item.ACTION === "CARD_FAVORITED" || a.Item.ACTION === "CARD_UNFAVORITED"){
            a.Item.WATCHLIST_ID = selectedWatchlistID
            return a
          }else{
            return a
          }
        }
      })
  return updatedActivities
}

export function getBasicPropertyStats(property){
  // console.log(property)
  const p = new Promise((res, rej)=>{
    axios.post(ANALYTICS_MICROSERVICE+"/basicPropertyStats", property)
      .then((data)=>{
        // console.log(data)
        res(data.data)
      })
  })
  return p
}

export function getPropertySums(statsData, sumName){
  // console.log(statsData)
  switch(sumName){
    case 'totalLeaseViews':
      return totalLeaseXCount(statsData, "CARD_ON_HOVER")

    case 'totalLeaseInquires':
      return totalLeaseXCount(statsData, "INQUIRE_CLICK")

    case 'totalEmailsSent':
      return totalLeaseXCount(statsData, "EMAIL_SENT")

    case 'totalLeaseFavorites':
      return totalLeaseXCount(statsData, "CARD_FAVORITED")

    case 'totalSubletViews':
      return totalSubletXCount(statsData, "CARD_ON_HOVER")

    case 'totalSubletContacts':
      return totalSubletXCount(statsData, "SUBLET_CONTACTED")

    case 'totalSubletting':
      return totalSubletting(statsData)

    case 'avgSubletPrice':
      return avgSubletPrice(statsData)

    case 'subletPriceRange':
      return subletPriceRange(statsData)

    default:
      return
  }

}

function totalLeaseXCount(statsData, action){
  let count = 0
  if(statsData){
    statsData.leaseData.Items.forEach((x)=>{
      if(x.ACTION == action){
        count++
      }
    })
  }
  return count
}

function totalSubletXCount(statsData, action){
  let count = 0
  if(statsData.subletData){
    statsData.subletData.Items.forEach((x)=>{
      if(x.ACTION == action){
        count++
      }
    })
  }
  return count
}

function totalSubletting(statsData){
  if(statsData.fbPosts){
    return statsData.fbPosts.length
  }else{
    return 0
  }
}

function avgSubletPrice(statsData){
  let sum = 0
  let counter = 0
  if(statsData.fbPosts){
    statsData.fbPosts.forEach((x)=>{
      // console.log(x)
      if(x.price > 100){
        sum += x.price
        counter++
      }
    })
    return sum/counter
  }else{
    return 0
  }
}

function subletPriceRange(statsData){
  let min = 0
  let max = 0
  if(statsData.fbPosts){
    statsData.fbPosts.forEach((x, i)=>{
      if(x.price > 100){
        if(min == 0){
          min = x.price
          max = x.price
        }
        if(x.price < min){
          min = x.price
        }
        if(x.price > max){
          max = x.price
        }
      }
    })
  }
  return {
    min: min,
    max: max
  }
}
