import axios from 'axios'
import { TENANT_MICROSERVICE, FBPARSER_MICROSERVICE } from './API_URLS'

export function grabFBProfile(){
	return grabFacebookProfile()
				.then(grabFacebookImage)
}

export function extractAndSaveFacebookPostsToDB(city, profile){
	getGroupsForCity({city, profile})
		.then(latestPostInServerPerGroup)
		.then(getPostsFromGroups)
		.then(filterNonSublets)
		.then(sendToServer)
}

// user profile is not saved to TENANT_MICROSERVICE
function getGroupsForCity({city, profile}){
	const p = new Promise((res, rej)=>{
		axios.post(TENANT_MICROSERVICE+"/city_groups", city)
			.then((response, err)=>{
				if(err){rej(err)}
				res({
					groups: response.data,
					profile
				})
			})
	})
	return p
}

function latestPostInServerPerGroup({groups, profile}){
	const p = new Promise((res, rej)=>{
		const groupsWithLatestPostTime = []
		for(let g = 0; g<groups.length; g++){
			axios.post(FBPARSER_MICROSERVICE+'/check_latest_sublet', groups[g])
				.then(function(data){
					let lastPostTime
					if(data){
						lastPostTime = data.data
						groupsWithLatestPostTime.push({...groups[g], lastPostTime})
					}
					if(g === groups.length-1){
						res({
							groupsTime: groupsWithLatestPostTime,
							profile
						})
					}
				})
		}
	})
	return p
}

function getPostsFromGroups({groupsTime, profile}){
	const p = new Promise((res, rej)=>{
		const postsArray = []
		let doneAsyncTicker = 0
		for(let g = 0; g<groupsTime.length; g++){
			FB.api(
	            "/"+groupsTime[g].groupid+"/feed?limit=100",
	            {access_token: localStorage.getItem('fbToken')},
	          	function (response) {
		            if (response && !response.error) {
					  	response.data.filter((post)=>{
					  		return post.updated_time > groupsTime[g].lastPostTime
					  	}).forEach((post)=>{
					  		postsArray.push({
					  			...post,
					  			city: groupsTime[g].city_name,
					  			groupid: groupsTime[g].groupid
					  		})
					  	})
		            }
		            doneAsyncTicker++
				  	if(doneAsyncTicker === groupsTime.length){
						res({
							postsArray,
							profile
						})
					}
		        }
		    )
		}
	})
	return p
}

function filterNonSublets({postsArray, profile}){
	const p = new Promise((res, rej)=>{
		const filteredSublets = postsArray.filter((post, index)=>{
			if(post.message){
				return post.message.match(/\(?(\d+[a-fA-F]?)(\s|\,\s|\.\s)(\b[a-zA-Z]*\b)\s(\.|,|\()?([a-zA-Z]*\b)(\.|,|\:|\)|\n)?\s(?:[a-zA-Z]*\b)?(\.|\,|\s)?/ig)
			}
		})
		res({
			filteredSublets,
			profile
		})
	})
	return p
}

function sendToServer({filteredSublets, profile}){
	axios.post(FBPARSER_MICROSERVICE+'/new_sublets', {newSublets: filteredSublets, profile})
		.then(function(data){
			// // console.log(data);
		}, function(err){
			// console.log(err);
		})
}

function grabFacebookProfile(){
	const p = new Promise((res, rej)=>{
		FB.api('/me',
	        {access_token: localStorage.getItem('fbToken')},
	        function(profile) {
				if(profile.id){
					res(profile)
				}else{
					// console.log(profile)
					rej(profile.error)
				}
		})
	})
	return p
}

function grabFacebookImage(profile){
	const p = new Promise((res, rej)=>{
		FB.api(
		    '/me?fields=picture&type=small',
		    {access_token: localStorage.getItem('fbToken')},
		    function (response) {
		      if (response && !response.error) {
		        const picurl = response.picture.data.url;
		        res({...profile, picurl});
		      }else{
	    		rej()
		      }
		    }
		);
	})
	return p
}

export function postToFacebook(groupid, newpost){
  	const p = new Promise((res, rej)=>{
  		FB.api(
          "/" + groupid + "/feed",
          "POST",
	      {access_token: localStorage.getItem('fbToken')},
          {
              "message": newpost
          },
          function (response) {
            if (response && !response.error) {
              res(response);
            }else{
              rej("Failed to post! Did you join the group first?");
            }
          }
      	)
  	})
  	return p
}


export function convertTokenIntoLongLived(accessToken){
	const p = new Promise((res, rej)=>{
		axios.post(TENANT_MICROSERVICE+'/longlivetoken', {accessToken})
			.then((longToken)=>{
				// console.log(longToken)
				res(longToken.data.longLiveToken)
			})
			.catch((err)=>{
				// console.log(err)
				rej(err)
			})
	})
	return p
}
