import React from 'react';

export const sortIconsSublet = (sublet) => {
	const iconArray = []
	if(sublet.rooms_left){
		iconArray.push(
			<div key="rooms_left" style={iconStyle}>
				<i className='ion-person'></i> &nbsp;
				{sublet.rooms_left} Rooms
			</div>
		)
	}
	if(sublet.ensuite_incl){
		iconArray.push(
			<div key="ensuite_incl" style={iconStyle}>
				<i className='ion-cube'></i> &nbsp;
				Ensuite Bath
			</div>
		)
	}
	if(sublet.utils_incl){
		iconArray.push(
			<div key="utils_incl" style={iconStyle}>
				<i className='ion-outlet'></i> &nbsp;
				Utilities
			</div>
		)
	}
	if(sublet.phone){
		iconArray.push(
			<div key="phone" style={iconStyle}>
				<i className='ion-ios-telephone'></i> &nbsp;
				{sublet.phone}
			</div>
		)
	}
	if(sublet.female_only){
		iconArray.push(
			<div key="female_only" style={iconStyle}>
				<i className='ion-female'></i> &nbsp;
				Female Only
			</div>
		)
	}
	if(sublet.semester){
		iconArray.push(
			<div key="semester" style={iconStyle}>
				<i className='ion-calendar'></i> &nbsp;
				{sublet.semester.charAt(0).toUpperCase() + sublet.semester.slice(1)}
			</div>
		)
	}
	return iconArray
}

const iconStyle = {
	margin: "2%",
	display: "inline-block"
}

const leaseText = {
	fontSize: "1.1rem",
	display: "inline-block"
}

export const sortIconsLease = (lease) => {
	const iconArray = []
	if(lease.utils_list.water){
		iconArray.push(
			<div style={iconStyle}>
				<i className='ion-waterdrop'></i> &nbsp;
				<span style={leaseText}>Hydro</span>
			</div>
		)
	}
	if(lease.utils_list.heat){
		iconArray.push(
			<div style={iconStyle}>
				<i className='ion-flame'></i> &nbsp;
				<span style={leaseText}>Heat</span>
			</div>
		)
	}
	if(lease.utils_list.electric){
		iconArray.push(
			<div style={iconStyle}>
				<i className='ion-flash'></i> &nbsp;
				<span style={leaseText}>Electricity</span>
			</div>
		)
	}
	if(lease.utils_list.furnished){
		iconArray.push(
			<div style={iconStyle}>
				<i className='ion-home'></i> &nbsp;
				<span style={leaseText}>Furniture</span>
			</div>
		)
	}
	if(lease.utils_list.parking || lease.utils_list.free_parking){
		if(lease.utils_list.parking && !lease.utils_list.free_parking){
			iconArray.push(
				<div style={iconStyle}>
					<i className='ion-model-s'></i> &nbsp;
					<span style={leaseText}>Parking</span>
				</div>
			)
		}else if(lease.utils_list.parking && lease.utils_list.free_parking){
			iconArray.push(
				<div style={iconStyle}>
					<i className='ion-model-s'></i> &nbsp;
					<span style={leaseText}>Free Parking</span>
				</div>
			)
		}
	}
	if(lease.utils_list.internet){
		iconArray.push(
			<div style={iconStyle}>
				<i className='ion-wifi'></i> &nbsp;
				<span style={leaseText}>Internet</span>
			</div>
		)
	}
	if(lease.utils_list.ac){
		iconArray.push(
			<div style={iconStyle}>
				<i className='ion-ios-snowy'></i> &nbsp;
				<span style={leaseText}>A/C</span>
			</div>
		)
	}
	if(lease.utils_list.laundry){
		iconArray.push(
			<div style={iconStyle}>
				<i className='ion-tshirt'></i> &nbsp;
				<span style={leaseText}>Laundry</span>
			</div>
		)
	}
	return iconArray
}

// to shorten a long street address by removing city and postal code
export function shortenAddress(address){
	const comma = address.indexOf(',')
	if(comma > 7){
		return address.slice(0, comma)
	}else{
		const nextAddr = address.slice(comma+1, address.length-1)
		const nextComma = nextAddr.indexOf(',')
		return address.slice(0, comma+nextComma+1)
	}
}

// filter out any files that are not .png or .jpg/.jpeg
export function filterNonImages(acceptedFiles){
	// console.log(acceptedFiles)
	const filteredFiles = acceptedFiles.filter((file)=>{
		const parts = file.name.split('.')
		const extension = parts[parts.length-1].toLowerCase()
		if(extension == "jpg" || extension == "jpeg" || extension == "png"){
			return true
		}else{
			return false
		}
	})
	// console.log(filteredFiles)
	return filteredFiles
}


// to randomize the order of an array
export function shuffleArray(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}


export function countWordLimit(text){
	let overLimit = false
	const charCount = text.length
	const wordCount = text.split(" ").length
	if(charCount > 1200){
		overLimit = "Max 1200 characters. You are at " + charCount
	}
	if(wordCount > 200){
		overLimit = "Max 200 words. You are at " + wordCount
	}
	return overLimit
}


// this function should only return a sorted list if all items are sublets (cannot mix with leases)
export function sortBySubletDate(arrayOfProperties){
	const result = arrayOfProperties.sort((a, b)=>{
		if(a.created_time_unix > b.created_time_unix){
			return -1
		}else{
			return 1
		}
	})
	return result
}
