import axios from 'axios'
import { LANDLORD_MICROSERVICE } from './API_URLS'
import {sendAWSClaimEmail} from './aws/aws-SES'

export function getLandlord(email){
	const jwtConfig = {headers: {"jwt": localStorage.getItem("cognito_landlord_token")}}
	const p = new Promise((res, rej)=>{
		axios.post(LANDLORD_MICROSERVICE+"/get_landlord", {email}, jwtConfig)
			.then((data, err)=>{
				if(err){
					// console.log(err)
				}
				res(data.data)
			})
			.catch((err)=>{
				// console.log(err)
				rej(err)
			})
	})
	return p
}


export function computeForRegion(landlord, extractedAttribute){
	let returnedValue = "Default (see settings)"
	if(extractedAttribute == 'default_email'){
		if(landlord.default_email && landlord.default_email != "" || landlord.default_email != " "){
			returnedValue = landlord.default_email
		}else{
			returnedValue = "Default Email (see settings)"
		}
	}else if(extractedAttribute == 'default_phone'){
		if(landlord.default_phone && landlord.default_phone != "" || landlord.default_phone != " "){
			returnedValue = landlord.default_phone
		}else{
			returnedValue = "Default Phone (see settings)"
		}
	}else if(extractedAttribute == 'city'){
		returnedValue = "waterloo"
	}
	return returnedValue
}


export function saveLandlordPropertyToDB(property){
	const jwtConfig = {headers: {"jwt": localStorage.getItem("cognito_landlord_token")}}
	const p = new Promise((res, rej)=>{
		axios.post(LANDLORD_MICROSERVICE+"/submit_new_lease", property, jwtConfig)
			.then((data)=>{
				res(data.data)
			})
			.catch((err)=>{
				rej(err)
			})
	})
	return p
}

export function updateLandlordPropertyInDB(property){
	const jwtConfig = {headers: {"jwt": localStorage.getItem("cognito_landlord_token")}}
	const p = new Promise((res, rej)=>{
		axios.post(LANDLORD_MICROSERVICE+"/update_lease", property, jwtConfig)
			.then((data)=>{
				res(data.data)
			})
			.catch((err)=>{
				rej(err)
			})
	})
	return p
}

export function getLeasesForLandlord(landlordId){
	const jwtConfig = {headers: {"jwt": localStorage.getItem("cognito_landlord_token")}}
	// console.log("Getting leases for landlord...", landlordId)
	// console.log(jwtConfig)
	const p = new Promise((res, rej)=>{
		axios.post(LANDLORD_MICROSERVICE+"/get_leases_for_landlord", {landlordId}, jwtConfig)
			.then((data)=>{
				// console.log("Got leases from landlord!")
				// console.log(data)
				res(data.data)
			})
	})
	return p
}

export function deletePropertyFromDb(propertyId){
	const jwtConfig = {headers: {"jwt": localStorage.getItem("cognito_landlord_token")}}
	// console.log(propertyId)
	const p = new Promise((res, rej)=>{
		axios.post(LANDLORD_MICROSERVICE+"/delete_lease", {propertyId}, jwtConfig)
			.then((data)=>{
				res(data.data)
			})
	})
	return p
}

export function toggleActiveStatusOfProperty(property){
	const params = {
		propertyId: property._id,
		boolValue: !property.active
	}
	const jwtConfig = {headers: {"jwt": localStorage.getItem("cognito_landlord_token")}}
	const p = new Promise((res, rej)=>{
		axios.post(LANDLORD_MICROSERVICE+"/toggle_lease", params, jwtConfig)
			.then((data)=>{
				res(data.data)
			})
	})
	return p
}


export function handleClaimClick(lease){
	const p = new Promise((res, rej)=>{
		createClaim(lease.contacts[0].email, lease._id, lease.address)
			.then((claim)=>{
				// console.log(claim)
				return sendAWSClaimEmail(claim)
			})
			.then(()=>{
				alert("A confirmation email has been sent to "+ lease.contacts[0].email + ". Follow the instructions on the email to claim this property.")
				res()
			})
			.catch((err)=>{
				console.log(err.message)
				rej(err)
			})
	})
	return p
}

function createClaim(email_confirm, lease_id, address){
	const p = new Promise((res, rej)=>{
		axios.post(LANDLORD_MICROSERVICE+"/create_claim", {email_confirm, lease_id, address: address})
			.then((data)=>{
				if(data.data.success){
					res(data.data.claim)
				}else{
					rej(data.data)
				}
			})
	})
	return p
}

export function claimProperty(claimId, landlordId){
	const p = new Promise((res, rej)=>{
		axios.post(LANDLORD_MICROSERVICE + "/check_claim", {claimId, landlordId})
			.then((data)=>{
				// console.log(data)
				if(data.data.success){
					res(data.data.lease)
				}else{
					rej(data.data.message)
				}
			})
			.catch((err)=>{
				console.log(err)
				rej(err)
			})
	})
	return p
}
