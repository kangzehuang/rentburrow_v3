import axios from 'axios'
import { TENANT_MICROSERVICE } from './API_URLS'
import { BeginParsingChain, extractAddress, extractFemalesOnly, extractPrice, extractRoomsLeft, extractUtilsIncl, extractEnsuite, extractSemester, extractPhone } from './subletExtractor/nlp'
import {shuffleArray} from './iconSorter'

export function attemptLoginFromLocalCache(){
	// console.log(city)
	const p = new Promise((res, rej)=>{
		const email = localStorage.getItem('email');
		const token = localStorage.getItem('token');
		axios.post(LANDLORD_MICROSERVICE+"/check_login_token")
			.then((data)=>{
				// console.log(data)
				res()
			})
	})
	return p
}

export function getSubletsFromDB(city){
	// console.log(city)
	const p = new Promise((res, rej)=>{
		axios.post(TENANT_MICROSERVICE+"/get_sublets", city)
			.then((sublets)=>{
				res(sublets.data)
			})
	})
	return p
}

export function getLeasesFromDB(city){
	const p = new Promise((res, rej)=>{
		axios.post(TENANT_MICROSERVICE+"/get_leases", city)
			.then((leases)=>{
				const shuffledLeases = shuffleArray(leases.data)
				res(shuffledLeases)
			})
	})
	return p
}

export function getCitiesFromDB(){
	const p = new Promise((res, rej)=>{
		axios.get(TENANT_MICROSERVICE+"/get_cities")
			.then((data)=>{
				res(data.data)
			})
	})
	return p
}

export function parseTextToSublet(text, city_name, fbUser){
	const dummyPost = {
		id: "",
		updated_time: "",
		updated_at: new Date(),
		message: text,
		city: city_name,
		groupid: "",
		userid: fbUser.id,
		username: fbUser.name,
		userurl: "http://facebook.com/"+fbUser.id,
		userpic: fbUser.picurl
	}
	return BeginParsingChain(dummyPost)
			.then(extractAddress)
			.then(extractFemalesOnly)
			.then(extractPrice)
			.then(extractRoomsLeft)
			.then(extractUtilsIncl)
			.then(extractEnsuite)
			.then(extractPhone)
			.then(extractSemester)
			.then((data)=>{
				return data
			})
}


export function getSpecificLease(leaseId){
	const p = new Promise((res, rej)=>{
		axios.post(TENANT_MICROSERVICE+"/specific_lease", {leaseId})
			.then((data)=>{
				res(data.data)
			})
			.catch((err)=>{
				rej(err)
			})
	})
	return p
}


export function getSpecificSublet(subletId){
	const p = new Promise((res, rej)=>{
		axios.post(TENANT_MICROSERVICE+"/specific_sublet", {subletId})
			.then((data)=>{
				res(data.data)
			})
			.catch((err)=>{
				rej(err)
			})
	})
	return p
}

export function getReviewsForProperty(propertyId){
	const p = new Promise((res, rej)=>{
		axios.post(TENANT_MICROSERVICE+"/get_property_reviews", {propertyId})
			.then((data)=>{
				// console.log(data)
				res(data.data)
			})
			.catch((err)=>{
				// console.log(err)
				rej(err.message)
			})
	})
	return p
}

export function submitPropertyReview(review){
	review.scores = {
		priceValue: review.scores.priceValue,
		quality: review.scores.quality,
		location: review.scores.location,
	}
	const p = new Promise((res, rej)=>{
		axios.post(TENANT_MICROSERVICE+"/submit_property_review", review)
			.then((data)=>{
				// console.log(data)
				res("Review posted!")
			})
			.catch((err)=>{
				// console.log(err)
				rej(err.message)
			})
	})
	return p
}

export function submitLandlordReview(review){
	review.scores = {
		professionalism: review.scores.professionalism,
		effectiveness: review.scores.effectiveness,
		satisfaction: review.scores.satisfaction,
	}
	const p = new Promise((res, rej)=>{
		axios.post(TENANT_MICROSERVICE+"/submit_landlord_review", review)
			.then((data)=>{
				// console.log(data)
				res("Review posted!")
			})
			.catch((err)=>{
				// console.log(err)
				rej(err.message)
			})
	})
	return p
}

export function getReviewsAndPropertiesForLandlord(landlordId){
	const p = new Promise((res, rej)=>{
		axios.post(TENANT_MICROSERVICE+"/landlord_profile", {landlordId})
			.then((data)=>{
				// console.log(data)
				res(data.data)
			})
			.catch((err)=>{
				// console.log(err)
				rej(err.message)
			})
	})
	return p
}

export function checkIfLandlordExists(landlordId){
	const p = new Promise((res, rej)=>{
		axios.post(TENANT_MICROSERVICE + "/checkIfLandlordExists", {landlordId})
			.then((data)=>{
				// console.log(data)
				if(data.data.exists){
					res(true)
				}else{
					res(false)
				}
			})
			.catch((err)=>{
				rej(err.message)
			})
	})
	return p
}
