import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import moment from 'moment'
import {toggleMobileMap} from '../../actions/mobile_actions'
import {sortIconsSublet, shortenAddress} from '../../api/iconSorter'
import {panToMap} from '../../actions/map_actions'
import {triggerSubletPopup, exitPopup, popupImage, popupImageExit } from '../../actions/popup_actions'
import {goToWatchlistView} from '../../actions/tenant_actions'
import { xGreyText, xBootstrapRed } from '../../stylesJS/base_colors'
import {saveWatchlistsToState, setWatchlist} from '../../actions/watchlist_actions'
import {getWatchlistsForUser, addPropertyToWatchlist, removePropertyFromWatchlist} from '../../api/watchlistsManager'
import {setSubletCardIndex} from '../../actions/performance_actions'
import {trackActivity} from '../../actions/activity_actions'


class SubletCard extends Component {

	constructor(){
		super()
		this.state = {
			heartHover: false,
			heartSaved: false
		}
	}

	viewSubletDetails(bool, sublet){
		if(bool){
			this.props.triggerSubletPopup(sublet)
		}else{
			this.props.exitPopup()
		}
	}

	addToFavorites(bool){
		if(this.props.selectedWatchlist){
			if(bool){
				this.setState({
					heartSaved: false
				})
				removePropertyFromWatchlist({
					watchlist_id: this.props.selectedWatchlist._id,
					postId: this.props.sublet._id,
					type: "sublet"
				}).then((data)=>{
					this.props.trackActivity("SUBLET", "CARD_UNFAVORITED", this.props.sublet, this.props.fbUser, this.props.geoCoords)
					this.props.setWatchlist(data)
				})
			}else{
				this.setState({
					heartSaved: true
				})
				addPropertyToWatchlist({
					watchlist_id: this.props.selectedWatchlist._id,
					postId: this.props.sublet._id,
					type: "sublet"
				}).then((data)=>{
					this.props.trackActivity("SUBLET", "CARD_FAVORITED", this.props.sublet, this.props.fbUser, this.props.geoCoords)
					this.props.setWatchlist(data)
				})
			}
		}else{
			// console.log("Setting heart")
			localStorage.setItem('hearted', JSON.stringify({
				postId: this.props.sublet._id,
				type: "sublet"
			}))
			// console.log(localStorage.getItem('hearted'))
			this.props.trackActivity("SUBLET", "CARD_FAVORITED", this.props.sublet, this.props.fbUser, this.props.geoCoords)
			this.props.goToWatchlistView()
		}
	}

	renderHeart(){
		let alreadySaved = false
		if(this.props.selectedWatchlist){
			this.props.selectedWatchlist.sublets.forEach((sub)=>{
				if(sub == this.props.sublet._id){
					alreadySaved = true
				}
			})
		}
		if(this.state.heartSaved){
			alreadySaved = true
		}
		if(alreadySaved || this.state.heartHover || this.state.heartSaved){
			return (
				<i className='ion-ios-heart' onClick={()=>this.addToFavorites(alreadySaved)} style={comStyles().heartIcon} onMouseOver={()=>this.setState({heartHover: true})} onMouseOut={()=>this.setState({heartHover: false})}></i>
			)
		}else{
			return (
				<i className='ion-ios-heart-outline' onClick={()=>this.addToFavorites(alreadySaved)} style={comStyles().heartIcon} onMouseOver={()=>this.setState({heartHover: true})} onMouseOut={()=>this.setState({heartHover: false})}></i>
			)
		}
	}

	handleMap(){
		this.props.panToMap(this.props.sublet)
		this.props.trackActivity("SUBLET", "CARD_MAP_BUTTON", this.props.sublet, this.props.fbUser, this.props.geoCoords)
		if(this.props.mobileViewer){
			this.props.toggleMobileMap(true)
		}
	}

	handleOriginal(){
		this.viewSubletDetails(true, this.props.sublet)
		this.props.trackActivity("SUBLET", "CARD_ORIGINAL_BUTTON", this.props.sublet, this.props.fbUser, this.props.geoCoords)
		if(this.props.mobileViewer){
			this.props.toggleMobileMap(true)
		}
	}

	renderThumbnailGallery(){
		if(!this.props.mobileViewer){
			if(this.props.subletCardIndex-10 < this.props.index && this.props.index < this.props.subletCardIndex+10){
				return this.props.sublet.images.map((img)=>{
					return (
						<img key={img.id} src={img.url} onClick={()=>this.handleMobileImageClick(img)} onMouseOver={()=>{this.props.popupImage(img.url)}} onMouseOut={this.props.popupImageExit} style={comStyles().thumbnail} />
					)
				})
			}else{
				if(this.props.sublet.images.length > 0){
					return (<div style={comStyles().blankPlaceholderImage}></div>)
				}
			}
		}else{
			return this.props.sublet.images.map((img)=>{
				return (
					<img key={img.id} src={img.url} onClick={()=>this.handleMobileImageClick(img)} onMouseOver={()=>{this.props.popupImage(img.url)}} onMouseOut={this.props.popupImageExit} style={comStyles().thumbnail} />
				)
			})
		}
	}

	handleMobileImageClick(img){
		this.props.popupImage(img.url)
		if(this.props.mobileViewer){
			this.props.toggleMobileMap(true)
		}
	}

	subletCardHover(){
		this.props.setSubletCardIndex(this.props.index)
		this.props.trackActivity("SUBLET", "CARD_ON_HOVER", this.props.sublet, this.props.fbUser, this.props.geoCoords)
	}

	subletContacted(){
		this.props.trackActivity("SUBLET", "SUBLET_CONTACTED", this.props.sublet, this.props.fbUser, this.props.geoCoords)
	}

	render() {
		return (
			<div key={this.props.sublet._id} style={comStyles().card} onMouseEnter={this.subletCardHover.bind(this)}>

				<div id='infobar' style={comStyles().infobar}>
					{/* Profile Picture */}
					<a href={this.props.sublet.userurl} target='_blank' onClick={this.subletContacted.bind(this)}>
						<div className='img-circle' style={profileStyle(this.props.sublet.userpic).profilePic}></div>
					</a>

					<div id='infobadge' style={comStyles().infobadge}>
						{/* Address */}
						<div style={comStyles().address}>
							{shortenAddress(this.props.sublet.address)}
						</div>
						{/* User Name */}
						<div style={comStyles().userinfo}>
							<a href={this.props.sublet.userurl} target="_blank" onClick={this.subletContacted.bind(this)}>{this.props.sublet.username}</a> &nbsp;
							on &nbsp;
							<b>{moment(this.props.sublet.created_time).format("MMM Do")}</b>
						</div>
					</div>

					{this.renderHeart()}
				</div>

				{/* Price */}
				<div style={comStyles().pricediv}>
					<div style={comStyles().price}>$ {this.props.sublet.price}</div>
				</div>

				{/* Icons */}
				<div id='iconbar' style={comStyles().iconbar}>
					{sortIconsSublet(this.props.sublet)}
				</div>

				{/* Buttons Bar */}
				<div id='buttonsBar' style={comStyles().buttonsBar}>
					<a href={this.props.mobileViewer?null:this.props.sublet.posturl} target={this.props.mobileViewer?null:'_blank'} style={comStyles().seeOriginalAhref}>
						<button className="btn btn-info" onClick={this.handleOriginal.bind(this)} style={comStyles().seeOriginal} onMouseOver={this.props.mobileViewer?null:()=>this.viewSubletDetails(true, this.props.sublet)} onMouseOut={this.props.mobileViewer?null:()=>this.viewSubletDetails(false)} >See Original</button>
					</a>
					<button className="btn btn-danger" onClick={this.handleMap.bind(this)} style={comStyles().map}>
						<i className='ion-ios-location' style='font-size:1em;'></i> &nbsp;
						Map
					</button>
				</div>

				<div className='thumbnailBar' style={comStyles().thumbnailBar}>
					{this.renderThumbnailGallery()}
				</div>

			</div>
		);
	}
}

SubletCard.propTypes = {
	sublet: React.PropTypes.object.isRequired,	// passed in
	selectedWatchlist: React.PropTypes.object,
	fbUser: React.PropTypes.object.isRequired,
	geoCoords: React.PropTypes.array,
	filteredResults: React.PropTypes.array,
	selectedPins: React.PropTypes.array,
	watchlistView: React.PropTypes.bool,
	mobileViewer: React.PropTypes.bool,
	index: React.PropTypes.number,						// passed in
	subletCardIndex: React.PropTypes.number
};

const RadiumHOC = Radium(SubletCard);

function mapStateToProps(state){
	return {
		selectedWatchlist: state.tenant.selectedWatchlist,
		fbUser: state.tenant.fbUser,
		geoCoords: state.app.geoCoords,
		filteredResults: state.content.filteredResults,
		selectedPins: state.content.selectedPins,
		watchlistView: state.tenant.watchlistView,
		mobileViewer: state.app.mobileViewer,
		subletCardIndex: state.perform.subletCardIndex
	}
}

export default connect(mapStateToProps, {panToMap, triggerSubletPopup, exitPopup, goToWatchlistView, saveWatchlistsToState, setWatchlist, toggleMobileMap, popupImage, popupImageExit, setSubletCardIndex, trackActivity})(RadiumHOC);


// ==============================

const comStyles = () => {
	return {
		card: {
			margin: "20px",
			padding: "3%",
			borderRadius: "5px",
			backgroundColor: "white",
		},
		infobar: {
			width: "100%",
			display: "flex",
			justifyContent: "flex-start",
			alignItems: "flex-start",
			flexDirection: "row",
			padding: "10px",
		},
		infobadge: {
			display: "flex",
			flexDirection: "column",
			margin: "1% 1% 1% 5%",
			width: "70%"
		},
		address: {
			fontSize:"1.5rem",
			fontWeight:"bold",
			width: "100%",
			height:"60%",
			color: xGreyText,
		},
		userinfo: {
			width: "100%",
			height: "35%",
			color: xGreyText,
			fontSize: "1.1rem",
			margin: "5px 0px 0px 0px"
		},
		pricediv: {
			textAlign:"center",
			padding: "30px",
			display: "flex",
			flexDirection: "row",
			justifyContent: "center",
			position: 'relative',
		},
		price: {
			fontSize:"2.5rem",
			fontWeight:"bold",
			color: xGreyText,
			width: "100%",
		},
		imageTile: {
			position: 'absolute',
			right: "0px",
			margin: "0 auto",
		},
		imageTileIcon: {
			fontSize: "1.7rem",
			margin: "0 auto",
			cursor: "pointer",
			":hover": {
				color: xBootstrapRed
			}
		},
		iconbar: {
			width: "100%",
			display: "flex",
			justifyContent: "center",
			fontSize: "1rem",
			color: xGreyText,
			flexWrap: "wrap"
		},
		buttonsBar: {
			display: "flex",
			justifyContent: "center",
			width: "100%",
			margin: "auto",
			fontSize: "1.1rem",
			fontWeight: "bold"
		},
		seeOriginalAhref: {
			flexGrow: 3
		},
		seeOriginal: {
			borderRadius: "0px",
			width: "100%"
		},
		map: {
			borderRadius: "0px",
			flexGrow: 1
		},
		heartIcon: {
			fontSize: "2rem",
			fontWeight: "bold",
			right: "0px",
			width: "10%",
			color: xBootstrapRed,
			cursor: "pointer"
		},
		thumbnailBar: {
			display: "flex",
			flexDirection: "row",
			justifyContent: "center",
			maxHeight: "30px",
			margin: "10px 0px 0px 0px",
			overflowX: "scroll"
		},
		thumbnail: {
			height: "30px",
			width: 'auto',
			opacity: "0.3",
			":hover": {
				opacity: "1"
			},
			"@media (max-width: 600px), and (max-height: 740px)":{
				opacity: "1"
			}
		},
		blankPlaceholderImage: {
			height: "30px",
			width: "100%"
		}
	}
}


const profileStyle = (src) => {
	return {
		profilePic: {
			height: "70px",
			width: "70px",
		    borderRadius:'50%',
		    backgroundImage: "url("+src+")",
		    backgroundPosition: 'center',
		    backgroundSize:'100% auto',
		    backgroundRepeat: 'no-repeat',
			display: "inline-block",
		}
	}
}
