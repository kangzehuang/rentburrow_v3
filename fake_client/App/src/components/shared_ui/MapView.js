import React, {Component} from 'react'
import {connect} from 'react-redux'
import Radium from 'radium'
import {toggleMobileMap} from '../../actions/mobile_actions'
import { selectPinsFromHere, panToMap, unPanToMap } from '../../actions/map_actions'
import {goToListingsView} from '../../actions/tenant_actions'
import {trackActivity} from '../../actions/activity_actions'

class MapView extends Component {

	constructor(){
		super()
		this.state = {
			mapview: null
		}
		this.pins = []
		this.recolorRed.bind(this)
		this.highlightPin.bind(this)
		this.refreshPins.bind(this)
		this.displayPinOnSearchResults.bind(this)
	    this.loadResults.bind(this)
	}

	componentDidMount(){
		if(this.props.rootView){
			google.maps.event.addDomListener(window, "load", this.mountGoogleMap.bind(this))
		}else{
			this.mountGoogleMap()
		}
	}

	componentDidUpdate(){
		if(this.props.selectedPins){
			this.recolorRed()
		}
	    this.loadResults()
	    if(this.props.panTo){
	    	for(let p = 0; p<this.pins.length; p++){
	    		if(!this.props.panTo._id){
	    			let x = new google.maps.LatLng(this.props.panTo.coords[1], this.props.panTo.coords[0])
	    			this.state.mapview.panTo(x)
	    			break;
	    		}
	    		if(this.pins[p].position.lat().toFixed(7) == this.props.panTo.coords[1].toFixed(7) && this.pins[p].position.lng().toFixed(7) == this.props.panTo.coords[0].toFixed(7)){
	    			this.highlightPin(this.pins[p])
	    		}
	    	}
	    }
	}

	mountGoogleMap(){
		let coords = {lat: 43.473897, lng: -80.531995}
		if(this.props.preDefinedCoords){
			coords = this.props.preDefinedCoords
		}
		let mapOptions = {
	        center: coords,
	        zoom: 14
	    }
	    if(this.props.mobileViewer){
	    	mapOptions = {
	    		...mapOptions,
	    		disableDefaultUI: true
	    	}
	    }
		const mapview = new google.maps.Map(document.getElementById('mapview'), mapOptions)
	    this.setState({
	    	mapview: mapview
	    })
	    this.loadResults()
	}

	loadResults(){
		if(this.props.filteredResults){
			this.refreshPins(this.props.filteredResults)
		}else if(this.props.listOfResults){
			this.refreshPins(this.props.listOfResults)
		}
	}

	// map the pins on every update
	refreshPins(newPins){
		const self = this
		this.clearPins()
		newPins.forEach((n,i)=>{
			let marker
			if(n.type && n.type=='Apartment'){
              // if apartment, use this pin
              marker = new google.maps.Marker({
                  position: new google.maps.LatLng(n.coords[1], n.coords[0]),
                  //map: self.state.mapview,
                  icon: "../../../res/images/orange-dot.png",
                  prop_type: "Apartment"
              })
              marker.cardId = n._id
            }else{
              // otherwise create a regular marker for each pin
              marker = new google.maps.Marker({
                  position: new google.maps.LatLng(n.coords[1], n.coords[0]),
                  //map: self.state.mapview,
                  icon: "../../../res/images/red-dot.png"
              });
              marker.cardId = n._id
            }
            // action on click of pin
            google.maps.event.addListener(marker, 'click', function(e){
            	if(!self.props.watchlistView && !self.props.selectedLandlord){
            		self.props.goToListingsView()
            	}
			    		// Delete the panTo from Redux state (as it is called on componentDidMount, thus overriding onClick of map marker)
							if(self.props.panTo){
								self.props.unPanToMap()
							}
	            // set the clicked post as a blue marker
	            self.displayPinOnSearchResults(n)
							// track the action
							let type
							if(n.company){
								type = 'LEASE'
							}else{
								type = 'SUBLET'
							}
							self.props.trackActivity(type, "PIN_CLICK", n, self.props.fbUser, self.props.geoCoords)
							// delay to switch screen views on mobile
	            if(self.props.mobileViewer){
	            	setTimeout(()=>{
	            		self.props.toggleMobileMap(false)
	            	}, 250)
	            }
            });
            self.pins.push(marker)
		})
		self.pins.forEach((pin, index)=>{
			pin.setMap(self.state.mapview)
		})
	}

	clearPins(){
		if(this.pins){
			this.pins.forEach((pin)=>{
				pin.setMap(null)
			})
			this.pins.length = 0
		}
	}

	highlightPin(marker){
		this.recolorRed()
		// and color the desired marker blue
	    for(let m = 0; m<this.pins.length; m++){
	        if(this.pins[m].position.lat().toFixed(7) == marker.position.lat().toFixed(7) && this.pins[m].position.lng().toFixed(7) == marker.position.lng().toFixed(7)){
	          this.pins[m].setIcon("../../../res/images/blue-dot.png")
	        }
	    }
	    // pan the map view to blue marker
	    this.state.mapview.panTo(marker.position)
	}

	recolorRed(){
		if(this.pins){
			for(let m = 0; m<this.pins.length; m++){
		        if(this.pins[m].prop_type=='Apartment'){
		          this.pins[m].setIcon("../../../res/images/orange-dot.png");
		        }else{
		          this.pins[m].setIcon("../../../res/images/red-dot.png");
		        }
		    }
		}
	}

	displayPinOnSearchResults(match){
		let dispayThesePins
		if(this.props.watchlistView){
			dispayThesePins = this.props.filteredResults.filter((pin)=>{
				return pin.coords[0] == match.coords[0] && pin.coords[1] == match.coords[1]
			})
		}else{
			dispayThesePins = this.props.listOfResults.filter((pin)=>{
				return pin.coords[0] == match.coords[0] && pin.coords[1] == match.coords[1]
			})
		}
		this.props.selectPinsFromHere(dispayThesePins)
		this.props.panToMap(dispayThesePins[0])
	}

	render() {
		return (
			<div id="mapview" style={comStyles().mapview}></div>
		)
	}
}

MapView.propTypes = {
	listOfResults: React.PropTypes.array,
	filteredResults: React.PropTypes.array,
	selectedPins: React.PropTypes.array,
	panTo: React.PropTypes.object,
	rootView: React.PropTypes.bool, // determines if this mapview is loaded from the rootview or not
	preDefinedCoords: React.PropTypes.object, 	// determines if this mapview is loaded with predefined coords to view at
	watchlistView: React.PropTypes.bool,
	mobileViewer: React.PropTypes.bool,
	selectedLandlord: React.PropTypes.object,
	fbUser: React.PropTypes.object,
	geoCoords: React.PropTypes.array
}

const RadiumHOC = Radium(MapView)

function mapStateToProps(state){
	return {
		listOfResults: state.content.listOfResults,
		filteredResults: state.content.filteredResults,
		selectedPins: state.content.selectedPins,
		panTo: state.content.panTo,
		watchlistView: state.tenant.watchlistView,
		mobileViewer: state.app.mobileViewer,
		selectedLandlord: state.tenant.selectedLandlord,
		fbUser: state.tenant.fbUser,
		geoCoords: state.app.geoCoords
	}
}

export default connect(mapStateToProps, {selectPinsFromHere, panToMap, unPanToMap, goToListingsView, toggleMobileMap, trackActivity})(RadiumHOC)


// =====================================
const comStyles = () => {
	return {
		mapview: {
			width: "100%",
			height: "100%"
		}
	}
}
