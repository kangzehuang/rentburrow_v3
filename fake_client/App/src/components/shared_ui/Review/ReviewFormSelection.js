import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import ReviewPropertyForm from './ReviewPropertyForm'
import ReviewLandlordForm from './ReviewLandlordForm'

class ReviewFormSelection extends Component {

	constructor(){
		super()
		this.state = {
			selected: false,
			selection: ""
		}
	}

	renderForm(){
		if(this.state.selection == "property"){
			return (<ReviewPropertyForm exitForm={this.props.exitForm} />)
		}else if(this.state.selection == "landlord"){
			return (<ReviewLandlordForm exitForm={this.props.exitForm} />)
		}
	}

	selectForReview(selection){
		// console.log(selection)
		this.setState({
			selected: true,
			selection: selection
		})
	}

	render() {
		return (
			<div style={comStyles().mainview}>
				{
					this.state.selected
					?
					<div>
						{this.renderForm()}
					</div>
					:
					<div style={comStyles().selectionBox}>
						<button className='btn btn-primary' onClick={()=>this.selectForReview('property')} style={comStyles().reviewSelect}>
							RATE THE PROPERTY
						</button>
						<button className='btn btn-primary' onClick={()=>this.selectForReview('landlord')} style={comStyles().reviewSelect}>
							RATE THE LANDLORD
						</button>
					</div>
				}
			</div>
		);
	}
}

ReviewFormSelection.propTypes = {
	exitForm: React.PropTypes.func.isRequired		// passed in
};

const RadiumHOC = Radium(ReviewFormSelection);

function mapStateToProps(state){
	return {
	}
}

export default connect()(RadiumHOC);

// ==========================

const comStyles = () => {
	return {
		mainview: {
			width: "100%",
			height: "100%",
			margin: "10px 0px 0px 0px"
		},
		selectionBox: {
			minHeight: "250px",
			display: "flex",
			flexDirection:"row",
			alignItems: "stretch"
		},
		reviewSelect: {
			width: "50%",
			height: "100%",
			minHeight: "250px",
			fontSize: "1rem",
			fontWeight: "bold"
		}
	}
}

