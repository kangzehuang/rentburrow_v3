import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import {exitPopup} from '../../../actions/popup_actions'
import {xWhiteSmoke, xGreyText, xStarYellow,xDeepBlue} from '../../../stylesJS/base_colors'
import {submitLandlordReview} from '../../../api/tenantManager'
import {countWordLimit} from '../../../api/iconSorter'
import {trackActivity} from '../../../actions/activity_actions'

class ReviewLandlordForm extends Component {

	constructor(){
		super()
		this.state = {
			address: "",
			coords: [],
			property_id: "",
			landlord_name: "",
			landlord_id: "",
			landlord_logo: "",
			landlord_url: "",
			text: "",
			scores: {
				professionalism: 3,				// how fast do they respond
				effectiveness: 3,				// do they fix the problem
				satisfaction: 3,				// overall satisfaction
				professionalism_hover: 0,
				effectiveness_hover: 0,
				satisfaction_hover: 0
			},
			reviewer_name: "",
			reviewer_img: "",
			reviewer_id: "",
			successMessage: "",
			errorMessage: ""
		}
	}

	componentWillMount(){
		if(this.props.leaseForReview){
			this.setState({
				address: this.props.leaseForReview.address,
				coords: this.props.leaseForReview.coords,
				property_id: this.props.leaseForReview._id,
				landlord_name: this.props.leaseForReview.company,
				landlord_id: this.props.leaseForReview.landlord_id,
				landlord_logo: this.props.leaseForReview.thumbnail,
				landlord_url: this.props.leaseForReview.url,
				reviewer_name: this.props.fbUser.name,
				reviewer_img: this.props.fbUser.picurl,
				reviewer_id: this.props.fbUser.id
			})
		}else{
			this.setState({
				address: "No Property Specified",
				coords: [],
				property_id: "No Property Specified",
				landlord_name: this.props.landlordForReview.company,
				landlord_id: this.props.landlordForReview.landlord_id,
				landlord_logo: this.props.landlordForReview.thumbnail,
				landlord_url: this.props.landlordForReview.url,
				reviewer_name: this.props.fbUser.name,
				reviewer_img: this.props.fbUser.picurl,
				reviewer_id: this.props.fbUser.id
			})
		}
	}

	handleChange(attr, event){
		this.setState({
			[attr]: event.target.value,
			successMessage: "",
			errorMessage: ""
		})
	}

	rateStar(topic, rating){
		this.setState({
			scores: {
				...this.state.scores,
				[topic]: rating
			}
		})
	}

	colorStar(topic, rating){
		const topic_hover = topic+"_hover"
		this.setState({
			scores: {
				...this.state.scores,
				[topic_hover]: rating
			}
		})
	}

	determineIcon(topic, rating){
		let emptyOrFilledStar = 'ion-ios-star-outline'
		const topic_hover = topic+"_hover"
		// if hover = 0, then follow set rating
		if(this.state.scores[topic_hover] == 0){
			if(this.state.scores[topic] >= rating){
				emptyOrFilledStar = 'ion-ios-star'
			}
		// or else follow hover's rating
		}else{
			if(this.state.scores[topic_hover] >= rating){
				emptyOrFilledStar = 'ion-ios-star'
			}
		}
		return emptyOrFilledStar
	}

	deselectHovers(topic){
		const topic_hover = topic + "_hover"
		this.setState({
			scores: {
				...this.state.scores,
				[topic_hover]: 0
			}
		})
	}

	generateStarbox(topic){
		return (
			<div onMouseOut={()=>this.deselectHovers(topic)} >
				<i id='star1' className={this.determineIcon(topic, 1)} onMouseOver={()=>this.colorStar(topic, 1)} onClick={()=>this.rateStar(topic, 1)} style={starStyles(this.state, topic, 1).star} />
				<i id='star2' className={this.determineIcon(topic, 2)} onMouseOver={()=>this.colorStar(topic, 2)} onClick={()=>this.rateStar(topic, 2)} style={starStyles(this.state, topic, 2).star} />
				<i id='star3' className={this.determineIcon(topic, 3)} onMouseOver={()=>this.colorStar(topic, 3)} onClick={()=>this.rateStar(topic, 3)} style={starStyles(this.state, topic, 3).star} />
				<i id='star4' className={this.determineIcon(topic, 4)} onMouseOver={()=>this.colorStar(topic, 4)} onClick={()=>this.rateStar(topic, 4)} style={starStyles(this.state, topic, 4).star} />
				<i id='star5' className={this.determineIcon(topic, 5)} onMouseOver={()=>this.colorStar(topic, 5)} onClick={()=>this.rateStar(topic, 5)} style={starStyles(this.state, topic, 5).star} />
			</div>
		)
	}

	postReview(){
		if(this.state.text != ""){
			if(!countWordLimit(this.state.text)){
				submitLandlordReview(this.state)
					.then((msg)=>{
						this.setState({
							successMessage: msg,
							errorMessage:""
						})
						this.props.trackActivity("LANDLORD", "LANDLORD_REVIEW_POSTED", {id: this.state.landlord_id}, this.props.fbUser, this.props.geoCoords, this.state)
						this.props.getReviewsAndProperties()
						setTimeout(()=>{
							this.props.toggleReviewForm(false)
						}, 500)
					})
					.catch((err)=>{
						this.setState({
							successMessage: "",
							errorMessage: err
						})
					})
			}else{
				this.setState({
					successMessage: "",
					errorMessage: countWordLimit(this.state.text)
				})
				setTimeout(()=>{
					this.setState({errorMessage: ""})
				}, 2000)
			}
		}else{
			this.setState({
				successMessage: "",
				errorMessage: "Please type something!"
			})
			setTimeout(()=>{
				this.setState({errorMessage: ""})
			}, 2000)
		}
	}

	render() {
		return (
			<div id='ReviewLandlordFormBox' style={comStyles().form}>
				<textarea style={comStyles().reviewTextForm} value={this.state.text} onChange={this.handleChange.bind(this, "text")} placeholder='Post your honest review here.'></textarea>
				<div style={comStyles().topicsBox}>
					<div style={comStyles().topicBox}>
						<div style={comStyles().topic}>
							<div style={comStyles().topicTitle}>
								Professionalism
							</div>
							<div style={comStyles().topicInfo}>
								How professional does the landlord respond and act?
							</div>
						</div>
						<div style={comStyles().starsbox}>
							{this.generateStarbox('professionalism')}
						</div>
					</div>
					<div style={comStyles().topicBox}>
						<div style={comStyles().topic}>
							<div style={comStyles().topicTitle}>
								Effectiveness
							</div>
							<div style={comStyles().topicInfo}>
								How effective is the landlord at resolving issues?
							</div>
						</div>
						<div style={comStyles().starsbox}>
							{this.generateStarbox('effectiveness')}
						</div>
					</div>
					<div style={comStyles().topicBox}>
						<div style={comStyles().topic}>
							<div style={comStyles().topicTitle}>
								Satisfaction
							</div>
							<div style={comStyles().topicInfo}>
								How is your overall satisfaction with this landlord?
							</div>
						</div>
						<div style={comStyles().starsbox}>
							{this.generateStarbox('satisfaction')}
						</div>
					</div>
					{
						this.state.successMessage || this.state.errorMessage
						?
						<div className={this.state.successMessage ? 'alert alert-success' : 'alert alert-danger'} style={comStyles().resultMessage}>
							{this.state.successMessage ? this.state.successMessage : this.state.errorMessage}
						</div>
						:
						<button className='btn btn-block btn-info' onClick={this.postReview.bind(this)} style={comStyles().postButton}>Post Review</button>
					}
				</div>
			</div>
		);
	}
}

ReviewLandlordForm.propTypes = {
	leaseForReview: React.PropTypes.object,
	fbUser: React.PropTypes.object,
	geoCoords: React.PropTypes.array,
	exitForm: React.PropTypes.func,		// passed in
	landlordForReview: React.PropTypes.object,	// passed in
	toggleReviewForm: React.PropTypes.func, 	// passed in
	getReviewsAndProperties: React.PropTypes.func
};

const RadiumHOC = Radium(ReviewLandlordForm);

function mapStateToProps(state){
	return {
		leaseForReview: state.popup.leaseForReview,
		fbUser: state.tenant.fbUser,
		geoCoords: state.app.geoCoords
	}
}

export default connect(mapStateToProps, {exitPopup, trackActivity})(RadiumHOC);

// ==========================

const comStyles = () => {
	return {
		form: {
			display: "inline-block",
			display: "flex",
			flexDirection: "column"
		},
		reviewTextForm: {
			height: "250px",
			borderRadius: "10px",
			margin: "10px 10px 10px 10px",
			padding: "10px",
			backgroundColor: "rgba(256,256,256,0.3)",
			color: "black",
			"@media (max-width: 600px), and (max-height: 740px)":{
				width: "90%"
			}
		},
		topicsBox: {
			display: 'flex',
			flexDirection: 'column',
			margin: "10px 10px 10px 10px",
		},
		topicBox: {
			display: 'flex',
			flexDirection: 'row',
			height: "100%",
			alignItems: "center",
			padding: "5px"
		},
		topic: {
			display: 'flex',
			flexDirection: 'column',
			width: "50%",
			color: "white",
			"@media (max-width: 600px), and (max-height: 740px)":{
				width: "40%"
			}
		},
		topicTitle: {
			fontSize: '1.3rem',
			fontWeight: 'bold',
			"@media (max-width: 600px), and (max-height: 740px)":{
				fontSize: '1rem'
			}
		},
		topicInfo: {
			fontSize: '1rem',
			"@media (max-width: 600px), and (max-height: 740px)":{
				fontSize: '0.7rem'
			}
		},
		starsbox: {
			width: "50%",
			padding: "5px 5px 5px 10px",
			"@media (max-width: 600px), and (max-height: 740px)":{
				width: "60%"
			}
		},
		postButton: {
			fontWeight: "bold"
		},
		resultMessage: {
			width: "100%",
			margin: "10px auto",
		}
	}
}


const starStyles = (state, topic, rating) => {
	let color = "white"
	const topic_hover = topic+"_hover"
	// if hover = 0, then follow set rating
	if(state.scores[topic_hover] == 0){
		if(state.scores[topic] >= rating){
			color = xStarYellow
		}
	// or else follow hover's rating
	}else{
		if(state.scores[topic_hover] >= rating){
			color = xStarYellow
		}
	}
	return {
		star: {
			fontSize: "2rem",
			color: color,
			margin: "0px 2px 0px 2px",
			cursor: "pointer"
		}
	}
}
