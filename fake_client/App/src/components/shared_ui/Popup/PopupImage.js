import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'

import {xMidBlue} from '../../../stylesJS/base_colors'

class PopupImage extends Component {

	render() {
		return (
			<div style={comStyles(this.props.mobileViewer).popup}>
				<img src={this.props.imgForPopup} style={comStyles().img} />
			</div>
		);
	}
}

PopupImage.propTypes = {
	imgForPopup: React.PropTypes.string.isRequired,
	mobileViewer: React.PropTypes.bool
};

const RadiumHOC = Radium(PopupImage);

function mapStateToProps(state){
	return {
		imgForPopup: state.popup.imgForPopup,
		mobileViewer: state.app.mobileViewer
	}
}

export default connect(mapStateToProps)(RadiumHOC)

// =====================

const comStyles = (mobileViewer) => {
	let width = "auto"
	let height = "auto"
	let top = "10%"
	let left = "15px"
	if(mobileViewer){
		width = "100vw"
		height = "100vh"
		top = "0px"
		left = "0px"
	}
	return {
		popup: {
			zIndex: "40",
			position: "absolute",
			width: width,
			height: height,
			border: "3px solid "+xMidBlue,
			top: top,
			left: left,
			display: "flex",
			alignItems: "center",
			justifyContent: "center",
			backgroundColor: xMidBlue
		},
		img: {
			width: "auto",
			maxWidth: "60vw",
			height: "80vh",
		}
	}
}
