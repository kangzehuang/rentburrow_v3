import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'

import Chart from 'chart.js'


class Sandbox extends Component {

	constructor(props) {
    super(props);

    this.state = {
			graph: null,
			data1: [
				{
					x: 0,
					y: 0
				},
				{
					x: 2,
					y: 2
				},
				{
					x: 4,
					y: 3
				},
				{
					x: 6,
					y: 5
				},
				{
					x: 8,
					y: 8
				},
				{
					x: 10,
					y: 6
				}
			],
			data2: [
				{
					x: 0,
					y: 1
				},
				{
					x: 2,
					y: 5
				},
				{
					x: 4,
					y: 6
				},
				{
					x: 6,
					y: 4
				},
				{
					x: 8,
					y: 8
				},
				{
					x: 10,
					y: 6
				}
			]
    };
  }

	componentDidMount(){
		const ctx = document.getElementById("myChart").getContext("2d");
		const scatterChart = new Chart(ctx, {
		    type: 'line',
		    data: {
		        datasets: [
							{
		            label: 'Scatter Dataset 1',
		            data: this.state.data1
		        	},
							{
		            label: 'Scatter Dataset 2',
		            data: this.state.data2
		        	}
						]
		    },
		    options: {
		        scales: {
		            xAxes: [{
		                type: 'linear',
		                position: 'bottom'
		            }]
		        }
		    }
		});
	}

  handleValuesChange(component, values) {
    this.setState({
      values: values,
    });
  }

  render() {
    return (
      <div>
				SANDBOX
				<canvas id="myChart" style={comStyles().graph}></canvas>
			</div>
    );
  }
}

Sandbox.propTypes = {
};

const RadiumHOC = Radium(Sandbox);

function mapStateToProps(state){
	return {
	}
}

export default connect(mapStateToProps)(RadiumHOC);


// ==============================

const comStyles = () => {
	return {
		mainview: {
			width: "400px",
			margin: "50px"
		},
		graph: {
			maxHeight: "70vh",
			width: "80vw"
		}
	}
}
