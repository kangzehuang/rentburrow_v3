import React, { Component } from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import Header from './misc/header';
import {StyleRoot} from 'radium';
import { xMidBlue } from '../stylesJS/base_colors'
import {setMobileViewer} from '../actions/mobile_actions'
import {submitActivities} from '../api/aws/aws-dynamodb'
import {clearTrackedActivities} from '../actions/activity_actions'
import {setGeoCoords} from '../actions/map_actions'
import { getGeolocation } from '../api/mapManager'
import {setClaimId} from '../actions/landlord_actions'
import { updateTrackedActivities } from '../api/statisticsManager'

class App extends Component {

	componentWillMount(){
			getGeolocation().then((data)=>{
				// console.log(data)
				this.props.setGeoCoords(data)
			})
			this.getClaimFromURL().then((data)=>{
				this.props.setClaimId(data)
			})
	}

	getClaimFromURL(){
		const p = new Promise((res, rej)=>{
			const urlParams = this.props.location.query
			if(urlParams.claim){
				res(urlParams.claim)
			}else{
				rej()
			}
		})
		return p
	}

	componentDidMount(){

		if(screen.width <= 600 || screen.height <= 740){
			this.props.setMobileViewer()
		}

		setInterval(this.submitTrackedActivities.bind(this), 10000)

		window.onbeforeunload = this.submitTrackedActivities.bind(this)
	}

	submitTrackedActivities(){
		if(this.props.fbUser && this.props.trackedActivities.length > 0){
			// console.log(this.props.trackedActivities)
			let watchlistId = ""
			if(this.props.selectedWatchlist && this.props.selectedWatchlist._id){
				watchlistId = this.props.selectedWatchlist._id
			}
			const updatedActivities = updateTrackedActivities(this.props.trackedActivities, watchlistId)
			submitActivities(updatedActivities)
				.then(this.props.clearTrackedActivities)
		}
	}

	render() {
  	return (
  		<StyleRoot>
      		<div style={comStyles().app}>
      			{/*<Header />*/}
      			{this.props.children}
      		</div>
    		</StyleRoot>
  	)
	}
}

const RadiumHOC = Radium(App)

function mapStateToProps(state){
	return {
		trackedActivities: state.activity.trackedActivities,
		fbUser: state.tenant.fbUser,
		selectedWatchlist: state.tenant.selectedWatchlist
	}
}

export default connect(mapStateToProps, {setMobileViewer, clearTrackedActivities, setGeoCoords, setClaimId})(RadiumHOC)

// =============================

const comStyles = () => {
	return {
		app: {
			width: "100%",
			height: "100%",
			margin: "0",
			left: "0",
			top: "0",
			position: "fixed"
		}
	}
}
