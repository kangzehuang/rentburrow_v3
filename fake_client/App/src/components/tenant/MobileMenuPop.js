import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import {Glyphicon} from 'react-bootstrap'

import {xDeepBlue} from '../../stylesJS/base_colors'
import MobileMenu from './MobileMenu'

class MobileMenuPop extends Component {

	constructor(){
		super()
		this.state = {
			mobileMenu: false
		}
	}

	render() {
		return (
			<div style={comStyles().mainview}>
				<div style={comStyles().icon}> 
					<Glyphicon glyph="glyphicon glyphicon-menu-hamburger" onClick={()=>this.setState({mobileMenu: !this.state.mobileMenu})} style={comStyles().hamburger} />
				</div>
				{
					this.state.mobileMenu
					?
					<MobileMenu closeMenu={()=>this.setState({mobileMenu: false})} />
					:
					null
				}
			</div>
		);
	}
}

MobileMenuPop.propTypes = {
};

const RadiumHOC = Radium(MobileMenuPop);

function mapStateToProps(state){
	return {
	}
}

export default connect(mapStateToProps)(RadiumHOC);


// ======================

export const comStyles = () => {
	return {
		mainview: {
			position: "absolute",
			top: "0px",
			zIndex: "20",
			width: "100%",
		},
		icon: {
			zIndex: "19",
			borderRadius: "0px 0px 0px 50%",
			backgroundColor: xDeepBlue,
			width: "50px",
			height: "50px",
			right: "0px",
			display: "flex",
			justifyContent: "center",
			alignItems: "center",
			position: "absolute",
		},
		hamburger: {
			color: "white",
			fontSize: "1rem"
		}
	}
}
		