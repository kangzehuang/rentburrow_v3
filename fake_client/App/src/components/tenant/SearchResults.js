import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import { xDeepBlue } from '../../stylesJS/base_colors'
import { backToPins } from '../../actions/map_actions'
import {setSubletScroll, setLeaseScroll, setWatchlistScroll} from '../../actions/scroll_actions'
import SubletCard from '../shared_ui/SubletCard'
import LeaseCard from '../shared_ui/LeaseCard'
import {toggleMobileMap} from '../../actions/mobile_actions'
import {sortBySubletDate} from '../../api/iconSorter'

class SearchResults extends Component {

	constructor(){
		super()
		this.state = {
			browsingWholeList: true,
			scrollIntervalForPerformance: false
		}
	}

	componentDidMount(){
		if(this.props.viewMode == "sublet"){
			this.refs.scrollList.scrollTop = this.props.subletScrollPosition
		}else if(this.props.viewMode == "lease"){
			this.refs.scrollList.scrollTop = this.props.leaseScrollPosition
		}else if(this.props.viewMode == "watchlist"){
			this.refs.scrollList.scrollTop = this.props.watchlistScrollPosition
		}
		setInterval(()=>{
			if(this.state.scrollIntervalForPerformance){
				this.setState({
					scrollIntervalForPerformance: false
				})
			}
		}, 200)
	}

	componentDidUpdate(){
		if(!this.props.filteredResults && !this.props.selectedPins && !this.props.watchlistView){
			if(this.props.viewMode == "sublet"){
				this.refs.scrollList.scrollTop = this.props.subletScrollPosition
			}else if(this.props.viewMode == "lease"){
				this.refs.scrollList.scrollTop = this.props.leaseScrollPosition
			}else if(this.props.viewMode == "watchlist"){
				this.refs.scrollList.scrollTop = this.props.watchlistScrollPosition
			}
		}
	}

	renderBackButton(){
		if(this.props.selectedPins){
			return (
				<button className='btn btn-info btn-block' onClick={this.props.mobileViewer ? ()=>this.props.toggleMobileMap(true) : this.props.backToPins}>Back</button>
			)
		}
	}

	renderList(){
		if(this.props.selectedPins){
			if(this.props.viewMode == 'sublet'){
				return sortBySubletDate(this.props.selectedPins).map(this.renderCards.bind(this))
			}else{
				return this.props.selectedPins.map(this.renderCards.bind(this))
			}
		}else if(this.props.filteredResults){
			if(this.props.filteredResults.length == 0 ){
				return (
					<div style={comStyles().noneFoundMessage}>
						No Properties Matched
					</div>
				)
			}else{
				if(this.props.viewMode == 'sublet'){
					return sortBySubletDate(this.props.filteredResults).map(this.renderCards.bind(this))
				}else{
					return this.props.filteredResults.map(this.renderCards.bind(this))
				}
			}
		}else{
			if(!this.props.listOfResults || this.props.listOfResults.length < 1){
				return (
					<div><img src='../../../res/images/loading.gif' style={comStyles().loadingGif} /></div>
				)
			}else{
				if(this.props.viewMode == 'sublet'){
					return sortBySubletDate(this.props.listOfResults).map(this.renderCards.bind(this))
				}else{
					// return this.props.listOfResults.map(this.renderCards.bind(this))
						return (
							<div>
								{this.props.listOfResults.map(this.renderCards.bind(this))}
							</div>
						)
				}
			}
		}
	}

	renderCards(card, index){
		if(card.userid){
			return (<SubletCard key={card._id} sublet={card} index={index} />)
		}else if(card.company){
			return (<LeaseCard key={card._id} lease={card} index={index} />)
		}
	}

	trackScrollPosition(){
		this.setState({scrollIntervalForPerformance: true})
		if(!this.props.filteredResults && !this.props.selectedPins && !this.props.watchlistView){
			let y = this.refs.scrollList.scrollTop
			if(this.props.viewMode == 'sublet'){
				this.props.setSubletScroll(y)
			}else if(this.props.viewMode == 'lease'){
				this.props.setLeaseScroll(y)
			}
		}else if(this.props.viewMode == 'watchlist'){
			let z = this.refs.scrollList.scrollTop
			this.props.setWatchlistScroll(z)
		}
	}

	render() {
		return (
			<div id='SearchResults' style={comStyles().list}>
				{this.renderBackButton()}
				<div id='scrollList' ref='scrollList' onScroll={this.trackScrollPosition.bind(this)} style={comStyles().scroll}>
					{this.renderList()}
				</div>
			</div>
		);
	}
}

SearchResults.propTypes = {
	listOfResults: React.PropTypes.array.isRequired,
	filteredResults: React.PropTypes.array,
	selectedPins: React.PropTypes.array,
	viewMode: React.PropTypes.string,
	watchlistView: React.PropTypes.bool,
	mobileViewer: React.PropTypes.bool,
	index: React.PropTypes.number,						// passed in
	cardIndex: React.PropTypes.number
};

const RadiumHOC = Radium(SearchResults);

function mapStateToProps(state){
	return {
		listOfResults: state.content.listOfResults,
		filteredResults: state.content.filteredResults,
		selectedPins: state.content.selectedPins,
		viewMode: state.content.viewMode,
		subletScrollPosition: state.scroll.subletScrollPosition,
		leaseScrollPosition: state.scroll.leaseScrollPosition,
		watchlistScrollPosition: state.scroll.watchlistScrollPosition,
		watchlistView: state.tenant.watchlistView,
		mobileViewer: state.app.mobileViewer,
		cardIndex: state.perform.cardIndex
	}
}

export default connect(mapStateToProps, {backToPins, setSubletScroll, setLeaseScroll, setWatchlistScroll, toggleMobileMap})(RadiumHOC);


// ===============================

const comStyles = () => {
	return {
		list: {
			width: "100%",
			height: "100%"
		},
		scroll: {
			overflowY: "scroll",
			height: "90vh",
			margin: "5px 0px 15px 0px"
		},
		noneFoundMessage: {
			height: "150px",
			width: "80%",
			margin: "auto",
			borderRadius: "20px",
			display: "flex",
			textAlign: "center",
			justifyContent: "center",
			alignItems: "center",
			fontSize: "1.2rem",
			fontWeight: "bold",
			color: xDeepBlue,
			padding: "15px",
			backgroundColor: "rgba(256,256,256,0.2)"
		},
		someFoundMessage: {
			width: "80%",
			margin: "auto",
			borderRadius: "20px",
			display: "flex",
			textAlign: "center",
			justifyContent: "center",
			alignItems: "center",
			fontSize: "1.2rem",
			fontWeight: "bold",
			color: xDeepBlue,
			padding: "15px",
			backgroundColor: "rgba(256,256,256,0.2)"
		},
		loadingGif: {
			width: "20%",
			height: "auto",
			margin: "20% 35% 0% 40%",
			padding: "5%"
		}
	}
}
