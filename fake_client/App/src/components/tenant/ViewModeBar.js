import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'

import {setViewMode} from '../../actions/map_actions'
import {xMidBlue, xDeepBlue} from '../../stylesJS/base_colors'
import {goToListingsView} from '../../actions/tenant_actions'
import {trackActivity} from '../../actions/activity_actions'

class ViewModeBar extends Component {

	renderIcon(mode){
		if(this.props.viewMode == mode){
			return <img src='../../../res/images/red-dot.png' style={comStyles().icon} />
		}
	}

	triggerSublets(){
	this.props.trackActivity("APP", "SUBLET_VIEW", null, this.props.fbUser, this.props.geoCoords)
		this.props.setViewMode("sublet")
		this.props.goToListingsView()
	}

	triggerLeases(){
		this.props.trackActivity("APP", "LEASE_VIEW", null, this.props.fbUser, this.props.geoCoords)
		this.props.setViewMode("lease")
		this.props.goToListingsView()
	}

	render() {
		return (
			<div style={comStyles().ViewModeBar}>
				<button className='btn' style={comStyles(this.props.viewMode).sublet} onClick={this.triggerSublets.bind(this)}>
					{this.renderIcon('sublet')} &nbsp;
					SUBLETS
				</button>
				<button className='btn' style={comStyles(this.props.viewMode).lease} onClick={this.triggerLeases.bind(this)}>
					{this.renderIcon('lease')} &nbsp;
					LEASES
				</button>
			</div>
		);
	}
}

ViewModeBar.propTypes = {
	viewMode: React.PropTypes.string.isRequired,
	fbUser: React.PropTypes.object,
	geoCoords: React.PropTypes.array
};

const RadiumHOC = Radium(ViewModeBar);

function mapStateToDispatch(state){
	return {
		viewMode: state.content.viewMode,
		fbUser: state.tenant.fbUser,
		geoCoords: state.app.geoCoords
	}
}

export default connect(mapStateToDispatch, {setViewMode, goToListingsView, trackActivity})(RadiumHOC);


// ============================

const comStyles = (viewMode) => {
	let subletColor
	let leaseColor
	if(viewMode == "sublet"){
		subletColor = xDeepBlue
		leaseColor = xMidBlue
	}else if(viewMode == "lease"){
		subletColor = xMidBlue
		leaseColor = xDeepBlue
	}else if(viewMode == "watchlist"){
		subletColor = xMidBlue
		leaseColor = xMidBlue
	}
	return {
		ViewModeBar: {
			zIndex: "10",
			position: "absolute",
			width: "auto",
			right: "20px",
			top: "20px",
			"@media (max-width: 600px), and (max-height: 740px)": {
				position: "relative",
				display: "none"
			}
		},
		sublet: {
			backgroundColor: subletColor,
			borderRadius: "0px",
			width: "12%",
			minWidth: "120px",
			fontSize: "1rem",
			fontWeight: "bold",
			color: "white",
			padding: "5px 10px 5px 10px"
		},
		lease: {
			backgroundColor: leaseColor,
			borderRadius: "0px",
			width: "12%",
			minWidth: "120px",
			fontSize: "1rem",
			fontWeight: "bold",
			color: "white",
			padding: "5px 10px 5px 10px"
		},
		icon: {
			display:"inline-block",
			height:"17px",
			width:"auto",
		}
	}
}
