import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import WatchlistDetail from './WatchlistDetail'

import {xMidBlue, xDeepBlue, xGreyText} from '../../stylesJS/base_colors'
import {createWatchlist, getWatchlistsForUser, removeUserFromWatchlist} from '../../api/watchlistsManager'
import {saveWatchlistsToState, setWatchlist} from '../../actions/watchlist_actions'

class WatchlistsOverview extends Component {

	constructor(){
		super()
		this.state = {
			toggleAdd: false,
			addForm: "",
			selectedWatchlist: null
		}
	}

	componentWillMount(){
		getWatchlistsForUser(this.props.fbUser.id)
			.then((data)=>{
				this.props.saveWatchlistsToState(data)
			})
	}

	handleAddForm(event){
		this.setState({
			addForm: event.target.value
		})
	}

	submitNewWatchlist(){
		const self = this
		const newWatchlist = {
			watchlist_title: self.state.addForm,
			userId: self.props.fbUser.id
		}
		createWatchlist(newWatchlist).then((data)=>{
			getWatchlistsForUser(self.props.fbUser.id)
				.then((data)=>{
					// console.log(data)
					self.props.saveWatchlistsToState(data)
				})
			self.setState({
				addForm: "",
				toggleAdd: false
			})
		})
	}

	removeFromWatchlist(watchlist_id){
		const self = this
		const y = confirm("Are you sure you want to delete this favorites list?")
		if(y==true){
			const x = {
				watchlist_id: watchlist_id,
				userId: self.props.fbUser.id
			}
			removeUserFromWatchlist(x).then((data)=>{
				getWatchlistsForUser(self.props.fbUser.id)
					.then((data)=>{
						self.props.saveWatchlistsToState(data)
					})
			})
		}
	}

	renderAdd(){
		const self = this
		if(this.state.toggleAdd){
			return (
				<div style={comStyles().addBox}>
					<input type='text' value={self.state.addForm} onChange={self.handleAddForm.bind(self)} style={comStyles().addForm} />
					<button onClick={self.submitNewWatchlist.bind(self)} className='btn btn-success' type='button' style={comStyles().addButton}>Create</button>
				</div>
			)
		}
	}

	goBackToOverview(){
		this.setState({
			selectedWatchlist: null
		})
	}

	renderWatchlist(watchlist){
		const self = this
		return (
			<div className='card' style={comStyles().watchcard}>
				<div style={comStyles().watchcardTitle}>
					{watchlist.watchlist_title}
				</div>
				<div>
					<button className='btn btn-primary' style={comStyles().useThisListButton} type='button' onClick={()=>self.props.setWatchlist(watchlist)}>
						Use this list
					</button>
					<button className='btn btn-danger' style={comStyles().deleteListButton} type='button' onClick={()=>self.removeFromWatchlist(watchlist._id)}>
						<i className='ion-trash-a' style={comStyles().deleteListButtonIcon} />
					</button>
				</div>
			</div>
		)
	}

	renderMessageIfNoWatchlists(){
		if(this.props.watchlists.length == 0){
			return (
				<div style={comStyles().noneFoundMessage}>
					Create a favorites list to share with your roommates!
				</div>
			)
		}
	}

	render() {
		if(this.props.selectedWatchlist){
			return (
				<WatchlistDetail />
			)
		}else{
			return (
				<div style={comStyles().mainview}>
					<div style={comStyles().myWatchlistsOverview}>
						<div style={comStyles().myWatchlistsOverviewHeader}>
							<div className='col-md-9' style={comStyles().myWatchlistsOverviewTitle}>
								My Favorites
							</div>
							<div className='col-md-3' onClick={()=>this.setState({toggleAdd: !this.state.toggleAdd})} className='col-md-3'>
								{
									this.state.toggleAdd
									?
									<i className='ion-close-round' style={comStyles().addButton} />
									:
									<i className='ion-plus' style={comStyles().addButton} />
								}
							</div>
						</div>
						{this.renderAdd()}
						<div style={comStyles().listOfWatchlists}>
							{this.props.watchlists.map(this.renderWatchlist.bind(this))}
							{this.renderMessageIfNoWatchlists()}
						</div>
					</div>
				</div>
			)
		}
	}
}

WatchlistsOverview.propTypes = {
	fbUser: React.PropTypes.object.isRequired,
	watchlists: React.PropTypes.array.isRequired,
	selectedWatchlist: React.PropTypes.object
};

const RadiumHOC = Radium(WatchlistsOverview);

function mapStateToProps(state){
	return {
		fbUser: state.tenant.fbUser,
		watchlists: state.tenant.watchlists,
		selectedWatchlist: state.tenant.selectedWatchlist
	}
}

export default connect(mapStateToProps, {saveWatchlistsToState, setWatchlist})(RadiumHOC);


// ============================

const comStyles = () => {
	return {
		mainview: {
			padding: "20px",
			height: "100vh",
			overflowY: "scroll"
		},
		myWatchlistsOverview: {
			display: "flex",
			flexDirection: "column",
			justifyContent: "center",
			padding: "10px",
			top: "10px",
			backgroundColor: xMidBlue,
		},
		myWatchlistsOverviewHeader: {
			fontSize: "1.5rem",
			fontWeight: "bold",
			textAlign: "center",
			color: "white",
			padding: "10px"
		},
		myWatchlistsOverviewTitle: {
			fontSize: "1.7rem",
			fontWeight: "bold",
			textAlign: "center",
			color: "white"
		},
		addBox: {
			backgroundColor: xMidBlue,
			padding: "10px",
		},
		addForm: {
			width: "70%",
			borderRadius: "5px 0px 0px 5px",
			display: "inline-block",
			padding: "3px"
		},
		addButton: {
			width: "30%",
			borderRadius: "0px 5px 5px 0px",
			display: "inline-block",
			fontWeight: "bold",
			"@media (max-width: 600px), and (max-height: 740px)": {
				margin: "15px 0px 0px 0px"
			}
		},
		listOfWatchlists: {
			margin: "15px 0px 0px 0px"
		},
		watchcard: {
			padding: "10px",
			display: 'flex',
			flexDirection: "column"
		},
		watchcardTitle:{
			fontSize: "1.5rem",
			fontWeight: "bold",
			margin: "15px",
			color: xGreyText
		},
		useThisListButton:{
			display: "inline-block",
			fontSize: "1.2rem",
			fontWeight: "bold",
			borderRadius: "0px",
			width: "80%"
		},
		deleteListButton:{
			display: "inline-block",
			width: "20%",
			borderRadius: "0px"
		},
		deleteListButtonIcon:{
			fontSize: "1.2rem",
			fontWeight: "bold",
		},
		noneFoundMessage: {
			height: "150px",
			width: "80%",
			margin: "20px auto",
			borderRadius: "20px",
			display: "flex",
			textAlign: "center",
			justifyContent: "center",
			alignItems: "center",
			fontSize: "1.2rem",
			fontWeight: "bold",
			color: "white",
			padding: "15px",
			backgroundColor: "rgba(256,256,256,0.2)"
		}
	}
}