import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'

import {toggleMobileMap} from '../../actions/mobile_actions'
import {xDeepBlue} from '../../stylesJS/base_colors'


class MobileMenu extends Component {

	whiteOrNo(){
		let shouldBeWhite = false
		if(this.props.leaseForPopup){
			shouldBeWhite = true
		}
		if(this.props.subletForPopup){
			shouldBeWhite = true
		}
		if(this.props.toggle){
			shouldBeWhite = true
		}
		if(this.props.contact){
			shouldBeWhite = true
		}
		if(this.props.imgForPopup){
			shouldBeWhite = true
		}
		if(this.props.leaseForReview){
			shouldBeWhite = true
		}
		return shouldBeWhite
	}

	render() {
		return (
			<div>
				<i className='ion-backspace' onClick={()=>this.props.toggleMobileMap(false)} style={comStyles(this.whiteOrNo()).back}></i>
			</div>
		);
	}
}

MobileMenu.propTypes = {
	leaseForPopup: React.PropTypes.object,
	subletForPopup: React.PropTypes.object,
	toggle: React.PropTypes.string,
	contact: React.PropTypes.bool,
	imgForPopup: React.PropTypes.string,
	leaseForReview: React.PropTypes.object
};

const RadiumHOC = Radium(MobileMenu);

function mapStateToProps(state){
	return {
		leaseForPopup: state.popup.leaseForPopup,
		subletForPopup: state.popup.subletForPopup,
		toggle: state.popup.toggle,
		contact: state.popup.contact,
		imgForPopup: state.popup.imgForPopup,
		leaseForReview: state.popup.leaseForReview
	}
}

export default connect(mapStateToProps, {toggleMobileMap})(RadiumHOC);


// ======================

export const comStyles = (shouldBeWhite) => {
	// console.log(shouldBeWhite)
	let color = xDeepBlue
	if(shouldBeWhite){
		color = "white"
	}
	return {
		back: {
			color: color,
			fontSize: "2.5rem",
			position: "absolute",
			top: "10px",
			left: "10px",
			zIndex: "45",
		}
	}
}
