import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import { browserHistory } from 'react-router'

import {shortenAddress} from '../../api/iconSorter'
import {xDeepBlue} from '../../stylesJS/base_colors'
import { selectLandlordProperty } from '../../actions/landlord_actions'

class PropertyPreview extends Component {

	render() {
		return (
			<div onClick={()=>this.props.selectLandlordProperty(this.props.property)} style={comStyles().card}>
				<div style={comStyles().addressTitle}>{this.props.property.building_name || shortenAddress(this.props.property.address)}</div>
				{
					this.props.property.images[0]
					?
					<img src={this.props.property.images[0].urlThumbnail} style={comStyles().imgTile} />
					:
					null
				}
			</div>
		);
	}
}

PropertyPreview.propTypes = {
	property: React.PropTypes.object.isRequired
};

const RadiumHOC = Radium(PropertyPreview);

export default connect(null, {selectLandlordProperty})(RadiumHOC);


// =================

const comStyles = () => {
	return {
		card: {
			width: "250px",
			height: "250px",
			margin: "10px",
			overflow: "hidden",
			textAlign: "center",
			border: "3px solid white",
			borderRadius: "10px",
			backgroundColor: xDeepBlue,
			cursor: "pointer",
			":hover": {
				backgroundColor: "rgba(0,0,0,0.5)"
			}
		},
		imgTile: {
			width: "100%",
			height: "auto",
			marginTop: "5px"
		},
		addressTitle: {
			fontSize: "1.5rem",
			fontWeight: "bold",
			color: "white",
			padding: "5px"
		}
	}
}
