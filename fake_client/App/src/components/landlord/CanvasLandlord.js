import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'

import { xDeepBlue } from '../../stylesJS/base_colors'

import MapView from '../shared_ui/MapView'
import LeaseCard from '../shared_ui/LeaseCard'
import Popup from '../shared_ui/Popup/Popup'
import PopupImage from '../shared_ui/Popup/PopupImage'

class CanvasLandlord extends Component {

	constructor(){
		super()
		this.state = {
			mapview: null,
			matchedHeight: null
		}
	}

	// By calculating the node height
	componentDidMount(){
		let { clientHeight } = this.refs.leasecard
    	this.setState({
    		matchedHeight: clientHeight
    	})
	}

	renderPopup(){
		if(this.props.popupToggle){
			return (
				<Popup />
			)
		}
	}

	renderPopupImage(){
		if(this.props.imgToggle){
			return(
				<PopupImage />
			)
		}
	}

	render() {
		const preDefinedCoords = {
			lat: this.props.property.coords[1] || this.props.selectedProperty.coords[1], 
			lng: this.props.property.coords[0] || this.props.selectedProperty.coords[0]
		}
		return (
			<div id='canvas' style={matchedHeights(this.state.matchedHeight).canvas}>
				<div id='blueCanvas' style={comStyles(this.props.canvasWidth).blueCanvas}>
					<LeaseCard ref='leasecard' lease={this.props.property || this.props.selectedProperty} />
				</div>
				<div id='mapView' style={comStyles(this.state.matchedHeight).mapView}>
					{this.renderPopup()}
					{this.renderPopupImage()}
					<MapView preDefinedCoords={preDefinedCoords} />
				</div>
			</div>
		);
	}
}

CanvasLandlord.propTypes = {
	property: React.PropTypes.object.isRequired,		// passed in
	canvasWidth: React.PropTypes.number.isRequired,
	selectedProperty: React.PropTypes.object,
	popupToggle: React.PropTypes.string,
	imgToggle: React.PropTypes.bool,
};

CanvasLandlord.defaultProps = {
  canvasWidth: 15
};

const RadiumHOC = Radium(CanvasLandlord);

function mapStateToProps(state){
	return {
		selectedProperty: state.landlord.selectedProperty,
		popupToggle: state.popup.toggle,
		imgToggle: state.popup.imgForPopup,
	}
}

export default connect(mapStateToProps)(RadiumHOC);

// =====================================

const comStyles = (canvasWidth) => {
	let flexGrow = 100-canvasWidth
	return {
		blueCanvas: {
			width: "25vw",
			minWidth: "500px",
			display: "inline-block",
		},
		mapView: {
			width: "75vw",
			display: "inline-block",
			position: "relative"
		},
	}
}


const matchedHeights = (match) => {
	return {
		canvas: {
			display: "flex",
			justifyContent: "center",
			alignItems: "stretch",
			maxHeight: "800px",
			overflow: "hidden",
			maxHeight: match+"px",
			padding: "15px",
			margin: "10px",
			backgroundColor: xDeepBlue,
			borderRadius: "10px"
		}
	}
}