import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import { browserHistory } from 'react-router'

import { claimProperty, getLeasesForLandlord } from '../../api/landlordManager'
import { setClaimId, saveLeasesToState } from '../../actions/landlord_actions'

class Claim extends Component {

  constructor(){
    super()
    this.state = {
      message: "Claiming Property...",
      error: false
    }
  }

  componentWillMount(){
    claimProperty(this.props.claimId, this.props.landlord.id)
      .then((lease)=>{
        // console.log("Success!")
        // console.log(lease)
        this.props.setClaimId(null)
    		// this.props.trackActivity("LANDLORD", "CLAIMED_PROPERTY", {id: lease.landlord_id}, null, this.props.geoCoords, {leaseId: lease._id})
        return getLeasesForLandlord(this.props.landlord.id)
      })
      .then((data)=>{
        // console.log("Got leases for landlord!")
        this.props.saveLeasesToState(data)
        browserHistory.push('/landlord/dashboard')
      })
      .catch((err)=>{
        this.setState({
          message: err,
          error: true
        })
      })
  }

  goBackToDashboard(){
    browserHistory.push('/landlord/dashboard')
  }

	render() {
		return (
      <div style={comStyles().claimMessage}>
        <h4 style={comStyles().messageText}>{this.state.message}</h4>
        {
          this.state.error
          ?
          <button className='btn btn-primary' onClick={this.goBackToDashboard} style={comStyles().backButton}>Back To Home</button>
          :
          null
        }
      </div>
    )
	}
}

Claim.propTypes = {
  landlord: React.PropTypes.object.isRequired,
	claimId: React.PropTypes.string
};

const RadiumHOC = Radium(Claim);

function mapStateToProps(state){
	return {
    landlord: state.landlord.landlord,
		claimId: state.landlord.claimId
	}
}

export default connect(mapStateToProps, {setClaimId, saveLeasesToState})(RadiumHOC);

// ==================================


const comStyles = () => {
	return {
		background: {
			width: "100%",
			height: "100%",
			margin: "0",
			left: "0",
			top: "0"
		},
    claimMessage: {
      width: "100vw",
      textAlign: "center",
      color: "white",
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
    },
    messageText: {
      fontSize: "2rem",
      color: "white"
    },
    backButton: {
      margin: "30px auto"
    }
	}
}
