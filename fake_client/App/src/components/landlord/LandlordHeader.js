import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import {browserHistory} from 'react-router'

import { backToLandlordGrid, newLandlordProperty, goToLandlordSettings, goToCharts } from '../../actions/landlord_actions'

class LandlordHeader extends Component {

	goToMainSite(){
		browserHistory.push('/')
		location.reload()
	}

	render() {
		return (
			<div style={comStyles().logoSet}>
				<div style={comStyles().left}>
					<i onClick={this.props.goToLandlordSettings} title='Settings' className='ion-gear-a' style={comStyles().settingsButton}></i>
					<i onClick={this.props.backToLandlordGrid} title='Properties' className='ion-grid' style={comStyles().gridButton}></i>
					{/* <i onClick={this.props.goToCharts} title='Analytics' className='ion-pie-graph' style={comStyles().chartsButton}></i> */}
				</div>
				<div style={comStyles().center}>
					<div onClick={this.props.backToLandlordGrid} className='img-circle' style={logoStyle(this.props.companyLogo).logo}></div>
					<h2 style={comStyles().name}>{this.props.companyName}</h2>
				</div>
				<div style={comStyles().right}>
					<i onClick={this.props.newLandlordProperty} title='Add New' className='ion-plus-circled' style={comStyles().addButton}></i>
					<i onClick={this.goToMainSite} className='ion-ios-location' title='Go To Website' style={comStyles().seeMainSite}></i>
				</div>
			</div>
		);
	}
}

LandlordHeader.propTypes = {
	companyName: React.PropTypes.string.isRequired,
	companyLogo: React.PropTypes.string.isRequired,
	backToLandlordGrid: React.PropTypes.func.isRequired,
	newLandlordProperty: React.PropTypes.func.isRequired
};

const RadiumHOC = Radium(LandlordHeader);

function mapStateToProps(state){
	return {
		companyName: state.landlord.landlord.company_name,
		companyLogo: state.landlord.landlord.company_logo
	}
}

export default connect(mapStateToProps, {backToLandlordGrid, newLandlordProperty, goToLandlordSettings, goToCharts})(RadiumHOC);


// ===========================

const comStyles = () => {
	return {
		logoSet: {
			width: "100%",
			display: "flex",
			justifyContent: "center",
			flexDirection: "row",
			marginBottom: "50px",
		},
		left: {
			display: "flex",
			justifyContent: "flex-start",
			padding: "10px",
			width: "30%",
			flexDirection: "row"
		},
		center: {
			display: "flex",
			justifyContent: "center",
			padding: "10px",
			width: "40%",
		},
		right: {
			display: "flex",
			justifyContent: "flex-end",
			padding: "10px",
			width: "30%",
			flexDirection: "row"
		},
		name: {
			display: "inline-block",
			alignSelf: "center",
			margin: "10px",
			color: "white",
		},
		addButton: {
			fontSize: "2.5rem",
			color: "white",
			cursor: "pointer",
			margin: "25px"
		},
		seeMainSite: {
			fontSize: "2.5rem",
			color: "white",
			cursor: "pointer",
			margin: "25px"
		},
		settingsButton: {
			// position: "fixed",
			fontSize: "2.5rem",
			color: "white",
			cursor: "pointer",
			margin: "25px"
		},
		gridButton: {
			// position: "fixed",
			fontSize: "2.5rem",
			color: "white",
			cursor: "pointer",
			margin: "25px"
		},
		chartsButton: {
			// position: "fixed",
			fontSize: "2.5rem",
			color: "white",
			cursor: "pointer",
			margin: "25px"
		}
	}
}

const logoStyle = (src) => {
	let backgroundImg
	if(src=="" || src == null){
		backgroundImg = "../../../res/images/no_logo_company.png"
	}else{
		backgroundImg = src
	}
	return {
		logo: {
			width:'150px',
		    height:'150px',
		    borderRadius:'50%',
		    backgroundImage: "url("+backgroundImg+")",
		    backgroundPosition: 'center',
		    backgroundSize:'100% auto',
		    backgroundRepeat: 'no-repeat',
			display: "inline-block",
		}
	}
}
