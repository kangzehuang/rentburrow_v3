import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import Dropzone from 'react-dropzone'

import uuid from 'uuid'
import { xDeepBlue, xMidBlue, xLightBlue, xBootstrapRed, xBootstrapOrange } from '../../stylesJS/base_colors'
import { computeForRegion, saveLandlordPropertyToDB, getLeasesForLandlord, updateLandlordPropertyInDB } from '../../api/landlordManager'
import { exitPropertyForm, saveLeasesToState, selectLandlordProperty } from '../../actions/landlord_actions'
import {filterNonImages} from '../../api/iconSorter'
import {uploadImagesSeriesRx} from '../../api/aws/aws-S3'
import {trackActivity} from '../../actions/activity_actions'
import { MANUAL_FAKED_ENTRIES_CLAIMED } from '../../api/API_URLS'


class PropertyForm extends Component {

	constructor(){
		super()
		this.autocomplete = null
		this.state = {
				propertyEdit: {
					building_name: "",
					address: "",
					city_name: "",
					company: "",
					type: "",
					rooms: [],
					place_id: "",
					utils_list: {
						water: true,
						heat: true,
						electric: true,
						furnished: true,
						parking: false,
						free_parking: false,
						internet: false,
						ac: false,
						laundry: false
					},
					url: "",
					thumbnail: "",
					images: [],
					coords: [],
					contacts: [],
					note: "",
					landlord_id: "",
					claimed: MANUAL_FAKED_ENTRIES_CLAIMED,
					active: false,
					deleted: false
				},
				roomEdit: {
						id: "",
						visible: false,
						room_type: "",
						reg_price: 0,
						promo_price: 0,
						rooms_per: 0,
						lease_terms: 0,
						promo_terms: 0,
						bathrooms: 0,
						note: "",
						promo: false,
						position: 0
					},
				contactEdit: {
						id: "",
						visible: false,
						name: "",
						phone: "",
						email: "",
						position: 0
					},
				imageEdit: {
						visible: false,
						images: []
					},
				errorMessage: null,
				loading: false,
				loadingMessage: "",
				newProperty: true
			}
		this.triggerEditRoom.bind(this)
		this.triggerEditContact.bind(this)
		this.triggerImageDropzone.bind(this)
	}

	componentWillMount(){
		if(this.props.propertyOnForm && this.props.propertyOnForm.address){
			this.setState({
				propertyEdit: {
					thumbnail: this.props.landlord.company_logo || "https://s3.amazonaws.com/rentburrow-images/ideas%2540bytenectar.io/default_home_icon.png",
					...this.props.propertyOnForm
				},
				newProperty: false
			})
		}else{
			this.setState({
				propertyEdit: {
						...this.state.propertyEdit,
						type: "apartment",
						city_name: computeForRegion(this.props.landlord, "city"),
						company: this.props.landlord.company_name,
						url: this.props.landlord.company_website || "",
						thumbnail: this.props.landlord.company_logo || "https://s3.amazonaws.com/rentburrow-images/ideas%2540bytenectar.io/default_home_icon.png",
						landlord_id: this.props.landlord.id,
						active: true,
						rooms: [],
						contacts: [{
								id: uuid.v4(),
								name: this.props.landlord.company_name,
								phone: this.props.landlord.default_phone,
								email: this.props.landlord.email_forward || this.props.landlord.default_email
							}]
					},
				roomEdit: {
					...this.state.roomEdit,
					lease_terms: 12
				}
			})
		}
	}

	componentDidMount(){
		this.autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});
		// console.log(this.autocomplete)
        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        this.autocomplete.addListener('place_changed', this.fillInAddress.bind(this));
	}

	fillInAddress(){
		// console.log("filling in address")
		const place = this.autocomplete.getPlace()

		this.setState({
			propertyEdit: {
				...this.state.propertyEdit,
				address: place.formatted_address,
				coords: [place.geometry.location.lng().toFixed(7), place.geometry.location.lat().toFixed(7)],
				place_id: place.place_id
			}
		})
	}

	geolocate(){
		const self = this
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            const geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            const circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            self.autocomplete.setBounds(circle.getBounds());
          });
        }else{
        	const geolocation = {lat: 43.473897, lng: -80.531995}
          const circle = new google.maps.Circle({
            center: geolocation,
            radius: 10000
          });
          autocomplete.setBounds(circle.getBounds());
        }
      }

	componentDidUpdate(){
		if(this.props.roomOnPropertyForm){
			this.setState({
				roomEdit: {
					...this.props.roomOnPropertyForm,
					visible: true
				}
			})
		}
		if(this.props.contactOnPropertyForm){
			this.setState({
				contactEdit: {
					...this.props.contactOnPropertyForm,
					visible: true
				}
			})
		}
	}

	triggerEditRoom(room){
		if(room && room.room_type){
			this.setState({
				roomEdit: {
					...room,
					visible: true
				}
			})
		}else{
			this.setState({
				roomEdit: {
					...this.state.roomEdit,
					visible: !this.state.roomEdit.visible
				}
			})
		}
	}

	removeRoom(selectedRoom, autoDelete){
		let x
		if(!autoDelete){
			x = confirm("Are you sure you want to delete this?")
		}
		if(x == true || autoDelete){
			let roomExistsPosition
			const roomExists = this.state.propertyEdit.rooms.filter((room, index)=>{
				if(room.room_type == selectedRoom.room_type){
					roomExistsPosition = index
					return true
				}else{
					return false
				}
			})
			if(roomExists.length > 0){
				const x = this.state.propertyEdit.rooms
				x.splice(roomExistsPosition, 1)
				this.setState({
					propertyEdit: {
						...this.state.propertyEdit,
						rooms: x
					}
				})
			}
		}
	}

	triggerEditContact(contact){
		if(contact && contact.name){
			this.setState({
				contactEdit: {
					...contact,
					visible: true
				}
			})
		}else{
			this.setState({
				contactEdit: {
					...this.state.contactEdit,
					visible: !this.state.contactEdit.visible
				}
			})
		}
	}

	removeContact(selectedContact, autoDelete){
		let x
		if(!autoDelete){
			x = confirm("Are you sure you want to delete this?")
		}
		if(x === true || autoDelete){
			let contactExistsPosition
			const contactExists = this.state.propertyEdit.contacts.filter((contact, index)=>{
				if(contact.name == selectedContact.name){
					contactExistsPosition = index
					return true
				}else{
					return false
				}
			})
			if(contactExists.length > 0){
				const x = this.state.propertyEdit.contacts
				x.splice(contactExistsPosition, 1)
				this.setState({
					propertyEdit: {
						...this.state.propertyEdit,
						contacts: x
					}
				})
			}
		}
	}

	handleIconChange(iconName){
		this.setState({
			propertyEdit: {
				...this.state.propertyEdit,
				utils_list: {
					...this.state.propertyEdit.utils_list,
					[iconName]: !this.state.propertyEdit.utils_list[iconName]
				}
			}
		})
	}

	clearRoomForm(){
		this.setState({
			roomEdit: {
				visible: false,
				room_type: "",
				reg_price: 0,
				promo_price: 0,
				rooms_per: 0,
				lease_terms: 12,
				promo_terms: 0,
				bathrooms: 0,
				note: "",
				promo: false
			}
		})
	}

	togglePromo(){
		this.setState({
			...this.state,
			roomEdit: {
					...this.state.roomEdit,
					promo: !this.state.roomEdit.promo
				}
		})
	}

	updateRoomEdit(attr, event){
		this.setState({
			...this.state,
			roomEdit: {
					...this.state.roomEdit,
					[attr]: event.target.value
				}
		})
	}

	incrementRoomEdit(attr, x){
		let y = this.state.roomEdit[attr]
		if(y+x > -1){
			y = y+x
		}
		this.setState({
			...this.state,
			roomEdit: {
					...this.state.roomEdit,
					[attr]: y
				}
		})
	}

	addRoomForm(){
		if(this.state.roomEdit.room_type && this.state.roomEdit.reg_price && this.state.roomEdit.lease_terms && this.state.roomEdit.rooms_per && this.state.roomEdit.bathrooms){
			this.removeRoom(this.state.roomEdit, true)
			let propRooms = this.state.propertyEdit.rooms
			if(this.state.roomEdit.id){
				propRooms = this.state.propertyEdit.rooms.filter((r)=>{
					return r.id !== this.state.roomEdit.id
				})
				this.state.roomEdit.id = uuid.v4()
			}
			this.state.roomEdit.position = this.state.propertyEdit.rooms.length
			this.setState({
				propertyEdit: {
					...this.state.propertyEdit,
					rooms: [
							...propRooms,
							this.state.roomEdit
						]
				},
				roomEdit: {
					id: "",
					room_type: "",
					reg_price: 0,
					promo_price: 0,
					rooms_per: 0,
					lease_terms: 12,
					promo_terms: 0,
					bathrooms: 0,
					note: "",
					promo: false,
					visible: false,
					position: 0
				},
				errorMessage: null
			})
		}else{
			this.setState({
				errorMessage: "A suite must include details on number of rooms, bathrooms, lease terms, room type and price."
			})
		}
	}

	incPromoRoomPrice(amount){
		if(this.state.roomEdit.promo_price >= 5){
			const newPrice = parseInt(this.state.roomEdit.promo_price) + parseInt(amount)
			this.setState({
				roomEdit: {
					...this.state.roomEdit,
					promo_price: newPrice
				}
			})
		}
	}

	incRoomPrice(amount){
		if(this.state.roomEdit.reg_price >= 5){
			const newPrice = parseInt(this.state.roomEdit.reg_price) + parseInt(amount)
			this.setState({
				roomEdit: {
					...this.state.roomEdit,
					reg_price: newPrice
				}
			})
		}
	}

	renderRoomForm(){
		if(this.state.roomEdit.visible){
			return (
			  <div style={roomFormStyles().roomForm}>
			  		<div className='col-md-5' style={roomFormStyles().priceBox}>
					  	<div className="form-group">
					    	<input style={roomFormStyles().suitename} type="text" onChange={this.updateRoomEdit.bind(this, "room_type")} value={this.state.roomEdit.room_type} className="form-control" placeholder="Suite Name" required />
					  	</div>
							<div style={roomFormStyles().price}>
								$ {this.state.roomEdit.promo ? this.state.roomEdit.promo_price : this.state.roomEdit.reg_price}
							</div>
							<div style={roomFormStyles().sliderBox}>
								<div style={roomFormStyles().incrementalIconHorzPrice}><i className='ion-minus-round' onClick={this.state.roomEdit.promo ? this.incPromoRoomPrice.bind(this, -5) : this.incRoomPrice.bind(this, -5)}></i></div>
								<div style={roomFormStyles().rangeContainer}>
									<div className="item range" style={roomFormStyles().range}>
									  <input type="range" step={5} value={this.state.roomEdit.promo ? this.state.roomEdit.promo_price : this.state.roomEdit.reg_price} onChange={this.state.roomEdit.promo ? this.updateRoomEdit.bind(this, "promo_price") : this.updateRoomEdit.bind(this, "reg_price")} min='0' max='1500' />
									</div>
								</div>
								<div style={roomFormStyles().incrementalIconHorzPrice}><i className='ion-plus-round' onClick={this.state.roomEdit.promo ? this.incPromoRoomPrice.bind(this, 5) : this.incRoomPrice.bind(this, 5)}></i></div>
								</div>
						</div>
				    <div style={roomFormStyles().bedBathAndBeyond} className="input-group col-md-2">
							<div style={roomFormStyles().upAndDown}>
								<div className='col-md-2 col-md-offset-1' style={roomFormStyles().incrementalIconHorz}><i className='ion-minus-round' onClick={this.state.roomEdit.promo ? ()=>this.incrementRoomEdit("promo_terms", -1) : ()=>this.incrementRoomEdit("lease_terms", -1)}></i></div>
								<div className='col-md-6' style={roomFormStyles().leaseTerms}>{this.state.roomEdit.promo ? this.state.roomEdit.promo_terms : this.state.roomEdit.lease_terms} Months</div>
								<div className='col-md-2' style={roomFormStyles().incrementalIconHorz}><i className='ion-plus-round' onClick={this.state.roomEdit.promo ? ()=>this.incrementRoomEdit("promo_terms", 1) : ()=>this.incrementRoomEdit("lease_terms", 1)}></i></div>
							</div>
							<div style={roomFormStyles().upAndDown}>
								<div className='col-md-2 col-md-offset-1' style={roomFormStyles().incrementalIconHorz}><i className='ion-minus-round' onClick={()=>this.incrementRoomEdit("rooms_per", -1)}></i></div>
								<div className='col-md-6' style={roomFormStyles().leaseTerms}>{this.state.roomEdit.rooms_per} Beds</div>
								<div className='col-md-2' style={roomFormStyles().incrementalIconHorz}><i className='ion-plus-round' onClick={()=>this.incrementRoomEdit("rooms_per", 1)}></i></div>
							</div>
							<div style={roomFormStyles().upAndDown}>
								<div className='col-md-2 col-md-offset-1' style={roomFormStyles().incrementalIconHorz}><i className='ion-minus-round' onClick={()=>this.incrementRoomEdit("bathrooms", -0.5)}></i></div>
								<div className='col-md-6' style={roomFormStyles().leaseTerms}>{this.state.roomEdit.bathrooms} Baths</div>
								<div className='col-md-2' style={roomFormStyles().incrementalIconHorz}><i className='ion-plus-round' onClick={()=>this.incrementRoomEdit("bathrooms", 0.5)}></i></div>
							</div>
				    </div>
				    <div className="form-group col-md-3">
				      	<label>Notes</label>
				    	<textarea style={roomFormStyles().note} onChange={this.updateRoomEdit.bind(this, "note")} className="form-control" value={this.state.roomEdit.note} rows="3"></textarea>
				  	</div>
				  	<div className='col-md-2' style={roomFormStyles().lastBox}>
						<div style={roomFormStyles().promoBox}>
						  	<label style={roomFormStyles().promoToggle} className="toggle toggle-energized">
						       <input type="checkbox" onChange={()=>this.togglePromo()} checked={this.state.roomEdit.promo} />
						       <div className="track">
						         <div className="handle"></div>
						       </div>
						    </label>
							<div style={roomFormStyles().promoLabel}> &nbsp; PROMO</div>
					    </div> <br/>
					  	<div style={roomFormStyles().buttonRow}>
					  		<button style={roomFormStyles().addButton} onClick={()=>this.addRoomForm()} type='button' className='btn btn-success' type='button'>Save</button>
					  		<button style={roomFormStyles().cancelButton} onClick={()=>this.clearRoomForm()} className='btn btn-warning' type='button'>Cancel</button>
				    	</div>
				    </div>
				</div>
			)
		}
	}

	renderRoom(room){
		let regOrPromo
		if(room.promo){
			regOrPromo = (<div style={roomCardStyles().promoAlert}>${room.promo_price} for {room.promo_terms} months [PROMO]</div>)
		}else{
			regOrPromo = (<div>${room.reg_price} for {room.lease_terms} months</div>)
		}
		return (
			<div className='card' style={roomCardStyles().roomCard}>
			  	<div style={roomCardStyles().editOrDel}>
				  	<i onClick={()=>this.triggerEditRoom(room)} className='ion-document-text' style={roomCardStyles().icon}></i>
				  	<i onClick={()=>this.removeRoom(room)} className='ion-close-round' style={roomCardStyles().icon}></i>
				</div>
			  	<div style={roomCardStyles().name}>{room.room_type}</div>
			  	<div style={roomCardStyles().regOrPromo}>{regOrPromo}</div>
			  	<div style={roomCardStyles().roomAndBath}>{room.rooms_per} Rooms & {room.bathrooms} Baths</div>
			  	<div style={roomCardStyles().note}>{room.note}</div>
			</div>
		)
	}

	renderUtilitiesTray(){
		return (
			<div style={comStyles().utilsTray}>
			  	<div key='water' onClick={()=>this.handleIconChange("water")} style={comIconStyles("water", this.state.propertyEdit.utils_list["water"]).water}>
					<i className='ion-waterdrop'></i> &nbsp;
					<span>Water</span>
				</div>
			  	<div key='heat' onClick={()=>this.handleIconChange("heat")} style={comIconStyles("heat", this.state.propertyEdit.utils_list["heat"]).heat}>
					<i className='ion-flame'></i> &nbsp;
					<span>Heat</span>
				</div>
			  	<div key='electric' onClick={()=>this.handleIconChange("electric")} style={comIconStyles("electric", this.state.propertyEdit.utils_list["electric"]).electric}>
					<i className='ion-flash'></i> &nbsp;
					<span>Electricity</span>
				</div>
			  	<div key='furnished' onClick={()=>this.handleIconChange("furnished")} style={comIconStyles("furnished", this.state.propertyEdit.utils_list["furnished"]).furnished}>
					<i className='ion-home'></i> &nbsp;
					<span>Furniture</span>
				</div>
			  	<div key='parking' onClick={()=>this.handleIconChange("parking")} style={comIconStyles("parking", this.state.propertyEdit.utils_list["parking"]).parking}>
					<i className='ion-model-s'></i> &nbsp;
					<span>Parking</span>
				</div>
			  	<div key='free_parking' onClick={()=>this.handleIconChange("free_parking")} style={comIconStyles("free_parking", this.state.propertyEdit.utils_list["free_parking"]).free_parking}>
					<i className='ion-model-s'></i> &nbsp;
					<span>Free Parking</span>
				</div>
			  	<div key='internet' onClick={()=>this.handleIconChange("internet")} style={comIconStyles("internet", this.state.propertyEdit.utils_list["internet"]).internet}>
					<i className='ion-wifi'></i> &nbsp;
					<span>Internet</span>
				</div>
			  	<div key='ac' onClick={()=>this.handleIconChange("ac")} style={comIconStyles("ac", this.state.propertyEdit.utils_list["ac"]).ac}>
					<i className='ion-ios-snowy'></i> &nbsp;
					<span>A/C</span>
				</div>
			  	<div key='laundry' onClick={()=>this.handleIconChange("laundry")} style={comIconStyles("laundry", this.state.propertyEdit.utils_list["laundry"]).laundry}>
					<i className='ion-tshirt'></i> &nbsp;
					<span>Laundry</span>
				</div>
			</div>
		)
	}

	clearContactForm(){
		this.setState({
			contactEdit: {
				visible: false,
				name: "",
				phone: "",
				email: ""
			}
		})
	}

	updateContactEdit(attr, event){
		this.setState({
			...this.state,
			contactEdit: {
					...this.state.contactEdit,
					[attr]: event.target.value
				}
		})
	}

	addContactForm(){
		if(this.state.contactEdit.name && this.state.contactEdit.email){
			this.removeContact(this.state.contactEdit, true)
			this.state.contactEdit.id = uuid.v4()
			this.state.contactEdit.position = this.state.propertyEdit.contacts.length
			this.setState({
				propertyEdit: {
					...this.state.propertyEdit,
					contacts: [
							...this.state.propertyEdit.contacts,
							this.state.contactEdit
						]
				},
				contactEdit: {
					id: "",
					visible: false,
					name: "",
					phone: "",
					email: "",
					position: 0
				},
				errorMessage: null
			})
		}else{
			this.setState({
				errorMessage: "A contact must include at least a name and email"
			})
			window.scrollBy(0, 800)
		}
	}

	renderContactForm(){
		if(this.state.contactEdit.visible){
			return (
				<div style={contactFormStyles().contactForm}>
				  	<div className="form-group">
				    	<input type="text" style={contactFormStyles().contactFormField} onChange={this.updateContactEdit.bind(this, "name")} value={this.state.contactEdit.name} className="form-control" placeholder="Name" />
				    	<input type="text" style={contactFormStyles().contactFormField} onChange={this.updateContactEdit.bind(this, "phone")} value={this.state.contactEdit.phone} className="form-control" placeholder="Phone" />
				    	<input type="email" style={contactFormStyles().contactFormField} onChange={this.updateContactEdit.bind(this, "email")} value={this.state.contactEdit.email} className="form-control" placeholder="Email" />
				    </div>
				  	<button style={contactFormStyles().addButton} className='btn btn-success btn-block' onClick={()=>this.addContactForm()} type='button'>Add</button>
				  	<button style={contactFormStyles().cancelButton} className='btn btn-warning btn-block' onClick={()=>this.clearContactForm()} type='button'>Cancel</button>
				</div>
			)
		}
	}

	renderContact(contact){
		return (
			<div className='card' style={contactFormStyles().contactCard}>
		  	<div style={roomCardStyles().editOrDel}>
				  	<i onClick={()=>this.triggerEditContact(contact)} className='ion-document-text' style={roomCardStyles().icon}></i>
				  	<i onClick={()=>this.removeContact(contact)} className='ion-close-round' style={roomCardStyles().icon}></i>
				</div>
				<div style={contactFormStyles().contactCardDetails}>
				  	<b>{contact.name}</b>
				  	{contact.phone}
				  	{contact.email}
			  	</div>
			</div>
		)
	}

	handlePropertyAttribute(attr, event){
		this.setState({
			propertyEdit: {
				...this.state.propertyEdit,
				[attr]: event.target.value
			}
		})
	}

	handlePropertyType(type){
		this.setState({
			propertyEdit: {
				...this.state.propertyEdit,
				type: type
			}
		})
	}

	toggleActive(){
		// console.log(this.state.propertyEdit.active)
		// console.log(!this.state.propertyEdit.active)
		this.setState({
			propertyEdit: {
				...this.state.propertyEdit,
				active: !this.state.propertyEdit.active
			}
		})

		// console.log(this.state.propertyEdit)
	}

	triggerImageDropzone(){
		this.setState({
			imageEdit: {
				...this.state.imageEdit,
				visible: !this.state.imageEdit.visible
			}
		})
	}

	renderImageDropzone(){
		if(this.state.imageEdit.visible){
			return (
				<div style={imageFormStyles().dropbox}>
								<Dropzone onDrop={this.addImageToArray.bind(this)} style={imageFormStyles().dropzone}>
		              <div>Drag files here, or click to select files to upload.</div>
		            </Dropzone>
		            <div id='landlordImageCol' style={imageFormStyles().previewRow}>
		            	{this.state.imageEdit.images.map((img)=>{
		            		return (
		            			<div key={img.preview} onClick={()=>this.removeImageFromDropzone(img)} style={imageFormStyles().imgWithDeleteIcon}>
		            				<i key={img.preview+"_delIcon"} className='ion-close-circled' style={imageFormStyles().deleteIcon}></i>
		            				<img key={img.preview+"_img"} src={img.preview} style={imageFormStyles().preview} />
		            			</div>
		            		)
		            	})}
		            </div>
		        </div>
			)
		}
	}

	addImageToArray(acceptedFiles, rejectedFiles){
		const filteredFiles = filterNonImages(acceptedFiles)
      	this.setState({
      		imageEdit: {
      			...this.state.imageEdit,
      			images: this.state.imageEdit.images.concat(filteredFiles)
      		}
      	})
	}

	removeImageFromArray(image){
		const c = confirm("Are you sure you want to remove this image?")
		if(c == true){
			let imgExistsPosition
			const imgExists = this.state.propertyEdit.images.filter((img, index)=>{
				if(img == image){
					imgExistsPosition = index
					return true
				}else{
					return false
				}
			})
			if(imgExists.length > 0){
				const x = this.state.propertyEdit.images
				x.splice(imgExistsPosition, 1)
				this.setState({
					propertyEdit: {
						...this.state.propertyEdit,
						images: x
					}
				})
			}
		}
	}

	removeImageFromDropzone(image){
		const c = confirm("Are you sure you want to remove this image?")
		if(c == true){
			let imgExistsPosition
			const imgExists = this.state.imageEdit.images.filter((img, index)=>{
				if(img == image){
					imgExistsPosition = index
					return true
				}else{
					return false
				}
			})
			if(imgExists.length > 0){
				const x = this.state.imageEdit.images
				x.splice(imgExistsPosition, 1)
				this.setState({
					imageEdit: {
						...this.state.imageEdit,
						images: x
					}
				})
			}
		}
	}

	renderImageThumbnail(img){
		return (
			<div key={img.photoKey+"_formSaved"} onClick={()=>this.removeImageFromArray(img)} style={imageFormStyles().imgWithDeleteIcon}>
				<i key={img.photoKey+"_delIcon_formSaved"} style={imageFormStyles().deleteIcon} className='ion-close-circled'></i>
				<img key={img.photoKey+"_img_formSaved"} src={img.urlThumbnail} style={imageFormStyles().thumb} />
			</div>
		)
	}

	submitPropertyToBeSaved(){
		if(this.state.propertyEdit.address && this.state.propertyEdit.rooms.length > 0 && this.state.propertyEdit.contacts.length >0){
			this.setState({
				loadingMessage: "Uploading images... this may take a few minutes",
				loading: true
			})
			if(this.state.imageEdit.images.length > 0){
				// observable onNext function
				const S3ImageObjs = []

				const imageUploaded = (S3ImageObj) => {
					S3ImageObjs.push(S3ImageObj)
					this.setState({
						loadingMessage: `Successfully uploaded ${S3ImageObj.photoKey}`
					})
				}

				// observable onComplete function
				const allImagesUploaded = () => {
						this.setState({
							loadingMessage: `Successfully uploaded all ${S3ImageObjs.length} images`,
							propertyEdit: {
								...this.state.propertyEdit,
								images: this.state.propertyEdit.images.concat(S3ImageObjs)
							}
						})
						if(this.state.newProperty){
							saveLandlordPropertyToDB(this.state.propertyEdit)
								.then((data)=>{
										if(!this.state.newProperty){
											this.props.selectLandlordProperty(data)
											this.props.trackActivity("APP", "NEW_PROPERTY", {id: this.props.landlord.id}, this.props.fbUser, this.props.geoCoords, {leaseId: data._id, lease: data})
										}
										this.props.trackActivity("APP", "PROPERTY_EDITED", {id: this.props.landlord.id}, this.props.fbUser, this.props.geoCoords, {leaseId: data._id, lease: data})
										return getLeasesForLandlord(this.props.landlord.id)
									})
									.then((data)=>{
										this.props.saveLeasesToState(data)
										this.props.exitPropertyForm()
									})
									.catch((err)=>{
										// console.log(err)
										this.setState({loading: false})
									})
							// return saveLandlordPropertyToDB(this.state.propertyEdit)
						}else{
							updateLandlordPropertyInDB(this.state.propertyEdit)
								.then((data)=>{
										if(!this.state.newProperty){
											this.props.selectLandlordProperty(data)
											this.props.trackActivity("APP", "NEW_PROPERTY", {id: this.props.landlord.id}, this.props.fbUser, this.props.geoCoords, {leaseId: data._id, lease: data})
										}
										this.props.trackActivity("APP", "PROPERTY_EDITED", {id: this.props.landlord.id}, this.props.fbUser, this.props.geoCoords, {leaseId: data._id, lease: data})
										return getLeasesForLandlord(this.props.landlord.id)
									})
									.then((data)=>{
										this.props.saveLeasesToState(data)
										this.props.exitPropertyForm()
									})
									.catch((err)=>{
										// console.log(err)
										this.setState({loading: false})
									})
							// return updateLandlordPropertyInDB(this.state.propertyEdit)
						}
				}

				// upload batch of images to S3 as an observable stream
				uploadImagesSeriesRx(this.props.landlord.email, this.state.imageEdit.images)
					.subscribe(
						imageUploaded,
						null,
						allImagesUploaded
				)

			}else{
				let saveAction
				if(this.state.newProperty){
					saveAction = saveLandlordPropertyToDB(this.state.propertyEdit)
				}else{
					saveAction = updateLandlordPropertyInDB(this.state.propertyEdit)
				}
				// console.log(saveAction)
				saveAction.then((data)=>{
							if(!this.state.newProperty){
								this.props.selectLandlordProperty(data)
							}
							return getLeasesForLandlord(this.props.landlord.id)
						})
						.then((data)=>{
							this.props.saveLeasesToState(data)
							this.props.exitPropertyForm()
						})
						.catch((err)=>{
							// console.log(err)
							this.setState({loading: false})
						})
			}
		}else{
			this.setState({
				errorMessage: "A property must have at least an address, one suite, and one contact"
			})
			window.scrollBy(0, 800)
		}
	}

	render() {
		return (
			<div style={comStyles().mainview}>
				<i onClick={this.props.exitPropertyForm} className='ion-close-round' style={comStyles().exitIcon}></i>
    			<form>
					{/* Google Autocomplete Address & Building Name */}
				  	<div className="form-group">
				    	<input id="autocomplete" type="text" onFocus={this.geolocate.bind(this)} onChange={this.handlePropertyAttribute.bind(this, "address")} value={this.state.propertyEdit.address} className="form-control" placeholder="Address" style={comStyles().address} />
				    	<input type="text" onChange={this.handlePropertyAttribute.bind(this, "building_name")} value={this.state.propertyEdit.building_name} className="form-control" placeholder="Building Name (optional)" style={comStyles().buildingName} />
				    </div>

					{/* Building Type */}
				  	<div className="form-group" style={comStyles().propTypeBox}>
				  		<div key='type_apartment' onClick={()=>this.handlePropertyType("apartment")} style={comStylesPropType(this.state.propertyEdit.type).apartment}>Apartment</div>
				  		<div key='type_house' onClick={()=>this.handlePropertyType("house")} style={comStylesPropType(this.state.propertyEdit.type).house}>House</div>
				  	</div>

					{/* Rooms Tray */}
				  	<div style={roomCardStyles().roomsTray}>

						{/* Room Header */}
					  	<div className="card" style={roomCardStyles().header}>
					  		<div style={roomCardStyles().roomHeader}>Suites</div>
					  		{
					  			this.state.roomEdit.visible
					  			?
					  			<i onClick={()=>this.triggerEditRoom()} style={roomCardStyles().add} className='ion-minus-circled'></i>
					  			:
					  			<i onClick={()=>this.triggerEditRoom()} style={roomCardStyles().add} className='ion-android-add-circle'></i>
					  		}

							{/* Room Form */}
						  	{this.renderRoomForm()}
						  	{
						  		this.state.roomEdit.visible && this.state.errorMessage
						  		?
							  	<div className='alert alert-danger' style={comStyles().errorMessage}>
									{this.state.errorMessage}
								</div>
						  		:
						  		null
						  	}
					  	</div>

						{/* Rooms Existing */}
					  	<div style={comStyles().roomsBox}>
					  		{this.state.propertyEdit.rooms.sort((a, b)=>{
									return a.position < b.position
								}).map(this.renderRoom.bind(this))}
					  	</div>
					</div>

					{/* Contacts Tray */}
				  	<div style={contactFormStyles().contactsTray}>

				  		<div className='card' style={contactFormStyles().header}>
				  			<div style={contactFormStyles().title}>
				  				Contact Info
				  			</div>
							{/* Add New */}
				  			<div style={contactFormStyles().addIcon} onClick={()=>this.triggerEditContact()}>
				  				{
				  					this.state.contactEdit.visible
				  					?
				  					<i className='ion-minus-circled'></i>
				  					:
				  					<i className='ion-android-add-circle'></i>
				  				}
				  			</div>
							{/* Contact Form */}
						  	{this.renderContactForm()}
						  	{
						  		this.state.contactEdit.visible && this.state.errorMessage
						  		?
							  	<div className='alert alert-danger' style={comStyles().errorMessage}>
									{this.state.errorMessage}
								</div>
						  		:
						  		null
						  	}
				  		</div>

					  	<div style={contactFormStyles().contactsBox}>
							{/* Contacts Existing */}
						  	{this.state.propertyEdit.contacts.sort((a, b)=>{
									return a.position - b.position
								}).map(this.renderContact.bind(this))}
					  	</div>
					</div>

					{/* Image Uploader */}
				  	<div style={imageFormStyles().imagesTray}>

				  		<div className='card' style={contactFormStyles().header}>
				  			<div style={contactFormStyles().title}>
				  				Images
				  			</div>
							{/* Add New */}
				  			<div style={contactFormStyles().addIcon} onClick={()=>this.triggerImageDropzone()}>
				  				{
				  					this.state.imageEdit.visible
				  					?
				  					<i className='ion-minus-circled'></i>
				  					:
				  					<i className='ion-android-add-circle'></i>
				  				}
				  			</div>
							{/* Drag and Drop Images */}
						  	{this.renderImageDropzone()}
				  		</div>

					  	<div style={imageFormStyles().thumbRow}>
							{/* Images Existing */}
						  	{this.state.propertyEdit.images.map(this.renderImageThumbnail.bind(this))}
					  	</div>
					</div>

					{/* Notes */}
				    <div style={noteStyles().noteBox} className="form-group">
				      	<label style={noteStyles().title}>Notes</label>
				    	<textarea style={noteStyles().noteField} onChange={this.handlePropertyAttribute.bind(this, "note")} value={this.state.propertyEdit.note} className="form-control" rows="4" placeholder='Any additional notes about this property. Nearby to bus stops, groceries, school... etc'></textarea>
				  	</div>

					{/* Utilities Tray */}
					{this.renderUtilitiesTray()}

					{
						this.state.errorMessage
						?
						<div className='alert alert-danger' style={comStyles().errorMessage}>
							{this.state.errorMessage}
						</div>
						:
						null
					}

				  	{
						this.state.loading
						?
						<div style={comStyles().loadingBox}>
							<img src='../../../res/images/loading.gif' style={comStyles().loadingGif} />
							<div style={comStyles().loadingMessage}>{this.state.loadingMessage}</div>
						</div>
						:
						<div className='col-md-12' style={comStyles().finalBar}>
						  	{/* Action Button Bars */}
						    <div className='col-md-10' style={comStyles().actionButtons}>
								{/* Save Button */}
								<button onClick={this.submitPropertyToBeSaved.bind(this)} style={comStyles().saveButton} type="button" className="btn btn-success">Save</button>
								{/* Cancel Button */}
								<button onClick={this.props.exitPropertyForm} style={comStyles().cancelButton} type="button" className="btn btn-warning">Cancel</button>
							</div>

						  	{/* Active Toggle */}
						  	<div className='col-md-2' style={comStyles().toggleActive}>
						     <label className="toggle toggle-balanced">
						       <input onChange={()=>this.toggleActive()} type="checkbox" checked={this.state.propertyEdit.active} />
						       <div className="track" style={comStyles().activeCheckbox}>
						         <div className="handle"></div>
						       </div>
						     </label>
						     <div style={comStyles().toggleActiveText}>ACTIVE</div>
						    </div>
						</div>
					}

				</form>
			</div>
		);
	}
}

PropertyForm.propTypes = {
	landlord: React.PropTypes.object,
	propertyOnForm: React.PropTypes.object,
	region: React.PropTypes.string.isRequired,
	geoCoords: React.PropTypes.array
}

const RadiumHOC = Radium(PropertyForm)

function mapStateToProps(state){
	return {
		landlord: state.landlord.landlord,
		propertyOnForm: state.landlord.propertyOnForm,
		region: state.app.region,
		geoCoords: state.app.geoCoords
	}
}

export default connect(mapStateToProps, { exitPropertyForm, saveLeasesToState, selectLandlordProperty, trackActivity })(RadiumHOC)

// ===============================================

const comStyles = () => {
	return {
		mainview: {
			padding: "10px",
			width: "1000px",
			margin: "auto",
			backgroundColor: xDeepBlue,
			padding: "20px"
		},
		address: {
			fontSize: "1.2rem",
			textAlign: "center",
			borderRadius: "5px 5px 0px 0px",
			fontWeight: "bold",
			backgroundColor: "rgba(256,256,256,0.95)"
		},
		buildingName: {
			fontSize: "1.2rem",
			textAlign: "center",
			borderRadius: "0px 0px 5px 5px",
			backgroundColor: "rgba(256,256,256,0.95)"
		},
		propTypeBox: {
			flex: "display",
			flexDirection: "row",
			justifyContent: "center",
			width: "100%",
		},
		utilsTray: {
			display: "flex",
			flexWrap: "wrap",
			flexDirection: "row",
			justifyContent: "center",
			padding: "10px",
			fontSize: "1.2rem"
		},
		toggleActive: {
			display: "flex",
			flexDirection: "row",
			cursor: "pointer"
		},
		toggleActiveText: {
			display: "inline-block",
			fontSize: "1.3rem",
			color: "white",
			padding: "5px",
		},
		actionButtons: {
			display: "flex"
		},
		saveButton: {
			display: "inline-block",
			width: "70%",
			borderRadius: "5px 0xp 0px 5px",
			fontSize: "1.3rem",
			fontWeight: "bold"
		},
		cancelButton: {
			display: "inline-block",
			width: "30%",
			borderRadius: "0px 5xp 5px 0px",
			fontSize: "1.3rem",
			fontWeight: "bold"
		},
		finalBar: {
			margin: "20px 0px 0px 0px",
			backgroundColor: xDeepBlue,
			width: "100%",
			padding: "10px",
			borderRadius: "0px 0px 10px 10px"
		},
		exitIcon: {
			fontSize: "2.2rem",
			color: "white",
			position: "relative",
			float: "right",
			margin: "0px 0px 10px 0px"
		},
		errorMessage: {
			width: "100%",
		},
		loadingBox: {
			width: "500px",
			margin: "auto",
			display: "flex",
			flexDirection: "column",
			justifyContent: "center",
			alignItems: "center"
		},
		loadingGif: {
			width: "50px",
			height: "50px",
			margin: "20px"
		},
		loadingMessage: {
			fontSize: "1rem",
			color: "white"
		},
		activeCheckbox: {
			cursor: "pointer"
		}
	}
}

const comIconStyles = (attr, bool) => {

	const iconList = {
		water: false,
		heat: false,
		electric: false,
		furnished: false,
		parking: false,
		free_parking: false,
		internet: false,
		ac: false,
		laundry: false
	}
	iconList[attr] = bool

	for(let key in iconList){
		if(iconList[key] == true){
			iconList[key] = "white"
		}else{
			iconList[key] = "rgba(256,256,256,0.3)"
		}
	}

	return {
		water: {
			color: iconList["water"],
			display: "inline-block",
			flexGrow: 1,
			textAlign: "center",
			margin: "10px",
			cursor: "pointer",
			":hover":{
				color: "white"
			}
		},
		heat: {
			color: iconList["heat"],
			display: "inline-block",
			flexGrow: 1,
			textAlign: "center",
			margin: "10px",
			cursor: "pointer",
			":hover":{
				color: "white"
			}
		},
		electric: {
			color: iconList["electric"],
			display: "inline-block",
			flexGrow: 1,
			textAlign: "center",
			margin: "10px",
			cursor: "pointer",
			":hover":{
				color: "white"
			}
		},
		furnished: {
			color: iconList["furnished"],
			display: "inline-block",
			flexGrow: 1,
			textAlign: "center",
			margin: "10px",
			cursor: "pointer",
			":hover":{
				color: "white"
			}
		},
		parking: {
			color: iconList["parking"],
			display: "inline-block",
			flexGrow: 1,
			textAlign: "center",
			margin: "10px",
			cursor: "pointer",
			":hover":{
				color: "white"
			}
		},
		free_parking: {
			color: iconList["free_parking"],
			display: "inline-block",
			flexGrow: 1,
			textAlign: "center",
			margin: "10px",
			cursor: "pointer",
			":hover":{
				color: "white"
			}
		},
		internet: {
			color: iconList["internet"],
			display: "inline-block",
			flexGrow: 1,
			textAlign: "center",
			margin: "10px",
			cursor: "pointer",
			":hover":{
				color: "white"
			}
		},
		ac: {
			color: iconList["ac"],
			display: "inline-block",
			flexGrow: 1,
			textAlign: "center",
			margin: "10px",
			cursor: "pointer",
			":hover":{
				color: "white"
			}
		},
		laundry: {
			color: iconList["laundry"],
			display: "inline-block",
			flexGrow: 1,
			textAlign: "center",
			margin: "10px",
			cursor: "pointer",
			":hover":{
				color: "white"
			}
		}
	}
}


const comStylesPropType = (type) => {

	const typeList = {
		apartment: false,
		house: false
	}
	typeList[type] = true

	for(let key in typeList){
		if(typeList[key]){
			typeList[key] = "white"
		}else{
			typeList[key] = "rgba(256,256,256,0.3)"
		}
	}

	return {
		apartment: {
			color: typeList["apartment"],
			display: "inline-block",
			width: "50%",
			textAlign: "center",
			padding: "7px",
			fontSize: "1.2rem",
			fontWeight: "bold",
			":hover": {
	          color: "rgba(256,256,256,1)",
	          cursor: "pointer"
	        }
		},
		house: {
			color: typeList["house"],
			display: "inline-block",
			width: "50%",
			textAlign: "center",
			padding: "7px",
			fontSize: "1.2rem",
			fontWeight: "bold",
			":hover": {
	          color: "rgba(256,256,256,1)",
	          cursor: "pointer"
	        }
		}
	}
}

const roomCardStyles = () => {
	return {
		roomsTray: {
			padding: "10px",
			borderRadius: "5px",
			display: "flex",
			flexDirection: "column",
			flexWrap: "wrap",
			backgroundColor: xDeepBlue
		},
		header: {
			margin: "0px 0px 10px 0px",
			padding: "5px",
			backgroundColor: xDeepBlue,
			color: "white"
		},
		roomHeader: {
			fontSize: "1.7rem",
			fontWeight: "bold",
			display: "inline-block",
			backgroundColor: xDeepBlue,
			textAlign: "center",
			width: "95%"
		},
		add: {
			fontSize: "2rem",
			fontWeight: "bold",
			display: "inline-block",
			float: "right",
			width: "5%",
			cursor: "pointer"
		},
		roomsBox: {
			display: "flex",
			flexDirection: "row",
			justifyContent: "center",
			margin: "5px 0px 5px 0px"
		},
		roomCard: {
			borderRadius: "5px",
			display: "inline-block",
			textAlign: "center",
			padding: "5px",
			width: "30%",
			height: "200px",
			margin: "3px 1% 3px 1%",
			backgroundColor: "rgba(0,0,0,0.1)",
			color: "white",
			cursor: "move"
		},
		name: {
			fontSize: "1.2rem",
			fontWeight: "bold",
			margin: "5px"
		},
		regOrPromo: {
			fontSize: "1rem",
		},
		roomAndBath: {
			fontSize: "1rem",
		},
		note: {
			fontSize: "0.9rem",
			margin: "10px"
		},
		editOrDel: {
			fontSize: "1.3rem",
			padding: "10px"
		},
		icon: {
			margin: "10px"
		},
		promoAlert: {
			color: xBootstrapRed,
			fontWeight: "bold"
		}
	}
}


const roomFormStyles = () => {
	return {
		roomForm: {
			display: "flex",
			margin: "5px 0px 0px 0px",
			width: "100%",
			color: "white",
			backgroundColor: "rgba(256,256,256,0)"
		},
		suitename: {
			textAlign: "center",
			backgroundColor: "rgba(256,256,256,0.05)",
			color: "white",
		},
		sliderBox: {
			width: "100%",
			display: "flex",
			flexDirection: "row",
			padding: "5px"
		},
		rangeContainer: {
			width: "100%",
		},
		range: {
			backgroundColor: "rgba(0,0,0,0)",
			fontSize: "2rem",
			fontWeight: "bold",
			color: "white"
		},
		incrementalIconHorzPrice: {
			width: "6%",
			display:"flex",
			alignItems: "center",
			justifyContent: "center",
			cursor: "pointer"
		},
		priceBox: {
			padding: "5px"
		},
		price: {
			textAlign: "center",
			fontSize: "1.2rem",
			fontWeight: "bold",
		},
		leaseTerms: {
			textAlign: "center",
			fontSize: "1rem",
			padding: "5px 0px 5px 0px"
		},
		promoBox: {
			display: "flex",
			flexDirection: "row",
			justifyContent: "center",
			alignItems: "center",
			height: "70%",
		},
		promoToggle: {
			margin: "auto",
			display: "inline-block",
			width: "40%"
		},
		promoLabel: {
			display: "inline-block",
			textAlign: "center",
			alignItems: "center",
			fontWeight: "bold",
			width: "60%",
			fontSize: "1rem"
		},
		bedBathAndBeyond: {
			display: "flex",
			flexDirection: "column",
			alignItems: "center",
			padding: "10px 0px 5px 0px"
		},
		upAndDown: {
			display: "flex",
			flexDirection: "row",
			justifyContent: "center",
			width: "100%",
			height: "30%",
		},
		incrementalIconHorz: {
			margin: "0px 5px 0px 5px",
			fontSize: "1rem",
			fontWeight: "bold",
			alignItems: "center",
			cursor: "pointer"
		},
		note: {
			height: "95%",
			backgroundColor: "rgba(256,256,256,0.05)",
			border: "rgba(256,256,256,0.1)",
			color: "white"
		},
		lastBox: {
			display: "flex",
			flexDirection: "column",
			height: "30%"
		},
		buttonRow: {
			display: "flex",
			flexDirection: "column",
			height: "30%"
		},
		addButton: {
			borderRadius: "5px 5px 0px 0px"
		},
		cancelButton: {
			borderRadius: "0px 0px 5px 5px"
		}
	}
}


const contactFormStyles = () => {
	return {
		contactsTray: {
			padding: "5px",
			borderRadius: "5px",
			display: "flex",
			flexDirection: "column",
			flexWrap: "wrap",
			backgroundColor: xDeepBlue,
			margin: "0px 0px 0px 0px"
		},
		header: {
			margin: "0px 0px 10px 0px",
			backgroundColor: xDeepBlue,
			color: "white",
			padding: "10px",
			borderRadius: "5px"
		},
		title: {
			fontSize: "1.7rem",
			fontWeight: "bold",
			display: "inline-block",
			textAlign: "center",
			width: "95%"
		},
		addIcon: {
			fontSize: "2rem",
			fontWeight: "bold",
			display: "inline-block",
			float: "right",
			width: "5%",
			cursor: "pointer"
		},
		contactForm: {
			display: "flex",
			flexDirection: "column",
			margin: "10px",
			backgroundColor: "rgba(256,256,256,0)",
			color: "white"
		},
		contactFormField: {
			textAlign: "center",
			backgroundColor: "rgba(256,256,256,0.05)",
			color: "white",
			fontSize: "1rem"
		},
		addButton: {
			margin: "0px 0px 0px 0px",
			borderRadius: "5px 5px 0px 0px"
		},
		cancelButton: {
			margin: "0px 0px 5px 0px",
			borderRadius: "0px 0px 5px 5px"
		},
		contactsBox: {
			display: "flex",
			flexDirection: "row",
			justifyContent: "center"
		},
		contactCard: {
			width: "30%",
			height: "150px",
			textAlign: "center",
			borderRadius: "5px",
			backgroundColor: "rgba(0,0,0,0.1)",
			color: "white",
			cursor: "move"
		},
		contactCardDetails: {
			display: "flex",
			flexDirection: "column",
			padding: "10px"
		}
	}
}

const imageFormStyles = () => {
	return {
		imagesTray: {
			padding: "5px",
			borderRadius: "5px",
			display: "flex",
			flexDirection: "column",
			flexWrap: "wrap",
			backgroundColor: xDeepBlue,
			margin: "0px 0px 0px 0px"
		},
		dropbox: {
			display: "flex",
			flexDirection: "column",
			padding: "20px"
		},
		dropzone: {
			width: "100%",
			height: "100px",
			textAlign: "center",
			alignItems: "center",
			border: "3px dashed white",
			color: "white",
			padding: "10px",
			fontSize: "1rem"
		},
		previewRow: {
			display: "flex",
			flexDirection: "row",
			justifyContent: "center",
			overflowX: "scroll",
			overflowY: "hidden",
			height: "100px",
			padding: "5px"
		},
		preview: {
			width: "auto",
			height: "80px",
			borderRadius: "0px"
		},
		thumbRow: {
			display: "flex",
			flexDirection: "row",
			justifyContent: "center",
			overflowX: "scroll",
			overflowY: "hidden",
			height: "100px"
		},
		thumb: {
			height: "80px",
			width: "auto",
			borderRadius: "0px",
			":hover":{
				opacity: "0.5"
			}
		},
		imgWithDeleteIcon: {
			position: "relative",
			color: "rgba(0,0,0,0)",
			":hover":{
				color: "rgba(256,256,256,1)"
			},
		},
		deleteIcon: {
			zIndex: "10",
			fontSize: "2rem",
			position: "absolute",
			top: "25%",
			left: "40%",
		}
	}
}

const noteStyles = () => {
	return {
		noteBox: {
			padding: "10px",
			borderRadius: "5px",
			display: "flex",
			flexDirection: "column",
			flexWrap: "wrap",
			backgroundColor: xDeepBlue,
			margin: "0px 0px 0px 0px"
		},
		title: {
			margin: "0px 0px 0px 0px",
			backgroundColor: xDeepBlue,
			color: "white",
			padding: "10px",
			fontSize: "1.7rem",
			fontWeight: "bold",
			textAlign: "center",
			width: "100%"
		},
		noteField: {
			backgroundColor: "rgba(256,256,256,0.95)",
			textAlign: "center"
		}
	}
}
