import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import { browserHistory } from 'react-router'
import { xMidBlue } from '../../stylesJS/base_colors'

import LandlordHeader from './LandlordHeader'
import { getLeasesForLandlord } from '../../api/landlordManager'
import { saveLeasesToState } from '../../actions/landlord_actions'

class Landlord extends Component {

	componentDidMount(){
		if(!this.props.authenticated){
			browserHistory.push('/signin')
		}else{
			getLeasesForLandlord(this.props.landlord.id)
				.then((data)=>{
					this.props.saveLeasesToState(data)
				})
				.catch((err)=>{
					// console.log(err)
				})
			if(this.props.claimId){
				browserHistory.push('/landlord/claim')
			}
		}
	}

	render() {
		if(this.props.authenticated){
			return (
				<div style={comStyles().background}>
					<LandlordHeader />
					{this.props.children}
				</div>
			)
		}else{
			return (<div></div>)
		}
	}
}

Landlord.propTypes = {
	landlord: React.PropTypes.object.isRequired,
	authenticated: React.PropTypes.bool,
	claimId: React.PropTypes.string
};

const RadiumHOC = Radium(Landlord);

function mapStateToProps(state){
	return {
		landlord: state.landlord.landlord,
		authenticated: state.auth.authenticated,
		claimId: state.landlord.claimId
	}
}

export default connect(mapStateToProps, {saveLeasesToState})(RadiumHOC);

// ==================================


const comStyles = () => {
	return {
		background: {
			backgroundColor: xMidBlue,
			width: "100%",
			height: "100%",
			margin: "0",
			left: "0",
			top: "0",
			overflow: "scroll"
		}
	}
}
