import React, {Component} from 'react';
import {connect} from 'react-redux';
import Radium from 'radium'
import {browserHistory} from 'react-router'
import {updateLandlordInfo, signOutLandlord} from '../../api/aws/aws-cognito'
import {setLandlord} from '../../actions/auth_actions'
import {filterNonImages} from '../../api/iconSorter'
import {uploadImagesBatch, createThumbnail} from '../../api/aws/aws-S3'

import Dropzone from 'react-dropzone'

class LandlordSettings extends Component {

	constructor(){
		super()
		this.state = {
			"custom:company_name": "",
			"custom:default_email": "",
			"custom:default_phone": "",
			"custom:email_forward": "",
			"website": "",
			"custom:company_logo": null,
			uploadedFiles: null,
			errorMessage: null,
			loading: false
		}
	}

	componentWillMount(){
		this.setState({
			"custom:company_name": this.props.landlord.company_name || null,
			"custom:company_logo": this.props.landlord.company_logo || null,
			"custom:default_email": this.props.landlord.default_email || null,
			"custom:default_phone": this.props.landlord.default_phone || null,
			"custom:email_forward": this.props.landlord.email_forward || null,
			"website": this.props.landlord.website || null
		})
	}

	handleChange(attr, event){
		this.setState({
			[attr]: event.target.value
		})
	}

	uploadNewLogo(acceptedFiles, rejectedFiles){
		const filteredFiles = filterNonImages(acceptedFiles)
      	this.setState({
      		uploadedFiles: filteredFiles
      	})
	}

	whichImage(){
		if(this.state.uploadedFiles){
			return this.state.uploadedFiles[0].preview
		}else{
			return this.props.landlord.company_logo
		}
	}

	logout(){
		signOutLandlord()
		browserHistory.push('/signout')
	}

	saveChanges(){
		const self = this
		self.setState({loading: true})
		if(self.state.uploadedFiles){

			uploadImagesBatch(this.props.landlord.email, self.state.uploadedFiles)
				.then((S3ImageObjs)=>{
					const editedInfo = {}
					self.setState({
						"custom:company_logo": S3ImageObjs[0].urlThumbnail
					})
					for(let key in this.state){
						editedInfo[key] = this.state[key]
						if(this.state[key] == "" || !this.state[key]){
							editedInfo[key] = " "
						}
					}
					// console.log(editedInfo)
					return updateLandlordInfo(self.props.landlord.email, editedInfo)
				})
				.then((data)=>{
					self.props.setLandlord(data)
					browserHistory.push('/landlord/dashboard')
				})
				.catch((err)=>{
					// console.log(err)
					self.setState({
						errorMessage: err.message,
						loading: false
					})
				})
			}else{
				const editedInfo = {}
				for(let key in this.state){
					editedInfo[key] = this.state[key]
					if(this.state[key] == ""){
						editedInfo[key] = " "
					}
				}
				updateLandlordInfo(self.props.landlord.email, editedInfo)
					.then((data)=>{
						self.props.setLandlord(data)
						browserHistory.push('/landlord/dashboard')
					})
					.catch((err)=>{
						// console.log(err)
						self.setState({
							errorMessage: err.message,
							loading: false
						})
					})
			}
	}

	render() {
		return (
			<div style={comStyles().mainview}>
				<form style={comStyles().form}>
					<div className='form-group'>
						<label style={comStyles().formText}>Company Name</label>
						{/* Field name determines the name of the variables being passed to handleSubmit(this.props.signinLandlord) */}
						<input value={this.state["custom:company_name"]} onChange={this.handleChange.bind(this, 'custom:company_name')} type="text" className='form-control' style={comStyles().formInput} />
					</div>
					<label style={comStyles().formText}>Company Logo</label>
					<div style={comStyles().dropbox}>
						<Dropzone onDrop={this.uploadNewLogo.bind(this)} multiple={false} style={comStyles().dropzone}>
			              <div>Drag files here, or click to select files to upload.</div>
			            </Dropzone>
			            <div style={comStyles().logoOutline}>
				            <img src={this.whichImage()} style={comStyles().logoThumb} />
			            </div>
		            </div>
					<div className='form-group'>
						<label style={comStyles().formText}>Default Contact Email</label>
						{/* Field name determines the name of the variables being passed to handleSubmit(this.props.signinLandlord) */}
						<input value={this.state["custom:default_email"]} onChange={this.handleChange.bind(this, 'custom:default_email')} type="email" className='form-control' style={comStyles().formInput} />
					</div>
					<div className='form-group'>
						<label style={comStyles().formText}>Default Contact Phone</label>
						{/* Field name determines the name of the variables being passed to handleSubmit(this.props.signinLandlord) */}
						<input value={this.state["custom:default_phone"]} onChange={this.handleChange.bind(this, 'custom:default_phone')} type="text" className='form-control' style={comStyles().formInput} />
					</div>
				</form>
				<form style={comStyles().form}>
					<div className='form-group'>
						<label style={comStyles().formText}>Forward Customer Emails To:</label>
						{/* Field name determines the name of the variables being passed to handleSubmit(this.props.signinLandlord) */}
						<input value={this.state["custom:email_forward"]} onChange={this.handleChange.bind(this, 'custom:email_forward')} type="email" className='form-control' style={comStyles().formInput} />
					</div>
					<div className='form-group'>
						<label style={comStyles().formText}>Website:</label>
						{/* Field name determines the name of the variables being passed to handleSubmit(this.props.signinLandlord) */}
						<input value={this.state["website"]} onChange={this.handleChange.bind(this, 'website')} type="text" className='form-control' style={comStyles().formInput} />
					</div>
				</form>
				{
					this.state.errorMessage
					?
					<div className='alert alert-danger' style={comStyles().errorMessage}>
						{this.state.errorMessage}
					</div>
					:
					null
				}
				{
					this.state.loading
					?
					<div style={comStyles().loadingBox}>
						<img src='../../../res/images/loading.gif' style={comStyles().loadingGif} />
					</div>
					:
					<button style={comStyles().logout} onClick={this.saveChanges.bind(this)} className='btn btn-success' type='button'>SAVE CHANGES</button>
				}

				<button style={comStyles().logout} onClick={this.logout} className='btn btn-warning' type='button'>LOG OUT</button>
			</div>
		);
	}
}

LandlordSettings.propTypes = {
	landlord: React.PropTypes.object
};

const RadiumHOC = Radium(LandlordSettings);

function mapStateToProps(state){
	return {
		landlord: state.landlord.landlord
	}
}

export default connect(mapStateToProps, {setLandlord})(RadiumHOC);

// ================================

const comStyles = () => {
	return {
		mainview: {
			display: "flex",
			flexDirection: "column",
			justifyContent: "center"
		},
		form: {
			width: "500px",
			margin: "auto",
			"@media (max-width: 600px), and (max-height: 740px)": {
				width: "90%"
			}
		},
		logout: {
			width: "500px",
			margin: "auto",
			"@media (max-width: 600px), and (max-height: 740px)": {
				width: "90%"
			}
		},
		formText: {
			color: "white",
			fontSize: "1.2rem",
			fontWeight: "bold",
		},
		formInput: {
			fontSize: "1.2rem",
			textAlign: "center",
			backgroundColor: "rgba(256,256,256,0.9)"
		},
		dropbox: {
			width: "100%",
			display: "flex",
			flexDirection: "row"
		},
		dropzone: {
			width: "75%",
			height: "100px",
			textAlign: "center",
			alignItems: "center",
			border: "3px dashed white",
			color: "white",
			padding: "10px",
			fontSize: "1rem"
		},
		logoOutline: {
			width: "100%",
			maxWidth: "25%",
			maxHeight: "100%",
			overflow: "hidden",
			border: "2px dashed white",
		},
		logoThumb: {
			width: "100%",
			height: "100%"
		},
		errorMessage: {
			width: "500px",
			margin: "auto",
			"@media (max-width: 600px), and (max-height: 740px)": {
				width: "90%"
			}
		},
		loadingBox: {
			width: "500px",
			margin: "auto",
			display: "flex",
			flexDirection: "row",
			justifyContent: "center",
			alignItems: "center",
			padding: "10px",
			"@media (max-width: 600px), and (max-height: 740px)": {
				width: "90%"
			}
		},
		loadingGif: {
			width: "50px",
			height: "50px"
		}
	}
}
