import React, {Component} from 'react';
import { connect } from 'react-redux';
import {signupLandlord, sendError, authError} from '../../actions/auth_actions';
import Radium from 'radium'
import { browserHistory } from 'react-router'
import {loadEmailCredsOfVerification} from '../../actions/landlord_actions'
import { xMidBlue } from '../../stylesJS/base_colors'


import {signUpLandlord} from '../../api/aws/aws-cognito'


class Signup extends Component {

	constructor(){
		super()
		this.state = {
			email: "",
			company_name: "",
			default_email: "",
			default_phone: "",
			password: "",
			confirmPassword: "",
			email_forward: "",
			errorMessage: null,
			loading: false
		}
	}

	handleChange(attr, event){
		this.setState({
			[attr]: event.target.value
		})
	}

	renderAlert(){
		if(this.state.errorMessage){
			return (
				<div className='alert alert-danger' style={comStyles().errorMessage}>
					<strong>Oops!</strong> {this.state.errorMessage}
				</div>
			);
		}
	}

	signup(){
		if(this.state.company_name && this.state.email && this.state.password){
			if(this.state.password == this.state.passwordConfirm){
				this.setState({loading: true})
				signUpLandlord(this.state)
					.then(({email, password})=>{
						localStorage.setItem('RentBurrow_Email', email)
						browserHistory.push('/verify_account')
					})
					.catch((err)=>{
						this.setState({
							errorMessage: err.message,
							loading: false
						})
					})
			}else{
				this.setState({
					errorMessage: "Passwords do not match"
				})
			}
		}else{
			this.setState({
				errorMessage: "Please include a company name, email address and password."
			})
		}
	}

	redirectToSignin(){
		this.props.authError(null)
		browserHistory.push('/signin')
	}

	render(){
		return (
			<div style={comStyles().mainview}>
				<div style={comStyles().entrance}>
					<img src='../../../res/images/rentburrow_logo.png' style={comStyles().logo} />
					<h1 style={comStyles().landlordText}>Landlord Sign Up</h1>
					<form style={comStyles().form}>
						<div className='form-group'>
							<label style={comStyles().formText}>Company Name:</label>
							<input value={this.state.company_name} onChange={this.handleChange.bind(this, "company_name")} type="text" className='form-control' style={comStyles().formInput} />
						</div>
						<div className='form-group'>
							<label style={comStyles().formText}>Email: <span style={comStyles().note}>(Used only for verifying account & resetting password)</span></label>
							<input value={this.state.email} onChange={this.handleChange.bind(this, "email")} type="email" className='form-control' style={comStyles().formInput} />
						</div>
						<div className='form-group'>
							<label style={comStyles().formText}>Password:</label>
							<input value={this.state.password} onChange={this.handleChange.bind(this, "password")} type="password" className='form-control' style={comStyles().formInput} />
						</div>
						<div className='form-group'>
							<label style={comStyles().formText}>Confirm Password:</label>
							<input value={this.state.passwordConfirm} onChange={this.handleChange.bind(this, "passwordConfirm")} type="password" className='form-control' style={comStyles().formInput} />
						</div>
						{this.renderAlert()}

						{
							this.state.loading
							?
							<div style={comStyles().loadingBox}>
								<img src='../../../res/images/loading.gif' style={comStyles().loadingGif} />
							</div>
							:
							<button type='button' style={comStyles().formText} onClick={this.signup.bind(this)} className='btn btn-primary btn-block'>Sign Up</button>
						}

					</form>
					<div onClick={this.redirectToSignin.bind(this)} style={comStyles().signin}>Sign In</div>
				</div>
			</div>
		);
	}
};

Signup.propTypes = {
	errorMessage: React.PropTypes.string,
	signupLandlord: React.PropTypes.func.isRequired,
	handleSubmit: React.PropTypes.func,
	sendError: React.PropTypes.func
};

const RadiumHOC = Radium(Signup);

function mapStateToProps(state){
	return {
		errorMessage: state.auth.error
	}
}

export default connect(mapStateToProps, {signupLandlord, sendError, authError, loadEmailCredsOfVerification})(RadiumHOC);

// ========================================================



const comStyles = () => {
	return {
		mainview: {
			backgroundColor: xMidBlue,
			width: "100%",
			height: "100%",
			margin: "0",
			left: "0",
			top: "0",
			display: "flex",
			justifyContent: "center"
		},
		entrance: {
			display: "flex",
			flexDirection: "column",
			margin: "auto",
			justifyContent: "center"
		},
		form: {
			width: "500px",
			margin: "auto",
			"@media (max-width: 600px), and (max-height: 740px)": {
				width: "90%"
			}
		},
		formText: {
			color: "white",
			fontSize: "1.2rem",
			fontWeight: "bold"
		},
		formInput: {
			fontSize: "1.2rem",
			textAlign: "center",
			backgroundColor: "rgba(256,256,256,0.9)"
		},
		landlordText: {
			color: "white",
			fontWeight: "bold",
			textAlign: "center",
			margin: "15px auto"
		},
		logo: {
			height: "150px",
			width: "auto",
			margin: "15px auto"
		},
		signup: {
			color: "white",
			fontSize: "1.2rem",
			textAlign: "center",
			margin: "50px auto",
			cursor: "pointer"
		},
		note: {
			fontSize: "1rem",
			color: "white",
		},
		signin: {
			color: "white",
			fontSize: "1.2rem",
			textAlign: "center",
			margin: "50px auto",
			cursor: "pointer"
		},
		errorMessage: {
			margin: "15px auto",
		},
		loadingBox: {
			width: "500px",
			margin: "auto",
			display: "flex",
			flexDirection: "row",
			justifyContent: "center",
			alignItems: "center",
			"@media (max-width: 600px), and (max-height: 740px)": {
				width: "90%"
			}
		},
		loadingGif: {
			width: "50px",
			height: "50px"
		}
	}
}
