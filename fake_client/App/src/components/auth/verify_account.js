import React, {Component} from 'react';
import axios from 'axios';
import {connect} from 'react-redux';
import Radium from 'radium'
import { browserHistory } from 'react-router'

import { xMidBlue } from '../../stylesJS/base_colors'
import {signinLandlord, authError, setLandlord} from '../../actions/auth_actions';

import {verifyLandlordAccount, signInLandlord, resetVerificationPIN} from '../../api/aws/aws-cognito'

class VerifyAccount extends Component {

	constructor(){
		super()
		this.state = {
			email: "",
			pin: "",
			resend: false,
			errorMessage: null,
			loading: false
		}
	}

	componentDidMount(){
		const savedEmail = localStorage.getItem('RentBurrow_Email')
		if(savedEmail){
			this.setState({
				email: savedEmail
			})
		}
	}
	
	handleChange(attr, event){
		this.setState({
			[attr]: event.target.value
		})
	}

	resendPIN(){
		this.setState({loading: true})
		resetVerificationPIN(this.state.email)
			.then(()=>{
				this.setState({
					resend: false,
					loading: false
				})
			})
			.catch((err)=>{
				// console.log(err.message)
				this.setState({
					errorMessage: err.message,
					loading: false
				})
			})
	}

	verifyPin(){
		const self = this
		//this.setState({loading: true})
		verifyLandlordAccount({
			email: self.state.email,
			pin: self.state.pin
		})
		.then((data)=>{
			browserHistory.push('/signin')
		})
		.catch((err)=>{
			// console.log(err)
			this.setState({
				errorMessage: err.message,
				loading: false
			})
		})
	}

	redirectTo(route){
		this.props.authError(null)
		browserHistory.push(route)
	}

	render(){
		return (
			<div style={comStyles().mainview}>
				<div style={comStyles().entrance}>
					<img src='../../../res/images/rentburrow_logo.png' style={comStyles().logo} />
					<h1 style={comStyles().landlordText}>Landlord Account Verification</h1>
							
					{
						this.state.resend
						?
						<div>
							<div className='form-group'>
								<label style={comStyles().formText}>Email</label>
								<input value={this.state.email} onChange={this.handleChange.bind(this, 'email')} type="email" className='form-control' style={comStyles().formInput} />
							</div>
							{
								this.state.errorMessage
								?
								<div className='alert alert-danger' style={comStyles().errorMessage}>
									{this.state.errorMessage}
								</div>
								:
								null
							}
							{
								this.state.loading
								?
								<div style={comStyles().loadingBox}>
									<img src='../../../res/images/loading.gif' style={comStyles().loadingGif} />
								</div>
								:
								<button onClick={this.resendPIN.bind(this)} style={comStyles().formText} type='button' className='btn btn-primary btn-block'>Resend Verification PIN</button>
							}
							<div onClick={()=>this.redirectTo('/signup')} style={comStyles().login}>Back To Login</div>
						</div>
						:
						<div>
							<form style={comStyles().form}>
								<div className='form-group'>
									<label style={comStyles().formText}>Email</label>
									<input value={this.state.email} onChange={this.handleChange.bind(this, 'email')} type="email" className='form-control' style={comStyles().formInput} />
								</div>
								<div className='form-group'>
									<label style={comStyles().formText}>Verification PIN</label>
									<input value={this.state.pin} onChange={this.handleChange.bind(this, 'pin')} type="text" className='form-control' style={comStyles().formInput} />
								</div>
								<div className='alert alert-success' style={comStyles().errorMessage}>
									Check your email for a verification PIN
								</div>
								{
									this.state.loading
									?
									<div style={comStyles().loadingBox}>
										<img src='../../../res/images/loading.gif' style={comStyles().loadingGif} />
									</div>
									:
									<button onClick={this.verifyPin.bind(this)} style={comStyles().formText} type='button' className='btn btn-primary btn-block'>Verify</button>
								}
								
							</form>
							{
								this.state.errorMessage
								?
								<div className='alert alert-danger' style={comStyles().errorMessage}>
									{this.state.errorMessage}
								</div>
								:
								null
							}
							<div onClick={()=>this.setState({resend: true})} style={comStyles().resend}>Resend Verification PIN</div>
							<div onClick={()=>browserHistory.push('/signin')} style={comStyles().back}>Back To Login</div>
						</div>
					}
				</div>
			</div>
		);
	}
}

VerifyAccount.propTypes = {
	landlord: React.PropTypes.object
};

const RadiumHOC = Radium(VerifyAccount);

// if there is an error, it will appear on the state tree
function mapStateToProps(state){
	return {
		landlord: state.landlord.landlord
	}
}

export default connect(mapStateToProps, {signinLandlord, authError, setLandlord})(RadiumHOC); 


// =========================================================


const comStyles = () => {
	return {
		mainview: {
			backgroundColor: xMidBlue,
			width: "100%",
			height: "100%",
			margin: "0",
			left: "0",
			top: "0",
			display: "flex",
			justifyContent: "center"
		},
		entrance: {
			display: "flex",
			flexDirection: "column",
			margin: "auto",
			justifyContent: "center"
		},
		form: {
			width: "500px",
			margin: "auto",
			"@media (max-width: 600px), and (max-height: 740px)": {
				width: "90%"
			}
		},
		formText: {
			color: "white",
			fontSize: "1.2rem",
			fontWeight: "bold",
		},
		formInput: {
			fontSize: "1.2rem",
			textAlign: "center",
			backgroundColor: "rgba(256,256,256,0.9)"
		},
		landlordText: {
			color: "white",
			fontWeight: "bold",
			textAlign: "center",
			margin: "15px auto"
		},
		logo: {
			height: "250px",
			width: "auto",
			margin: "15px auto"
		},
		resend: {
			color: "white",
			fontSize: "1.2rem",
			textAlign: "center",
			margin: "30px 0px 0px 0px",
			cursor: "pointer"
		},
		back: {
			color: "white",
			fontSize: "1.2rem",
			textAlign: "center",
			margin: "10px auto",
			cursor: "pointer"
		},
		errorMessage: {
			width: "500px",
			margin: "15px auto",
			"@media (max-width: 600px), and (max-height: 740px)": {
				width: "90%"
			}
		},
		loadingBox: {
			width: "500px",
			margin: "auto",
			display: "flex",
			flexDirection: "row",
			justifyContent: "center",
			alignItems: "center",
			"@media (max-width: 600px), and (max-height: 740px)": {
				width: "90%"
			}
		},
		loadingGif: {
			width: "50px",
			height: "50px"
		},
		login: {
			color: "white",
			fontSize: "1.2rem",
			textAlign: "center",
			margin: "30px 0px 0px 0px",
			cursor: "pointer"
		}
	}
}