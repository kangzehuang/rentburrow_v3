import React, {Component} from 'react';
import axios from 'axios';
import {connect} from 'react-redux';
import Radium from 'radium'
import { browserHistory } from 'react-router'

import { xMidBlue } from '../../stylesJS/base_colors'
import {signinLandlord, authError, setLandlord} from '../../actions/auth_actions';
import {trackActivity} from '../../actions/activity_actions'
import {signInLandlord, forgotPassword, retrieveLandlordFromLocalStorage} from '../../api/aws/aws-cognito'

class Signin extends Component {

	constructor(){
		super()
		this.state = {
			email: "",
			password: "",
			errorMessage: null,
			loading: false
		}
	}

	componentDidMount(){
		retrieveLandlordFromLocalStorage()
			.then((data)=>{
				this.props.setLandlord(data)
				// console.log(data)
				// wait a bit for the gps to be found
				setTimeout(()=>{
					// console.log(this.props.geoCoords)
					this.props.trackActivity("LANDLORD", "LOGGED_IN", data, null, this.props.geoCoords)
				}, 5000)
			})
		const savedEmail = localStorage.getItem('RentBurrow_Email')
		if(savedEmail){
			this.setState({
				email: savedEmail
			})
		}
	}

	handleChange(attr, event){
		this.setState({
			[attr]: event.target.value
		})
	}

	renderAlert(){
		if(this.props.errorMessage){
			return (
				<div className='alert alert-danger'>
					<strong>Oops!</strong> {this.props.errorMessage}
				</div>
			);
		}
	}

	signin(){
		this.setState({loading: true})
		signInLandlord({
			email: this.state.email,
			password: this.state.password
		}).then((data)=>{
			localStorage.setItem('RentBurrow_Email', this.state.email)
			this.props.setLandlord(data)
			this.props.trackActivity("LANDLORD", "LOGGED_IN", data, null, this.props.geoCoords)
		})
		.catch((err)=>{
			this.setState({
				errorMessage: err.message,
				loading: false
			})
		})
		// this.props.signinLandlord(this.state)
	}

	redirectTo(route){
		this.props.authError(null)
		browserHistory.push(route)
	}

	render(){
		return (
			<div style={comStyles().mainview}>
				<div style={comStyles().entrance}>
					<img src='../../../res/images/rentburrow_logo.png' style={comStyles().logo} />
					<h1 style={comStyles().landlordText}>Landlord Sign In</h1>
					<form style={comStyles().form}>
						<div className='form-group'>
							<label style={comStyles().formText}>Email</label>
							{/* Field name determines the name of the variables being passed to handleSubmit(this.props.signinLandlord) */}
							<input value={this.state.email} onChange={this.handleChange.bind(this, 'email')} type="email" className='form-control' style={comStyles().formInput} />
						</div>
						<div className='form-group'>
							<label style={comStyles().formText}>Password</label>
							<input value={this.state.password} onChange={this.handleChange.bind(this, 'password')} type="password" className='form-control' style={comStyles().formInput} />
						</div>
						{this.renderAlert()}
						{
							this.state.loading
							?
							<div style={comStyles().loadingBox}>
								<img src='../../../res/images/loading.gif' style={comStyles().loadingGif} />
							</div>
							:
							<button onClick={this.signin.bind(this)} style={comStyles().formText} type='button' className='btn btn-primary btn-block'>Sign in</button>
						}
					</form>
					{
						this.state.errorMessage
						?
						<div className='alert alert-danger' style={comStyles().errorMessage}>
							{this.state.errorMessage}
						</div>
						:
						null
					}
					<div onClick={()=>this.redirectTo('/signup')} style={comStyles().signup}>Sign Up</div>
					<div onClick={()=>this.redirectTo('/reset_password')} style={comStyles().forgot}>Forgot Password</div>
				</div>
			</div>
		);
	}
}

Signin.propTypes = {
	errorMessage: React.PropTypes.string,
	signinLandlord: React.PropTypes.func.isRequired,
	handleSubmit: React.PropTypes.func,
	geoCoords: React.PropTypes.array
};

const RadiumHOC = Radium(Signin);

// if there is an error, it will appear on the state tree
function mapStateToProps(state){
	return {
		errorMessage: state.auth.error,
		geoCoords: state.app.geoCoords
	}
}

export default connect(mapStateToProps, {signinLandlord, authError, setLandlord, trackActivity})(RadiumHOC);


// =========================================================


const comStyles = () => {
	return {
		mainview: {
			backgroundColor: xMidBlue,
			width: "100%",
			height: "100%",
			margin: "0",
			left: "0",
			top: "0",
			display: "flex",
			justifyContent: "center",
		},
		entrance: {
			display: "flex",
			flexDirection: "column",
			margin: "auto",
			justifyContent: "center"
		},
		form: {
			width: "500px",
			margin: "auto",
			"@media (max-width: 600px), and (max-height: 740px)": {
				width: "90%"
			}
		},
		formText: {
			color: "white",
			fontSize: "1.2rem",
			fontWeight: "bold",
		},
		formInput: {
			fontSize: "1.2rem",
			textAlign: "center",
			backgroundColor: "rgba(256,256,256,0.9)"
		},
		landlordText: {
			color: "white",
			fontWeight: "bold",
			textAlign: "center",
			margin: "15px auto"
		},
		logo: {
			height: "250px",
			width: "auto",
			margin: "15px auto"
		},
		signup: {
			color: "white",
			fontSize: "1.2rem",
			textAlign: "center",
			margin: "50px 0px 0px 0px",
			cursor: "pointer"
		},
		forgot: {
			color: "white",
			fontSize: "1.2rem",
			textAlign: "center",
			margin: "10px",
			cursor: "pointer"
		},
		errorMessage: {
			width: "500px",
			margin: "15px auto",
			"@media (max-width: 600px), and (max-height: 740px)": {
				width: "90%"
			}
		},
		loadingBox: {
			width: "500px",
			margin: "auto",
			display: "flex",
			flexDirection: "row",
			justifyContent: "center",
			alignItems: "center",
			"@media (max-width: 600px), and (max-height: 740px)": {
				width: "90%"
			}
		},
		loadingGif: {
			width: "50px",
			height: "50px"
		}
	}
}
