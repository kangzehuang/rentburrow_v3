import { combineReducers } from 'redux'
import appReducer from './app_reducer'
import authReducer from './auth_reducer'
import landlordReducer from './landlord_reducer'
import tenantReducer from './tenant_reducer'
import contentReducer from './content_reducer'
import popReducer from './popup_reducer'
import searchReducer from './search_reducer'
import scrollReducer from './scroll_reducer'
import performanceReducer from './performance_reducer'
import activityReducer from './activity_reducer'

const rootReducer = combineReducers({
		app: appReducer,
  	auth: authReducer,
  	landlord: landlordReducer,
  	tenant: tenantReducer,
  	content: contentReducer,
  	popup: popReducer,
  	searchForm: searchReducer,
  	scroll: scrollReducer,
		perform: performanceReducer,
		activity: activityReducer
});

export default rootReducer;
