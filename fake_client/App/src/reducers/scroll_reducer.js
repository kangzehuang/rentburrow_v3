import { SET_SUBLET_SCROLL, SET_LEASE_SCROLL, SET_WATCHLIST_SCROLL } from '../actions/action_types'

const INITIAL_STATE = {
	subletScrollPosition: 0,
	leaseScrollPosition: 0,
	watchlistScrollPosition: 0
}

export default function(state = INITIAL_STATE, action){
	switch(action.type){
		case SET_SUBLET_SCROLL:
			return {
				...state,
				subletScrollPosition: action.payload
			}
		case SET_LEASE_SCROLL:
			return {
				...state,
				leaseScrollPosition: action.payload
			}
		case SET_WATCHLIST_SCROLL:
			return {
				...state,
				watchlistScrollPosition: action.payload
			}
	}
	return state;
}
