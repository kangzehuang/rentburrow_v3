import { SET_LANDLORD, LANDLORD_SETTINGS, SELECT_LANDLORD_PROPERTY, EDIT_LANDLORD_PROPERTY, BACK_TO_LANDLORD_GRID, EXIT_PROPERTY_FORM, SAVE_LEASES_TO_STATE, LANDLORD_CHARTS_VIEW, SET_CLAIM_ID } from '../actions/action_types'
import {mockLandlordState} from '../mock/landlord_state'

const INITIAL_STATE = {
	landlord: null,			// the landlord data
	properties: [],			// all the properties owned by this landlord
	selectedProperty: null,	// detailed view of a specific property (for view or edit)
	propertyOnForm: null,
	chartsView: false,
	settingsView: false,
	claimId: null
}

export default function(state = INITIAL_STATE, action){
	switch(action.type){
		case SET_LANDLORD:
			return {
				...state,
				landlord: action.payload
			}
		case LANDLORD_SETTINGS:
			return {
				...state,
				chartsView: false,
				settingsView: true
			}
		case SELECT_LANDLORD_PROPERTY:
			return {
				...state,
				selectedProperty: action.payload,
				chartsView: false,
				settingsView: false
			}
		case EDIT_LANDLORD_PROPERTY:
			return {
				...state,
				propertyOnForm: action.payload
			}
		case EXIT_PROPERTY_FORM:
			return {
				...state,
				propertyOnForm: null
			}
		case BACK_TO_LANDLORD_GRID:
			return {
				...state,
				selectedProperty: null,
				propertyOnForm: null,
				chartsView: false,
				settingsView: false
			}
		case SAVE_LEASES_TO_STATE:
			return {
				...state,
				properties: action.payload
			}
		case LANDLORD_CHARTS_VIEW:
			return {
				...state,
				chartsView: true,
				settingsView: false
			}
		case SET_CLAIM_ID:
			return {
				...state,
				claimId: action.payload
			}
	}
	return state;
}
