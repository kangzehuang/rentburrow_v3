import { SET_GEO_COORD, SET_MOBILE_VIEWER, TOGGLE_MOBILE_VIEW, SET_PROPERTY_CLAIM } from '../actions/action_types';

const INITIAL_STATE = {
	city: "Waterloo",
	region: "KW",
	geoCoords: [],
	mobileViewer: false,
	mobileMapView: true,
	propertyClaim: false
}

export default function(state = INITIAL_STATE, action){
	switch(action.type){
		case SET_GEO_COORD:
			return {
				...state,
				geoCoords: action.payload
			}
		case SET_MOBILE_VIEWER:
			return {
				...state,
				mobileViewer: true
			}
		case TOGGLE_MOBILE_VIEW:
			return {
				...state,
				mobileMapView: action.payload
			}
		case SET_PROPERTY_CLAIM:
			return {
				...state,
				propertyClaim: action.payload
			}
	}
	return state;
}
