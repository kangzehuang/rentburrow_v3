import { GET_SUBLETS_FROM_DB, GET_LEASES_FROM_DB, SELECT_PINS, BACK_TO_PINS, SET_CITY, SET_VIEWMODE, LOAD_FOR_VIEWMODE, LOAD_BOTH, STRING_FILTER, PAN_TO, UNPAN_TO, TOGGLE_ADVANCED_SEARCH, SEARCH_SUBLET_RESULTS, SEARCH_LEASE_RESULTS, FILTERED_RESULTS, SELECT_LANDLORD_PROPERTY, CLEAR_FILTERED_RESULTS, APPEND_LEASE, APPEND_SUBLET } from '../actions/action_types'
import {mockLandlordState, mockSublets} from '../mock/landlord_state'

const INITIAL_STATE = {
	sublets: [],			// all sublets
	leases: [],					// all leases
	listOfResults: null,								// list of properties to show, depending on viewMode (sublet, leases, both)
	filteredResults: null,							// filtered list derived from listOfResults
	selectedPins: null,								// when a pin is selected, show this array of matching locations
	panTo: null, 					// a pin to pan to when a card's map button is clicked
	viewMode: "sublet",				// options include = ["sublet", "lease", "both"]
	advancedSearchToggle: false
}

export default function(state = INITIAL_STATE, action){
	switch(action.type){
		case GET_SUBLETS_FROM_DB:
			return {
				...state,
				sublets: action.payload
			}
		case GET_LEASES_FROM_DB:
			return {
				...state,
				leases: action.payload
			}
		case SELECT_PINS:
			return {
				...state,
				selectedPins: action.payload
			}
		case BACK_TO_PINS:
			return {
				...state,
				selectedPins: null
			}
		case SET_VIEWMODE:
			return {
				...state,
				filteredResults: null,
				selectedPins: null,
				viewMode: action.payload,
				advancedSearchToggle: false
			}
		case LOAD_FOR_VIEWMODE:
			return {
				...state,
				listOfResults: action.payload
			}
		case STRING_FILTER:
			return {
				...state,
				filteredResults: action.payload,
				selectedPins: null
			}
		case PAN_TO:
			return {
				...state,
				panTo: action.payload
			}
		case UNPAN_TO:
			return {
				...state,
				panTo: null
			}
		case TOGGLE_ADVANCED_SEARCH:
			return {
				...state,
				advancedSearchToggle: !state.advancedSearchToggle
			}
		case FILTERED_RESULTS:
			// console.log(action)
			return {
				...state,
				filteredResults: action.payload,
				selectedPins: null
			}
		case SELECT_LANDLORD_PROPERTY:
			return {
				...state,
				listOfResults: [action.payload],
				filteredResults: [action.payload]
			}
		case SET_CITY:
			return {
				...state,
				listOfResults: null,
				filteredResults: null,
				selectedPins: null
			}
		case CLEAR_FILTERED_RESULTS:
			return {
				...state,
				filteredResults: null
			}
		case APPEND_LEASE:
			return {
				...state,
				leases: state.leases.concat([action.payload]),
				listOfResults: state.listOfResults.concat([action.payload])
			}
		case APPEND_SUBLET:
			return {
				...state,
				sublets: state.sublets.concat([action.payload]),
				listOfResults: state.listOfResults.concat([action.payload])
			}
	}
	return state;
}
