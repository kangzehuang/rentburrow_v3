import { SET_TENANT, SET_CITY, SET_FB_PROFILE, SETTINGS_VIEW, WATCHLIST_VIEW, ADD_VIEW, LISTINGS_VIEW, LIVE_PREVIEW_SUBLET, SAVE_WATCHLISTS, SET_WATCHLIST, UNSET_WATCHLIST, LANDLORD_VIEW } from '../actions/action_types'
import {mockTenantState} from '../mock/landlord_state'

const INITIAL_STATE = {
	tenant: mockTenantState.tenant,			// the tenant data
	city: null,			// all the properties owned by this tenant
	fbUser: null,
	settingsView: false,
	watchlistView: false,
	addView: false,
	livePreviewSublet: {},
	watchlists: [],
	selectedWatchlist: null,
	selectedLandlord: null
}

export default function(state = INITIAL_STATE, action){
	switch(action.type){
		case SET_TENANT:
			return {
				...state,
				tenant: action.payload
			}
		case SET_CITY:
			return {
				...state,
				city: action.payload
			}
		case SET_FB_PROFILE:
			return {
				...state,
				fbUser: action.payload
			}
		case LANDLORD_VIEW: 
			return {
				...state,
				settingsView: true,
				watchlistView: false,
				addView: false,
				livePreviewSublet: {},
				selectedLandlord: action.payload
			}
		case SETTINGS_VIEW:
			return {
				...state,
				settingsView: true,
				watchlistView: false,
				addView: false,
				livePreviewSublet: {},
				selectedLandlord: null
			}
		case WATCHLIST_VIEW:
			return {
				...state,
				settingsView: false,
				watchlistView: true,
				addView: false,
				livePreviewSublet: {},
				selectedLandlord: null
			}
		case ADD_VIEW:
			return {
				...state,
				settingsView: false,
				watchlistView: false,
				addView: true,
				livePreviewSublet: {},
				selectedLandlord: null
			}
		case LISTINGS_VIEW: 
			return {
				...state,
				settingsView: false,
				watchlistView: false,
				addView: false,
				livePreviewSublet: {},
				selectedLandlord: null
			}
		case LIVE_PREVIEW_SUBLET:
			return {
				...state,
				livePreviewSublet: action.payload
			}
		case SAVE_WATCHLISTS:
			return {
				...state,
				watchlists: action.payload
			}
		case SET_WATCHLIST:
			return {
				...state,
				selectedWatchlist: action.payload
			}
		case UNSET_WATCHLIST: 
			return {
				...state,
				selectedWatchlist: null
			}
	}
	return state;
}
