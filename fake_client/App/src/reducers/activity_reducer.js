import { TRACK_ACTIVITY, CLEAR_ACTIVITIES_LIST } from '../actions/action_types';

const INITIAL_STATE = {
	trackedActivities: []
}

export default function(state = INITIAL_STATE, action){
	switch(action.type){
		case TRACK_ACTIVITY:
			return {
				...state,
				trackedActivities: state.trackedActivities.concat([action.payload])
			}
    case CLEAR_ACTIVITIES_LIST:
      return {
        ...state,
        trackedActivities: []
      }
	}
	return state;
}
