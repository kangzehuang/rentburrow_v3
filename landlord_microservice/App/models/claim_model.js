// Import dependencies on Mongoose
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Create the Rental post Schema
var ClaimSchema = new mongoose.Schema({
	email_confirm: String,
  unique_code: String,
	lease_id: String,
	address: String,
  added_at: Date
});

ClaimSchema.pre('save', function(next){
	var currentDate = new Date();

	this.added_at = currentDate;
	next();
});

var Claim = mongoose.model('Claim', ClaimSchema);

module.exports = Claim;
