const mongoose = require('mongoose');
const async = require('async');
const uuid = require('uuid')

const Claim = require('../models/claim_model');
const Lease = require('../models/lease_model');

// GET /create_claim
// check the updated_time of the latest post
exports.create_claim = function(req, res, next){

	console.log("....................New Claim.......................")
	console.log(req.body)
	const newClaim = {
		email_confirm: req.body.email_confirm,
		unique_code: uuid.v4(),
		lease_id: req.body.lease_id,
		address: req.body.address
	}
	//Claim.find({$and: [{province: province}, {country: country}]}, function(err, cities){
  const claim = new Claim(newClaim);
  claim.save(function(err, savedClaim){
    if(err){
      console.log(err)
      rej(err)
    }
    console.log(savedClaim);
    res.json({
      success: true,
      message: "Successfully saved claim!",
			claim: savedClaim
    })
  });
};


// GET /check_claim
// check the claim if it matches in db
exports.check_claim = function(req, res, next){

	console.log("....................Checking Claim.......................")
  const landlord_id = req.body.landlordId
  const unique_code = req.body.claimId
  console.log(req.body)
	//Claim.find({$and: [{province: province}, {country: country}]}, function(err, cities){
	Claim.find({unique_code: unique_code}, function(err, claims){
		if(err){console.log(err)};

		if(claims){
			setLeaseClaim(claims[0].lease_id, landlord_id)
				.then((updatedLease)=>{
					res.json({
		        success: true,
		        message: "Successfully claimed lease!",
						lease: updatedLease
		      });
				})
				.catch((message)=>{
					console.log(message)
					res.json({
		        success: false,
		        message: message
		      });
				})
		}else{
			res.json({
        success: false,
        message: "Failure to verify claim. No such claim found."
      });
		}
	});
};

function setLeaseClaim(leaseId, landlordId){
	const p = new Promise((res, rej)=>{
		Lease.findById(leaseId, function(err, lease){
			if(err){
				console.log(err)
				rej("Failure to verify claim. Could not update lease.")
			}
			if(!lease.claimed){
				lease.claimed = true
				lease.landlord_id = landlordId
				lease.save(function (err, updatedLease) {
				  if (err){
				  	console.log(err)
				  	rej("Could not update lease")
						return
				  }
					console.log("Successfully updated lease!")
					console.log(updatedLease)
		      res(updatedLease)
			  })
			}else{
				rej("Failure to verify claim. This lease has already been claimed")
			}
		})
	})
	return p
}
