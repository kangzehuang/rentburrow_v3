const mongoose = require('mongoose');

const Lease = require('../models/lease_model');
const LeaseCheck = require('../api/legacy_apis/newlease-checker.js');

const mockLeases = require('./mockLeases')

// ======================================================================================================

exports.test = function(req, res, next){
	res.json({
		message: "Microservice up and running!"
	})
}

exports.get_leases_for_landlord = function(req, res, next){
	const landlordId = req.body.landlordId
	console.log(req.body)
	// find leases with matching landlord_id
	Lease.find({$and:[{landlord_id: landlordId}, {deleted: false}]}, function(err, leases){
		if(err){console.log(err)};
		console.log(leases);
		// return the matching Leases
		if(leases.length > 0){
			res.json(leases);
		}else{
			res.json([]);
		}
	});
}

exports.submit_new_lease = function(req, res, next){
	console.log("================= SUBMIT NEW LEASE ====================")
	const new_lease = req.body;
	console.log(new_lease)
	LeaseCheck.check_lease(new_lease)
		.then(function(lease){
			console.log(lease)
			console.log("FINISHED ADDING LEASE")
			let response = {
				status: "Success",
				message: "Successfully added the new lease for " + lease.address,
				lease: lease
			}
			res.json(response);
		}, function(err){
			let response = {
				status: "Failure",
				message: err
			}
			res.json(response);
		})
}

// For flagging a lease as `deleted=true`
exports.flagLeaseDeleted = function(req, res, next){
	const propertyId = req.body.propertyId
	console.log("deleting lease")
	console.log(res.body)
	Lease.findById(propertyId, function (err, lease) {
		  if (err){
		  	console.log(err)
		  	res.status(500).send({error: err})
		  }
		  lease.deleted = true;
		  lease.active = false;
		  lease.save(function (err, updatedLease) {
			  if (err){
			  	console.log(err)
			  	res.status(500).send({error: err})
			  }
				console.log("Successfully deleted")
		      res.json({
		      	message: "Successfully deleted"
		      });
		  })
	});
}

// To update a lease and overwrite its file, but keep its _id
exports.updateLease = function(req, res, next){
	console.log("================= UPDATE LEASE ====================")
	console.log(req.body._id)
	const property = req.body
	console.log(req.body)
	Lease.findById(property._id, function(err, lease) {
		  if (err){
		  	console.log(err)
		  	res.status(500).send({error: err})
		  }
		  for(let key in property){
		  	lease[key] = property[key]
		  }
		  lease.save(function (err, updatedLease) {
			  if (err){
			  	console.log(err)
			  	res.status(500).send({error: err})
			  }
		      res.send(updatedLease);
		  })
	});
}


// Toggle a lease between active and inactive
exports.toggle_lease = function(req, res, next){
	const propertyId = req.body.propertyId
	const boolValue = req.body.boolValue
	console.log(req.body)

	Lease.findById(propertyId, function (err, lease) {
		  if (err){
		  	console.log(err)
		  	res.status(500).send({error: err})
		  }
		  lease.active = boolValue;
		  lease.save(function (err, updatedLease) {
			  if (err){
			  	console.log(err)
			  	res.status(500).send({error: err})
			  }
		      res.send(updatedLease);
		  })
	});
}
