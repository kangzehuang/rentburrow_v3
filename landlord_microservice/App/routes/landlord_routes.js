const mongoose = require('mongoose');
const Landlord = require('../models/landlord_model');

// GET /get_landlord
exports.get_landlord = function(req, res, next){
	const email = req.body.email

	Landlord.find({email: email}, function(err, landlords){
		if(err){console.log(err)}
		if(landlords.length > 0){
			res.json(landlords[0]);
		}else{
			res.json("No landlord found");
		}
	});
}



exports.test = function(res, res, next){
	res.json({
		message: "Test to landlord_microservice success!"
	})
}