
const Authentication = require('./routes/auth_routes');
const Landlord = require('./routes/landlord_routes');
const Leases = require('./routes/lease_routes');
const Claim = require('./routes/claim_routes')

// router middlewear
const authCheck = require('./api/authCheck').authCheck

module.exports = function(app){
	// Auth related routes
	app.get('/auth_test', authCheck, Authentication.authtest);

	// Landlord related routes
	app.get('/test', Landlord.test)
	app.post('/get_landlord', authCheck, Landlord.get_landlord);

	// Landlord Property related routes
	app.post('/get_leases_for_landlord', authCheck, Leases.get_leases_for_landlord)
	app.post('/submit_new_lease', authCheck, Leases.submit_new_lease)
	app.post('/update_lease', authCheck, Leases.updateLease)
	app.post('/delete_lease', authCheck, Leases.flagLeaseDeleted)
	app.post('/toggle_lease', authCheck, Leases.toggle_lease)

	// Claim related routes
	app.post('/create_claim', Claim.create_claim)
	app.post('/check_claim', Claim.check_claim)
}
