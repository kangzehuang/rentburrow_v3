const mongoose = require('mongoose');
const async = require('async');

const Sublet = require('../models/sublet_model');

const subletParser = require('../api/subletParser')
const fbExtractor = require('../api/fbExtractor')

// ======================================================================================================

exports.test = function(res, res, next){
	res.json({
		message: "Test to fbParser_microservice success!"
	})
}

// parse and save new posts (received from clients)
exports.new_sublets = function(req, res, next){
	const newSublets = req.body.newSublets
	const tenant = req.body.profile
  console.log(newSublets)
	fbExtractor.setFacebookToken(tenant.accessToken)
	subletParser.parseAndSaveSublets(newSublets)
	res.send("Thanks for helping populate the database!");
}


// check the updated_time of the latest sublet
exports.check_latest_sublet = function(req, res, next){
	const groupid = req.body.groupid
	console.log("============== ////// CHECK LATEST FB POST ////// ==============")
	console.log("Checking for latest facebook post in group #" + groupid)
	Sublet.findOne({$and: [{groupid: groupid}, {active: true}]}, null, {sort: {created_time: -1}}, function(err, sublet){
		if(err){console.log(err)};
		// console.log(sublet);
		// return the latest sublet date
		if(sublet){
			console.log("Successfully found post in this group #" + groupid)
			res.json(sublet.created_time);
		}else{
			console.log("No sublet found in this group #" + groupid)
			// if no posts in db, return an assumed date of Sept 1, 2016
			const emptydb = new Date("2016-09-01");
			res.json(emptydb);
		}
	});
}
