
const Sublets = require('./routes/sublet_routes');

// use `requireAuth` or `requireSignin` to wrap routes with auth conditions
module.exports = function(app){
	
	// Sublet related routes
	app.get('/test', Sublets.test)
	app.post('/new_sublets', Sublets.new_sublets)
	app.post('/check_latest_sublet', Sublets.check_latest_sublet)
}